## [Colibri2](https://bitbucket.org/rappoport/colibri2)

colibri2 is a distributed computational engine for chemical exploration.

### Distribution

The Python environment for colibri2 is packaged using both
[conda](https://www.anaconda.com/distribution/), which also takes
care of a few dependencies installed via [pip](https://pypi.org/project/pip/).
The dependencies are described in the `environment.yml` file.
In order to recreate the full environment, run
`conda env create -f environment.yml`. Once the environment is created 
run `conda activate colibri2` to turn on the conda environment. Then, 
navigate into the colibri2 top directory (the one with setup.py) and run `pip install .` 
(note the period at the end). 
Alternatively, use the supplied Dockerfile to create a docker image
with the conda environment.
`docker build -t colibri2:latest .`
or use the supplied Singularity definition file to create a singularity
container with the conda environment.
`sudo singularity build colibri2.sif Singularity.def`


### Requirements

colibri2 also requires a redis server and a postgresql server to be able to run. 
This can either be local or on a remote server.


### Testing

Use [pytest](https://docs.pytest.org/) for running colibri2 
tests from the base path.
`pip install -e .`
`pytest tests`

Pytest requires a local installation of a postgresql server and a redis-server. 
Make sure that locations of postgresql and redis-server executables
are updated in the `pytest.ini` configuration file.
Running the actual code does not require a local installation of 
the servers and a host and path can be specified. 

### Running

To run the package, a `rules.toml` file must be created with the allowed reaction rules. 
An example of this file can be found under `examples/toml/`. 
To run first navigate to the bin folder. Update the EXECUTABLE, DB_URL, CACHE_URL, 
and QUEUE_URL variables in initialize3, network3, and export3. The format of the 
DB_URL is `postgresql+psycopg2://user:password@host:port/database`, where you want to 
update the user, password,host, port, and database values. The default for a 
postregsql server is 
`postgres:postgres@localhost:5432/postgres`. The order to run the scripts is as 
follows: `./initialize3 ‘molecule’` where molecule is your starting desired 
molecule such as ‘CC’. This initializes the initial flask of the network. 
Next run `./network3`, which builds the chemical network. 
Lastly run `./export3 ‘path/file.xml’`, to export the network to an `.xml` file.  
