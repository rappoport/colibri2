#!/usr/bin/env python

from setuptools import setup, find_packages

setup(name='colibri2',
      version='0.0.0',
      description='colibri2 is a distributed computational engine for chemical exploration.',
      author='Dmitrij Rappoport',
      author_email='dmrappoport@gmail.com',
      url='https://bitbucket.org/rappoport/colibri2',
      download_url='https://bitbucket.org/rappoport/colibri2',
      classifiers=[
          'Development Status :: 4 - Beta',
          'Environment :: Console',
          'Intended Audience :: Science/Research',
          'License :: OSI Approved :: Apache Software License',
          'Operating System :: POSIX',
          'Programming Language :: Python :: 3.10',
          'Topic :: Scientific/Engineering :: Chemistry'
      ],
      license='Apache v2',
      platforms=['any'],
      packages=find_packages(exclude=['tests*'])
      )
