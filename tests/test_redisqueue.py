import json

import pytest
from pytest_redis import factories

from colibri2.config import LocalConfig
from colibri2.data import Formula, Superformula
from colibri2.taskqueue import RedisQueue

redis_proc = factories.redis_proc()


@pytest.mark.cache
class TestRedisQueue:

    @pytest.fixture(scope="function")
    def redisqueue(self, redis_proc):
        config = LocalConfig()
        config.update({'QUEUE__URL': f'redis://@{redis_proc.host}:{redis_proc.port}/0',
                       'QUEUE__MOLECULE_QUEUES__FORMULA': 'formulas',
                       'QUEUE__FLASK_QUEUES__SUPERFORMULA': 'superformulas'})
        return RedisQueue(config)

    def test_remove_molecule(self, redisqueue):
        redisqueue.redis.rpush(
            'formulas',
            json.dumps(
                {'smiles': 'CC', 'index': False, 'program': None, 'version': None, 'method': None,
                 'options': None, 'stereo': False, 'tag': '0f3c4e26-fbb9-40ba-a449-7f567e246c0a'})
        )
        mol = redisqueue.remove_molecule('Formula')
        assert mol == Formula('CC')

    def test_remove_flask(self, redisqueue):
        redisqueue.redis.rpush(
            'superformulas',
            json.dumps(
                {'smiles': 'C.C', 'index': False, 'stereo': False, 'program': None, 'version': None,
                 'method': None, 'options': None, 'tag': '77115048-7e21-4fd5-a87d-5056f6cbc2f8'}
            )
        )
        flask = redisqueue.remove_flask('Superformula')
        assert flask == Superformula('C.C', index=False)

    def test_append_molecule(self, redisqueue):
        redisqueue.append_molecule('Formula', Formula('CC', index=False, tag='58bb49c5-c408-40a4-9985-443b045a9dd9'))
        result = json.loads(redisqueue.redis.lpop('formulas'))
        assert result == {'index': False, 'method': None, 'options': None, 'program': None, 'stereo': False,
                          'smiles': 'CC', 'version': None, 'tag': '58bb49c5-c408-40a4-9985-443b045a9dd9'}

    def test_append_flask(self, redisqueue):
        redisqueue.append_flask('Superformula', Superformula('C.C', index=False,
                                                             tag='5289ecfe-681a-4f11-b1d3-6dfcd72b4efe'))
        result = json.loads(redisqueue.redis.lpop('superformulas'))
        assert result == {'index': False, 'stereo': False, 'method': None, 'options': None, 'program': None,
                          'smiles': 'C.C', 'version': None, 'tag': '5289ecfe-681a-4f11-b1d3-6dfcd72b4efe'}

    def test_clear_molecules(self, redisqueue):
        redisqueue.redis.rpush(
            'formulas',
            json.dumps(
                {'smiles': 'CC', 'index': False, 'stereo': False, 'program': None, 'version': None,
                 'method': None, 'options': None, 'tag': '3ff42e0c-9e1d-40eb-a1cd-428d4a9d1b6b'})
        )
        redisqueue.clear_molecules('Formula')
        count = redisqueue.redis.llen('formulas')
        assert count == 0
