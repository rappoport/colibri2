import pytest

from colibri2.config import LocalConfig
from colibri2.data import Superformula, PendingSupergeometry, Formula
from colibri2.task import FlaskMapperTask


class TestFlaskMapperTask:

    @pytest.fixture(scope="class")
    def flask_mapper_task(self):
        config = LocalConfig()
        config.COLLECT__ATTEMPTS = 10
        return FlaskMapperTask(config)

    def test_O_O_O_plain(self, flask_mapper_task):
        out, molecules = flask_mapper_task.process(Superformula('[H+].O.O.[OH-]'))
        assert type(out) == PendingSupergeometry
        assert out.smiles == 'O.O.[H+].[OH-]'
        assert out.attempts_left == 10
        assert type(molecules[0]) == Formula
        assert molecules[0].smiles == 'O'
        assert type(molecules[1]) == Formula
        assert molecules[1].smiles == 'O'
        assert type(molecules[2]) == Formula
        assert molecules[2].smiles == '[H+]'
        assert type(molecules[3]) == Formula
        assert molecules[3].smiles == '[OH-]'

    def test_C3H9_index(self, flask_mapper_task):
        out, molecules = flask_mapper_task.process(Superformula('[CH3-].[CH3].[CH3+]', index=True))
        assert type(out) == PendingSupergeometry
        assert out.smiles == '[CH3+].[CH3-].[CH3] |^1:2,atomProp:0.p.0:1.p.1:2.p.2|'
        assert out.attempts_left == 10
        assert type(molecules[0]) == Formula
        assert molecules[0].smiles == '[CH3+] |atomProp:0.p.0|'
        assert type(molecules[1]) == Formula
        assert molecules[1].smiles == '[CH3-] |atomProp:0.p.1|'
        assert type(molecules[2]) == Formula
        assert molecules[2].smiles == '[CH3] |^1:0,atomProp:0.p.2|'
