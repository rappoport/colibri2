import pytest

from colibri2.config import LocalConfig
from colibri2.data import Superformula
from colibri2.exceptions import ExecutionError
from colibri2.task import RDKitFlaskReactionTask


# TODO(dr): Add examples for invalid flasks
class TestRDKitFlaskReactionTask:

    @pytest.fixture(scope="class")
    def rdkit_flask_reaction_task(self):
        config = LocalConfig()
        config.update({'REACTION__RULES': ['[C+0:1]-[C+0:2]>>[C+1:1].[C-1:2]',
                                           '[C+1:1].[O-1:2]>>[C+0:1]-[O+0:2]']})
        return RDKitFlaskReactionTask(config)

    @pytest.fixture(scope="class")
    def rdkit_flask_reaction_task_index(self):
        config = LocalConfig()
        config.update({'REACTION__RULES': ['[C+0:1]-[C+0:2]>>[C+1:1].[C-1:2]',
                                           '[C+1:1].[O-1:2]>>[C+0:1]-[O+0:2]'],
                       'REACTION__INDEX': True})
        return RDKitFlaskReactionTask(config)

    @pytest.fixture(scope="class")
    def rdkit_flask_reaction_task_OH(self):
        config = LocalConfig()
        config.update({'REACTION__RULES': ['[O+0:1].[#1+1:2]>>[O+1:1]-[#1+0:2]'],
                       'REACTION__INDEX': True})
        return RDKitFlaskReactionTask(config)

    @pytest.fixture(scope="class")
    def rdkit_flask_reaction_task_CO(self):
        config = LocalConfig()
        config.update({'REACTION__RULES': ['[C+0:1]-[O+1:2]>>[C+1:1].[O+0:2]',
                                           '[C+1:1].[O+0:2]>>[C+0:1]-[O+1:2]']})
        return RDKitFlaskReactionTask(config)

    @pytest.fixture(scope="class")
    def rdkit_flask_reaction_task_CC(self):
        config = LocalConfig()
        config.update({'REACTION__RULES': ['[C+1:1].[C-1:2]>>[C+0:1]-[C+0:2]'],
                       'REACTION__INCLUDE_BIMOLECULAR': False})
        return RDKitFlaskReactionTask(config)

    @pytest.fixture(scope="class")
    def rdkit_flask_reaction_task_CN(self):
        config = LocalConfig()
        config.update({'REACTION__RULES': ['[C+1:1].[N-1:2]>>[C+0:1]-[N+0:2]']})
        return RDKitFlaskReactionTask(config)

    @pytest.fixture(scope="class")
    def rdkit_flask_reaction_task_CH(self):
        config = LocalConfig()
        config.update({'REACTION__RULES': ['[H+1:1].[C-1:2]>>[H+0:1]-[C+0:2]']})
        return RDKitFlaskReactionTask(config)

    def test_CC(self, rdkit_flask_reaction_task):
        out, valid, invalid, transformations = rdkit_flask_reaction_task.process(
            Superformula('CC'))
        assert type(out) is Superformula
        assert len(valid) == 1
        assert len(invalid) == 0
        assert len(transformations) == 1
        assert type(valid[0]) is Superformula
        assert repr(valid[0]) == "Superformula(smiles='[CH3+].[CH3-]', index=False, stereo=False)"

    def test_CC_index(self, rdkit_flask_reaction_task_index):
        out, valid, invalid, transformations = rdkit_flask_reaction_task_index.process(
            Superformula('CC', index=True))
        assert type(out) is Superformula
        assert len(valid) == 2
        assert len(invalid) == 0
        assert len(transformations) == 2
        assert type(valid[0]) is Superformula
        assert repr(valid[0]) == \
               "Superformula(smiles='[CH3+].[CH3-] |atomProp:0.p.0:1.p.1|', " \
               "index=True, stereo=False)"
        assert type(valid[1]) is Superformula
        assert repr(valid[1]) == \
               "Superformula(smiles='[CH3+].[CH3-] |atomProp:0.p.1:1.p.0|', " \
               "index=True, stereo=False)"
        assert transformations[0].rule == '[C+0:1]-[C+0:2]>>[C+1:1].[C-1:2]'
        assert transformations[1].rule == '[C+0:1]-[C+0:2]>>[C+1:1].[C-1:2]'

    def test_CC_missing_indices(self, rdkit_flask_reaction_task_index):
        with pytest.raises(ExecutionError, match="permutation indices missing in reactants"):
            rdkit_flask_reaction_task_index.process(Superformula('CC'))

    def test_CO_C(self, rdkit_flask_reaction_task_index):
        out, valid, invalid, transformations = rdkit_flask_reaction_task_index.process(
            Superformula('C[O-].[CH3+] |atomProp:0.p.0:1.p.1:2.p.2|', index=True))
        assert type(out) is Superformula
        assert len(valid) == 1
        assert len(invalid) == 0
        assert len(transformations) == 1
        assert type(valid[0]) is Superformula
        assert repr(valid[0]) == \
               "Superformula(smiles='COC |atomProp:0.p.0:1.p.1:2.p.2|', " \
               "index=True, stereo=False)"

    def test_C(self, rdkit_flask_reaction_task_index):
        out, valid, invalid, transformations = rdkit_flask_reaction_task_index.process(
            Superformula('C', index=True))
        assert type(out) is Superformula
        assert len(valid) == 0
        assert len(invalid) == 0
        assert len(transformations) == 0

    def test_C2H2O2(self, rdkit_flask_reaction_task_OH):
        out, valid, invalid, transformations = rdkit_flask_reaction_task_OH.process(
            Superformula('O=[C-][C-]=[OH+].[H+] |atomProp:0.p.0:1.p.2:2.p.1:3.p.3|', index=True))
        assert type(out) is Superformula
        assert len(valid) == 1
        assert len(invalid) == 0
        assert len(transformations) == 1
        assert (repr(valid[0])) == \
               "Superformula(smiles='[OH+]=[C-][C-]=[OH+] |atomProp:0.p.0:1.p.2:2.p.1:3.p.3|', " \
               "index=True, stereo=False)"

    def test_C2O4_rdkit_bug(self, rdkit_flask_reaction_task_CO):
        """RDKit implementation incorrectly changes stoichiometry on bond break,
        works correctly on bond formation."""
        out, valid, invalid, transformations = rdkit_flask_reaction_task_CO.process(
            Superformula('[O-]C1=[O+]C([O-])=[O+]1'))
        assert type(out) is Superformula
        assert len(valid) == 0
        assert len(invalid) == 1
        assert len(transformations) == 0
        assert invalid[0].smiles == 'O=C([O+])[O-].[O-][C+]=[O+]C[O-]'
        assert invalid[0].error == 'stoichiometry change'

        out, valid, invalid, transformations = rdkit_flask_reaction_task_CO.process(
            Superformula('O=C=O.O=[C+]-[O-]'))
        assert type(out) is Superformula
        assert len(valid) == 1
        assert len(invalid) == 1
        assert len(transformations) == 1
        assert repr(valid[0]) == "Superformula(smiles='O=C=[O+]C(=O)[O-]', " \
                                 "index=False, stereo=False)"
        assert invalid[0].smiles == 'O=C=O.[O-]C[OH2+]'
        assert invalid[0].error == 'stoichiometry change'

    def test_C5H5N5_aromatic_atoms(self, rdkit_flask_reaction_task_CH):
        """RDKit implementation generates a structure with aromatic atoms but no
        aromatic bonds."""
        out, valid, invalid, transformations = rdkit_flask_reaction_task_CH.process(
            Superformula('C=[NH2+].[C+]1=[N+]2[C-]=N[C-]3[N-][C-]2N13.[H+]'))
        assert type(out) is Superformula
        assert len(valid) == 3
        assert len(invalid) == 0
        assert len(transformations) == 3
        assert repr(valid[0]) == \
               "Superformula(smiles='C=[NH2+].[c+]1=[n+]2-c=n-[c-]3-[n-]-[c-]-2-n-1-3', " \
                'index=False, stereo=False)'
        assert repr(valid[1]) == \
               "Superformula(smiles='C=[NH2+].[C+]1=[N+]2[C-]=NC3[N-][C-]2N13', index=False, " \
                'stereo=False)'
        assert repr(valid[2]) == \
               "Superformula(smiles='C=[NH2+].[C+]1=[N+]2[C-]=N[C-]3[N-]C2N13', index=False, " \
                'stereo=False)'

    def test_C6H6(self, rdkit_flask_reaction_task_CC):
        out, valid, invalid, transformations = rdkit_flask_reaction_task_CC.process(
            Superformula('[CH+]=CC=CC=[CH-]'))
        assert type(out) is Superformula
        assert len(valid) == 1
        assert len(invalid) == 0
        assert len(transformations) == 1
        assert repr(valid[0]) == "Superformula(smiles='c1ccccc1', index=False, stereo=False)"

    def test_C3H3N3(self, rdkit_flask_reaction_task_CN):
        out, valid, invalid, transformations = rdkit_flask_reaction_task_CN.process(
            Superformula('[CH+]=NC=NC=[N-]'))
        assert type(out) is Superformula
        assert len(valid) == 1
        assert len(invalid) == 0
        assert len(transformations) == 1
        assert repr(valid[0]) == "Superformula(smiles='c1ncncn1', index=False, stereo=False)"
