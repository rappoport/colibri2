import pytest

from colibri2.utils import base26, invert_letter, stringhash, \
    controlhash, lexicographic_index, camelcase_to_uppercase


class TestStringhash:

    def test_equality(self):
        s1 = "a" * 128
        s2 = "a" * 128
        assert stringhash(s1) == stringhash(s2)

    def test_inequality(self):
        s1 = "a" * 128
        s2 = "a" * 127 + "b"
        assert stringhash(s1) != stringhash(s2)


class TestBase26:

    def test_1(self):
        assert base26(1) == 'AAAAAAAAAB'

    def test_1_2(self):
        assert base26(1, 2) == 'AB'

    def test_675(self):
        assert base26(675) == 'AAAAAAAAZZ'

    def test_675_1(self):
        assert base26(675, 1) == 'Z'

    def test_0(self):
        assert base26(0) == 'AAAAAAAAAA'

    def test_invalid_input(self):
        with pytest.raises(ValueError, match="input must be non-negative"):
            base26(-1)


class TestInvertLetter:

    def test_A(self):
        assert invert_letter('A') == 'Z'

    def test_Z(self):
        assert invert_letter('Z') == 'A'

    def test_W(self):
        assert invert_letter('W') == 'D'


class TestControlhash:

    def test_A(self):
        assert controlhash('A') == 'XJTEAHWAZT'

    def test_A_1(self):
        assert controlhash('A', 1) == 'T'

    def test_empty(self):
        assert controlhash('') == 'JHXWGORHUJ'


class TestLexicographicIndex:

    def test_012(self):
        assert lexicographic_index([0, 1, 2]) == 0

    def test_021(self):
        assert lexicographic_index([0, 2, 1]) == 1

    def test_102(self):
        assert lexicographic_index([1, 0, 2]) == 2

    def test_120(self):
        assert lexicographic_index([1, 2, 0]) == 3

    def test_201(self):
        assert lexicographic_index([2, 0, 1]) == 4

    def test_210(self):
        assert lexicographic_index([2, 1, 0]) == 5

    def test_empty(self):
        assert lexicographic_index([]) == 0


class TestCamelcaseToUppercase:

    def test_1_word(self):
        assert camelcase_to_uppercase('Formula') == 'FORMULA'

    def test_2_words(self):
        assert camelcase_to_uppercase('InvalidSuperformula') == \
               'INVALID_SUPERFORMULA'
