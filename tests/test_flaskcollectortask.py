import pytest
from pytest import approx

from colibri2.config import LocalConfig
from colibri2.data import Geometry, PendingSupergeometry
from colibri2.task import FlaskCollectorTask


class TestFlaskCollectorTask:

    @pytest.fixture(scope="class")
    def flask_collector_task(self):
        config = LocalConfig()
        config.COLLECT__ATTEMPTS = 10
        return FlaskCollectorTask(config)

    def test_O_O(self, flask_collector_task):
        out = flask_collector_task.process((
            PendingSupergeometry('O.O', attempts_left=10),
            {
                Geometry(coord='''


  3  2  0  0  0  0  0  0  0  0999 V2000
   -0.0000    0.0543    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0
    0.7664    0.6418   -0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0
   -0.7663    0.6417    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  1  0  0  0  0
  1  3  1  0  0  0  0
M  END''', energy=-323.00294)
            }
        ))
        assert out.smiles == 'O.O'
        assert out.energy == approx(-646.00588)
