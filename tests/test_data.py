import pytest
from pytest import approx

from colibri2.data import Formula, Configuration, Geometry, Property, FailedFormula, \
    InvalidFormula, FailedConfiguration, InvalidConfiguration, FailedGeometry, \
    InvalidGeometry, FailedProperty, InvalidProperty, Superformula, Supergeometry, \
    Superproperty, PendingSupergeometry, IncompleteSupergeometry, Transformation, \
    Rule
from colibri2.exceptions import MoleculeError, FlaskError


class TestFormula:

    def test_CO(self):
        assert repr(Formula('CO')) == \
               "Formula(smiles='CO', index=False, stereo=False)"

    def test_OC(self):
        assert repr(Formula("OC")) == \
               "Formula(smiles='CO', index=False, stereo=False)"

    def test_NH2(self):
        assert repr(Formula('[NH2]')) == \
               "Formula(smiles='[NH2]', index=False, stereo=False)"

    def test_OHn(self):
        assert repr(Formula('[OH-]')) == \
               "Formula(smiles='[OH-]', index=False, stereo=False)"

    def test_Hp(self):
        assert repr(Formula('[H+]')) == \
               "Formula(smiles='[H+]', index=False, stereo=False)"

    def test_C6H6_aromatic(self):
        assert repr(Formula('C1=CC=CC=C1')) == \
               "Formula(smiles='C1=CC=CC=C1', index=False, stereo=False)"

    def test_C5H4N4_kekule(self):
        assert repr(Formula('C1(N=CN2)=C2N=CN=C1')) == \
               "Formula(smiles='C1=NC=NC2=C1N=CN2', index=False, stereo=False)"

    def test_C5H4N4_aromatic(self):
        assert repr(Formula('c12nc[nH]c1ncnc2')) == \
               "Formula(smiles='c1ncc2nc[nH]c2n1', index=False, stereo=False)"

    def test_C3H8O_idempotent(self):
        assert repr(Formula('CC(C)O')) == \
               "Formula(smiles='CC(C)O', index=False, stereo=False)"

    def test_C5H4O4_aromatic_idempotent(self):
        assert repr(Formula('c1ncc2nc[nH]c2n1')) == \
               "Formula(smiles='c1ncc2nc[nH]c2n1', index=False, stereo=False)"

    def test_C4H4N4_kekule_idempotent(self):
        assert repr(Formula('NC1C2=NC3=C(N3)N21')) == \
               "Formula(smiles='NC1C2=NC3=C(N3)N21', index=False, stereo=False)"

    def test_CO_remove_H(self):
        assert repr(Formula("[CH3][OH]")) == \
               "Formula(smiles='CO', index=False, stereo=False)"

    def test_CO_remove_index(self):
        assert repr(Formula('CO |atomProp:0.p.0:1.p.1|')) == \
               "Formula(smiles='CO', index=False, stereo=False)"

    def test_C2H5N_remove_stereo(self):
        assert repr(Formula('C/C=N/[H]')) == \
               "Formula(smiles='CC=N', index=False, stereo=False)"

    def test_C3H8O2_remove_stereo(self):
        assert repr(Formula('C[C@H](CO)O')) == \
               "Formula(smiles='CC(O)CO', index=False, stereo=False)"

    def test_C3H6O2_remove_index_stereo(self):
        assert repr(Formula('C[C@@H](O)C=O |atomProp:0.p.0:1.p.1:2.p.2:3.p.3:4.p.4|')) == \
               "Formula(smiles='CC(O)C=O', index=False, stereo=False)"

    def test_CO_preserve_index_01(self):
        assert repr(Formula('CO |atomProp:0.p.0:1.p.1|', index=True)) == \
               "Formula(smiles='CO |atomProp:0.p.0:1.p.1|', index=True, stereo=False)"

    def test_CO_preserve_index_10(self):
        assert repr(Formula('CO |atomProp:0.p.1:1.p.0|', index=True)) == \
               "Formula(smiles='CO |atomProp:0.p.1:1.p.0|', index=True, stereo=False)"

    def test_OC_preserve_index_01(self):
        assert repr(Formula('OC |atomProp:0.p.0:1.p.1|', index=True)) == \
               "Formula(smiles='CO |atomProp:0.p.1:1.p.0|', index=True, stereo=False)"

    def test_OC_preserve_index_10(self):
        assert repr(Formula('OC |atomProp:0.p.1:1.p.0|', index=True)) == \
               "Formula(smiles='CO |atomProp:0.p.0:1.p.1|', index=True, stereo=False)"

    def test_CO_remove_H_preserve_index_01(self):
        assert repr(Formula('[CH3][OH] |atomProp:0.p.0:1.p.1|', index=True)) == \
               "Formula(smiles='CO |atomProp:0.p.0:1.p.1|', index=True, stereo=False)"

    def test_CO_add_index(self):
        assert repr(Formula('CO', index=True)) == \
               "Formula(smiles='CO |atomProp:0.p.0:1.p.1|', index=True, stereo=False)"

    def test_CC_01_preserve_index(self):
        assert repr(Formula('CC |atomProp:0.p.0:1.p.1|', index=True)) == \
               "Formula(smiles='CC |atomProp:0.p.0:1.p.1|', index=True, stereo=False)"

    def test_CC_10_preserve_index(self):
        assert repr(Formula('CC |atomProp:0.p.1:1.p.0|', index=True)) == \
               "Formula(smiles='CC |atomProp:0.p.0:1.p.1|', index=True, stereo=False)"

    def test_NH2_preserve_index(self):
        assert repr(Formula('[NH2] |^1:0,atomProp:0.p.0|', index=True)) == \
               "Formula(smiles='[NH2] |^1:0,atomProp:0.p.0|', index=True, stereo=False)"

    def test_OH_preserve_index(self):
        assert repr(Formula('[OH-] |atomProp:0.p.0|', index=True)) == \
               "Formula(smiles='[OH-] |atomProp:0.p.0|', index=True, stereo=False)"

    def test_OC_add_index(self):
        assert repr(Formula('OC', index=True)) == \
               "Formula(smiles='CO |atomProp:0.p.0:1.p.1|', index=True, stereo=False)"

    def test_CC_add_index(self):
        assert repr(Formula('CC', index=True)) == \
               "Formula(smiles='CC |atomProp:0.p.0:1.p.1|', index=True, stereo=False)"

    def test_NH2_add_index(self):
        assert repr(Formula('[NH2]', index=True)) == \
               "Formula(smiles='[NH2] |^1:0,atomProp:0.p.0|', index=True, stereo=False)"

    def test_OH_add_index(self):
        assert repr(Formula('[OH-]', index=True)) == \
               "Formula(smiles='[OH-] |atomProp:0.p.0|', index=True, stereo=False)"

    def test_H_no_index_added(self):
        assert repr(Formula('[H+]', index=True)) == \
               "Formula(smiles='[H+]', index=True, stereo=False)"

    def test_C2H5N_preserve_stereo(self):
        assert repr(Formula('C/C=N/[H]', stereo=True)) == \
               "Formula(smiles='[H]/N=C/C', index=False, stereo=True)"

    def test_C3H8O2_preserve_stereo(self):
        assert repr(Formula('C[C@H](CO)O', stereo=True)) == \
               "Formula(smiles='C[C@@H](O)CO', index=False, stereo=True)"

    def test_C3H8O2_stereo_unspecified(self):
        assert repr(Formula('CC(O)CC', stereo=True)) == \
               "Formula(smiles='CCC(C)O', index=False, stereo=True)"

    def test_C3H6O2_preserve_index_stereo(self):
        assert repr(Formula('C[C@@H](O)C=O |atomProp:0.p.0:1.p.1:2.p.2:3.p.3:4.p.4|',
                            index=True, stereo=True)) == \
               "Formula(smiles='C[C@@H](O)C=O |atomProp:0.p.0:1.p.1:2.p.2:3.p.3:4.p.4|', " \
               "index=True, stereo=True)"

    def test_C3H8O2_add_index_preserve_stereo(self):
        assert repr(Formula('C[C@@H](O)CO', index=True, stereo=True)) == \
               "Formula(smiles='C[C@@H](O)CO |atomProp:0.p.0:1.p.1:2.p.2:3.p.3:4.p.4|', " \
               "index=True, stereo=True)"

    def test_empty(self):
        with pytest.raises(MoleculeError, match="empty SMILES string"):
            Formula('')

    def test_invalid_element(self):
        with pytest.raises(MoleculeError, match="reading SMILES string failed"):
            Formula('XY')

    def test_invalid_bonding(self):
        with pytest.raises(MoleculeError, match="reading SMILES string failed"):
            Formula('N#O')

    def test_multiple_molecules(self):
        with pytest.raises(MoleculeError, match="multiple molecules in SMILES string"):
            Formula('C.C')

    def test_invalid_indices(self):
        with pytest.raises(MoleculeError, match="invalid permutation indices in SMILES string"):
            Formula('CO |atomProp:0.p.0|', index=True)

    def test_frozen(self):
        struct = Formula('OO')
        with pytest.raises(AttributeError):
            struct.smiles = 'OOO'

    def test_smiles(self):
        assert Formula('OO').smiles == 'OO'

    def test_smiles_index(self):
        assert Formula('OO', index=True).smiles == 'OO |atomProp:0.p.0:1.p.1|'

    def test_smiles_stereo(self):
        assert Formula('C[C@H](CO)O', stereo=True).smiles == 'C[C@@H](O)CO'

    def test_smiles_index_stereo(self):
        assert Formula('C[C@@H](O)C=O |atomProp:0.p.0:1.p.1:2.p.2:3.p.3:4.p.4|',
                       index=True, stereo=True).smiles == \
               'C[C@@H](O)C=O |atomProp:0.p.0:1.p.1:2.p.2:3.p.3:4.p.4|'

    def test_plainsmiles(self):
        assert Formula('CC').plainsmiles == 'CC'

    def test_plainsmiles_index(self):
        assert Formula('OO |atomProp:0.p.0:1.p.1|', index=True).plainsmiles == 'OO'

    def test_plainsmiles_stereo(self):
        assert Formula('C[C@H](CO)O', stereo=True).plainsmiles == 'C[C@@H](O)CO'

    def test_plainsmiles_index_stereo(self):
        assert Formula('C[C@@H](O)C=O |atomProp:0.p.0:1.p.1:2.p.2:3.p.3:4.p.4|',
                       index=True, stereo=True).plainsmiles == 'C[C@@H](O)C=O'

    def test_charge_positive(self):
        assert Formula('[CH3+]').charge == 1

    def test_charge_negative(self):
        assert Formula('[CH3-]').charge == -1

    def test_charge_carbene(self):
        assert Formula('[CH3]').charge == 0

    def test_mult_singlet(self):
        assert Formula('O').mult == 1

    def test_mult_doublet(self):
        assert Formula('[CH3]').mult == 2

    def test_mult_triplet(self):
        assert Formula('[O][O]').mult == 3

    def test_mult_carbene(self):
        assert Formula('[CH2]').mult == 3

    def test_mult_halogene_cation(self):
        assert Formula('[Br+]').mult == 3

    def test_key(self):
        assert Formula('CO').key == 'OKKJLVBELUTLKV-UHFFFAOYSA-N-NEMZRWRQOB-AAAAA'

    def test_key_index(self):
        assert Formula('CO |atomProp:0.p.1:1.p.0|', index=True).key == \
               'OKKJLVBELUTLKV-UHFFFAOYSA-N-NEMZRWRQOB-AAAAB'

    def test_key_stereo(self):
        assert Formula('C[C@@H](O)CO', stereo=True).key == \
               'DNIAPMSPPWPWGF-GSVOUGTGSA-N-BHVADRQMSN-AAAAA'

    def test_key_index_stereo(self):
        assert Formula('C[C@@H](O)C=O |atomProp:0.p.0:1.p.1:2.p.2:3.p.3:4.p.4|',
                       index=True, stereo=True).key == \
               'BSABBBMNWQWLLU-GSVOUGTGSA-N-ZBCGANNDUV-AAAAA'

    def test_plainkey(self):
        assert Formula('CO').plainkey == 'OKKJLVBELUTLKV-UHFFFAOYSA-N-NEMZRWRQOB'

    def test_plainkey_index(self):
        assert Formula('CO |atomProp:0.p.1:1.p.0|', index=True).plainkey == \
               'OKKJLVBELUTLKV-UHFFFAOYSA-N-NEMZRWRQOB'

    def test_plainkey_stereo(self):
        assert Formula('C[C@@H](O)CO', stereo=True).plainkey == \
               'DNIAPMSPPWPWGF-GSVOUGTGSA-N-BHVADRQMSN'

    def test_plainkey_index_stereo(self):
        assert Formula('C[C@@H](O)C=O |atomProp:0.p.0:1.p.1:2.p.2:3.p.3:4.p.4|',
                       index=True, stereo=True).plainkey == \
               'BSABBBMNWQWLLU-GSVOUGTGSA-N-ZBCGANNDUV'

    def test_inchi(self):
        assert Formula('CO').inchi == 'InChI=1S/CH4O/c1-2/h2H,1H3'

    def test_inchi_stereo(self):
        assert Formula('C/C=N/[H]', stereo=True).inchi == \
               'InChI=1S/C2H5N/c1-2-3/h2-3H,1H3/b3-2+'

    def test_inchikey(self):
        assert Formula('CO').inchikey == 'OKKJLVBELUTLKV-UHFFFAOYSA-N'

    def test_inchikey_stereo(self):
        assert Formula('C/C=N/[H]', stereo=True).inchikey == \
               'MPAYEWNVIPXRDP-NSCUHMNNSA-N'

    def test_smileskey(self):
        assert Formula('CO').smileskey == 'NEMZRWRQOB'

    def test_smileskey_stereo(self):
        assert Formula('C/C=N/[H]').smileskey == 'BVTZQFMGMY'

    def test_permindices(self):
        assert Formula('CO').permindices == []

    def test_permindices_index(self):
        assert Formula('CO |atomProp:0.p.1:1.p.0|', index=True).permindices == [1, 0]

    def test_perm(self):
        assert Formula('CO').perm == ''

    def test_perm_index(self):
        assert Formula('CO |atomProp:0.p.1:1.p.0|', index=True).perm == '1,0'

    def test_permkey(self):
        assert Formula('CO').permkey == 'AAAAA'

    def test_permkey_index(self):
        assert Formula('CO |atomProp:0.p.1:1.p.0|', index=True).permkey == 'AAAAB'

    def test_sumformula(self):
        assert Formula('CCO').sumformula == 'C2H6O'

    def test_adjancency(self):
        assert Formula(smiles='OO').adjacency == [[1, 2], [0, 3], [0], [1]]

    def test_adjkey(self):
        assert Formula(smiles='OO').adjkey == ';0;0;1'

    def test_atoms(self):
        assert Formula(smiles='OO').atoms == [(0, 'O'), (1, 'O'), (2, 'H'), (3, 'H')]

    def test_atomkey(self):
        assert Formula(smiles='OO').atomkey == 'H 2,O 2'

    def test_bonds(self):
        assert Formula(smiles='OO').bonds == [(0, 1, 'SINGLE'), (0, 2, 'SINGLE'), (1, 3, 'SINGLE')]

    def test_bondkey(self):
        assert Formula(smiles='C=CC#C').bondkey == '0#1;0-4;1-2;2=3;2-5;3-6;3-7'

    def test_ez_stereo(self):
        assert Formula(smiles=r'C/C=C/C\C=C/C', stereo=True).ez_stereo == [(1, 'Z'), (4, 'E')]

    def test_chiral_stereo(self):
        assert Formula(smiles='C[C@H](O)[C@@H](C)O', stereo=True).chiral_stereo == [(1, 'CCW'), (3, 'CW')]

    def test_priority(self):
        assert Formula(smiles='ClC(C(Cl)(Cl)Cl)(Cl)Cl').priority == 237

    def test_str(self):
        assert str(Formula('[O][O]')) == '[O][O] (MYMOFIZGZYHOMD-UHFFFAOYSA-N-SHFVOHTNSH-AAAAA/32)'

    def test_repr(self):
        assert repr(Formula('CN')) == "Formula(smiles='CN', index=False, stereo=False)"

    def test_format(self):
        assert f"{Formula('CN')}" == 'CN (BAVYZALUXZFZLV-UHFFFAOYSA-N-NXJQXGBTJO-AAAAA/31)'

    def test_equality(self):
        struct1 = Formula('COC')
        struct2 = Formula('COC')
        assert struct1 == struct2

    def test_identity(self):
        struct1 = Formula('COC')
        struct2 = Formula('COC')
        assert struct1 is not struct2

    def test_comparison(self):
        struct1 = Formula('CCO')
        struct2 = Formula('COC')
        assert struct1 < struct2

    def test_hash(self):
        struct1 = Formula('CCO')
        struct2 = Formula('CCO')
        struct3 = Formula('COC')
        assert struct1 in {struct2}
        assert struct3 not in {struct2}

    def test_fromdict(self):
        assert repr(Formula.fromdict({"smiles": "CCO", "index": True})) == \
               "Formula(smiles='CCO |atomProp:0.p.0:1.p.1:2.p.2|', index=True, stereo=False)"

    def test_todict(self):
        assert Formula('ICI', tag='82ff219b-031d-4311-8e56-7fe4ee01b77b').todict() == \
               {'index': False, 'method': None, 'options': None, 'program': None,
                'smiles': 'ICI', 'stereo': False,
                'tag': '82ff219b-031d-4311-8e56-7fe4ee01b77b', 'version': None}

    def test_todict_skip_none_values(self):
        assert Formula('ICI', tag='82ff219b-031d-4311-8e56-7fe4ee01b77b').todict(skip_none_values=True) == \
               {'index': False, 'smiles': 'ICI', 'stereo': False, 'tag': '82ff219b-031d-4311-8e56-7fe4ee01b77b'}

    def test_todict_skip_keys(self):
        assert Formula('ICI', tag='82ff219b-031d-4311-8e56-7fe4ee01b77b').todict(skip_keys=['stereo','tag']) == \
               {'index': False, 'method': None, 'options': None, 'program': None,
                'smiles': 'ICI', 'version': None}

    def test_copy(self):
        struct1 = Formula('O1CC1')
        struct2 = struct1.copy()
        assert repr(struct2) == "Formula(smiles='C1CO1', index=False, stereo=False)"
        assert struct1 == struct2
        assert struct1 is not struct2

    def test_copy_update(self):
        assert Formula(smiles='CC').copy(
            program='RDKit', version='2019.3').version == '2019.3'


class TestConfiguration:

    @classmethod
    def setup_class(cls):
        cls.NH2 = Configuration(coord='''


  3  2  0  0  0  0  0  0  0  0999 V2000
   -0.0057    0.4060   -0.0000 N   0  0  0  0  0  2  0  0  0  0  0  0
   -0.8403   -0.2051   -0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0
    0.8460   -0.2009    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  1  0
  1  3  1  0
M  RAD  1   1   2
M  END''', tag='d9a523e8-0bba-4cf3-9c34-ff9da0805569')

        cls.C2H2F2 = Configuration(coord='''
 OpenBabel05242311373D

  6  5  0  0  0  0  0  0  0  0999 V2000
    0.9192   -0.0629   -0.0559 F   0  0  0  0  0  0  0  0  0  0  0  0
    2.2625    0.0013   -0.0484 C   0  0  0  0  0  0  0  0  0  0  0  0
    2.9046    1.1538    0.0851 C   0  0  0  0  0  0  0  0  0  0  0  0
    4.2479    1.2180    0.0925 F   0  0  0  0  0  0  0  0  0  0  0  0
    2.7123   -0.9732   -0.1613 H   0  0  0  0  0  0  0  0  0  0  0  0
    2.4548    2.1283    0.1980 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  1  0  0  0  0
  2  3  2  3  0  0  0
  2  5  1  0  0  0  0
  3  4  1  0  0  0  0
  3  6  1  0  0  0  0
M  END''', tag='b90a52a2-3b97-456a-88a4-44cc0b002d45')

        cls.C3H6O2 = Configuration(coord='''
 OpenBabel05302322123D

 11 10  0  0  1  0  0  0  0  0999 V2000
    1.0303    0.0698    0.0028 C   0  0  0  0  0  0  0  0  0  0  0  0
    2.5462    0.0793    0.0533 C   0  0  1  0  0  0  0  0  0  0  0  0
    2.9621    0.0418    1.4094 O   0  0  0  0  0  0  0  0  0  0  0  0
    3.0926   -1.0978   -0.7391 C   0  0  0  0  0  0  0  0  0  0  0  0
    3.6773   -2.0452   -0.2155 O   0  0  0  0  0  0  0  0  0  0  0  0
    0.6211    0.9151    0.5678 H   0  0  0  0  0  0  0  0  0  0  0  0
    0.6308   -0.8416    0.4609 H   0  0  0  0  0  0  0  0  0  0  0  0
    0.6623    0.1223   -1.0275 H   0  0  0  0  0  0  0  0  0  0  0  0
    2.9390    0.9977   -0.3953 H   0  0  0  0  0  0  0  0  0  0  0  0
    3.0834   -0.9013    1.6385 H   0  0  0  0  0  0  0  0  0  0  0  0
    2.9228   -1.0628   -1.8293 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  1  0  0  0  0
  1  6  1  0  0  0  0
  1  7  1  0  0  0  0
  1  8  1  0  0  0  0
  2  3  1  0  0  0  0
  2  4  1  0  0  0  0
  2  9  1  6  0  0  0
  3 10  1  0  0  0  0
  4  5  2  0  0  0  0
  4 11  1  0  0  0  0
M  END''', stereo=True)

        cls.C5H4N3O = Configuration(coord=''' xtb: 6.6.0 (conda-forge)
          06202312343D

 13 13  0     0  0            999 V2000
   -2.1379   -0.7866    0.1283 N   0  0  0  0  0  0  0  0  0  0  0  0
   -0.8879   -0.2832    0.0330 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.1119   -1.1292    0.2793 N   0  0  0  0  0  0  0  0  0  0  0  0
    1.3365   -0.6381    0.2307 C   0  0  0  0  0  0  0  0  0  0  0  0
    2.3317   -1.4898    0.4794 O   0  0  0  0  0  0  0  0  0  0  0  0
    1.6772    0.6153   -0.0441 N   0  0  0  0  0  0  0  0  0  0  0  0
    0.6767    1.4549   -0.2937 C   0  0  0  0  0  0  0  0  0  0  0  0
   -0.6417    1.0675   -0.2722 C   0  0  0  0  0  0  0  0  0  0  0  0
   -2.2275   -1.7803    0.2455 H   0  0  0  0  0  0  0  0  0  0  0  0
   -2.9238   -0.2567   -0.2010 H   0  0  0  0  0  0  0  0  0  0  0  0
    3.1629   -0.9981    0.4091 H   0  0  0  0  0  0  0  0  0  0  0  0
    0.9696    2.4721   -0.5191 H   0  0  0  0  0  0  0  0  0  0  0  0
   -1.4477    1.7524   -0.4749 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  4  0  0  0  0
  2  3  4  0  0  0  0
  3  4  4  0  0  0  0
  4  5  1  0  0  0  0
  4  6  4  0  0  0  0
  6  7  4  0  0  0  0
  2  8  4  0  0  0  0
  7  8  4  0  0  0  0
  1  9  1  0  0  0  0
  1 10  1  0  0  0  0
  5 11  1  0  0  0  0
  7 12  1  0  0  0  0
  8 13  1  0  0  0  0
M  END''')

    def test_NH2(self):
        assert str(self.NH2) == \
               '[NH2] (MDFFNEOEWAXZRQ-UHFFFAOYSA-N-LVELFWPLOP-AAAAA/16)'

    def test_C2H2F2_no_stereo(self):
        assert str(self.C2H2F2) == \
               'FC=CF (WFLOTYSKFUPZQB-UHFFFAOYSA-N-LCGROIUHXB-AAAAA/64)'

    def test_preserve_aromatic(self):
        assert str(self.C5H4N3O) == \
               'Nc1ccnc(O)n1 (OPTASPLRGRRNAP-UHFFFAOYSA-N-XHMAPADKWL-AAAAA/111)'

    def test_mult(self):
        assert self.NH2.mult == 2

    def test_charge(self):
        assert self.NH2.charge == 0

    def test_sumformula(self):
        assert self.NH2.sumformula == 'H2N'

    def test_atoms(self):
        assert self.NH2.atoms == [(0, 'N'), (1, 'H'), (2, 'H')]

    def test_atomkey(self):
        assert self.NH2.atomkey == 'H 2,N 1'

    def test_adjacency(self):
        assert self.NH2.adjacency == [[1, 2], [0], [0]]

    def test_adjkey(self):
        assert self.NH2.adjkey == ';0;0'

    def test_bonds(self):
        assert self.C5H4N3O.bonds == \
               [(0, 1, 'SINGLE'), (1, 2, 'AROMATIC'), (2, 3, 'AROMATIC'), (3, 4, 'SINGLE'),
                (3, 5, 'AROMATIC'), (5, 6, 'AROMATIC'), (1, 7, 'AROMATIC'), (6, 7, 'AROMATIC'),
                (0, 8, 'SINGLE'), (0, 9, 'SINGLE'), (4, 10, 'SINGLE'), (6, 11, 'SINGLE'),
                (7, 12, 'SINGLE')]

    def test_bondkey(self):
        assert self.C5H4N3O.bondkey == '0-1;0-8;0-9;1:2;1:7;2:3;3-4;3:5;4-10;5:6;6:7;6-11;7-12'

    def test_smiles(self):
        assert self.NH2.smiles == '[NH2]'

    def test_plainsmiles(self):
        assert self.NH2.smiles == '[NH2]'

    def test_plainsmiles_stereo(self):
        assert self.C3H6O2.smiles == 'C[C@@H](O)C=O'

    def test_inchi(self):
        assert self.NH2.inchi == 'InChI=1S/H2N/h1H2'

    def test_inchi_stereo(self):
        assert self.C3H6O2.inchi == 'InChI=1S/C3H6O2/c1-3(5)2-4/h2-3,5H,1H3/t3-/m1/s1'

    def test_inchikey(self):
        assert self.NH2.inchikey == 'MDFFNEOEWAXZRQ-UHFFFAOYSA-N'

    def test_inchikey_stereo(self):
        assert self.C3H6O2.inchikey == 'BSABBBMNWQWLLU-GSVOUGTGSA-N'

    def test_priority(self):
        assert self.NH2.priority == 16

    def test_equality(self):
        assert self.NH2 == self.NH2.copy()

    def test_identity(self):
        assert self.NH2 is not self.NH2.copy()

    def test_repr(self):
        assert repr(self.NH2) == ("Configuration(coord='\\n\\n\\n  3  2  0  0  0  0  0  0  0  0999 V2000\\n   "
                                  '-0.0057    0.4060   -0.0000 N   0  0  0  0  0  2  0  0  0  0  0  0\\n   '
                                  '-0.8403   -0.2051   -0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\\n    '
                                  '0.8460   -0.2009    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0\\n  1  2  '
                                  "1  0\\n  1  3  1  0\\nM  RAD  1   1   2\\nM  END', stereo=False)")

    def test_format(self):
        assert f"{self.NH2}" == '[NH2] (MDFFNEOEWAXZRQ-UHFFFAOYSA-N-LVELFWPLOP-AAAAA/16)'

    def test_frozen(self):
        with pytest.raises(AttributeError):
            self.NH2.coord = ''

    def test_todict(self):
        assert self.NH2.todict() == {'coord': '''


  3  2  0  0  0  0  0  0  0  0999 V2000
   -0.0057    0.4060   -0.0000 N   0  0  0  0  0  2  0  0  0  0  0  0
   -0.8403   -0.2051   -0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0
    0.8460   -0.2009    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  1  0
  1  3  1  0
M  RAD  1   1   2
M  END''',
                                     'method': None, 'options': None,
                                     'program': None, 'stereo': False,
                                     'tag': 'd9a523e8-0bba-4cf3-9c34-ff9da0805569',
                                     'version': None}

    def test_todict_skip_none_values(self):
            assert self.NH2.todict(skip_none_values=True) == {'coord': '''


  3  2  0  0  0  0  0  0  0  0999 V2000
   -0.0057    0.4060   -0.0000 N   0  0  0  0  0  2  0  0  0  0  0  0
   -0.8403   -0.2051   -0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0
    0.8460   -0.2009    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  1  0
  1  3  1  0
M  RAD  1   1   2
M  END''',
                                        'stereo': False,
                                        'tag': 'd9a523e8-0bba-4cf3-9c34-ff9da0805569'}   

    def test_todict_skip_keys(self):
            assert self.NH2.todict(skip_keys=['stereo','tag']) == {'coord': '''


  3  2  0  0  0  0  0  0  0  0999 V2000
   -0.0057    0.4060   -0.0000 N   0  0  0  0  0  2  0  0  0  0  0  0
   -0.8403   -0.2051   -0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0
    0.8460   -0.2009    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  1  0
  1  3  1  0
M  RAD  1   1   2
M  END''',
                                     'method': None, 'options': None,
                                     'program': None,
                                     'version': None} 
            
    def test_copy(self):
        assert self.NH2.copy(program='MOPAC', version='2016').program == 'MOPAC'

    def test_error(self):
        with pytest.raises(MoleculeError, match='reading MOL format failed'):
            Configuration('''


  3  2  0  0  0  0  0  0  0  0999 V2000
   -0.0057    0.4060   -0.0000 N   0  0  0  0  0  2  0  0  0  0  0  0
   -0.8403   -0.2051   -0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  1  0
  1  3  1  0
M  RAD  1   1   2
M  END''')


class TestGeometry:

    @classmethod
    def setup_class(cls):
        cls.HF = Geometry(coord='''


  2  1  0  0  0  0  0  0  0  0999 V2000
    0.9726    0.0243    0.0920 F   0  0  0  0  0  0  0  0  0  0  0  0
    1.9118    0.0243    0.0920 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  1  0  0  0  0
M  END''', tag='291cc59a-8515-450d-8c28-ceaffecfa079', energy=-200.0)

        cls.C2H5N = Geometry(coord='''xtb: 6.6.0 (conda-forge)
          05242311223D

  8  7  0     0  0            999 V2000
    1.0746    0.0293   -0.0671 C   0  0  0  0  0  0  0  0  0  0  0  0
    2.5737    0.0869   -0.1140 C   0  0  0  0  0  0  0  0  0  0  0  0
    3.2872    0.8856   -0.7640 N   0  0  0  0  0  0  0  0  0  0  0  0
    0.7243   -0.7678    0.5816 H   0  0  0  0  0  0  0  0  0  0  0  0
    0.6810   -0.1302   -1.0710 H   0  0  0  0  0  0  0  0  0  0  0  0
    0.6810    0.9799    0.2930 H   0  0  0  0  0  0  0  0  0  0  0  0
    3.1086   -0.6468    0.4832 H   0  0  0  0  0  0  0  0  0  0  0  0
    2.7701    1.5680   -1.3195 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  1  0  0  0  0
  2  3  2  0  0  0  0
  1  4  1  0  0  0  0
  1  5  1  0  0  0  0
  1  6  1  0  0  0  0
  2  7  1  0  0  0  0
  3  8  1  0  0  0  0
M  END''', tag='2f029503-9269-4dd7-b8a5-1a19beee1c4a', stereo=True,
                             energy=-9.711263876293)

        cls.C4H5Np = Geometry(coord='''xtb: 6.6.0 (conda-forge)
     RDKit          3D

 10  0  0  0  0  0  0  0  0  0999 V2000
    0.3329    1.1477    0.0027 C   0  0  0  0  0  0  0  0  0  0  0  0
    1.1706    0.0549    0.0217 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.3938   -1.0742    0.0106 N   0  0  0  0  0  0  0  0  0  0  0  0
   -0.9286   -0.7150   -0.0152 C   0  0  0  0  0  0  0  0  0  0  0  0
   -0.9958    0.6604   -0.0206 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.6448    2.1841    0.0054 H   0  0  0  0  0  0  0  0  0  0  0  0
    2.2498   -0.0160    0.0421 H   0  0  0  0  0  0  0  0  0  0  0  0
    0.7421   -2.0243    0.0200 H   0  0  0  0  0  0  0  0  0  0  0  0
   -1.7061   -1.4669   -0.0274 H   0  0  0  0  0  0  0  0  0  0  0  0
   -1.9036    1.2494   -0.0393 H   0  0  0  0  0  0  0  0  0  0  0  0
M  CHG  1   1   1
M  END''', tag='5f249f19-a112-4413-81b5-ae93d1b56a1d', stereo=False,
                              energy=-367.795)

    def test_HF(self):
        assert str(self.HF) == 'F (KRHYYFGTRYWZRS-UHFFFAOYSA-N-MMIJCGAIQJ-AAAAA/20)'

    def test_C2H5N_stereo(self):
        assert str(self.C2H5N) == r'[H]/N=C\C (MPAYEWNVIPXRDP-IHWYPQMZSA-N-RLPYXRVLMR-AAAAA/43)'

    def test_C4H5Np_no_bonds(self):
        assert len(self.C4H5Np.bonds) == 0

    def test_sumformula(self):
        assert self.HF.sumformula == 'HF'

    def test_sumformula_stereo(self):
        assert self.C2H5N.sumformula == 'C2H5N'

    def test_sumformula_no_bonds(self):
        assert self.C4H5Np.sumformula == 'C4H5N+'

    def test_bondkey_no_bonds(self):
        assert self.C4H5Np.bondkey == ''

    def test_smiles(self):
        assert self.HF.smiles == 'F'

    def test_smiles_stereo(self):
        assert self.C2H5N.smiles == r'[H]/N=C\C'

    def test_smiles_no_bonds(self):
        assert self.C4H5Np.smiles == '[C+].[C].[C].[C].[H].[H].[H].[H].[H].[N]'

    def test_inchi(self):
        assert self.HF.inchi == 'InChI=1S/FH/h1H'

    def test_inchi_stereo(self):
        assert self.C2H5N.inchi == 'InChI=1S/C2H5N/c1-2-3/h2-3H,1H3/b3-2-'

    def test_inchi_no_bonds(self):
        assert self.C4H5Np.inchi == 'InChI=1S/4C.N.5H/q;;;+1;;;;;;'

    def test_inchikey(self):
        assert self.HF.inchikey == 'KRHYYFGTRYWZRS-UHFFFAOYSA-N'

    def test_inchikey_stereo(self):
        assert self.C2H5N.inchikey == 'MPAYEWNVIPXRDP-IHWYPQMZSA-N'

    def test_inchikey_no_bonds(self):
        assert self.C4H5Np.inchikey == 'BTYZZAXIDJCKJY-UHFFFAOYSA-N'

    def test_energy(self):
        assert self.C4H5Np.energy == approx(-367.795)

    def test_equality(self):
        assert self.HF == self.HF.copy()

    def test_identity(self):
        assert self.HF is not self.HF.copy()

    def test_repr(self):
        assert repr(self.HF) == ("Geometry(coord='\\n\\n\\n  2  1  0  0  0  0  0  0  0  0999 V2000\\n    "
                                 '0.9726    0.0243    0.0920 F   0  0  0  0  0  0  0  0  0  0  0  0\\n    '
                                 '1.9118    0.0243    0.0920 H   0  0  0  0  0  0  0  0  0  0  0  0\\n  1  2  '
                                 "1  0  0  0  0\\nM  END', stereo=False, energy=-200.0, converged=None, "
                                 "free_energy=None, stable=None, raw_output=None)")

    def test_format(self):
        assert f"{self.HF}" == 'F (KRHYYFGTRYWZRS-UHFFFAOYSA-N-MMIJCGAIQJ-AAAAA/20)'

    def test_frozen(self):
        with pytest.raises(AttributeError):
            self.HF.energy = 0.0

    def test_todict(self):
        assert self.HF.todict() == {'coord': '''


  2  1  0  0  0  0  0  0  0  0999 V2000
    0.9726    0.0243    0.0920 F   0  0  0  0  0  0  0  0  0  0  0  0
    1.9118    0.0243    0.0920 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  1  0  0  0  0
M  END''',
                                    'energy': -200.0, 'raw_output': None,
                                    'converged': None, 'free_energy': None,
                                    'stable': None, 'method': None,
                                    'options': None, 'program': None, 'stereo': False,
                                    'tag': '291cc59a-8515-450d-8c28-ceaffecfa079',
                                    'version': None}

    def test_todict_skip_none_values(self):
        assert self.HF.todict(skip_none_values=True) == {'coord': '''


  2  1  0  0  0  0  0  0  0  0999 V2000
    0.9726    0.0243    0.0920 F   0  0  0  0  0  0  0  0  0  0  0  0
    1.9118    0.0243    0.0920 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  1  0  0  0  0
M  END''',
                                    'energy': -200.0, 'stereo': False,
                                    'tag': '291cc59a-8515-450d-8c28-ceaffecfa079'}
      
    def test_todict_skip_keys(self):
        assert self.HF.todict(skip_keys=['stereo','tag']) == {'coord': '''


  2  1  0  0  0  0  0  0  0  0999 V2000
    0.9726    0.0243    0.0920 F   0  0  0  0  0  0  0  0  0  0  0  0
    1.9118    0.0243    0.0920 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  1  0  0  0  0
M  END''',
                                    'energy': -200.0, 'raw_output': None,
                                    'converged': None, 'free_energy': None,
                                    'stable': None, 'method': None,
                                    'options': None, 'program': None,
                                    'version': None}
          
    def test_copy(self):
        assert self.HF.copy(energy=-201.0).energy == pytest.approx(-201.0)

    def test_error(self):
        with pytest.raises(MoleculeError, match='reading MOL format failed'):
            Geometry('''


  2  2  0  0  0  0  0  0  0  0999 V2000
    0.9726    0.0243    0.0920 F   0  0  0  0  0  0  0  0  0  0  0  0
    1.9118    0.0243    0.0920 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  1  0  0  0  0
M  END''', energy=-200.)


class TestProperty:

    @classmethod
    def setup_class(cls):
        cls.HF = Property(coord='''


  2  1  0  0  0  0  0  0  0  0999 V2000
    0.9726    0.0243    0.0920 F   0  0  0  0  0  0  0  0  0  0  0  0
    1.9118    0.0243    0.0920 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  1  0  0  0  0
M  END''', tag='291cc59a-8515-450d-8c28-ceaffecfa079', properties={
            'dipole_x': 0, 'dipole_y': 0, 'dipole_z': 1.0})

    def test_property(self):
        assert self.HF.properties == {'dipole_x': 0, 'dipole_y': 0, 'dipole_z': 1.0}

    def test_repr(self):
        assert repr(self.HF) == "Property(coord='\\n\\n\\n  2  1  0  0  0  0  0  0  0  0999 V2000\\n    " \
                                '0.9726    0.0243    0.0920 F   0  0  0  0  0  0  0  0  0  0  0  0\\n    ' \
                                '1.9118    0.0243    0.0920 H   0  0  0  0  0  0  0  0  0  0  0  0\\n  1  2  ' \
                                "1  0  0  0  0\\nM  END', stereo=False, " \
                                "properties={'dipole_x': 0, 'dipole_y': 0, 'dipole_z': 1.0})"

    def test_todict(self):
        assert self.HF.todict() == {'coord': '''


  2  1  0  0  0  0  0  0  0  0999 V2000
    0.9726    0.0243    0.0920 F   0  0  0  0  0  0  0  0  0  0  0  0
    1.9118    0.0243    0.0920 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  1  0  0  0  0
M  END''',
                                    'method': None,
                                    'options': None,
                                    'program': None,
                                    'properties': {'dipole_x': 0, 'dipole_y': 0, 'dipole_z': 1.0},
                                    'stereo': False,
                                    'tag': '291cc59a-8515-450d-8c28-ceaffecfa079',
                                    'version': None}

    def test_todict_skip_none_values(self):
        assert self.HF.todict(skip_none_values=True) == {'coord': '''


  2  1  0  0  0  0  0  0  0  0999 V2000
    0.9726    0.0243    0.0920 F   0  0  0  0  0  0  0  0  0  0  0  0
    1.9118    0.0243    0.0920 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  1  0  0  0  0
M  END''',
                                    'properties': {'dipole_x': 0, 'dipole_y': 0, 'dipole_z': 1.0},
                                    'stereo': False,
                                    'tag': '291cc59a-8515-450d-8c28-ceaffecfa079'}

    def test_todict_skip_keys(self):
        assert self.HF.todict(skip_keys=['stereo','tag']) == {'coord': '''


  2  1  0  0  0  0  0  0  0  0999 V2000
    0.9726    0.0243    0.0920 F   0  0  0  0  0  0  0  0  0  0  0  0
    1.9118    0.0243    0.0920 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  1  0  0  0  0
M  END''',
                                    'method': None,
                                    'options': None,
                                    'program': None,
                                    'properties': {'dipole_x': 0, 'dipole_y': 0, 'dipole_z': 1.0},
                                    'version': None}
        

class TestFailedFormula:
    def test_repr(self):
        assert repr(FailedFormula('C1CCCCC', "reading SMILES string failed",
                                  tag='0a198102-14d1-4e04-9a14-bac1df2fc624')) == \
               "FailedFormula(smiles='C1CCCCC', error='reading SMILES string failed', " \
               "tag='0a198102-14d1-4e04-9a14-bac1df2fc624')"

    def test_str(self):
        assert str(FailedFormula('C1CCCCC', "reading SMILES string failed",
                                 tag='0a198102-14d1-4e04-9a14-bac1df2fc624')) == \
               'C1CCCCC (reading SMILES string failed)'


class TestInvalidFormula:
    def test_repr(self):
        assert repr(InvalidFormula('C1CCCCC', "reading SMILES string failed",
                                   tag='0a198102-14d1-4e04-9a14-bac1df2fc624')) == \
               "InvalidFormula(smiles='C1CCCCC', error='reading SMILES string failed', " \
               "tag='0a198102-14d1-4e04-9a14-bac1df2fc624')"

    def test_str(self):
        assert str(InvalidFormula('C1CCCCC', "reading SMILES string failed",
                                  tag='0a198102-14d1-4e04-9a14-bac1df2fc624')) == \
               'C1CCCCC (reading SMILES string failed)'


class TestFailedConfiguration:

    @classmethod
    def setup_class(cls):
        cls.C8HNO5 = FailedConfiguration(coord='''xtb: 6.6.0 (conda-forge)
          06112314433D

 15 16  0     0  0            999 V2000
    3.4192    0.6554    0.2541 O   0  0  0  0  0  0  0  0  0  0  0  0
    2.3159    0.9872    0.2247 C   0  0  0  0  0  0  0  0  0  0  0  0
    1.0776    1.4135    0.1967 C   0  0  0  0  0  0  0  0  0  0  0  0
   -0.1005    0.6222    0.0545 C   0  0  0  0  0  0  0  0  0  0  0  0
   -1.4228    1.2776    0.0438 C   0  0  0  0  0  0  0  0  0  0  0  0
   -1.6338    2.4581    0.1460 O   0  0  0  0  0  0  0  0  0  0  0  0
   -2.4550    0.1583   -0.1220 C   0  0  0  0  0  0  0  0  0  0  0  0
   -3.6443    0.2820   -0.1766 O   0  0  0  0  0  0  0  0  0  0  0  0
   -1.6538   -1.1445   -0.2029 C   0  0  0  0  0  0  0  0  0  0  0  0
   -2.0933   -2.2567   -0.3349 O   0  0  0  0  0  0  0  0  0  0  0  0
   -0.2386   -0.7405   -0.0847 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.7369   -1.6615   -0.1191 N   0  0  0  0  0  0  0  0  0  0  0  0
    1.8777   -2.0142   -0.0898 C   0  0  0  0  0  0  0  0  0  0  0  0
    2.9133   -2.5164   -0.0799 O   0  0  0  0  0  0  0  0  0  0  0  0
    0.9013    2.4795    0.2898 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  2  0  0  0  0
  2  3  2  0  0  0  0
  3  4  1  0  0  0  0
  4  5  1  0  0  0  0
  5  6  2  0  0  0  0
  5  7  1  0  0  0  0
  7  8  2  0  0  0  0
  7  9  1  0  0  0  0
  9 10  2  0  0  0  0
  4 11  4  0  0  0  0
  9 11  1  0  0  0  0
 11 12  1  0  0  0  0
 12 13  2  0  0  0  0
 12 14  1  0  0  0  0
 13 14  2  0  0  0  0
  3 15  1  0  0  0  0
M  END''', error='cannot determine bonds', tag='048a8f41-6af8-4ccd-93ba-c924682fbaa8')

    def test_str(self):
        assert str(self.C8HNO5) == '**INVALID** (cannot determine bonds)'

    def test_key(self):
        assert self.C8HNO5.key == '048a8f41-6af8-4ccd-93ba-c924682fbaa8'


class TestInvalidConfiguration:

    @classmethod
    def setup_class(cls):
        cls.C8H2N2O4 = InvalidConfiguration('''xtb: 6.6.0 (conda-forge)
          06112314503D

 16 16  0     0  0            999 V2000
    3.4253   -0.6922   -0.2839 N   0  0  0  0  0  0  0  0  0  0  0  0
    2.2044   -0.4376   -0.1605 C   0  0  0  0  0  0  0  0  0  0  0  0
    1.1025   -1.4588   -0.1962 C   0  0  0  0  0  0  0  0  0  0  0  0
    1.2222   -2.6445   -0.3363 O   0  0  0  0  0  0  0  0  0  0  0  0
   -0.1716   -0.7076   -0.0239 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.1335    0.7287    0.1322 C   0  0  0  0  0  0  0  0  0  0  0  0
    1.5959    0.9313    0.0472 C   0  0  0  0  0  0  0  0  0  0  0  0
    2.2197    1.9574    0.1192 O   0  0  0  0  0  0  0  0  0  0  0  0
   -0.6714    1.7386    0.3608 C   0  0  0  0  0  0  0  0  0  0  0  0
   -1.2940    2.6868    0.5718 O   0  0  0  0  0  0  0  0  0  0  0  0
   -3.0623    0.3319   -0.0485 C   0  0  0  0  0  0  0  0  0  0  0  0
   -3.6612    1.3096   -0.1978 O   0  0  0  0  0  0  0  0  0  0  0  0
   -2.5340   -0.8699    0.0651 C   0  0  0  0  0  0  0  0  0  0  0  0
   -1.2727   -1.3422   -0.0202 N   0  0  0  0  0  0  0  0  0  0  0  0
    4.0415    0.1212   -0.2339 H   0  0  0  0  0  0  0  0  0  0  0  0
   -3.2776   -1.6529    0.2052 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  2  0  0  0  0
  2  3  1  0  0  0  0
  3  4  2  0  0  0  0
  3  5  1  0  0  0  0
  5  6  1  0  0  0  0
  2  7  1  0  0  0  0
  6  7  1  0  0  0  0
  7  8  2  0  0  0  0
  6  9  2  0  0  0  0
  9 10  2  0  0  0  0
 11 12  2  0  0  0  0
 11 13  2  0  0  0  0
  5 14  2  0  0  0  0
 13 14  4  0  0  0  0
  1 15  1  0  0  0  0
 13 16  1  0  0  0  0
M  END''', error='rearrangement', tag='fa2ddf5b-55e2-4760-9155-193410160013')

    def test_str(self):
        assert str(self.C8H2N2O4) == r'[H]/N=C1\C(=O)C(=C=O)/C(=N\C=C=O)C1=O (rearrangement)'

    def test_key(self):
        assert self.C8H2N2O4.key == 'fa2ddf5b-55e2-4760-9155-193410160013'


class TestFailedGeometry:

    @classmethod
    def setup_class(cls):
        cls.C8H2N2O4 = FailedGeometry('', error='optimization timed out',
                                      tag='690c03ac-9955-4ba1-ab2e-92288515f72e')

    def test_str(self):
        assert str(self.C8H2N2O4) == r'**INVALID** (optimization timed out)'

    def test_key(self):
        assert self.C8H2N2O4.key == '690c03ac-9955-4ba1-ab2e-92288515f72e'


class TestInvalidGeometry:

    @classmethod
    def setup_class(cls):
        cls.C8H2N2O4 = InvalidGeometry('''xtb: 6.6.0 (conda-forge)
          06112314503D

 16 16  0     0  0            999 V2000
    3.4253   -0.6922   -0.2839 N   0  0  0  0  0  0  0  0  0  0  0  0
    2.2044   -0.4376   -0.1605 C   0  0  0  0  0  0  0  0  0  0  0  0
    1.1025   -1.4588   -0.1962 C   0  0  0  0  0  0  0  0  0  0  0  0
    1.2222   -2.6445   -0.3363 O   0  0  0  0  0  0  0  0  0  0  0  0
   -0.1716   -0.7076   -0.0239 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.1335    0.7287    0.1322 C   0  0  0  0  0  0  0  0  0  0  0  0
    1.5959    0.9313    0.0472 C   0  0  0  0  0  0  0  0  0  0  0  0
    2.2197    1.9574    0.1192 O   0  0  0  0  0  0  0  0  0  0  0  0
   -0.6714    1.7386    0.3608 C   0  0  0  0  0  0  0  0  0  0  0  0
   -1.2940    2.6868    0.5718 O   0  0  0  0  0  0  0  0  0  0  0  0
   -3.0623    0.3319   -0.0485 C   0  0  0  0  0  0  0  0  0  0  0  0
   -3.6612    1.3096   -0.1978 O   0  0  0  0  0  0  0  0  0  0  0  0
   -2.5340   -0.8699    0.0651 C   0  0  0  0  0  0  0  0  0  0  0  0
   -1.2727   -1.3422   -0.0202 N   0  0  0  0  0  0  0  0  0  0  0  0
    4.0415    0.1212   -0.2339 H   0  0  0  0  0  0  0  0  0  0  0  0
   -3.2776   -1.6529    0.2052 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  2  0  0  0  0
  2  3  1  0  0  0  0
  3  4  2  0  0  0  0
  3  5  1  0  0  0  0
  5  6  1  0  0  0  0
  2  7  1  0  0  0  0
  6  7  1  0  0  0  0
  7  8  2  0  0  0  0
  6  9  2  0  0  0  0
  9 10  2  0  0  0  0
 11 12  2  0  0  0  0
 11 13  2  0  0  0  0
  5 14  2  0  0  0  0
 13 14  4  0  0  0  0
  1 15  1  0  0  0  0
 13 16  1  0  0  0  0
M  END''', error='rearrangement', tag='2b9ce845-dc52-41dc-8296-8a6ce2caf34d')
        
        cls.C2H4N2O = InvalidGeometry('''
            RDKit          3D

  9  9  0  0  0  0  0  0  0  0999 V2000
   -1.6980   -0.0748   -0.0346 N   0  0  0  0  0  0  0  0  0  0  0  0
   -0.2800   -0.1942    0.0020 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.7450    0.6693   -0.0118 N   0  0  0  0  0  0  0  0  0  0  0  0
    1.5911   -0.3680    0.0510 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.5693   -1.2004    0.0635 O   0  0  0  0  0  0  0  0  0  0  0  0
   -2.2950   -0.9303   -0.0117 H   0  0  0  0  0  0  0  0  0  0  0  0
   -2.1424    0.8681   -0.0845 H   0  0  0  0  0  0  0  0  0  0  0  0
    0.8427    1.7075   -0.0536 H   0  0  0  0  0  0  0  0  0  0  0  0
    2.6674   -0.4771    0.0796 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  1  0
  2  3  1  0
  3  4  1  0
  4  5  1  0
  5  2  1  0
  1  6  1  0
  1  7  1  0
  3  8  1  0
  4  9  1  0
M  CHG  2   2  -1   4   1
M  END''', error='no convergence', tag='7f1b7b3e-df45-4c62-bb2e-fd8b2a8d5e89')

    def test_str(self):
        assert str(self.C8H2N2O4) == '[H]/N=C1\\C(=O)C(=C=O)/C(=N\\C=C=O)C1=O (rearrangement)'

    def test_key(self):
        assert self.C8H2N2O4.key == '2b9ce845-dc52-41dc-8296-8a6ce2caf34d'

    def test_str(self):
        assert str(self.C2H4N2O) == "Can't kekulize mol.  Unkekulized atoms: 3 (no convergence)"

    def test_key(self):
        assert self.C2H4N2O.key == '7f1b7b3e-df45-4c62-bb2e-fd8b2a8d5e89' 


class TestFailedProperty:

    @classmethod
    def setup_class(cls):
        cls.C8H2N2O4 = FailedProperty('', error='abnormal program exit',
                                      tag='690c03ac-9955-4ba1-ab2e-92288515f72e')

    def test_str(self):
        assert str(self.C8H2N2O4) == r'**INVALID** (abnormal program exit)'

    def test_key(self):
        assert self.C8H2N2O4.key == '690c03ac-9955-4ba1-ab2e-92288515f72e'


class TestInvalidProperty:

    @classmethod
    def setup_class(cls):
        cls.C8H2N2O4 = InvalidProperty('''


  1  0  0  0  0  0  0  0  0  0999 V2000
    0.0000    0.0000    0.0000 Os  0  0  0  0  0  0  0  0  0  0  0  0
M  END''', error='property not found',
                                       tag='2b9ce845-dc52-41dc-8296-8a6ce2caf34d')

    def test_str(self):
        assert str(self.C8H2N2O4) == r'[Os] (property not found)'

    def test_key(self):
        assert self.C8H2N2O4.key == '2b9ce845-dc52-41dc-8296-8a6ce2caf34d'


class TestSuperformula:

    @classmethod
    def setup_class(cls):
        cls.C3H9 = Superformula('[CH3-].[CH3].[CH3+]', index=True)
        cls.CH3N2O3 = Superformula('[N]=O.[O-]N=O.[CH3]')
        cls.CO2 = Superformula('O=C=O |atomProp:0.p.1:1.p.0:2.p.2|', index=True)
        cls.C7H18O3 = Superformula('C[C@H](CO)O.C[C@@H](O)CC', stereo=True)
        cls.C3H8O2 = Superformula('C[C@@H](O)C=O.O |atomProp:0.p.1:1.p.2:2.p.3:3.p.4:4.p.5:5.p.0|',
                                  index=True, stereo=True)

    def test_C2H4O2(self):
        assert repr(Superformula('COC=O')) == \
               "Superformula(smiles='COC=O', index=False, stereo=False)"

    def test_CH2O_CO(self):
        assert repr(Superformula('C=O.CO')) == \
               "Superformula(smiles='C=O.CO', index=False, stereo=False)"

    def test_CO_CH2O(self):
        assert repr(Superformula('CO.C=O')) == \
               "Superformula(smiles='C=O.CO', index=False, stereo=False)"

    def test_CO_CO(self):
        assert repr(Superformula('CO.CO')) == \
               "Superformula(smiles='CO.CO', index=False, stereo=False)"

    def test_CH2O_CO_CO(self):
        assert repr(Superformula('CO.C=O.CO')) == \
               "Superformula(smiles='C=O.CO.CO', index=False, stereo=False)"

    def test_H2O(self):
        assert repr(Superformula('[OH-].[H+]')) == \
               "Superformula(smiles='[H+].[OH-]', index=False, stereo=False)"

    def test_CH2O_CO_CO_idempotent(self):
        assert repr(Superformula('C=O.CO.CO')) == \
               "Superformula(smiles='C=O.CO.CO', index=False, stereo=False)"

    def test_C6H6_C6H6O_preserve_kekule(self):
        assert repr(Superformula('C1=CC=CC=C1.C1=CC=CC=C1O')) == \
               "Superformula(smiles='C1=CC=CC=C1.OC1=CC=CC=C1', index=False, stereo=False)"

    def test_C6H6_C6H6O_preserve_aromatic(self):
        assert repr(Superformula('c1ccccc1.c1ccccc1O')) == \
               "Superformula(smiles='Oc1ccccc1.c1ccccc1', index=False, stereo=False)"

    def test_C6H6_C6H6O_mixed_aromatic(self):
        assert repr(Superformula('C1=CC=CC=C1.c1ccccc1O')) == \
               "Superformula(smiles='C1=CC=CC=C1.Oc1ccccc1', index=False, stereo=False)"

    def test_C5H5N5_aromatic_atoms(self):
        assert (repr(Superformula('[CH-]=[NH2+].[H+].[H+].[c+]1=[n+]2-[c-]=n-[c-]3-[n-]-[c-]-2-n-1-3',
                                 index=False, stereo=False)) == \
               "Superformula(smiles='[CH-]=[NH2+].[H+].[H+].[c+]1=[n+]2-[c-]=n-[c-]3-[n-]-[c-]-2-n-1-3', "
               "index=False, stereo=False)")


    def test_C2H4O2_remove_index(self):
        assert repr(Superformula('COC=O |atomProp:0.p.0:1.p.1:2.p.2:3.p.3|')) == \
               "Superformula(smiles='COC=O', index=False, stereo=False)"

    def test_CH2O_CO_remove_index(self):
        assert repr(Superformula('C=O.CO |atomProp:0.p.0:1.p.1:2.p.2:3.p.3|')) == \
               "Superformula(smiles='C=O.CO', index=False, stereo=False)"

    def test_C4H10O_C3H8O2_remove_stereo(self):
        assert repr(Superformula('C[C@H](CO)O.C[C@@H](O)CC')) == \
               "Superformula(smiles='CC(O)CO.CCC(C)O', index=False, stereo=False)"

    def test_C3H8O2_remove_index_stereo(self):
        assert repr(Superformula('C[C@@H](O)C=O.O |atomProp:0.p.0:1.p.1:2.p.2:3.p.3:4.p.4:5.p.5|')) == \
               "Superformula(smiles='CC(O)C=O.O', index=False, stereo=False)"

    def test_C2H4O2_preserve_index(self):
        assert repr(Superformula('COC=O |atomProp:0.p.0:1.p.1:2.p.2:3.p.3|', index=True)) == \
               "Superformula(smiles='COC=O |atomProp:0.p.0:1.p.1:2.p.2:3.p.3|', " \
               "index=True, stereo=False)"

    def test_CH2O_CO_preserve_index(self):
        assert repr(Superformula('C=O.CO |atomProp:0.p.0:1.p.1:2.p.2:3.p.3|', index=True)) == \
               "Superformula(smiles='C=O.CO |atomProp:0.p.0:1.p.1:2.p.2:3.p.3|', " \
               "index=True, stereo=False)"

    def test_CO_CH2O_preserve_index(self):
        assert repr(Superformula('CO.C=O |atomProp:0.p.2:1.p.3:2.p.0:3.p.1|', index=True)) == \
               "Superformula(smiles='C=O.CO |atomProp:0.p.0:1.p.1:2.p.2:3.p.3|', " \
               "index=True, stereo=False)"

    def test_CO_CO_preserve_index(self):
        assert repr(Superformula('CO.CO |atomProp:0.p.0:1.p.1:2.p.2:3.p.3|', index=True)) == \
               "Superformula(smiles='CO.CO |atomProp:0.p.0:1.p.1:2.p.2:3.p.3|', " \
               "index=True, stereo=False)"

    def test_CH2O_CO_CO_preserve_index(self):
        assert repr(Superformula('C=O.CO.CO |atomProp:0.p.0:1.p.1:2.p.4:3.p.5:4.p.2:5.p.3|', index=True)) == \
               "Superformula(smiles='C=O.CO.CO |atomProp:0.p.0:1.p.1:2.p.2:3.p.3:4.p.4:5.p.5|', " \
               "index=True, stereo=False)"

    def test_CH2O_CO_CO_preserve_index_idempotent(self):
        assert repr(Superformula('C=O.CO.CO |atomProp:0.p.0:1.p.1:2.p.2:3.p.3:4.p.4:5.p.5|', index=True)) == \
               "Superformula(smiles='C=O.CO.CO |atomProp:0.p.0:1.p.1:2.p.2:3.p.3:4.p.4:5.p.5|', " \
               "index=True, stereo=False)"

    def test_H2O_preserve_index(self):
        assert repr(Superformula('[OH-].[H+]', index=True)) == \
               "Superformula(smiles='[H+].[OH-] |atomProp:1.p.0|', index=True, stereo=False)"

    def test_C2H2O2_preserve_index(self):
        assert repr(Superformula('[OH+]=[C-][C-]=[OH+] |atomProp:0.p.0:1.p.2:2.p.1:3.p.3|',
                                 index=True)) == \
               "Superformula(smiles='[OH+]=[C-][C-]=[OH+] |atomProp:0.p.0:1.p.2:2.p.1:3.p.3|', " \
               "index=True, stereo=False)"

    def test_C2H4O2_add_index(self):
        assert repr(Superformula('COC=O', index=True)) == \
               "Superformula(smiles='COC=O |atomProp:0.p.0:1.p.1:2.p.2:3.p.3|', " \
               "index=True, stereo=False)"

    def test_CH2O_CO_add_index(self):
        assert repr(Superformula('C=O.CO', index=True)) == \
               "Superformula(smiles='C=O.CO |atomProp:0.p.0:1.p.1:2.p.2:3.p.3|', " \
               "index=True, stereo=False)"

    def test_CO_CH2O_add_index(self):
        assert repr(Superformula('CO.C=O', index=True)) == \
               "Superformula(smiles='C=O.CO |atomProp:0.p.0:1.p.1:2.p.2:3.p.3|', " \
               "index=True, stereo=False)"

    def test_CO_CO_add_index(self):
        assert repr(Superformula('CO.CO', index=True)) == \
               "Superformula(smiles='CO.CO |atomProp:0.p.0:1.p.1:2.p.2:3.p.3|', " \
               "index=True, stereo=False)"

    def test_C4H10O_preserve_stereo(self):
        assert repr(Superformula('C[C@H](CO)O', stereo=True)) == \
               "Superformula(smiles='C[C@@H](O)CO', index=False, stereo=True)"

    def test_C4H10O_C3H8O2_preserve_stereo(self):
        assert repr(Superformula('C[C@H](CO)O.C[C@@H](O)CC', stereo=True)) == \
               "Superformula(smiles='CC[C@@H](C)O.C[C@@H](O)CO', index=False, stereo=True)"

    def test_C4H10O_C4H10O_preserve_stereo(self):
        assert repr(Superformula('C[C@H](CO)O.C[C@@H](CO)O', stereo=True)) == \
               "Superformula(smiles='C[C@@H](O)CO.C[C@H](O)CO', index=False, stereo=True)"

    def test_C4H10O_C3H8O2_preserve_stereo_idempotent(self):
        assert repr(Superformula('CC[C@@H](C)O.C[C@@H](O)CO', stereo=True)) == \
               "Superformula(smiles='CC[C@@H](C)O.C[C@@H](O)CO', index=False, stereo=True)"

    def test_C4H10O_C3H8O2_stereo_unspecified(self):
        assert repr(Superformula('CC(O)CO.CCC(C)O', stereo=True)) == \
               "Superformula(smiles='CC(O)CO.CCC(C)O', index=False, stereo=True)"

    def test_C3H8O2_preserve_index_stereo(self):
        assert repr(Superformula('C[C@@H](O)C=O.O |atomProp:0.p.0:1.p.1:2.p.2:3.p.3:4.p.4:5.p.5|',
                                 index=True, stereo=True)) == \
               "Superformula(smiles='C[C@@H](O)C=O.O " \
               "|atomProp:0.p.0:1.p.1:2.p.2:3.p.3:4.p.4:5.p.5|', index=True, stereo=True)"

    def test_C3H8O2_add_index_preserve_stereo(self):
        assert repr(Superformula('C[C@@H](O)C=O.O', index=True, stereo=True)) == \
               "Superformula(smiles='C[C@@H](O)C=O.O " \
               "|atomProp:0.p.0:1.p.1:2.p.2:3.p.3:4.p.4:5.p.5|', index=True, stereo=True)"

    def test_invalid_input(self):
        with pytest.raises(FlaskError, match="reading SMILES string failed"):
            Superformula('CO.')

    def test_smiles(self):
        assert self.CH3N2O3.smiles == 'O=N[O-].[CH3].[N]=O'

    def test_smiles_index(self):
        assert self.C3H9.smiles == '[CH3+].[CH3-].[CH3] |^1:2,atomProp:0.p.0:1.p.1:2.p.2|'

    def test_smiles_stereo(self):
        assert self.C7H18O3.smiles == 'CC[C@@H](C)O.C[C@@H](O)CO'

    def test_smiles_index_stereo(self):
        assert self.C3H8O2.smiles == \
               'C[C@@H](O)C=O.O |atomProp:0.p.1:1.p.2:2.p.3:3.p.4:4.p.5:5.p.0|'

    def test_plainsmiles(self):
        assert self.CH3N2O3.plainsmiles == 'O=N[O-].[CH3].[N]=O'

    def test_plainsmiles_index(self):
        assert self.C3H9.plainsmiles == '[CH3+].[CH3-].[CH3]'

    def test_plainsmiles_stereo(self):
        assert self.C7H18O3.plainsmiles == 'CC[C@@H](C)O.C[C@@H](O)CO'

    def test_plainsmiles_index_stereo(self):
        assert self.C3H8O2.plainsmiles == 'C[C@@H](O)C=O.O'

    def test_charge(self):
        assert self.CH3N2O3.charge == -1

    def test_mult(self):
        assert self.CH3N2O3.mult == 3

    def test_key(self):
        assert self.CH3N2O3.key == 'RPWSXAJCTKRGAR-UHFFFAOYSA-M-YRSOYZYJUP-AAAAA'

    def test_key_index(self):
        assert self.CO2.key == 'CURLTUGMZLYLDI-UHFFFAOYSA-N-QVJQPNRXRU-AAAAC'

    def test_key_stereo(self):
        assert self.C3H8O2.key == 'SHXBFPYKGXMZST-AENDTGMFSA-N-CKARLSSMBF-AAAFX'

    def test_key_index_stereo(self):
        assert self.C7H18O3.key == 'NWZOFFKNPMMYJW-ZOANPFLCSA-N-GCHXWCDYXX-AAAAA'

    def test_plainkey(self):
        assert self.CH3N2O3.plainkey == 'RPWSXAJCTKRGAR-UHFFFAOYSA-M-YRSOYZYJUP'

    def test_plainkey_index(self):
        assert self.CO2.plainkey == 'CURLTUGMZLYLDI-UHFFFAOYSA-N-QVJQPNRXRU'

    def test_plainkey_stereo(self):
        assert self.C3H8O2.plainkey == 'SHXBFPYKGXMZST-AENDTGMFSA-N-CKARLSSMBF'

    def test_plainkey_index_stereo(self):
        assert self.C7H18O3.plainkey == 'NWZOFFKNPMMYJW-ZOANPFLCSA-N-GCHXWCDYXX'

    def test_inchi(self):
        assert self.CO2.inchi == 'InChI=1S/CO2/c2-1-3'

    def test_inchi_stereo(self):
        assert self.C3H8O2.inchi == 'InChI=1S/C3H6O2.H2O/c1-3(5)2-4;/h2-3,5H,1H3;1H2/t3-;/m1./s1'

    def test_inchikey(self):
        assert self.CO2.inchikey == 'CURLTUGMZLYLDI-UHFFFAOYSA-N'

    def test_inchikey_stereo(self):
        assert self.C3H8O2.inchikey == 'SHXBFPYKGXMZST-AENDTGMFSA-N'

    def test_smileskey(self):
        assert self.CO2.smileskey == 'QVJQPNRXRU'

    def test_smileskey_stereo(self):
        assert self.C3H8O2.smileskey == 'CKARLSSMBF'

    def test_permindices(self):
        assert self.CO2.permindices == [1, 0, 2]

    def test_perm(self):
        assert self.CO2.perm == '1,0,2'

    def test_permkey(self):
        assert self.CO2.permkey == 'AAAAC'

    def test_sumformula(self):
        assert self.C3H9.sumformula == 'C3H9'

    def test_priority(self):
        assert self.C3H9.priority == 675

    def test_count(self):
        assert self.C3H9.count() == 3

    def test_split(self):
        assert self.CH3N2O3.split() == \
               [Formula(smiles='O=N[O-]'), Formula(smiles='[CH3]'), Formula(smiles='[N]=O')]

    def test_split_index(self):
        assert self.C3H9.split() == \
               [Formula('[CH3+] |atomProp:0.p.0|', index=True),
                Formula('[CH3-] |atomProp:0.p.1|', index=True),
                Formula('[CH3] |^1:0,atomProp:0.p.2|', index=True)]

    def test_split_stereo(self):
        assert self.C7H18O3.split() == \
               [Formula(smiles='CC[C@@H](C)O', stereo=True),
                Formula(smiles='C[C@@H](O)CO', stereo=True)]

    def test_combine(self):
        assert repr(Superformula.combine(['[CH3+]', '[CH3-]', '[CH3]'])) == \
               "Superformula(smiles='[CH3+].[CH3-].[CH3]', index=False, stereo=False)"

    def test_combine_index(self):
        assert repr(Superformula.combine(['[CH3+] |atomProp:0.p.2|',
                                          '[CH3-] |atomProp:0.p.0|',
                                          '[CH3] |atomProp:0.p.1|'], index=True)) == \
               "Superformula(smiles='[CH3+].[CH3-].[CH3] |^1:2,atomProp:0.p.2:1.p.0:2.p.1|', " \
               "index=True, stereo=False)"

    def test_combine_stereo(self):
        assert repr(Superformula.combine(['CC[C@@H](C)O', 'C[C@@H](O)CO'], stereo=True)) == \
               "Superformula(smiles='CC[C@@H](C)O.C[C@@H](O)CO', index=False, stereo=True)"

    def test_str(self):
        assert str(self.CH3N2O3) == \
               'O=N[O-].[CH3].[N]=O (RPWSXAJCTKRGAR-UHFFFAOYSA-M-YRSOYZYJUP-AAAAA/3241)'

    def test_equality(self):
        assert self.C3H9 == self.C3H9.copy()

    def test_identity(self):
        assert self.C3H9 is not self.C3H9.copy()

    def test_frozen(self):
        with pytest.raises(AttributeError):
            self.C3H9.energy = 0.0

    def test_fromdict(self):
        assert repr(Superformula.fromdict({"smiles": "[C]=O.C", "index": False, "stereo": False})) == \
               "Superformula(smiles='C.[C]=O', index=False, stereo=False)"

    def test_todict(self):
        assert Superformula(smiles='O=C=O.C', index=True,
                            tag='60205969-8044-4e2d-af9e-c7a83397a16e').todict() == \
               {'index': True, 'method': None, 'options': None,
                'program': None, 'stereo': False, 'version': None,
                'tag': '60205969-8044-4e2d-af9e-c7a83397a16e',
                'smiles': 'C.O=C=O |atomProp:0.p.0:1.p.1:2.p.2:3.p.3|'}

    def test_todict_skip_none_values(self):
        assert Superformula(smiles='O=C=O.C', index=True,
                            tag='60205969-8044-4e2d-af9e-c7a83397a16e').todict(skip_none_values=True) == \
               {'index': True, 'stereo': False,
                'tag': '60205969-8044-4e2d-af9e-c7a83397a16e',
                'smiles': 'C.O=C=O |atomProp:0.p.0:1.p.1:2.p.2:3.p.3|'}      

    def test_todict_skip_keys(self):
        assert Superformula(smiles='O=C=O.C', index=True,
                            tag='60205969-8044-4e2d-af9e-c7a83397a16e').todict(skip_keys=['stereo','tag']) == \
               {'index': True, 'method': None, 'options': None,
                'program': None, 'version': None,
                'smiles': 'C.O=C=O |atomProp:0.p.0:1.p.1:2.p.2:3.p.3|'}    
        
    def test_copy(self):
        assert Superformula(smiles='O.CO.CCO').copy(program="colibri2").program == "colibri2"


class TestSupergeometry:

    @classmethod
    def setup_class(cls):
        cls.C3H9 = Supergeometry('[CH3-].[CH3].[CH3+]', index=True, energy=-200)
        cls.CH3N2O3 = Supergeometry('[N]=O.[O-]N=O.[CH3]', energy=-300)
        cls.C7H18O3 = Supergeometry('C[C@H](CO)O.C[C@@H](O)CC', stereo=True, energy=-400)
        cls.C3H8O2 = Supergeometry('C[C@@H](O)C=O.O |atomProp:0.p.1:1.p.2:2.p.3:3.p.4:4.p.5:5.p.0|',
                                   index=True, stereo=True, energy=-500)

    def test_C2H4O2(self):
        assert repr(Supergeometry('COC=O', energy=-100)) == \
               "Supergeometry(smiles='COC=O', index=False, stereo=False, energy=-100, " \
               "converged=None, free_energy=None, stable=None)"

    def test_H2O(self):
        assert repr(Supergeometry('[OH-].[H+]')) == \
               "Supergeometry(smiles='[H+].[OH-]', index=False, stereo=False, energy=None, " \
               "converged=None, free_energy=None, stable=None)"

    def test_C2H4O2_remove_index(self):
        assert repr(Supergeometry('COC=O |atomProp:0.p.0:1.p.1:2.p.2:3.p.3|', energy=-100)) == \
               "Supergeometry(smiles='COC=O', index=False, stereo=False, energy=-100, " \
               "converged=None, free_energy=None, stable=None)"

    def test_C7H18O3_remove_stereo(self):
        assert repr(Supergeometry('C[C@H](CO)O.C[C@@H](O)CC', energy=-400)) == \
               "Supergeometry(smiles='CC(O)CO.CCC(C)O', index=False, stereo=False, " \
               "energy=-400, converged=None, free_energy=None, stable=None)"

    def test_C3H8O2_remove_index_stereo(self):
        assert repr(Supergeometry('C[C@@H](O)C=O.O |atomProp:0.p.1:1.p.2:2.p.3:3.p.4:4.p.5:5.p.0|',
                                  energy=-500)) == \
               "Supergeometry(smiles='CC(O)C=O.O', index=False, stereo=False, energy=-500, " \
               "converged=None, free_energy=None, stable=None)"

    def test_C2H4O2_preserve_index(self):
        assert repr(Supergeometry('COC=O |atomProp:0.p.0:1.p.1:2.p.2:3.p.3|', index=True, energy=-100)) == \
               "Supergeometry(smiles='COC=O |atomProp:0.p.0:1.p.1:2.p.2:3.p.3|', " \
               "index=True, stereo=False, energy=-100, converged=None, free_energy=None, stable=None)"

    def test_C7H18O3_preserve_stereo(self):
        assert repr(Supergeometry('C[C@H](CO)O.C[C@@H](O)CC', stereo=True, energy=-400)) == \
               "Supergeometry(smiles='CC[C@@H](C)O.C[C@@H](O)CO', index=False, stereo=True, " \
               "energy=-400, converged=None, free_energy=None, stable=None)"

    def test_C3H8O2_preserve_index_stereo(self):
        assert repr(Supergeometry('C[C@@H](O)C=O.O |atomProp:0.p.1:1.p.2:2.p.3:3.p.4:4.p.5:5.p.0|',
                                  index=True, stereo=True, energy=-500)) == \
               "Supergeometry(smiles='C[C@@H](O)C=O.O |atomProp:0.p.1:1.p.2:2.p.3:3.p.4:4.p.5:5.p.0|', " \
               "index=True, stereo=True, energy=-500, converged=None, free_energy=None, stable=None)"

    def test_invalid_input(self):
        with pytest.raises(FlaskError, match="reading SMILES string failed"):
            Supergeometry('CO.')

    def test_smiles(self):
        assert self.CH3N2O3.smiles == 'O=N[O-].[CH3].[N]=O'

    def test_smiles_index(self):
        assert self.C3H9.smiles == '[CH3+].[CH3-].[CH3] |^1:2,atomProp:0.p.0:1.p.1:2.p.2|'

    def test_smiles_stereo(self):
        assert self.C7H18O3.smiles == 'CC[C@@H](C)O.C[C@@H](O)CO'

    def test_smiles_index_stereo(self):
        assert self.C3H8O2.smiles == 'C[C@@H](O)C=O.O |atomProp:0.p.1:1.p.2:2.p.3:3.p.4:4.p.5:5.p.0|'

    def test_plainsmiles(self):
        assert self.CH3N2O3.plainsmiles == 'O=N[O-].[CH3].[N]=O'

    def test_plainsmiles_index(self):
        assert self.C3H9.plainsmiles == '[CH3+].[CH3-].[CH3]'

    def test_plainsmiles_stereo(self):
        assert self.C7H18O3.plainsmiles == 'CC[C@@H](C)O.C[C@@H](O)CO'

    def test_plainsmiles_index_stereo(self):
        assert self.C3H8O2.plainsmiles == 'C[C@@H](O)C=O.O'

    def test_charge(self):
        assert self.CH3N2O3.charge == -1

    def test_mult(self):
        assert self.CH3N2O3.mult == 3

    def test_key(self):
        assert self.CH3N2O3.key == 'RPWSXAJCTKRGAR-UHFFFAOYSA-M-YRSOYZYJUP-AAAAA'

    def test_key_index(self):
        assert self.C3H9.key == 'WBCXBGDXOGTVKX-UHFFFAOYSA-N-IBOPVQJNMH-AAAAA'

    def test_key_stereo(self):
        assert self.C7H18O3.key == 'NWZOFFKNPMMYJW-ZOANPFLCSA-N-GCHXWCDYXX-AAAAA'

    def test_key_index_stereo(self):
        assert self.C3H8O2.key == 'SHXBFPYKGXMZST-AENDTGMFSA-N-CKARLSSMBF-AAAFX'

    def test_plainkey(self):
        assert self.CH3N2O3.plainkey == 'RPWSXAJCTKRGAR-UHFFFAOYSA-M-YRSOYZYJUP'

    def test_plainkey_index(self):
        assert self.C3H9.plainkey == 'WBCXBGDXOGTVKX-UHFFFAOYSA-N-IBOPVQJNMH'

    def test_plainkey_stereo(self):
        assert self.C7H18O3.plainkey == 'NWZOFFKNPMMYJW-ZOANPFLCSA-N-GCHXWCDYXX'

    def test_plainkey_index_stereo(self):
        assert self.C3H8O2.plainkey == 'SHXBFPYKGXMZST-AENDTGMFSA-N-CKARLSSMBF'

    def test_inchi(self):
        assert self.C3H9.inchi == 'InChI=1S/3CH3/h3*1H3/q;-1;+1'

    def test_inchi_stereo(self):
        assert self.C3H8O2.inchi == 'InChI=1S/C3H6O2.H2O/c1-3(5)2-4;/h2-3,5H,1H3;1H2/t3-;/m1./s1'

    def test_inchikey(self):
        assert self.C3H9.inchikey == 'WBCXBGDXOGTVKX-UHFFFAOYSA-N'

    def test_inchikey_stereo(self):
        assert self.C3H8O2.inchikey == 'SHXBFPYKGXMZST-AENDTGMFSA-N'

    def test_smileskey(self):
        assert self.C3H9.smileskey == 'IBOPVQJNMH'

    def test_smileskey_stereo(self):
        assert self.C3H8O2.smileskey == 'CKARLSSMBF'

    def test_permindices(self):
        assert self.C3H9.permindices == [0, 1, 2]

    def test_perm(self):
        assert self.C3H9.perm == '0,1,2'

    def test_permkey(self):
        assert self.C3H9.permkey == 'AAAAA'

    def test_sumformula(self):
        assert self.C3H9.sumformula == 'C3H9'

    def test_priority(self):
        assert self.C3H9.priority == 675

    def test_count(self):
        assert self.C3H9.count() == 3

    def test_split(self):
        assert self.CH3N2O3.split() == \
               [Formula(smiles='O=N[O-]'), Formula(smiles='[CH3]'), Formula(smiles='[N]=O')]

    def test_split_index(self):
        assert self.C3H9.split() == \
               [Formula('[CH3+] |atomProp:0.p.0|', index=True),
                Formula('[CH3-] |atomProp:0.p.1|', index=True),
                Formula('[CH3] |^1:0,atomProp:0.p.2|', index=True)]

    def test_split_stereo(self):
        assert self.C7H18O3.split() == \
               [Formula(smiles='CC[C@@H](C)O', stereo=True),
                Formula(smiles='C[C@@H](O)CO', stereo=True)]

    def test_combine(self):
        assert repr(Supergeometry.combine(['[CH3+]', '[CH3-]', '[CH3]'])) == \
               "Supergeometry(smiles='[CH3+].[CH3-].[CH3]', index=False, stereo=False, " \
               "energy=None, converged=None, free_energy=None, stable=None)"

    def test_combine_index(self):
        assert repr(Supergeometry.combine(['[CH3+] |atomProp:0.p.2|',
                                           '[CH3-] |atomProp:0.p.0|',
                                           '[CH3] |atomProp:0.p.1|'], index=True)) == \
               "Supergeometry(smiles='[CH3+].[CH3-].[CH3] |^1:2,atomProp:0.p.2:1.p.0:2.p.1|', " \
               "index=True, stereo=False, energy=None, converged=None, free_energy=None, stable=None)"

    def test_combine_stereo(self):
        assert repr(Supergeometry.combine(['CC[C@@H](C)O', 'C[C@@H](O)CO'], stereo=True)) == \
               "Supergeometry(smiles='CC[C@@H](C)O.C[C@@H](O)CO', index=False, " \
               "stereo=True, energy=None, converged=None, free_energy=None, stable=None)"

    def test_str(self):
        assert str(self.CH3N2O3) == \
               'O=N[O-].[CH3].[N]=O (RPWSXAJCTKRGAR-UHFFFAOYSA-M-YRSOYZYJUP-AAAAA/3241)'

    def test_equality(self):
        assert self.C3H9 == self.C3H9.copy()

    def test_identity(self):
        assert self.C3H9 is not self.C3H9.copy()

    def test_frozen(self):
        with pytest.raises(AttributeError):
            self.C3H9.energy = 0.0

    def test_fromdict(self):
        assert repr(Supergeometry.fromdict({"smiles": "[C]=O.C", "index": True, "stereo": False})) == \
               "Supergeometry(smiles='C.[C]=O |^2:1,atomProp:0.p.0:1.p.1:2.p.2|', " \
               "index=True, stereo=False, energy=None, converged=None, free_energy=None, stable=None)"

    def test_todict(self):
        assert Supergeometry(smiles='O=C=O.C', tag='df4555f7-3c72-46f3-ba36-7822df842a3c').todict() == \
               {'converged': None, 'energy': None, 'free_energy': None, 'index': False,
                'method': None, 'options': None, 'program': None, 'smiles': 'C.O=C=O',
                'tag': 'df4555f7-3c72-46f3-ba36-7822df842a3c', 'stable': None,
                'stereo': False, 'version': None}

    def test_todict_skip_none_values(self):
        assert Supergeometry(smiles='O=C=O.C', tag='df4555f7-3c72-46f3-ba36-7822df842a3c').todict(skip_none_values=True) == \
               {'index': False, 'smiles': 'C.O=C=O',
                'tag': 'df4555f7-3c72-46f3-ba36-7822df842a3c', 'stereo': False}      

    def test_todict_skip_keys(self):
        assert Supergeometry(smiles='O=C=O.C', tag='df4555f7-3c72-46f3-ba36-7822df842a3c').todict(skip_keys=['stereo','tag']) == \
               {'converged': None, 'energy': None, 'free_energy': None, 'index': False,
                'method': None, 'options': None, 'program': None, 'smiles': 'C.O=C=O',
                'stable': None, 'version': None}  
        
    def test_copy(self):
        assert Supergeometry(smiles='O.CO.CCO').copy(energy=-1000.0).energy == pytest.approx(-1000.0)


class TestSuperproperty:

    @classmethod
    def setup_class(cls):
        cls.C3H9 = Superproperty('[CH3-].[CH3].[CH3+]', properties={'charge': 0, 'mult': 2})

    def test_C3H9(self):
        assert repr(self.C3H9) == "Superproperty(smiles='[CH3+].[CH3-].[CH3]', index=False, " \
                                  "stereo=False, properties={'charge': 0, 'mult': 2})"

    def test_properties(self):
        assert self.C3H9.properties == {'charge': 0, 'mult': 2}


class TestPendingSupergeometry:

    def test_H2O(self):
        assert repr(PendingSupergeometry('O', attempts_left=3)) == \
               "PendingSupergeometry(smiles='O', index=False, stereo=False, attempts_left=3)"

    def test_equality(self):
        assert PendingSupergeometry('O', attempts_left=3) == \
               PendingSupergeometry('O', attempts_left=2)

    def test_todict(self):
        assert PendingSupergeometry('O', tag='0bd516ff-9fe7-471a-ac90-764237d176a3',
                                    attempts_left=2).todict() == \
               {'attempts_left': 2, 'index': False, 'method': None, 'options': None,
                'program': None, 'smiles': 'O', 'tag': '0bd516ff-9fe7-471a-ac90-764237d176a3',
                'stereo': False, 'version': None}

    def test_todict_skip_none_values(self):
        assert PendingSupergeometry('O', tag='0bd516ff-9fe7-471a-ac90-764237d176a3',
                                    attempts_left=2).todict(skip_none_values=True) == \
               {'attempts_left': 2, 'index': False,
                'smiles': 'O', 'tag': '0bd516ff-9fe7-471a-ac90-764237d176a3',
                'stereo': False}     

    def test_todict_skip_keys(self):
        assert PendingSupergeometry('O', tag='0bd516ff-9fe7-471a-ac90-764237d176a3',
                                    attempts_left=2).todict(skip_keys=['stereo','tag']) == \
               {'attempts_left': 2, 'index': False, 'method': None, 'options': None,
                'program': None, 'smiles': 'O', 'version': None}     
        
    def test_from_dict(self):
        assert repr(
            PendingSupergeometry.fromdict({'attempts_left': 2, 'index': False, 'smiles': 'O',
                                           'stereo': False, 'version': None})) == \
               "PendingSupergeometry(smiles='O', index=False, stereo=False, attempts_left=2)"


class TestIncompleteSupergeometry:

    def test_H2O(self):
        assert repr(IncompleteSupergeometry('O')) == \
               "IncompleteSupergeometry(smiles='O', index=False, stereo=False)"

    def test_todict(self):
        assert IncompleteSupergeometry('O', tag='8cbc2552-15b3-42df-bd7d-41f37522c5eb').todict() == \
               {'index': False, 'method': None, 'options': None, 'program': None,
                'smiles': 'O', 'stereo': False, 'tag': '8cbc2552-15b3-42df-bd7d-41f37522c5eb',
                'version': None}

    def test_todict_skip_none_values(self):
        assert IncompleteSupergeometry('O', tag='8cbc2552-15b3-42df-bd7d-41f37522c5eb').todict(skip_none_values=True) == \
               {'index': False, 'smiles': 'O', 'stereo': False, 'tag': '8cbc2552-15b3-42df-bd7d-41f37522c5eb'}    

    def test_todict_skip_keys(self):
        assert IncompleteSupergeometry('O', tag='8cbc2552-15b3-42df-bd7d-41f37522c5eb').todict(skip_keys=['stereo','tag']) == \
               {'index': False, 'method': None, 'options': None, 'program': None,
                'smiles': 'O', 'version': None}    
        
    def test_fromdict(self):
        assert repr(IncompleteSupergeometry.fromdict(
            {'index': False, 'smiles': 'O', 'stereo': False})) == \
               "IncompleteSupergeometry(smiles='O', index=False, stereo=False)"


class TestTransformation:

    @classmethod
    def setup_class(cls):
        cls.OH = Transformation('CO.CO |atomProp:0.p.0:1.p.1:2.p.2:3.p.3|',
                                'CO.C[O-].[H+] |atomProp:0.p.0:1.p.1:2.p.2:3.p.3|',
                                '[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2]',
                                index=True,
                                tag='d91190d1-1cd4-406f-a2cf-e96e5dffc21d')

    def test_reactantsmiles(self):
        assert self.OH.reactantsmiles == 'CO.CO |atomProp:0.p.0:1.p.1:2.p.2:3.p.3|'

    def test_productsmiles(self):
        assert self.OH.productsmiles == 'CO.C[O-].[H+] |atomProp:0.p.0:1.p.1:2.p.2:3.p.3|'

    def test_reactantplainsmiles(self):
        assert self.OH.reactantplainsmiles == 'CO.CO'

    def test_productplainsmiles(self):
        assert self.OH.productplainsmiles == 'CO.C[O-].[H+]'

    def test_reactantkey(self):
        assert self.OH.reactantkey == 'COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF-AAAAA'

    def test_productkey(self):
        assert self.OH.productkey == 'NPRPHDSQZBFINE-UHFFFAOYSA-O-DVBOZSIUCD-AAAAA'

    def test_ruleid(self):
        assert self.OH.rule == '[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2]'

    def test_equality(self):
        assert self.OH == self.OH.copy()

    def test_identity(self):
        assert self.OH is not self.OH.copy()

    def test_str(self):
        assert str(self.OH) == \
               'CO.CO->CO.C[O-].[H+] (COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF-AAAAA>>NPRPHDSQZBFINE-UHFFFAOYSA-O-DVBOZSIUCD-AAAAA)'

    def test_todict(self):
        assert self.OH.todict() == {
            'index': True, 'stereo': False,
            'productsmiles': 'CO.C[O-].[H+] |atomProp:0.p.0:1.p.1:2.p.2:3.p.3|',
            'reactantsmiles': 'CO.CO |atomProp:0.p.0:1.p.1:2.p.2:3.p.3|',
            'rule': '[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2]',
            'tag': 'd91190d1-1cd4-406f-a2cf-e96e5dffc21d'}

    def test_todict_skip_none_values(self):
        assert self.OH.todict(skip_none_values=True) == {
            'index': True, 'stereo': False,
            'productsmiles': 'CO.C[O-].[H+] |atomProp:0.p.0:1.p.1:2.p.2:3.p.3|',
            'reactantsmiles': 'CO.CO |atomProp:0.p.0:1.p.1:2.p.2:3.p.3|',
            'rule': '[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2]',
            'tag': 'd91190d1-1cd4-406f-a2cf-e96e5dffc21d'}    

    def test_todict_skip_keys(self):
        assert self.OH.todict(skip_keys=['stereo','tag']) == {
            'index': True,
            'productsmiles': 'CO.C[O-].[H+] |atomProp:0.p.0:1.p.1:2.p.2:3.p.3|',
            'reactantsmiles': 'CO.CO |atomProp:0.p.0:1.p.1:2.p.2:3.p.3|',
            'rule': '[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2]'}    
        
    def test_fromdict(self):
        assert repr(Transformation.fromdict(
            {'index': True, 'stereo': False,
             'productsmiles': 'CO.C[O-].[H+] |atomProp:0.p.0:1.p.1:2.p.2:3.p.3|',
             'reactantsmiles': 'CO.CO |atomProp:0.p.0:1.p.1:2.p.2:3.p.3|',
             'rule': '[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2]'})) == \
               ("Transformation(reactantsmiles='CO.CO |atomProp:0.p.0:1.p.1:2.p.2:3.p.3|', "
                "productsmiles='CO.C[O-].[H+] |atomProp:0.p.0:1.p.1:2.p.2:3.p.3|', "
                "rule='[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2]', index=True, stereo=False)")

class TestRule:

    @classmethod
    def setup_class(cls):
        cls.OH = Rule('[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2]',
                                1,
                                tag='a3f7e8a0-7db0-4c79-9c4f-63b8c3e503a0')

    def test_id(self):
        assert self.OH.ruleid == 1

    def test_rule(self):
        assert self.OH.rule == '[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2]'

    def test_equality(self):
        assert self.OH == self.OH.copy()

    def test_identity(self):
        assert self.OH is not self.OH.copy()

    def test_str(self):
        assert str(self.OH) == \
               '[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2] (1)'
        
    def test_todict(self):
        assert self.OH.todict() == {
            'rule': '[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2]', 'ruleid': 1,
            'tag': 'a3f7e8a0-7db0-4c79-9c4f-63b8c3e503a0'}

    def test_todict_skip_none_values(self):
        assert self.OH.todict(skip_none_values=True) == {
            'rule': '[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2]', 'ruleid': 1,
            'tag': 'a3f7e8a0-7db0-4c79-9c4f-63b8c3e503a0'}  

    def test_todict_skip_keys(self):
        assert self.OH.todict(skip_keys=['tag']) == {
            'rule': '[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2]', 'ruleid': 1}  
        
    def test_fromdict(self):
        assert repr(Rule.fromdict(
            {'rule': '[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2]', 'ruleid': 1,
            'tag': 'a3f7e8a0-7db0-4c79-9c4f-63b8c3e503a0'})) == \
               ("Rule(rule='[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2]', ruleid=1)")