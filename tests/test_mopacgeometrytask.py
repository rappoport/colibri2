import pytest
from pytest import approx

from colibri2.config import LocalConfig
from colibri2.data import Configuration, Geometry
from colibri2.task import MOPACGeometryTask


# TODO(dr): Add invalid and failed geometry examples
class TestMOPACGeometryTask:

    @pytest.fixture(scope="class")
    def mopac_geometry_task(self, request):
        config = LocalConfig()
        config.update({
            'GEOMETRY__EXECUTABLE': request.config.getini('mopac_exec'),
        })
        return MOPACGeometryTask(config)

    @pytest.fixture(scope="class")
    def mopac_geometry_task_stereo(self, request):
        config = LocalConfig()
        config.update({
            'GEOMETRY__EXECUTABLE': request.config.getini('mopac_exec'),
            'GEOMETRY__STEREO': True,
        })
        return MOPACGeometryTask(config)

    @pytest.fixture(scope="class")
    def mopac_geometry_task_no_opt(self, request):
        config = LocalConfig()
        config.update({
            'GEOMETRY__EXECUTABLE': request.config.getini('mopac_exec'),
            'GEOMETRY__NO_OPT': True,
        })
        return MOPACGeometryTask(config)

    def test_H(self, mopac_geometry_task):
        out = mopac_geometry_task.process(Configuration('''


  1  0  0  0  0  0  0  0  0  0999 V2000
    0.0000    0.0000    0.0000 H   0  0  0  0  0 15  0  0  0  0  0  0
M  CHG  1   1   1
M  END'''))
        assert type(out) is Geometry
        assert out.converged is True
        assert out.energy == 0.0

    def test_Na(self, mopac_geometry_task):
        out = mopac_geometry_task.process(Configuration('''


  1  0  0  0  0  0  0  0  0  0999 V2000
    0.0000    0.0000    0.0000 Na  0  0  0  0  0 15  0  0  0  0  0  0
M  CHG  1   1   1
M  END'''))
        assert type(out) is Geometry
        assert out.converged is True
        assert out.energy == 0.0

    def test_C(self, mopac_geometry_task):
        out = mopac_geometry_task.process(Configuration('''


  1  0  0  0  0  0  0  0  0  0999 V2000
    0.0000    0.0000    0.0000 C   0  0  0  0  0 15  0  0  0  0  0  0
M  END'''))
        assert type(out) is Geometry
        assert out.converged is True
        assert out.energy == approx(-111.83198, abs=1e-3)

    def test_H2O(self, mopac_geometry_task):
        out = mopac_geometry_task.process(Configuration('''


  3  2  0  0  0  0  0  0  0  0999 V2000
    0.0000    0.0000    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0
    1.2990    0.7500    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0
   -1.2990    0.7500    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  1  0
  1  3  1  0
M  END'''))
        assert type(out) is Geometry
        assert out.converged is True
        assert out.energy == approx(-323.00294, abs=1e-3)

    def test_NH2(self, mopac_geometry_task):
        out = mopac_geometry_task.process(Configuration('''


  3  2  0  0  0  0  0  0  0  0999 V2000
    0.0000    0.0000    0.0000 N   0  0  0  0  0  2  0  0  0  0  0  0
    1.2990    0.7500    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0
   -1.2990    0.7500    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  1  0
  1  3  1  0
M  RAD  1   1   2
M  END'''))
        assert type(out) is Geometry
        assert out.converged is True
        assert out.energy == approx(-211.71757, abs=1e-3)

    def test_O2(self, mopac_geometry_task):
        out = mopac_geometry_task.process(Configuration('''


  2  1  0  0  0  0  0  0  0  0999 V2000
    0.0000    0.0000    0.0000 O   0  0  0  0  0  1  0  0  0  0  0  0
    1.2990    0.7500    0.0000 O   0  0  0  0  0  1  0  0  0  0  0  0
  1  2  1  0
M  RAD  2   1   2   2   2
M  END'''))
        assert type(out) is Geometry
        assert out.converged is True
        assert out.energy == approx(-587.42613, abs=1e-3)
        assert out.mult == 3

    def test_CH3p(self, mopac_geometry_task):
        out = mopac_geometry_task.process(Configuration('''


  4  3  0  0  0  0  0  0  0  0999 V2000
    0.0021    0.0206    0.0002 C   0  0  0  0  0  3  0  0  0  0  0  0
    1.0573   -0.2121   -0.0108 H   0  0  0  0  0  0  0  0  0  0  0  0
   -0.6765   -0.8314   -0.0027 H   0  0  0  0  0  0  0  0  0  0  0  0
   -0.3828    1.0229    0.0133 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  1  0
  1  3  1  0
  1  4  1  0
M  CHG  1   1   1
M  END'''))
        assert type(out) is Geometry
        assert out.converged is True
        assert out.energy == approx(-155.6206, abs=1e-3)

    def test_CH3m(self, mopac_geometry_task):
        out = mopac_geometry_task.process(Configuration(coord='''


  4  3  0  0  0  0  0  0  0  0999 V2000
   -0.0047    0.0106    0.2562 C   0  0  0  0  0  3  0  0  0  0  0  0
    1.0116   -0.2372   -0.0839 H   0  0  0  0  0  0  0  0  0  0  0  0
   -0.6941   -0.7915   -0.0844 H   0  0  0  0  0  0  0  0  0  0  0  0
   -0.3129    1.0181   -0.0878 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  1  0
  1  3  1  0
  1  4  1  0
M  CHG  1   1  -1
M  END'''))
        assert type(out) is Geometry
        assert out.converged is True
        assert out.energy == approx(-165.1363, abs=1e-3)

    def test_CHCH3(self, mopac_geometry_task):
        out = mopac_geometry_task.process(Configuration('''


  6  5  0  0  0  0  0  0  0  0999 V2000
    0.9763   -0.6427   -0.1050 C   0  0  0  0  0  2  0  0  0  0  0  0
   -0.4091   -0.0040    0.0010 C   0  0  0  0  0  0  0  0  0  0  0  0
    1.6970    0.1839    0.0489 H   0  0  0  0  0  0  0  0  0  0  0  0
   -1.1701   -0.6221   -0.5011 H   0  0  0  0  0  0  0  0  0  0  0  0
   -0.3768    0.9940   -0.5057 H   0  0  0  0  0  0  0  0  0  0  0  0
   -0.7173    0.0909    1.0619 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  1  0
  1  3  1  0
  2  4  1  0
  2  5  1  0
  2  6  1  0
M  RAD  1   1   3
M  END'''))
        assert type(out) is Geometry
        assert out.converged is True
        assert out.energy == approx(-296.93830, abs=1e-3)

    def test_CH2CH2(self, mopac_geometry_task):
        out = mopac_geometry_task.process(Configuration('''


  6  5  0  0  0  0  0  0  0  0999 V2000
    0.7218   -0.0020   -0.0416 C   0  0  0  0  0  3  0  0  0  0  0  0
   -0.7455   -0.1965   -0.2139 C   0  0  0  0  0  3  0  0  0  0  0  0
    1.3472   -0.7710   -0.4840 H   0  0  0  0  0  0  0  0  0  0  0  0
    1.1009    0.8574    0.4928 H   0  0  0  0  0  0  0  0  0  0  0  0
   -1.1831   -0.6654    0.6920 H   0  0  0  0  0  0  0  0  0  0  0  0
   -1.2413    0.7775   -0.4452 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  1  0
  1  3  1  0
  1  4  1  0
  2  5  1  0
  2  6  1  0
M  CHG  2   1   1   2  -1
M  END'''))
        assert type(out) is Geometry
        assert out.converged is True
        assert out.energy == approx(-298.94752, abs=1e-3)

    def test_C5H5N(self, mopac_geometry_task):
        out = mopac_geometry_task.process(Configuration('''


 10 10  0  0  0  0  0  0  0  0999 V2000
    0.3095    1.1146    0.0201 C   0  0  0  0  0  0  0  0  0  0  0  0
    1.1732    0.0400    0.0632 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.4047   -1.1084    0.0183 C   0  0  0  0  0  0  0  0  0  0  0  0
   -0.9284   -0.6952   -0.0521 C   0  0  0  0  0  0  0  0  0  0  0  0
   -0.9461    0.6465   -0.0488 N   0  0  0  0  0  0  0  0  0  0  0  0
    0.5687    2.1631    0.0373 H   0  0  0  0  0  0  0  0  0  0  0  0
    2.2598    0.0464    0.1216 H   0  0  0  0  0  0  0  0  0  0  0  0
    0.7219   -2.1250    0.0322 H   0  0  0  0  0  0  0  0  0  0  0  0
   -1.7773   -1.3393   -0.0997 H   0  0  0  0  0  0  0  0  0  0  0  0
   -1.7859    1.2574   -0.0920 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  2  0
  2  3  1  0
  3  4  2  0
  4  5  1  0
  5  1  1  0
  1  6  1  0
  2  7  1  0
  3  8  1  0
  4  9  1  0
  5 10  1  0
M  END'''))
        assert type(out) is Geometry
        assert out.converged is True
        assert out.energy == approx(-744.7238, abs=1e-3)

    def test_C4H8O_remove_stereo(self, mopac_geometry_task):
        out = mopac_geometry_task.process(Configuration('''
            RDKit          3D

 13 12  0  0  0  0  0  0  0  0999 V2000
    1.6482   -0.8409   -0.0128 C   0  0  0  0  0  0  0  0  0  0  0  0
    1.0300    0.3383   -0.1671 C   0  0  0  0  0  0  0  0  0  0  0  0
   -0.4517    0.6030   -0.1692 C   0  0  1  0  0  0  0  0  0  0  0  0
   -1.3604   -0.6176   -0.0909 C   0  0  0  0  0  0  0  0  0  0  0  0
   -0.7563    1.4409    0.9419 O   0  0  0  0  0  0  0  0  0  0  0  0
    1.1057   -1.7665    0.1495 H   0  0  0  0  0  0  0  0  0  0  0  0
    2.7322   -0.9036   -0.0362 H   0  0  0  0  0  0  0  0  0  0  0  0
    1.6576    1.2137   -0.3257 H   0  0  0  0  0  0  0  0  0  0  0  0
   -0.6992    1.1612   -1.0794 H   0  0  0  0  0  0  0  0  0  0  0  0
   -1.2162   -1.1632    0.8486 H   0  0  0  0  0  0  0  0  0  0  0  0
   -2.4113   -0.3071   -0.1072 H   0  0  0  0  0  0  0  0  0  0  0  0
   -1.1918   -1.3013   -0.9288 H   0  0  0  0  0  0  0  0  0  0  0  0
   -0.0867    2.1430    0.9772 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  2  3
  2  3  1  0
  3  4  1  0
  3  5  1  0
  1  6  1  0
  1  7  1  0
  2  8  1  0
  3  9  1  6
  4 10  1  0
  4 11  1  0
  4 12  1  0
  5 13  1  0
M  END'''))
        assert type(out) is Geometry
        assert out.smiles == 'C=CC(C)O'
        assert out.converged is True
        assert out.energy == approx(-894.4886, abs=1e-3)

    def test_C4H8O_preserve_stereo(self, mopac_geometry_task_stereo):
        out = mopac_geometry_task_stereo.process(Configuration('''
            RDKit          3D

 13 12  0  0  0  0  0  0  0  0999 V2000
    1.6482   -0.8409   -0.0128 C   0  0  0  0  0  0  0  0  0  0  0  0
    1.0300    0.3383   -0.1671 C   0  0  0  0  0  0  0  0  0  0  0  0
   -0.4517    0.6030   -0.1692 C   0  0  1  0  0  0  0  0  0  0  0  0
   -1.3604   -0.6176   -0.0909 C   0  0  0  0  0  0  0  0  0  0  0  0
   -0.7563    1.4409    0.9419 O   0  0  0  0  0  0  0  0  0  0  0  0
    1.1057   -1.7665    0.1495 H   0  0  0  0  0  0  0  0  0  0  0  0
    2.7322   -0.9036   -0.0362 H   0  0  0  0  0  0  0  0  0  0  0  0
    1.6576    1.2137   -0.3257 H   0  0  0  0  0  0  0  0  0  0  0  0
   -0.6992    1.1612   -1.0794 H   0  0  0  0  0  0  0  0  0  0  0  0
   -1.2162   -1.1632    0.8486 H   0  0  0  0  0  0  0  0  0  0  0  0
   -2.4113   -0.3071   -0.1072 H   0  0  0  0  0  0  0  0  0  0  0  0
   -1.1918   -1.3013   -0.9288 H   0  0  0  0  0  0  0  0  0  0  0  0
   -0.0867    2.1430    0.9772 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  2  3
  2  3  1  0
  3  4  1  0
  3  5  1  0
  1  6  1  0
  1  7  1  0
  2  8  1  0
  3  9  1  6
  4 10  1  0
  4 11  1  0
  4 12  1  0
  5 13  1  0
M  END'''))
        assert type(out) is Geometry
        assert out.smiles == 'C=C[C@@H](C)O'
        assert out.converged is True
        assert out.energy == approx(-894.4886, abs=1e-3)

    def test_C5H5N_no_opt(self, mopac_geometry_task_no_opt):
        out = mopac_geometry_task_no_opt.process(Configuration('''


 10 10  0  0  0  0  0  0  0  0999 V2000
    0.3095    1.1146    0.0201 C   0  0  0  0  0  0  0  0  0  0  0  0
    1.1732    0.0400    0.0632 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.4047   -1.1084    0.0183 C   0  0  0  0  0  0  0  0  0  0  0  0
   -0.9284   -0.6952   -0.0521 C   0  0  0  0  0  0  0  0  0  0  0  0
   -0.9461    0.6465   -0.0488 N   0  0  0  0  0  0  0  0  0  0  0  0
    0.5687    2.1631    0.0373 H   0  0  0  0  0  0  0  0  0  0  0  0
    2.2598    0.0464    0.1216 H   0  0  0  0  0  0  0  0  0  0  0  0
    0.7219   -2.1250    0.0322 H   0  0  0  0  0  0  0  0  0  0  0  0
   -1.7773   -1.3393   -0.0997 H   0  0  0  0  0  0  0  0  0  0  0  0
   -1.7859    1.2574   -0.0920 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  2  0
  2  3  1  0
  3  4  2  0
  4  5  1  0
  5  1  1  0
  1  6  1  0
  2  7  1  0
  3  8  1  0
  4  9  1  0
  5 10  1  0
M  END'''))
        assert type(out) is Geometry
        assert out.converged is False
        assert out.energy == approx(-744.51538, abs=1e-3)
