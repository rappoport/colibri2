import pytest

from colibri2.chemistry import Element


class TestElement:

    def test_number_constructor(self):
        assert Element(13) == Element('Al')

    def test_name(self):
        assert Element('Es').name == 'Es'

    def test_number(self):
        assert Element('Ca').number == 20

    def test_mass(self):
        assert Element('Br').mass == 79.904

    def test_covalent_radius(self):
        assert Element('N').covalent_radius == 0.68

    def test_vdw_radius(self):
        assert Element('N').vdw_radius == 1.55

    def test_str_output(self):
        assert str(Element('Ca')) == 'Ca'

    def test_invalid_name(self):
        with pytest.raises(ValueError, match="unknown element name"):
            Element('Fo')

    def test_invalid_number(self):
        with pytest.raises(ValueError, match="unknown element number"):
            Element(116)
