def pytest_addoption(parser):
    parser.addini('mopac_exec', 'location of the MOPAC executable')
    parser.addini('xtb_exec', 'location of the XTB executable')
