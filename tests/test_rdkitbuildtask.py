import pytest

from colibri2.config import LocalConfig
from colibri2.data import Formula, Configuration, FailedFormula
from colibri2.task import RDKitBuildTask


class TestRDKitBuildTask:

    @pytest.fixture(scope="class")
    def rdkit_builder_task(self):
        return RDKitBuildTask(LocalConfig())

    @pytest.fixture(scope="class")
    def rdkit_builder_task_uff(self):
        config = LocalConfig()
        config.update({
            'CONFIGURATION__FF_METHOD': "UFF",
            'CONFIGURATION__FF_ITERATIONS': 1000,
        })
        return RDKitBuildTask(config)

    @pytest.fixture(scope="class")
    def rdkit_builder_task_stereo(self):
        config = LocalConfig()
        config.update({
            'CONFIGURATION__STEREO': True,
        })
        return RDKitBuildTask(config)

    @pytest.fixture(scope="class")
    def rdkit_builder_task_10_conf(self):
        config = LocalConfig()
        config.update({
            'CONFIGURATION__CONFORMERS': 10,
        })
        return RDKitBuildTask(config)

    def test_H(self, rdkit_builder_task):
        out = rdkit_builder_task.process(Formula('[H+]'))
        assert type(out) is Configuration
        assert out.charge == 1
        assert out.mult == 1
        assert out.smiles == '[H+]'

    def test_H2O(self, rdkit_builder_task):
        out = rdkit_builder_task.process(Formula('O'))
        assert type(out) is Configuration
        assert out.charge == 0
        assert out.mult == 1
        assert out.smiles == 'O'

    def test_SO42m(self, rdkit_builder_task):
        out = rdkit_builder_task.process(Formula('O=S(=O)([O-])[O-]'))
        assert type(out) == Configuration
        assert out.charge == -2
        assert out.mult == 1
        assert out.smiles == 'O=S(=O)([O-])[O-]'

    def test_Clm(self, rdkit_builder_task):
        out = rdkit_builder_task.process(Formula('[Cl-]'))
        assert type(out) == Configuration
        assert out.charge == -1
        assert out.mult == 1
        assert out.smiles == '[Cl-]'

    def test_O2m(self, rdkit_builder_task):
        out = rdkit_builder_task.process(Formula('[O-2]'))
        assert type(out) == Configuration
        assert out.charge == -2
        assert out.mult == 1
        assert out.smiles == '[O-2]'

    def test_Nap(self, rdkit_builder_task):
        out = rdkit_builder_task.process(Formula('[Na+]'))
        assert type(out) == Configuration
        assert out.charge == 1
        assert out.mult == 1
        assert out.smiles == '[Na+]'

    def test_Brp(self, rdkit_builder_task):
        out = rdkit_builder_task.process(Formula('[Br+]'))
        assert type(out) == Configuration
        assert out.charge == 1
        assert out.mult == 3
        assert out.smiles == '[Br+]'

    def test_Mg2p(self, rdkit_builder_task):
        out = rdkit_builder_task.process(Formula('[Mg+2]'))
        assert type(out) == Configuration
        assert out.charge == 2
        assert out.mult == 1
        assert out.smiles == '[Mg+2]'

    def test_N2H62p(self, rdkit_builder_task):
        out = rdkit_builder_task.process(Formula('[NH3+][NH3+]'))
        assert type(out) == Configuration
        assert out.charge == 2
        assert out.mult == 1
        assert out.smiles == '[NH3+][NH3+]'

    def test_H3Op(self, rdkit_builder_task):
        out = rdkit_builder_task.process(Formula('[OH3+]'))
        assert type(out) == Configuration
        assert out.charge == 1
        assert out.mult == 1
        assert out.smiles == '[OH3+]'

    def test_C4H8O_remove_stereo(self, rdkit_builder_task):
        out = rdkit_builder_task.process(Formula('C=C[C@@H](C)O', stereo=True))
        assert type(out) == Configuration
        assert out.smiles == 'C=CC(C)O'

    def test_C4H8O_preserve_stereo(self, rdkit_builder_task_stereo):
        out = rdkit_builder_task_stereo.process(Formula('C=C[C@@H](C)O', stereo=True))
        assert type(out) == Configuration
        assert out.smiles == 'C=C[C@@H](C)O'

    def test_C6H12_2p_ff_error(self, rdkit_builder_task):
        out = rdkit_builder_task.process(Formula('C[C+](C)[C+](C)C'))
        assert type(out) == FailedFormula
        assert out.smiles == 'C[C+](C)[C+](C)C'
        assert out.error == 'FF optimization failed'

    def test_C4H9N5_ff_error(self, rdkit_builder_task):
        out = rdkit_builder_task.process(Formula('NC1=C(N)C(N)=C(N)N1'))
        assert type(out) == FailedFormula
        assert out.smiles == 'NC1=C(N)C(N)=C(N)N1'
        assert out.error == 'FF optimization failed'

    def test_C4H9N5_ff_error_uff(self, rdkit_builder_task_uff):
        out = rdkit_builder_task_uff.process(Formula('NC1=C(N)C(N)=C(N)N1'))
        assert type(out) == Configuration
        assert out.smiles == 'NC1=C(N)C(N)=C(N)N1'

    def test_C4H9N5_ff_error_10_conf(self, rdkit_builder_task_10_conf):
        out = rdkit_builder_task_10_conf.process(Formula('NC1=C(N)C(N)=C(N)N1'))
        assert type(out) == Configuration
        assert out.smiles == 'NC1=C(N)C(N)=C(N)N1'

    def test_embedding_error(self, rdkit_builder_task):
        out = rdkit_builder_task.process(Formula('c12c3c4c5c1c1c6c2c2c3c3c4c4c5c1c1c6c2c3c41'))
        assert type(out) == FailedFormula
        assert out.smiles == 'c12c3c4c5c1c1c6c2c2c3c3c4c4c5c1c1c6c2c3c41'
        assert out.error == 'embedding failed'

    def test_embedding_runtime_error(self, rdkit_builder_task):
        out = rdkit_builder_task.process(Formula('[OH+]=[C-][NH+]1[C-]2N[C-]21'))
        assert type(out) == FailedFormula
        assert out.smiles == '[OH+]=[C-][NH+]1[C-]2N[C-]21'
        assert out.error == "embedding failed due to RuntimeError: Invariant Violation\n\
	upper bound not greater than lower bound\n\
	Violation occurred on line 186 in file Code/GraphMol/DistGeomHelpers/BoundsMatrixBuilder.cpp\n\
	Failed Expression: ub > lb\n\
	RDKIT: 2022.09.4\n\
	BOOST: 1_78\n"