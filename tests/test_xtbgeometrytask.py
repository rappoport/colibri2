import pytest
from pytest import approx

from colibri2.config import LocalConfig
from colibri2.data import Configuration, Geometry, InvalidGeometry
from colibri2.task import XTBGeometryTask


class TestXTBGeometryTask:

    @pytest.fixture(scope="class")
    def xtb_geometry_task(self, request):
        config = LocalConfig()
        config.update({
            'GEOMETRY__EXECUTABLE': request.config.getini('xtb_exec'),
            'GEOMETRY__VERSION': '',
            'GEOMETRY__OPTIONS': '',
            'GEOMETRY__METHOD': 'GFN2',
            'GEOMETRY__VALIDATE_BONDING': True
        })
        return XTBGeometryTask(config)

    @pytest.fixture(scope="class")
    def xtb_geometry_task_stereo(self, request):
        config = LocalConfig()
        config.update({
            'GEOMETRY__EXECUTABLE': request.config.getini('xtb_exec'),
            'GEOMETRY__VERSION': '',
            'GEOMETRY__OPTIONS': '',
            'GEOMETRY__METHOD': 'GFN2',
            'GEOMETRY__STEREO': True,
        })
        return XTBGeometryTask(config)

    @pytest.fixture(scope="class")
    def xtb_geometry_task_free_energy(self, request):
        config = LocalConfig()
        config.update({
            'GEOMETRY__EXECUTABLE': request.config.getini('xtb_exec'),
            'GEOMETRY__VERSION': '',
            'GEOMETRY__OPTIONS': '',
            'GEOMETRY__METHOD': 'GFN2',
            'GEOMETRY__FREE_ENERGY': True,
        })
        return XTBGeometryTask(config)

    @pytest.fixture(scope="class")
    def xtb_geometry_task_free_energy_reopt(self, request):
        config = LocalConfig()
        config.update({
            'GEOMETRY__EXECUTABLE': request.config.getini('xtb_exec'),
            'GEOMETRY__VERSION': '',
            'GEOMETRY__OPTIONS': '',
            'GEOMETRY__METHOD': 'GFN2',
            'GEOMETRY__FREE_ENERGY': True,
            'GEOMETRY__REOPTIMIZATIONS': 3,
        })
        return XTBGeometryTask(config)

    @pytest.fixture(scope="class")
    def xtb_geometry_task_no_opt(self, request):
        config = LocalConfig()
        config.update({
            'GEOMETRY__EXECUTABLE': request.config.getini('xtb_exec'),
            'GEOMETRY__VERSION': '',
            'GEOMETRY__OPTIONS': '',
            'GEOMETRY__METHOD': 'GFN2',
            'GEOMETRY__NO_OPT': True,
        })
        return XTBGeometryTask(config)

    @pytest.fixture(scope="class")
    def xtb_geometry_task_ignore_bonding(self, request):
        config = LocalConfig()
        config.update({
            'GEOMETRY__EXECUTABLE': request.config.getini('xtb_exec'),
            'GEOMETRY__VERSION': '',
            'GEOMETRY__OPTIONS': '',
            'GEOMETRY__METHOD': 'GFN2',
            'GEOMETRY__IGNORE_BONDING': True,
        })
        return XTBGeometryTask(config)

    def test_H2O(self, xtb_geometry_task):
        out = xtb_geometry_task.process(Configuration('''


  3  2  0  0  0  0  0  0  0  0999 V2000
    0.9166    0.0453    0.0267 O   0  0  0  0  0  0  0  0  0  0  0  0
    1.8845    0.0784    0.0593 H   0  0  0  0  0  0  0  0  0  0  0  0
    0.6378    0.7061    0.6782 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  1  0  0  0  0
  1  3  1  0  0  0  0
M  END
'''))
        assert type(out) is Geometry
        assert out.converged is True
        assert out.energy == approx(-137.9765)

    def test_NH2(self, xtb_geometry_task):
        out = xtb_geometry_task.process(Configuration('''


  3  2  0  0  0  0  0  0  0  0999 V2000
    0.0000    0.0000    0.0000 N   0  0  0  0  0  2  0  0  0  0  0  0
    1.2990    0.7500    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0
   -1.2990    0.7500    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  1  0
  1  3  1  0
M  RAD  1   1   2
M  END'''))
        assert type(out) is Geometry
        assert out.converged is True
        assert out.energy == approx(-103.6462, abs=1e-3)

    def test_CH3p(self, xtb_geometry_task):
        out = xtb_geometry_task.process(Configuration('''


  4  3  0  0  0  0  0  0  0  0999 V2000
    0.9988   -0.2110    0.0074 C   0  3  0  0  0  0  0  0  0  0  0  0
    2.0194    0.1461    0.0599 H   0  0  0  0  0  0  0  0  0  0  0  0
    0.4885   -0.5257    0.9088 H   0  0  0  0  0  0  0  0  0  0  0  0
    0.4884   -0.2532   -0.9463 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  1  0  0  0  0
  1  3  1  0  0  0  0
  1  4  1  0  0  0  0
M  CHG  1   1   1
M  END'''))
        assert type(out) is Geometry
        assert out.converged is True
        assert out.energy == approx(-82.1610, abs=1e-4)

    def test_C4HNO3(self, xtb_geometry_task):
        out = xtb_geometry_task.process(Configuration('''
     RDKit          3D

  9  9  0  0  0  0  0  0  0  0999 V2000
   -1.6049    1.6053    0.3103 O   0  0  0  0  0  0  0  0  0  0  0  0
   -0.7781    0.7314    0.2808 C   0  0  0  0  0  0  0  0  0  0  0  0
   -1.0602   -0.7445    0.1480 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.0042   -1.4861    0.1408 N   0  0  0  0  0  0  0  0  0  0  0  0
    1.1244   -0.6564    0.2629 C   0  0  0  0  0  0  0  0  0  0  0  0
    2.2848   -1.0199    0.2909 O   0  0  0  0  0  0  0  0  0  0  0  0
    0.7275    0.8307    0.3631 C   0  0  0  0  0  0  0  0  0  0  0  0
    1.4068    1.8158    0.4760 O   0  0  0  0  0  0  0  0  0  0  0  0
   -2.1045   -1.0764    0.0697 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  2  0
  2  3  1  0
  3  4  2  0
  4  5  1  0
  5  6  2  0
  5  7  1  0
  7  8  2  0
  7  2  1  0
  3  9  1  0
M  END'''))
        assert type(out) is Geometry
        assert out.converged is True
        assert out.energy == approx(-655.4151986143595)

    def test_C4H3N3O2(self, xtb_geometry_task):
        out = xtb_geometry_task.process(Configuration('''
     RDKit          3D

 12 12  0  0  0  0  0  0  0  0999 V2000
    2.0150   -1.1898    0.1256 N   0  0  0  0  0  0  0  0  0  0  0  0
    0.8332   -0.5384    0.0476 C   0  0  0  0  0  0  0  0  0  0  0  0
   -0.2349   -1.1645    0.4212 N   0  0  0  0  0  0  0  0  0  0  0  0
   -1.4484   -0.5299    0.3378 C   0  0  0  0  0  0  0  0  0  0  0  0
   -2.4935   -1.0374    0.7245 O   0  0  0  0  0  0  0  0  0  0  0  0
   -1.5097    0.8640   -0.2479 C   0  0  0  0  0  0  0  0  0  0  0  0
   -2.5183    1.5115   -0.4495 O   0  0  0  0  0  0  0  0  0  0  0  0
   -0.1828    1.4336   -0.5742 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.8952    0.7456   -0.4100 N   0  0  0  0  0  0  0  0  0  0  0  0
    2.0042   -1.9792    0.7597 H   0  0  0  0  0  0  0  0  0  0  0  0
    2.8183   -0.5820    0.2181 H   0  0  0  0  0  0  0  0  0  0  0  0
   -0.1783    2.4666   -0.9527 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  1  0
  2  3  2  0
  3  4  1  0
  4  5  2  0
  4  6  1  0
  6  7  2  0
  6  8  1  0
  8  9  2  0
  9  2  1  0
  1 10  1  0
  1 11  1  0
  8 12  1  0
M  END'''))
        assert type(out) is InvalidGeometry
        assert out.error == 'rearrangement'

    def test_C4H8O_remove_stereo(self, xtb_geometry_task):
        out = xtb_geometry_task.process(Configuration('''
                RDKit          3D

 13 12  0  0  0  0  0  0  0  0999 V2000
    1.6482   -0.8409   -0.0128 C   0  0  0  0  0  0  0  0  0  0  0  0
    1.0300    0.3383   -0.1671 C   0  0  0  0  0  0  0  0  0  0  0  0
   -0.4517    0.6030   -0.1692 C   0  0  1  0  0  0  0  0  0  0  0  0
   -1.3604   -0.6176   -0.0909 C   0  0  0  0  0  0  0  0  0  0  0  0
   -0.7563    1.4409    0.9419 O   0  0  0  0  0  0  0  0  0  0  0  0
    1.1057   -1.7665    0.1495 H   0  0  0  0  0  0  0  0  0  0  0  0
    2.7322   -0.9036   -0.0362 H   0  0  0  0  0  0  0  0  0  0  0  0
    1.6576    1.2137   -0.3257 H   0  0  0  0  0  0  0  0  0  0  0  0
   -0.6992    1.1612   -1.0794 H   0  0  0  0  0  0  0  0  0  0  0  0
   -1.2162   -1.1632    0.8486 H   0  0  0  0  0  0  0  0  0  0  0  0
   -2.4113   -0.3071   -0.1072 H   0  0  0  0  0  0  0  0  0  0  0  0
   -1.1918   -1.3013   -0.9288 H   0  0  0  0  0  0  0  0  0  0  0  0
   -0.0867    2.1430    0.9772 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  2  3
  2  3  1  0
  3  4  1  0
  3  5  1  0
  1  6  1  0
  1  7  1  0
  2  8  1  0
  3  9  1  6
  4 10  1  0
  4 11  1  0
  4 12  1  0
  5 13  1  0
M  END'''))
        assert type(out) is Geometry
        assert out.smiles == 'C=CC(C)O'
        assert out.converged is True
        assert out.energy == approx(-453.42996, abs=1e-3)

    def test_C4H8O_preserve_stereo(self, xtb_geometry_task_stereo):
        out = xtb_geometry_task_stereo.process(Configuration('''
                RDKit          3D

 13 12  0  0  0  0  0  0  0  0999 V2000
    1.6482   -0.8409   -0.0128 C   0  0  0  0  0  0  0  0  0  0  0  0
    1.0300    0.3383   -0.1671 C   0  0  0  0  0  0  0  0  0  0  0  0
   -0.4517    0.6030   -0.1692 C   0  0  1  0  0  0  0  0  0  0  0  0
   -1.3604   -0.6176   -0.0909 C   0  0  0  0  0  0  0  0  0  0  0  0
   -0.7563    1.4409    0.9419 O   0  0  0  0  0  0  0  0  0  0  0  0
    1.1057   -1.7665    0.1495 H   0  0  0  0  0  0  0  0  0  0  0  0
    2.7322   -0.9036   -0.0362 H   0  0  0  0  0  0  0  0  0  0  0  0
    1.6576    1.2137   -0.3257 H   0  0  0  0  0  0  0  0  0  0  0  0
   -0.6992    1.1612   -1.0794 H   0  0  0  0  0  0  0  0  0  0  0  0
   -1.2162   -1.1632    0.8486 H   0  0  0  0  0  0  0  0  0  0  0  0
   -2.4113   -0.3071   -0.1072 H   0  0  0  0  0  0  0  0  0  0  0  0
   -1.1918   -1.3013   -0.9288 H   0  0  0  0  0  0  0  0  0  0  0  0
   -0.0867    2.1430    0.9772 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  2  3
  2  3  1  0
  3  4  1  0
  3  5  1  0
  1  6  1  0
  1  7  1  0
  2  8  1  0
  3  9  1  6
  4 10  1  0
  4 11  1  0
  4 12  1  0
  5 13  1  0
M  END'''))
        assert type(out) is Geometry
        assert out.smiles == 'C=C[C@@H](C)O'
        assert out.converged is True
        assert out.energy == approx(-453.4296006939346)

    def test_C7N2O5(self, xtb_geometry_task):
        out = xtb_geometry_task.process(Configuration('''
     RDKit          3D

 14 15  0  0  0  0  0  0  0  0999 V2000
    3.4034    0.9453    0.3412 O   0  0  0  0  0  0  0  0  0  0  0  0
    2.2731    0.5017    0.2049 C   0  0  0  0  0  0  0  0  0  0  0  0
    1.1555    1.3274    0.2939 N   0  0  0  0  0  0  0  0  0  0  0  0
    0.0192    0.7404    0.1321 C   0  0  0  0  0  0  0  0  0  0  0  0
   -1.3492    1.4014    0.1791 C   0  0  0  0  0  0  0  0  0  0  0  0
   -1.5576    2.5717    0.3755 O   0  0  0  0  0  0  0  0  0  0  0  0
   -2.4043    0.3008   -0.0696 C   0  0  0  0  0  0  0  0  0  0  0  0
   -3.6000    0.4505   -0.1043 O   0  0  0  0  0  0  0  0  0  0  0  0
   -1.6402   -1.0274   -0.2656 C   0  0  0  0  0  0  0  0  0  0  0  0
   -2.1188   -2.1117   -0.4821 O   0  0  0  0  0  0  0  0  0  0  0  0
   -0.1562   -0.7233   -0.1359 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.8079   -1.5731   -0.2371 N   0  0  0  0  0  0  0  0  0  0  0  0
    2.0874   -1.0473   -0.0787 C   0  0  0  0  0  0  0  0  0  0  0  0
    3.0797   -1.7565   -0.1535 O   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  2  0
  2  3  1  0
  3  4  2  0
  4  5  1  0
  5  6  2  0
  5  7  1  0
  7  8  2  0
  7  9  1  0
  9 10  2  0
  9 11  1  0
 11 12  2  0
 12 13  1  0
 13 14  2  0
 13  2  1  0
 11  4  1  0
M  END'''))
        assert type(out) is InvalidGeometry
        assert out.error == "rearrangement"

    def test_C4H5N_free_energy(self, xtb_geometry_task_free_energy):
        out = xtb_geometry_task_free_energy.process(Configuration('''
     RDKit          3D

 10 10  0  0  0  0  0  0  0  0999 V2000
    0.3150    1.1474   -0.0462 C   0  0  0  0  0  0  0  0  0  0  0  0
    1.1470    0.0570   -0.1691 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.3967   -1.0726    0.0300 N   0  0  0  0  0  0  0  0  0  0  0  0
   -0.9032   -0.7161    0.2783 C   0  0  0  0  0  0  0  0  0  0  0  0
   -0.9827    0.6580    0.2370 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.6104    2.1835   -0.1492 H   0  0  0  0  0  0  0  0  0  0  0  0
    2.2055   -0.0119   -0.3805 H   0  0  0  0  0  0  0  0  0  0  0  0
    0.7476   -2.0213   -0.0015 H   0  0  0  0  0  0  0  0  0  0  0  0
   -1.6579   -1.4688    0.4625 H   0  0  0  0  0  0  0  0  0  0  0  0
   -1.8784    1.2449    0.3938 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  2  0
  2  3  1  0
  3  4  1  0
  4  5  2  0
  5  1  1  0
  1  6  1  0
  2  7  1  0
  3  8  1  0
  4  9  1  0
  5 10  1  0
M  END'''))

        assert type(out) is Geometry
        assert out.converged is True
        assert out.energy == approx(-381.42629315896943)
        assert out.stable is True
        assert out.free_energy == approx(-379.9318135082969)

    def test_C4H7N3O_free_energy_unstable(self, xtb_geometry_task_free_energy):
        out = xtb_geometry_task_free_energy.process(Configuration('''
             RDKit          3D

 15 15  0  0  0  0  0  0  0  0999 V2000
    2.4206   -0.9744    0.0162 N   0  0  0  0  0  0  0  0  0  0  0  0
    1.2314   -0.2871   -0.0157 C   0  0  0  0  0  0  0  0  0  0  0  0
    1.1090    1.0352   -0.2446 C   0  0  0  0  0  0  0  0  0  0  0  0
   -0.1198    1.6439   -0.2841 N   0  0  0  0  0  0  0  0  0  0  0  0
   -1.2494    0.8903   -0.0882 C   0  0  0  0  0  0  0  0  0  0  0  0
   -1.1787   -0.4358    0.1406 C   0  0  0  0  0  0  0  0  0  0  0  0
   -2.2581   -1.2545    0.3690 N   0  0  0  0  0  0  0  0  0  0  0  0
    0.0777   -1.0660    0.1842 O   0  0  0  0  0  0  0  0  0  0  0  0
    2.2670   -1.9498   -0.2257 H   0  0  0  0  0  0  0  0  0  0  0  0
    3.1394   -0.5406   -0.5537 H   0  0  0  0  0  0  0  0  0  0  0  0
    1.9577    1.6887   -0.4061 H   0  0  0  0  0  0  0  0  0  0  0  0
   -0.1924    2.6387   -0.4560 H   0  0  0  0  0  0  0  0  0  0  0  0
   -2.1853    1.4338   -0.1335 H   0  0  0  0  0  0  0  0  0  0  0  0
   -3.0445   -0.7611    0.7786 H   0  0  0  0  0  0  0  0  0  0  0  0
   -1.9746   -2.0612    0.9188 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  1  0
  2  3  2  0
  3  4  1  0
  4  5  1  0
  5  6  2  0
  6  7  1  0
  6  8  1  0
  8  2  1  0
  1  9  1  0
  1 10  1  0
  3 11  1  0
  4 12  1  0
  5 13  1  0
  7 14  1  0
  7 15  1  0
M  END'''))
        assert type(out) is Geometry
        assert out.converged is True
        assert out.energy == approx(-677.880269562021)
        assert out.stable is False
        assert out.free_energy == approx(-675.6045321935657)

    def test_C4H7N3O_free_energy_reopt(self, xtb_geometry_task_free_energy_reopt):
        out = xtb_geometry_task_free_energy_reopt.process(Configuration('''
             RDKit          3D

 15 15  0  0  0  0  0  0  0  0999 V2000
    2.4206   -0.9744    0.0162 N   0  0  0  0  0  0  0  0  0  0  0  0
    1.2314   -0.2871   -0.0157 C   0  0  0  0  0  0  0  0  0  0  0  0
    1.1090    1.0352   -0.2446 C   0  0  0  0  0  0  0  0  0  0  0  0
   -0.1198    1.6439   -0.2841 N   0  0  0  0  0  0  0  0  0  0  0  0
   -1.2494    0.8903   -0.0882 C   0  0  0  0  0  0  0  0  0  0  0  0
   -1.1787   -0.4358    0.1406 C   0  0  0  0  0  0  0  0  0  0  0  0
   -2.2581   -1.2545    0.3690 N   0  0  0  0  0  0  0  0  0  0  0  0
    0.0777   -1.0660    0.1842 O   0  0  0  0  0  0  0  0  0  0  0  0
    2.2670   -1.9498   -0.2257 H   0  0  0  0  0  0  0  0  0  0  0  0
    3.1394   -0.5406   -0.5537 H   0  0  0  0  0  0  0  0  0  0  0  0
    1.9577    1.6887   -0.4061 H   0  0  0  0  0  0  0  0  0  0  0  0
   -0.1924    2.6387   -0.4560 H   0  0  0  0  0  0  0  0  0  0  0  0
   -2.1853    1.4338   -0.1335 H   0  0  0  0  0  0  0  0  0  0  0  0
   -3.0445   -0.7611    0.7786 H   0  0  0  0  0  0  0  0  0  0  0  0
   -1.9746   -2.0612    0.9188 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  1  0
  2  3  2  0
  3  4  1  0
  4  5  1  0
  5  6  2  0
  6  7  1  0
  6  8  1  0
  8  2  1  0
  1  9  1  0
  1 10  1  0
  3 11  1  0
  4 12  1  0
  5 13  1  0
  7 14  1  0
  7 15  1  0
M  END'''))
        assert type(out) is Geometry
        assert out.converged is True
        assert out.energy == approx(-678.0230807514996)
        assert out.stable is True
        assert out.free_energy == approx(-675.7539029246705)

    def test_C4H3N3O2_no_opt(self, xtb_geometry_task_no_opt):
        out = xtb_geometry_task_no_opt.process(Configuration('''
     RDKit          3D

 12 12  0  0  0  0  0  0  0  0999 V2000
    2.0150   -1.1898    0.1256 N   0  0  0  0  0  0  0  0  0  0  0  0
    0.8332   -0.5384    0.0476 C   0  0  0  0  0  0  0  0  0  0  0  0
   -0.2349   -1.1645    0.4212 N   0  0  0  0  0  0  0  0  0  0  0  0
   -1.4484   -0.5299    0.3378 C   0  0  0  0  0  0  0  0  0  0  0  0
   -2.4935   -1.0374    0.7245 O   0  0  0  0  0  0  0  0  0  0  0  0
   -1.5097    0.8640   -0.2479 C   0  0  0  0  0  0  0  0  0  0  0  0
   -2.5183    1.5115   -0.4495 O   0  0  0  0  0  0  0  0  0  0  0  0
   -0.1828    1.4336   -0.5742 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.8952    0.7456   -0.4100 N   0  0  0  0  0  0  0  0  0  0  0  0
    2.0042   -1.9792    0.7597 H   0  0  0  0  0  0  0  0  0  0  0  0
    2.8183   -0.5820    0.2181 H   0  0  0  0  0  0  0  0  0  0  0  0
   -0.1783    2.4666   -0.9527 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  1  0
  2  3  2  0
  3  4  1  0
  4  5  2  0
  4  6  1  0
  6  7  2  0
  6  8  1  0
  8  9  2  0
  9  2  1  0
  1 10  1  0
  1 11  1  0
  8 12  1  0
M  END'''))
        assert type(out) is Geometry
        assert out.converged is False
        assert out.energy == approx(-732.7605073)

    def test_C4H5N_ignore_bonding(self, xtb_geometry_task_ignore_bonding):
        out = xtb_geometry_task_ignore_bonding.process(Configuration('''
RDKit          3D

 10 10  0  0  0  0  0  0  0  0999 V2000
    0.3329    1.1477    0.0027 C   0  0  0  0  0  0  0  0  0  0  0  0
    1.1706    0.0549    0.0217 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.3938   -1.0742    0.0106 N   0  0  0  0  0  0  0  0  0  0  0  0
   -0.9286   -0.7150   -0.0152 C   0  0  0  0  0  0  0  0  0  0  0  0
   -0.9958    0.6604   -0.0206 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.6448    2.1841    0.0054 H   0  0  0  0  0  0  0  0  0  0  0  0
    2.2498   -0.0160    0.0421 H   0  0  0  0  0  0  0  0  0  0  0  0
    0.7421   -2.0243    0.0200 H   0  0  0  0  0  0  0  0  0  0  0  0
   -1.7061   -1.4669   -0.0274 H   0  0  0  0  0  0  0  0  0  0  0  0
   -1.9036    1.2494   -0.0393 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  2  0
  2  3  1  0
  3  4  1  0
  4  5  2  0
  5  1  1  0
  1  6  1  0
  2  7  1  0
  3  8  1  0
  4  9  1  0
  5 10  1  0
M  END'''))
        assert type(out) is Geometry
        assert out.converged is True
        assert out.energy == approx(-381.42599)