import pytest

from colibri2.structure import canonicalize, split_smiles, combine_smiles, \
    is_smiles_aromatic, is_mol_aromatic, aromatize_smiles, kekulize_smiles, \
    cleanup_mol, remove_bonds, aromatize_mol, kekulize_mol, enumerate_ez, \
    enumerate_chiral, enumerate_stereo, adj_compress, is_connected, \
    composition, pathwayhash, bond_compress


class TestCanonicalize:

    def test_CO(self):
        assert canonicalize('CO') == 'CO'

    def test_OC(self):
        assert canonicalize('OC') == 'CO'

    def test_COn(self):
        assert canonicalize('C[O-]') == 'C[O-]'

    def test_NH2(self):
        assert canonicalize('[NH2]') == '[NH2]'

    def test_CO_CH2O(self):
        assert canonicalize('CO.C=O') == 'C=O.CO'

    def test_CO_remove_H(self):
        assert canonicalize('[CH3][OH]') == 'CO'

    def test_C6H6_preserve_kekule(self):
        assert canonicalize('C1=CC=CC=C1') == 'C1=CC=CC=C1'

    def test_C6H6_preserve_aromatic(self):
        assert canonicalize('c1ccccc1') == 'c1ccccc1'

    def test_C6H6_mixed_aromatic(self):
        assert canonicalize('C1=CC=CC=C1.c1ccccc1') == 'C1=CC=CC=C1.c1ccccc1'

    def test_CO_remove_index(self):
        assert canonicalize('CO |atomProp:0.p.0:1.p.1|') == 'CO'

    def test_C2H5N_remove_stereo(self):
        assert canonicalize('C/C=N/[H]') == 'CC=N'

    def test_C3H8O2_remove_stereo(self):
        assert canonicalize('C[C@H](CO)O') == 'CC(O)CO'

    def test_C3H6O2_remove_index_stereo(self):
        assert canonicalize('C[C@@H](O)C=O |atomProp:0.p.0:1.p.1:2.p.2:3.p.3:4.p.4|') == \
               'CC(O)C=O'

    def test_CO_preserve_index_01(self):
        assert canonicalize('CO |atomProp:0.p.0:1.p.1|', index=True) == \
               'CO |atomProp:0.p.0:1.p.1|'

    def test_CO_preserve_index_10(self):
        assert canonicalize('CO |atomProp:0.p.1:1.p.0|', index=True) == \
               'CO |atomProp:0.p.1:1.p.0|'

    def test_OC_preserve_index_01(self):
        assert canonicalize('OC |atomProp:0.p.0:1.p.1|', index=True) == \
               'CO |atomProp:0.p.1:1.p.0|'

    def test_OC_preserve_index_10(self):
        assert canonicalize('OC |atomProp:0.p.1:1.p.0|', index=True) == \
               'CO |atomProp:0.p.0:1.p.1|'

    def test_OCCO_preserve_index(self):
        assert canonicalize('OCCO |atomProp:0.p.3:1.p.1:2.p.2:3.p.0|', index=True) == \
               'OCCO |atomProp:0.p.0:1.p.2:2.p.1:3.p.3|'

    def test_CC_preserve_index_01(self):
        assert canonicalize('CC |atomProp:0.p.0:1.p.1|', index=True) == \
               'CC |atomProp:0.p.0:1.p.1|'

    def test_CC_preserve_index_10(self):
        assert canonicalize('CC |atomProp:0.p.1:1.p.0|', index=True) == \
               'CC |atomProp:0.p.0:1.p.1|'

    def test_CO_add_index(self):
        assert canonicalize('CO', index=True) == 'CO |atomProp:0.p.0:1.p.1|'

    def test_CO_CH2O_add_index(self):
        assert canonicalize('CO.C=O', index=True) == \
               'C=O.CO |atomProp:0.p.0:1.p.1:2.p.2:3.p.3|'

    def test_C3H8O2_stereo_unspecified(self):
        assert canonicalize('CC(O)CO', stereo=True) == 'CC(O)CO'

    def test_C2H5N_preserve_stereo(self):
        assert canonicalize('C/C=N/[H]', stereo=True) == '[H]/N=C/C'

    def test_C3H8O2_preserve_stereo(self):
        assert canonicalize('C[C@H](CO)O', stereo=True) == 'C[C@@H](O)CO'

    def test_C4H8_stereo_ez_symmetric(self):
        assert canonicalize('C=C(/C)C', stereo=True) == 'C=C(C)C'

    def test_C3H8O3_stereo_meso(self):
        assert canonicalize('OC[C@@H](O)CO', stereo=True) == 'OCC(O)CO'

    def test_C3H6O2_preserve_index_stereo(self):
        assert canonicalize('C[C@@H](O)C=O |atomProp:0.p.0:1.p.1:2.p.2:3.p.3:4.p.4|',
                            index=True, stereo=True) == \
               'C[C@@H](O)C=O |atomProp:0.p.0:1.p.1:2.p.2:3.p.3:4.p.4|'

    def test_C3H8O2_add_index_preserve_stereo(self):
        assert canonicalize('C[C@@H](O)CO', index=True, stereo=True) == \
               'C[C@@H](O)CO |atomProp:0.p.0:1.p.1:2.p.2:3.p.3:4.p.4|'


class TestSplitSmiles:

    def test_CO(self):
        assert split_smiles('CO') == ['CO']

    def test_CO_CO(self):
        assert split_smiles('CO.CO') == ['CO', 'CO']

    def test_C6H6_C6H6O_preserve_kekule(self):
        assert split_smiles('C1=CC=CC=C1.C1=CC=CC=C1O') == ['C1=CC=CC=C1', 'OC1=CC=CC=C1']

    def test_C6H6_C6H6O_preserve_aromatic(self):
        assert split_smiles('c1ccccc1.c1ccccc1O') == ['c1ccccc1', 'Oc1ccccc1']

    def test_C6H6_C6H6_mixed_aromatic(self):
        assert split_smiles('C1=CC=CC=C1.c1ccccc1O') == ['C1=CC=CC=C1', 'Oc1ccccc1']

    def test_CH3N2O3_preserve_radicals(self):
        assert split_smiles('O=N[O-].[CH3].[N]=O') == ['O=N[O-]', '[CH3]', '[N]=O']

    def test_CO_remove_index(self):
        assert split_smiles('CO |atomProp:0.p.0:1.p.1|') == ['CO']

    def test_CO_CO_remove_index(self):
        assert split_smiles('CO.CO |atomProp:0.p.0:1.p.1:2.p.2:3.p.3|') == \
               ['CO', 'CO']

    def test_C3H8O2_remove_stereo(self):
        assert split_smiles('C[C@@H](O)CO') == ['CC(O)CO']

    def test_C3H8O2_C4H10O_remove_stereo(self):
        assert split_smiles('C[C@H](CO)O.C[C@@H](O)CC') == ['CC(O)CO', 'CCC(C)O']

    def test_CO_CO_add_index(self):
        assert split_smiles('CO.CO', index=True) == \
               ['CO |atomProp:0.p.0:1.p.1|', 'CO |atomProp:0.p.0:1.p.1|']

    def test_C4H10O_C3H8O2_stereo_unspecified(self):
        assert split_smiles('CC(O)CO.CCC(C)O', stereo=True) == ['CC(O)CO', 'CCC(C)O']

    def test_CO_preserve_index(self):
        assert split_smiles('CO |atomProp:0.p.0:1.p.1|', index=True) == \
               ['CO |atomProp:0.p.0:1.p.1|']

    def test_CO_CO_preserve_index(self):
        assert split_smiles('CO.CO |atomProp:0.p.0:1.p.1:2.p.2:3.p.3|', index=True) == \
               ['CO |atomProp:0.p.0:1.p.1|', 'CO |atomProp:0.p.2:1.p.3|']

    def test_C3H8O2_preserve_stereo(self):
        assert split_smiles('C[C@@H](O)CO', stereo=True) == ['C[C@@H](O)CO']

    def test_C3H8O2_C4H10O_preserve_stereo(self):
        assert split_smiles('C[C@H](CO)O.C[C@@H](O)CC', stereo=True) == \
               ['C[C@@H](O)CO', 'CC[C@@H](C)O']

    def test_C3H8O2_preserve_index_stereo(self):
        assert split_smiles('C[C@@H](O)C=O.O |atomProp:0.p.0:1.p.1:2.p.2:3.p.3:4.p.4:5.p.5|',
                            index=True, stereo=True) == \
               ['C[C@@H](O)C=O |atomProp:0.p.0:1.p.1:2.p.2:3.p.3:4.p.4|', 'O |atomProp:0.p.5|']

    def test_empty(self):
        with pytest.raises(ValueError, match="empty input SMILES string"):
            split_smiles('', index=False)


class TestCombineSmiles:

    def test_CO(self):
        assert combine_smiles(['CO']) == 'CO'

    def test_CO_CO(self):
        assert combine_smiles(['CO', 'CO']) == 'CO.CO'

    def test_C6H6_C6H6O_preserve_kekule(self):
        assert combine_smiles(['C1=CC=CC=C1', 'OC1=CC=CC=C1']) == 'C1=CC=CC=C1.OC1=CC=CC=C1'

    def test_C6H6_C6H6O_preserve_aromatic(self):
        assert combine_smiles(['c1ccccc1', 'Oc1ccccc1']) == 'Oc1ccccc1.c1ccccc1'

    def test_C6H6_C6H6_mixed_aromatic(self):
        assert combine_smiles(['c1ccccc1', 'OC1=CC=CC=C1']) == 'OC1=CC=CC=C1.c1ccccc1'

    def test_CH3N2O3_preserve_radicals(self):
        assert combine_smiles(['O=N[O-]', '[CH3]', '[N]=O']) == 'O=N[O-].[CH3].[N]=O'

    def test_CO_remove_index(self):
        assert combine_smiles(['CO |atomProp:0.p.0:1.p.1|']) == 'CO'

    def test_C3H8O2_remove_stereo(self):
        assert combine_smiles(['C[C@@H](O)CO']) == 'CC(O)CO'

    def test_C3H8O2_C4H10O_remove_stereo(self):
        assert combine_smiles(['C[C@H](CO)O', 'C[C@@H](O)CC']) == 'CC(O)CO.CCC(C)O'

    def test_CO_add_index(self):
        assert combine_smiles(['CO'], index=True) == 'CO |atomProp:0.p.0:1.p.1|'

    def test_C4H10O_C3H8O2_stereo_unspecified(self):
        assert combine_smiles(['CC(O)CO.CCC(C)O'], stereo=True) == 'CC(O)CO.CCC(C)O'

    def test_CO_preserve_index(self):
        assert combine_smiles(['CO |atomProp:0.p.0:1.p.1|'], index=True) == 'CO |atomProp:0.p.0:1.p.1|'

    def test_CO_CO_preserve_index(self):
        assert combine_smiles(['CO |atomProp:0.p.0:1.p.1|', 'CO |atomProp:0.p.2:1.p.3|'], index=True) == \
               'CO.CO |atomProp:0.p.0:1.p.1:2.p.2:3.p.3|'

    def test_C3H8O2_preserve_stereo(self):
        assert combine_smiles(['C[C@@H](O)CO'], stereo=True) == 'C[C@@H](O)CO'

    def test_C3H8O2_C4H10O_preserve_stereo(self):
        assert combine_smiles(['C[C@H](CO)O', 'C[C@@H](O)CC'], stereo=True) == 'CC[C@@H](C)O.C[C@@H](O)CO'

    def test_C3H8O2_preserve_index_stereo(self):
        assert combine_smiles(['C[C@@H](O)C=O.O |atomProp:0.p.0:1.p.1:2.p.2:3.p.3:4.p.4:5.p.5|'],
                              index=True, stereo=True) == \
               'C[C@@H](O)C=O.O |atomProp:0.p.0:1.p.1:2.p.2:3.p.3:4.p.4:5.p.5|'

    def test_empty(self):
        with pytest.raises(ValueError, match="empty input list"):
            combine_smiles([], index=False)


class TestIsSmilesAromatic:

    def test_C(self):
        assert is_smiles_aromatic('C') is False

    def test_C_index(self):
        assert is_smiles_aromatic('C |atomProp:0.p.0|') is False

    def test_C6H6_kekule(self):
        assert is_smiles_aromatic('C1=CC=CC=C1') is False

    def test_C6H6_aromatic(self):
        assert is_smiles_aromatic('c1ccccc1') is True

    def test_C_C(self):
        assert is_smiles_aromatic('C.C') is False

    def test_C6H6_C6H6O_kekule(self):
        assert is_smiles_aromatic('C1=CC=CC=C1.C1=CC=CC=C1O') is False

    def test_C6H6_C6H6O_aromatic(self):
        assert is_smiles_aromatic('c1ccccc1.c1ccccc1O') is True

    def test_C6H6_C6H6_mixed(self):
        assert is_smiles_aromatic('C1=CC=CC=C1.c1ccccc1O') is True


class TestIsMolAromatic:

    def test_C4H5N3O_aromatic(self):
        assert is_mol_aromatic('''energy: -23.963618020933 gnorm: 0.000940243169 xtb: 6.5.1 (b24c23e)
          06202311103D

 13 13  0     0  0            999 V2000
    2.2036   -0.5524    0.1881 N   0  0  0  0  0  0  0  0  0  0  0  0
    0.8856   -0.2957    0.0977 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.0675   -1.2565    0.4491 N   0  0  0  0  0  0  0  0  0  0  0  0
   -1.2685   -1.0952    0.4115 C   0  0  0  0  0  0  0  0  0  0  0  0
   -2.0889   -1.9263    0.7153 O   0  0  0  0  0  0  0  0  0  0  0  0
   -1.7346    0.1800   -0.0322 N   0  0  0  0  0  0  0  0  0  0  0  0
   -0.9069    1.1728   -0.4002 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.4344    0.9860   -0.3539 C   0  0  0  0  0  0  0  0  0  0  0  0
    2.8898    0.1094   -0.1243 H   0  0  0  0  0  0  0  0  0  0  0  0
    2.4833   -1.4741    0.4739 H   0  0  0  0  0  0  0  0  0  0  0  0
   -2.7384    0.2943   -0.0595 H   0  0  0  0  0  0  0  0  0  0  0  0
   -1.3560    2.1015   -0.7259 H   0  0  0  0  0  0  0  0  0  0  0  0
    1.1291    1.7562   -0.6395 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  4  0  0  0  0
  2  3  4  0  0  0  0
  3  4  4  0  0  0  0
  4  5  2  0  0  0  0
  4  6  1  0  0  0  0
  6  7  4  0  0  0  0
  2  8  1  0  0  0  0
  7  8  2  0  0  0  0
  1  9  1  0  0  0  0
  1 10  1  0  0  0  0
  6 11  1  0  0  0  0
  7 12  1  0  0  0  0
  8 13  1  0  0  0  0
M  END''') is True

    def test_C4H5N3O_kekule(self):
        assert is_mol_aromatic('''xtb: 6.6.0 (conda-forge)
     RDKit          3D

 13 13  0  0  0  0  0  0  0  0999 V2000
    2.0410    0.0099    0.2657 N   0  0  0  0  0  0  0  0  0  0  0  0
    0.7002    0.0493    0.0776 C   0  0  0  0  0  0  0  0  0  0  0  0
   -0.0008    1.2161   -0.1831 C   0  0  0  0  0  0  0  0  0  0  0  0
   -1.3860    1.1034   -0.3120 C   0  0  0  0  0  0  0  0  0  0  0  0
   -2.0644   -0.0050   -0.2075 N   0  0  0  0  0  0  0  0  0  0  0  0
   -1.4359   -1.1672    0.0459 C   0  0  0  0  0  0  0  0  0  0  0  0
   -1.9436   -2.2532    0.1662 O   0  0  0  0  0  0  0  0  0  0  0  0
   -0.0046   -1.0891    0.1816 N   0  0  0  0  0  0  0  0  0  0  0  0
    2.5201   -0.8721    0.3182 H   0  0  0  0  0  0  0  0  0  0  0  0
    2.5853    0.8289    0.0625 H   0  0  0  0  0  0  0  0  0  0  0  0
    0.5077    2.1590   -0.2771 H   0  0  0  0  0  0  0  0  0  0  0  0
   -1.9769    1.9878   -0.5175 H   0  0  0  0  0  0  0  0  0  0  0  0
    0.4582   -1.9678    0.3798 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  1  0
  2  3  2  0
  3  4  1  0
  4  5  2  0
  5  6  1  0
  6  7  2  0
  2  8  1  0
  6  8  1  0
  1  9  1  0
  1 10  1  0
  3 11  1  0
  4 12  1  0
  8 13  1  0
M  END''') is False


class TestAromatizeSmiles:
    def test_CC(self):
        assert aromatize_smiles('CC') == 'CC'

    def test_C6H6O_kekule(self):
        assert aromatize_smiles('C1=CC=CC=C1O') == 'Oc1ccccc1'

    def test_C6H6O_aromatic(self):
        assert aromatize_smiles('c1ccccc1O') == 'Oc1ccccc1'

    def test_C6H6_C6H6O_kekule(self):
        assert aromatize_smiles('C1=CC=CC=C1.C1=CC=CC=C1O') == 'Oc1ccccc1.c1ccccc1'

    def test_C6H6_C6H6O_aromatic(self):
        assert aromatize_smiles('c1ccccc1.c1ccccc1O') == 'Oc1ccccc1.c1ccccc1'

    def test_C6H6_C6H6_mixed(self):
        assert aromatize_smiles('C1=CC=CC=C1.c1ccccc1O') == 'Oc1ccccc1.c1ccccc1'

    def test_C6H6O_remove_index(self):
        assert aromatize_smiles('OC1=CC=CC=C1 |atomProp:0.p.0:1.p.1:2.p.2:3.p.3:4.p.4:5.p.5:6.p.6|') == \
               'Oc1ccccc1'

    def test_C6H6O_preserve_index(self):
        assert aromatize_smiles('OC1=CC=CC=C1 |atomProp:0.p.0:1.p.1:2.p.2:3.p.3:4.p.4:5.p.5:6.p.6|',
                                index=True) == \
               'Oc1ccccc1 |atomProp:0.p.0:1.p.1:2.p.2:3.p.3:4.p.4:5.p.5:6.p.6|'

    def test_C6H6O_add_index(self):
        assert aromatize_smiles('OC1=CC=CC=C1', index=True) == \
               'Oc1ccccc1 |atomProp:0.p.0:1.p.1:2.p.2:3.p.3:4.p.4:5.p.5:6.p.6|'

    def test_C8H10O_remove_stereo(self):
        assert aromatize_smiles('C[C@@H](O)C1=CC=CC=C1') == 'CC(O)c1ccccc1'

    def test_C8H10O_preserve_stereo(self):
        assert aromatize_smiles('C[C@@H](O)C1=CC=CC=C1', stereo=True) == 'C[C@@H](O)c1ccccc1'

    def test_C8H10O_stereo_unspecified(self):
        assert aromatize_smiles('CC(O)C1=CC=CC=C1', stereo=True) == 'CC(O)c1ccccc1'


class TestKekulizeSmiles:
    def test_CC(self):
        assert kekulize_smiles('CC') == 'CC'

    def test_C6H6O_kekule(self):
        assert kekulize_smiles('C1=CC=CC=C1O') == 'OC1=CC=CC=C1'

    def test_C6H6O_aromatic(self):
        assert kekulize_smiles('c1ccccc1O') == 'OC1=CC=CC=C1'

    def test_C6H6_C6H6O_kekule(self):
        assert kekulize_smiles('C1=CC=CC=C1.C1=CC=CC=C1O') == 'C1=CC=CC=C1.OC1=CC=CC=C1'

    def test_C6H6_C6H6O_aromatic(self):
        assert kekulize_smiles('c1ccccc1.c1ccccc1O') == 'C1=CC=CC=C1.OC1=CC=CC=C1'

    def test_C6H6_C6H6_mixed(self):
        assert kekulize_smiles('C1=CC=CC=C1.c1ccccc1O') == 'C1=CC=CC=C1.OC1=CC=CC=C1'

    def test_C6H6O_remove_index(self):
        assert kekulize_smiles('Oc1ccccc1 |atomProp:0.p.0:1.p.1:2.p.2:3.p.3:4.p.4:5.p.5:6.p.6|') == \
               'OC1=CC=CC=C1'

    def test_C6H6O_preserve_index(self):
        assert kekulize_smiles('Oc1ccccc1 |atomProp:0.p.0:1.p.1:2.p.2:3.p.3:4.p.4:5.p.5:6.p.6|',
                               index=True) == \
               'OC1=CC=CC=C1 |atomProp:0.p.0:1.p.1:2.p.2:3.p.3:4.p.4:5.p.5:6.p.6|'

    def test_C6H6O_add_index(self):
        assert kekulize_smiles('Oc1ccccc1', index=True) == \
               'OC1=CC=CC=C1 |atomProp:0.p.0:1.p.1:2.p.2:3.p.3:4.p.4:5.p.5:6.p.6|'

    def test_C8H10O_remove_stereo(self):
        assert kekulize_smiles('C[C@@H](O)c1ccccc1') == 'CC(O)C1=CC=CC=C1'

    def test_C8H10O_preserve_stereo(self):
        assert kekulize_smiles('C[C@@H](O)c1ccccc1', stereo=True) == 'C[C@@H](O)C1=CC=CC=C1'

    def test_C8H10O_stereo_unspecified(self):
        assert kekulize_smiles('CC(O)c1ccccc1', stereo=True) == 'CC(O)C1=CC=CC=C1'


class TestCleanupMol:
    def test_C4H3N3O2(self):
        assert cleanup_mol('''xtb: 6.6.0 (conda-forge)
          06202315443D

 12 12  0     0  0            999 V2000
    1.9889   -1.0764    0.3988 N   0  0  0  0  0  0  0  0  0  0  0  0
    0.8436   -0.3848    0.1372 C   0  0  0  0  0  0  0  0  0  0  0  0
   -0.2935   -1.1039    0.4767 N   0  0  0  0  0  0  0  0  0  0  0  0
   -1.4623   -1.0483    0.7206 C   0  0  0  0  0  0  0  0  0  0  0  0
   -2.5728   -1.1969    0.9980 O   0  0  0  0  0  0  0  0  0  0  0  0
   -1.4588    1.3421   -0.6713 C   0  0  0  0  0  0  0  0  0  0  0  0
   -2.6089    1.2067   -0.8096 O   0  0  0  0  0  0  0  0  0  0  0  0
   -0.1816    1.6067   -0.5940 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.9017    0.7968   -0.3235 N   0  0  0  0  0  0  0  0  0  0  0  0
    1.9186   -2.0762    0.4791 H   0  0  0  0  0  0  0  0  0  0  0  0
    2.8295   -0.6976   -0.0028 H   0  0  0  0  0  0  0  0  0  0  0  0
    0.0957    2.6320   -0.8090 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  1  0  0  0  0
  2  3  1  0  0  0  0
  3  4  2  0  0  0  0
  3  5  1  0  0  0  0
  4  5  2  0  0  0  0
  6  7  2  0  0  0  0
  6  8  2  0  0  0  0
  2  9  2  0  0  0  0
  8  9  1  0  0  0  0
  1 10  1  0  0  0  0
  1 11  1  0  0  0  0
  8 12  1  0  0  0  0
M  END''') == '''xtb: 6.6.0 (conda-forge)
     RDKit          3D

 12 11  0  0  0  0  0  0  0  0999 V2000
    1.9889   -1.0764    0.3988 N   0  0  0  0  0  0  0  0  0  0  0  0
    0.8436   -0.3848    0.1372 C   0  0  0  0  0  0  0  0  0  0  0  0
   -0.2935   -1.1039    0.4767 N   0  0  0  0  0  4  0  0  0  0  0  0
   -1.4623   -1.0483    0.7206 C   0  0  0  0  0  0  0  0  0  0  0  0
   -2.5728   -1.1969    0.9980 O   0  0  0  0  0  1  0  0  0  0  0  0
   -1.4588    1.3421   -0.6713 C   0  0  0  0  0  0  0  0  0  0  0  0
   -2.6089    1.2067   -0.8096 O   0  0  0  0  0  0  0  0  0  0  0  0
   -0.1816    1.6067   -0.5940 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.9017    0.7968   -0.3235 N   0  0  0  0  0  0  0  0  0  0  0  0
    1.9186   -2.0762    0.4791 H   0  0  0  0  0  0  0  0  0  0  0  0
    2.8295   -0.6976   -0.0028 H   0  0  0  0  0  0  0  0  0  0  0  0
    0.0957    2.6320   -0.8090 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  1  0
  1 10  1  0
  1 11  1  0
  2  3  1  0
  2  9  2  0
  3  4  3  0
  4  5  1  0
  6  7  2  0
  6  8  2  0
  8  9  1  0
  8 12  1  0
M  CHG  2   3   1   5  -1
M  END
'''

    def test_C7H8N4O(self):
        assert cleanup_mol('''xtb: 6.6.0 (conda-forge)
          07062316053D

 20 21  0     0  0            999 V2000
   -1.9355   -1.9324    0.1878 N   0  0  0  0  0  0  0  0  0  0  0  0
   -1.0225   -0.9255    0.0914 C   0  0  0  0  0  0  0  0  0  0  0  0
   -1.2806    0.4177   -0.1864 C   0  0  0  0  0  0  0  0  0  0  0  0
   -2.5617    1.0196   -0.3454 N   0  0  0  0  0  0  0  0  0  0  0  0
   -0.0591    1.1373   -0.2187 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.0805    2.4569   -0.4671 N   0  0  0  0  0  0  0  0  0  0  0  0
    0.9853    0.2098    0.0080 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.4022   -1.0763    0.2067 C   0  0  0  0  0  0  0  0  0  0  0  0
    1.4548   -1.9357    0.4658 C   0  0  0  0  0  0  0  0  0  0  0  0
    2.6388   -1.2785    0.4395 N   0  0  0  0  0  0  0  0  0  0  0  0
    2.3613    0.0083    0.1686 C   0  0  0  0  0  0  0  0  0  0  0  0
    3.3370    0.9252    0.0872 O   0  0  0  0  0  0  0  0  0  0  0  0
   -1.6102   -2.7933    0.5950 H   0  0  0  0  0  0  0  0  0  0  0  0
   -2.8895   -1.6850    0.3952 H   0  0  0  0  0  0  0  0  0  0  0  0
   -2.5364    1.7360   -1.0633 H   0  0  0  0  0  0  0  0  0  0  0  0
   -3.2522    0.3283   -0.6136 H   0  0  0  0  0  0  0  0  0  0  0  0
    0.9909    2.8587   -0.3162 H   0  0  0  0  0  0  0  0  0  0  0  0
   -0.7180    3.0590   -0.3542 H   0  0  0  0  0  0  0  0  0  0  0  0
    1.4448   -2.9915    0.6727 H   0  0  0  0  0  0  0  0  0  0  0  0
    4.1702    0.4612    0.2470 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  4  0  0  0  0
  2  3  4  0  0  0  0
  3  4  1  0  0  0  0
  3  5  4  0  0  0  0
  5  6  4  0  0  0  0
  5  7  1  0  0  0  0
  2  8  1  0  0  0  0
  7  8  1  0  0  0  0
  8  9  4  0  0  0  0
  9 10  4  0  0  0  0
  7 11  4  0  0  0  0
 10 11  4  0  0  0  0
 11 12  1  0  0  0  0
  1 13  1  0  0  0  0
  1 14  1  0  0  0  0
  4 15  1  0  0  0  0
  4 16  1  0  0  0  0
  6 17  1  0  0  0  0
  6 18  1  0  0  0  0
  9 19  1  0  0  0  0
 12 20  1  0  0  0  0
M  END''') == '''xtb: 6.6.0 (conda-forge)
     RDKit          3D

 20 21  0  0  0  0  0  0  0  0999 V2000
   -1.9355   -1.9324    0.1878 N   0  0  0  0  0  0  0  0  0  0  0  0
   -1.0225   -0.9255    0.0914 C   0  0  0  0  0  0  0  0  0  0  0  0
   -1.2806    0.4177   -0.1864 C   0  0  0  0  0  0  0  0  0  0  0  0
   -2.5617    1.0196   -0.3454 N   0  0  0  0  0  0  0  0  0  0  0  0
   -0.0591    1.1373   -0.2187 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.0805    2.4569   -0.4671 N   0  0  0  0  0  0  0  0  0  0  0  0
    0.9853    0.2098    0.0080 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.4022   -1.0763    0.2067 C   0  0  0  0  0  0  0  0  0  0  0  0
    1.4548   -1.9357    0.4658 C   0  0  0  0  0  0  0  0  0  0  0  0
    2.6388   -1.2785    0.4395 N   0  0  0  0  0  0  0  0  0  0  0  0
    2.3613    0.0083    0.1686 C   0  0  0  0  0  0  0  0  0  0  0  0
    3.3370    0.9252    0.0872 O   0  0  0  0  0  0  0  0  0  0  0  0
   -1.6102   -2.7933    0.5950 H   0  0  0  0  0  0  0  0  0  0  0  0
   -2.8895   -1.6850    0.3952 H   0  0  0  0  0  0  0  0  0  0  0  0
   -2.5364    1.7360   -1.0633 H   0  0  0  0  0  0  0  0  0  0  0  0
   -3.2522    0.3283   -0.6136 H   0  0  0  0  0  0  0  0  0  0  0  0
    0.9909    2.8587   -0.3162 H   0  0  0  0  0  0  0  0  0  0  0  0
   -0.7180    3.0590   -0.3542 H   0  0  0  0  0  0  0  0  0  0  0  0
    1.4448   -2.9915    0.6727 H   0  0  0  0  0  0  0  0  0  0  0  0
    4.1702    0.4612    0.2470 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  1  0
  1 13  1  0
  1 14  1  0
  2  3  2  0
  2  8  1  0
  3  4  1  0
  3  5  1  0
  4 15  1  0
  4 16  1  0
  5  6  1  0
  5  7  2  0
  6 17  1  0
  6 18  1  0
  7  8  1  0
  7 11  1  0
  8  9  2  0
  9 10  1  0
  9 19  1  0
 10 11  2  0
 11 12  1  0
 12 20  1  0
M  END
'''

    def test_C4HN3O2(self):
        assert cleanup_mol('''xtb: 6.6.0 (conda-forge)
          07062322143D

 10 11  0     0  0            999 V2000
    3.6453    0.0787    0.2391 O   0  0  0  0  0  0  0  0  0  0  0  0
    2.5077   -0.0210    0.3503 C   0  0  0  0  0  0  0  0  0  0  0  0
    1.3687   -0.2769    0.6493 N   0  0  0  0  0  0  0  0  0  0  0  0
    0.1075   -0.0279    0.3399 C   0  0  0  0  0  0  0  0  0  0  0  0
   -0.9472   -0.5011    0.9016 N   0  0  0  0  0  0  0  0  0  0  0  0
   -2.0038    0.0804    0.1935 C   0  0  0  0  0  0  0  0  0  0  0  0
   -1.7210    0.8814   -0.7690 N   0  0  0  0  0  0  0  0  0  0  0  0
   -0.3343    0.9390   -0.8269 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.3910    1.5463   -1.5527 O   0  0  0  0  0  0  0  0  0  0  0  0
   -3.0141   -0.1598    0.4749 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  2  0  0  0  0
  1  3  1  0  0  0  0
  2  3  2  0  0  0  0
  3  4  4  0  0  0  0
  4  5  2  0  0  0  0
  5  6  1  0  0  0  0
  6  7  2  0  0  0  0
  4  8  1  0  0  0  0
  7  8  1  0  0  0  0
  8  9  2  0  0  0  0
  6 10  1  0  0  0  0
M  END''') == '''xtb: 6.6.0 (conda-forge)
     RDKit          3D

 10 10  0  0  0  0  0  0  0  0999 V2000
    3.6453    0.0787    0.2391 O   0  0  0  0  0  0  0  0  0  0  0  0
    2.5077   -0.0210    0.3503 C   0  0  0  0  0  0  0  0  0  0  0  0
    1.3687   -0.2769    0.6493 N   0  0  0  0  0  0  0  0  0  0  0  0
    0.1075   -0.0279    0.3399 C   0  0  0  0  0  0  0  0  0  0  0  0
   -0.9472   -0.5011    0.9016 N   0  0  0  0  0  0  0  0  0  0  0  0
   -2.0038    0.0804    0.1935 C   0  0  0  0  0  0  0  0  0  0  0  0
   -1.7210    0.8814   -0.7690 N   0  0  0  0  0  0  0  0  0  0  0  0
   -0.3343    0.9390   -0.8269 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.3910    1.5463   -1.5527 O   0  0  0  0  0  0  0  0  0  0  0  0
   -3.0141   -0.1598    0.4749 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  2  0
  2  3  2  0
  3  4  1  0
  4  5  2  0
  4  8  1  0
  5  6  1  0
  6  7  2  0
  6 10  1  0
  7  8  1  0
  8  9  2  0
M  END
'''

    def test_C7H9N5O2(self):
        assert cleanup_mol('''xtb: 6.6.0 (conda-forge)
          07072322113D

 23 24  0     0  0            999 V2000
    3.1442   -1.3390   -0.2617 N   0  0  0  0  0  0  0  0  0  0  0  0
    1.8886   -0.7671   -0.1720 C   0  0  0  0  0  0  0  0  0  0  0  0
    1.7676    0.6110   -0.3582 C   0  0  0  0  0  0  0  0  0  0  0  0
    2.8910    1.3805   -0.7478 N   0  0  0  0  0  0  0  0  0  0  0  0
    0.5251    1.2710   -0.2380 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.3098    2.4917   -0.3941 O   0  0  0  0  0  0  0  0  0  0  0  0
   -0.5390    0.4323    0.0835 N   0  0  0  0  0  0  0  0  0  0  0  0
   -0.4372   -0.9105    0.2997 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.7730   -1.5490    0.1726 C   0  0  0  0  0  0  0  0  0  0  0  0
   -1.6914   -1.3585    0.5951 N   0  0  0  0  0  0  0  0  0  0  0  0
   -2.5739   -0.2771    0.5984 C   0  0  0  0  0  0  0  0  0  0  0  0
   -3.9049   -0.4316    0.9843 N   0  0  0  0  0  0  0  0  0  0  0  0
   -1.8606    0.8297    0.2648 C   0  0  0  0  0  0  0  0  0  0  0  0
   -2.2329    2.1105    0.1152 O   0  0  0  0  0  0  0  0  0  0  0  0
    3.7762   -0.8281   -0.8604 H   0  0  0  0  0  0  0  0  0  0  0  0
    3.1544   -2.3367   -0.4072 H   0  0  0  0  0  0  0  0  0  0  0  0
    2.5912    2.3435   -0.8664 H   0  0  0  0  0  0  0  0  0  0  0  0
    3.6434    1.3403   -0.0688 H   0  0  0  0  0  0  0  0  0  0  0  0
    0.8655   -2.6090    0.3329 H   0  0  0  0  0  0  0  0  0  0  0  0
   -1.9224   -2.2825    0.9102 H   0  0  0  0  0  0  0  0  0  0  0  0
   -4.3901   -1.1590    0.4714 H   0  0  0  0  0  0  0  0  0  0  0  0
   -4.3948    0.4497    0.9027 H   0  0  0  0  0  0  0  0  0  0  0  0
   -1.3827    2.5881   -0.1082 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  1  0  0  0  0
  2  3  4  0  0  0  0
  3  4  1  0  0  0  0
  3  5  4  0  0  0  0
  5  6  4  0  0  0  0
  5  7  1  0  0  0  0
  7  8  1  0  0  0  0
  2  9  4  0  0  0  0
  8  9  4  0  0  0  0
  8 10  1  0  0  0  0
 10 11  1  0  0  0  0
 11 12  1  0  0  0  0
  7 13  1  0  0  0  0
 11 13  2  0  0  0  0
 13 14  1  0  0  0  0
  1 15  1  0  0  0  0
  1 16  1  0  0  0  0
  4 17  1  0  0  0  0
  4 18  1  0  0  0  0
  9 19  1  0  0  0  0
 10 20  1  0  0  0  0
 12 21  1  0  0  0  0
 12 22  1  0  0  0  0
 14 23  1  0  0  0  0
M  END''') == '''xtb: 6.6.0 (conda-forge)
     RDKit          3D

 23 24  0  0  0  0  0  0  0  0999 V2000
    3.1442   -1.3390   -0.2617 N   0  0  0  0  0  0  0  0  0  0  0  0
    1.8886   -0.7671   -0.1720 C   0  0  0  0  0  0  0  0  0  0  0  0
    1.7676    0.6110   -0.3582 C   0  0  0  0  0  0  0  0  0  0  0  0
    2.8910    1.3805   -0.7478 N   0  0  0  0  0  0  0  0  0  0  0  0
    0.5251    1.2710   -0.2380 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.3098    2.4917   -0.3941 O   0  0  0  0  0  0  0  0  0  0  0  0
   -0.5390    0.4323    0.0835 N   0  0  0  0  0  0  0  0  0  0  0  0
   -0.4372   -0.9105    0.2997 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.7730   -1.5490    0.1726 C   0  0  0  0  0  0  0  0  0  0  0  0
   -1.6914   -1.3585    0.5951 N   0  0  0  0  0  0  0  0  0  0  0  0
   -2.5739   -0.2771    0.5984 C   0  0  0  0  0  0  0  0  0  0  0  0
   -3.9049   -0.4316    0.9843 N   0  0  0  0  0  0  0  0  0  0  0  0
   -1.8606    0.8297    0.2648 C   0  0  0  0  0  0  0  0  0  0  0  0
   -2.2329    2.1105    0.1152 O   0  0  0  0  0  0  0  0  0  0  0  0
    3.7762   -0.8281   -0.8604 H   0  0  0  0  0  0  0  0  0  0  0  0
    3.1544   -2.3367   -0.4072 H   0  0  0  0  0  0  0  0  0  0  0  0
    2.5912    2.3435   -0.8664 H   0  0  0  0  0  0  0  0  0  0  0  0
    3.6434    1.3403   -0.0688 H   0  0  0  0  0  0  0  0  0  0  0  0
    0.8655   -2.6090    0.3329 H   0  0  0  0  0  0  0  0  0  0  0  0
   -1.9224   -2.2825    0.9102 H   0  0  0  0  0  0  0  0  0  0  0  0
   -4.3901   -1.1590    0.4714 H   0  0  0  0  0  0  0  0  0  0  0  0
   -4.3948    0.4497    0.9027 H   0  0  0  0  0  0  0  0  0  0  0  0
   -1.3827    2.5881   -0.1082 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  1  0
  1 15  1  0
  1 16  1  0
  2  3  2  0
  2  9  1  0
  3  4  1  0
  3  5  1  0
  4 17  1  0
  4 18  1  0
  5  6  2  0
  5  7  1  0
  7  8  1  0
  7 13  1  0
  8  9  2  0
  8 10  1  0
  9 19  1  0
 10 11  1  0
 10 20  1  0
 11 12  1  0
 11 13  2  0
 12 21  1  0
 12 22  1  0
 13 14  1  0
 14 23  1  0
M  END
'''

    def test_C3H3NO4(self):
        assert cleanup_mol('''xtb: 6.6.0 (conda-forge)
          07112310163D

 11 10  0     0  0            999 V2000
    2.2082    0.5399   -0.0624 N   0  0  0  0  0  0  0  0  0  0  0  0
    1.2995   -0.3971    0.2517 C   0  0  0  0  0  0  0  0  0  0  0  0
    1.4976   -1.4783    0.7020 O   0  0  0  0  0  0  0  0  0  0  0  0
    0.0309    0.1341   -0.0521 O   0  0  0  0  0  0  0  0  0  0  0  0
   -1.1160   -0.5368    0.1378 C   0  0  0  0  0  0  0  0  0  0  0  0
   -1.2585   -1.6384    0.5727 O   0  0  0  0  0  0  0  0  0  0  0  0
   -2.3008    0.3402   -0.2986 C   0  0  0  0  0  0  0  0  0  0  0  0
   -2.1988    1.4454   -0.7382 O   0  0  0  0  0  0  0  0  0  0  0  0
    1.9208    1.4256   -0.4400 H   0  0  0  0  0  0  0  0  0  0  0  0
    3.1825    0.3437    0.0855 H   0  0  0  0  0  0  0  0  0  0  0  0
   -3.2654   -0.1784   -0.1585 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  4  0  0  0  0
  2  3  2  0  0  0  0
  2  4  1  0  0  0  0
  4  5  1  0  0  0  0
  5  6  2  0  0  0  0
  5  7  1  0  0  0  0
  7  8  2  0  0  0  0
  1  9  1  0  0  0  0
  1 10  1  0  0  0  0
  7 11  1  0  0  0  0
M  END
''') == '''xtb: 6.6.0 (conda-forge)
     RDKit          3D

 11 10  0  0  0  0  0  0  0  0999 V2000
    2.2082    0.5399   -0.0624 N   0  0  0  0  0  0  0  0  0  0  0  0
    1.2995   -0.3971    0.2517 C   0  0  0  0  0  0  0  0  0  0  0  0
    1.4976   -1.4783    0.7020 O   0  0  0  0  0  0  0  0  0  0  0  0
    0.0309    0.1341   -0.0521 O   0  0  0  0  0  0  0  0  0  0  0  0
   -1.1160   -0.5368    0.1378 C   0  0  0  0  0  0  0  0  0  0  0  0
   -1.2585   -1.6384    0.5727 O   0  0  0  0  0  0  0  0  0  0  0  0
   -2.3008    0.3402   -0.2986 C   0  0  0  0  0  0  0  0  0  0  0  0
   -2.1988    1.4454   -0.7382 O   0  0  0  0  0  0  0  0  0  0  0  0
    1.9208    1.4256   -0.4400 H   0  0  0  0  0  0  0  0  0  0  0  0
    3.1825    0.3437    0.0855 H   0  0  0  0  0  0  0  0  0  0  0  0
   -3.2654   -0.1784   -0.1585 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  1  0
  1  9  1  0
  1 10  1  0
  2  3  2  0
  2  4  1  0
  4  5  1  0
  5  6  2  0
  5  7  1  0
  7  8  2  0
  7 11  1  0
M  END
'''

    def test_C4H2N4O2_unchanged(self):
        assert cleanup_mol('''
  ChemDraw07062322503D

 12 13  0  0  0  0  0  0  0  0999 V2000
   -3.4103    0.1019    0.5709 N   0  0  0  0  0  0  0  0  0  0  0  0
   -1.8744    0.0881    0.3214 C   0  0  0  0  0  0  0  0  0  0  0  0
   -1.1509    1.2091   -0.4034 N   0  0  0  0  0  0  0  0  0  0  0  0
    0.3219    0.7013   -0.3691 C   0  0  0  0  0  0  0  0  0  0  0  0
    1.6437    1.1534   -0.8281 N   0  0  0  0  0  0  0  0  0  0  0  0
    2.6153   -0.0354   -0.3535 C   0  0  0  0  0  0  0  0  0  0  0  0
    4.0083   -0.0800   -0.5582 O   0  0  0  0  0  0  0  0  0  0  0  0
    1.8206   -1.2091    0.3976 O   0  0  0  0  0  0  0  0  0  0  0  0
    0.3657   -0.7589    0.3986 C   0  0  0  0  0  0  0  0  0  0  0  0
   -0.9876   -1.1681    0.8281 N   0  0  0  0  0  0  0  0  0  0  0  0
   -3.8925   -0.9518    0.7218 H   0  0  0  0  0  0  0  0  0  0  0  0
   -4.0083    0.8198   -0.1321 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  1  0        0
  2  3  2  0        0
  3  4  1  0        0
  4  5  2  0        0
  5  6  1  0        0
  6  7  2  0        0
  6  8  1  0        0
  8  9  1  0        0
  9 10  2  0        0
 10  2  1  0        0
  9  4  1  0        0
  1 11  1  0        0
  1 12  1  0        0
M  END
''') == '''
  ChemDraw07062322503D

 12 13  0  0  0  0  0  0  0  0999 V2000
   -3.4103    0.1019    0.5709 N   0  0  0  0  0  0  0  0  0  0  0  0
   -1.8744    0.0881    0.3214 C   0  0  0  0  0  0  0  0  0  0  0  0
   -1.1509    1.2091   -0.4034 N   0  0  0  0  0  0  0  0  0  0  0  0
    0.3219    0.7013   -0.3691 C   0  0  0  0  0  0  0  0  0  0  0  0
    1.6437    1.1534   -0.8281 N   0  0  0  0  0  0  0  0  0  0  0  0
    2.6153   -0.0354   -0.3535 C   0  0  0  0  0  0  0  0  0  0  0  0
    4.0083   -0.0800   -0.5582 O   0  0  0  0  0  0  0  0  0  0  0  0
    1.8206   -1.2091    0.3976 O   0  0  0  0  0  0  0  0  0  0  0  0
    0.3657   -0.7589    0.3986 C   0  0  0  0  0  0  0  0  0  0  0  0
   -0.9876   -1.1681    0.8281 N   0  0  0  0  0  0  0  0  0  0  0  0
   -3.8925   -0.9518    0.7218 H   0  0  0  0  0  0  0  0  0  0  0  0
   -4.0083    0.8198   -0.1321 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  1  0        0
  2  3  2  0        0
  3  4  1  0        0
  4  5  2  0        0
  5  6  1  0        0
  6  7  2  0        0
  6  8  1  0        0
  8  9  1  0        0
  9 10  2  0        0
 10  2  1  0        0
  9  4  1  0        0
  1 11  1  0        0
  1 12  1  0        0
M  END
'''


class TestRemoveBonds:
    def test_C4H5Np(self):
        assert remove_bonds('''xtb: 6.6.0 (conda-forge)
          09072322103D

 10 10  0     0  0            999 V2000
    0.3053    1.1612    0.0022 C   0  0  0  0  0  0  0  0  0  0  0  0
    1.1624    0.0250    0.0216 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.3992   -1.0888    0.0108 N   0  0  0  0  0  0  0  0  0  0  0  0
   -0.9030   -0.7325   -0.0146 C   0  0  0  0  0  0  0  0  0  0  0  0
   -0.9834    0.6886   -0.0205 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.6316    2.1839    0.0052 H   0  0  0  0  0  0  0  0  0  0  0  0
    2.2394   -0.0035    0.0418 H   0  0  0  0  0  0  0  0  0  0  0  0
    0.7482   -2.0410    0.0202 H   0  0  0  0  0  0  0  0  0  0  0  0
   -1.7063   -1.4506   -0.0275 H   0  0  0  0  0  0  0  0  0  0  0  0
   -1.8933    1.2578   -0.0392 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  4  0  0  0  0
  2  3  4  0  0  0  0
  3  4  4  0  0  0  0
  1  5  2  0  0  0  0
  4  5  4  0  0  0  0
  1  6  1  0  0  0  0
  2  7  1  0  0  0  0
  3  8  1  0  0  0  0
  4  9  1  0  0  0  0
  5 10  1  0  0  0  0
M  CHG  1   1   1
M  END''') == '''xtb: 6.6.0 (conda-forge)
     RDKit          3D

 10  0  0  0  0  0  0  0  0  0999 V2000
    0.3053    1.1612    0.0022 C   0  0  0  0  0  0  0  0  0  0  0  0
    1.1624    0.0250    0.0216 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.3992   -1.0888    0.0108 N   0  0  0  0  0  0  0  0  0  0  0  0
   -0.9030   -0.7325   -0.0146 C   0  0  0  0  0  0  0  0  0  0  0  0
   -0.9834    0.6886   -0.0205 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.6316    2.1839    0.0052 H   0  0  0  0  0  0  0  0  0  0  0  0
    2.2394   -0.0035    0.0418 H   0  0  0  0  0  0  0  0  0  0  0  0
    0.7482   -2.0410    0.0202 H   0  0  0  0  0  0  0  0  0  0  0  0
   -1.7063   -1.4506   -0.0275 H   0  0  0  0  0  0  0  0  0  0  0  0
   -1.8933    1.2578   -0.0392 H   0  0  0  0  0  0  0  0  0  0  0  0
M  CHG  1   1   1
M  END
'''


class TestAromatizeMol:
    def test_C6H6(self):
        assert aromatize_mol('''
 OpenBabel06202314533D

 12 12  0  0  0  0  0  0  0  0999 V2000
    1.3829   -0.2211    0.0055 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.5068   -1.3064   -0.0076 C   0  0  0  0  0  0  0  0  0  0  0  0
   -0.8712   -1.0904   -0.0147 C   0  0  0  0  0  0  0  0  0  0  0  0
   -1.3730    0.2110   -0.0046 C   0  0  0  0  0  0  0  0  0  0  0  0
   -0.4968    1.2961    0.0109 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.8812    1.0801    0.0137 C   0  0  0  0  0  0  0  0  0  0  0  0
    2.4567   -0.3898    0.0094 H   0  0  0  0  0  0  0  0  0  0  0  0
    0.8977   -2.3203   -0.0126 H   0  0  0  0  0  0  0  0  0  0  0  0
   -1.5537   -1.9359   -0.0279 H   0  0  0  0  0  0  0  0  0  0  0  0
   -2.4466    0.3794   -0.0086 H   0  0  0  0  0  0  0  0  0  0  0  0
   -0.8877    2.3100    0.0204 H   0  0  0  0  0  0  0  0  0  0  0  0
    1.5639    1.9256    0.0225 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  2  0  0  0  0
  1  6  1  0  0  0  0
  1  7  1  0  0  0  0
  2  3  1  0  0  0  0
  2  8  1  0  0  0  0
  3  4  2  0  0  0  0
  3  9  1  0  0  0  0
  4  5  1  0  0  0  0
  4 10  1  0  0  0  0
  5  6  2  0  0  0  0
  5 11  1  0  0  0  0
  6 12  1  0  0  0  0
M  END''') == '''
     RDKit          3D

 12 12  0  0  0  0  0  0  0  0999 V2000
    1.3829   -0.2211    0.0055 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.5068   -1.3064   -0.0076 C   0  0  0  0  0  0  0  0  0  0  0  0
   -0.8712   -1.0904   -0.0147 C   0  0  0  0  0  0  0  0  0  0  0  0
   -1.3730    0.2110   -0.0046 C   0  0  0  0  0  0  0  0  0  0  0  0
   -0.4968    1.2961    0.0109 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.8812    1.0801    0.0137 C   0  0  0  0  0  0  0  0  0  0  0  0
    2.4567   -0.3898    0.0094 H   0  0  0  0  0  0  0  0  0  0  0  0
    0.8977   -2.3203   -0.0126 H   0  0  0  0  0  0  0  0  0  0  0  0
   -1.5537   -1.9359   -0.0279 H   0  0  0  0  0  0  0  0  0  0  0  0
   -2.4466    0.3794   -0.0086 H   0  0  0  0  0  0  0  0  0  0  0  0
   -0.8877    2.3100    0.0204 H   0  0  0  0  0  0  0  0  0  0  0  0
    1.5639    1.9256    0.0225 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  4  0
  1  6  4  0
  1  7  1  0
  2  3  4  0
  2  8  1  0
  3  4  4  0
  3  9  1  0
  4  5  4  0
  4 10  1  0
  5  6  4  0
  5 11  1  0
  6 12  1  0
M  END
'''

    def test_C4H5N3O(self):
        assert aromatize_mol(''' xtb: 6.6.0 (conda-forge)
     RDKit          3D

 13 13  0  0  0  0  0  0  0  0999 V2000
    2.2330   -0.4018    0.2211 N   0  0  0  0  0  0  0  0  0  0  0  0
    0.9032   -0.2381    0.0932 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.1635   -1.3176    0.1489 N   0  0  0  0  0  0  0  0  0  0  0  0
   -1.1777   -1.2603    0.0486 C   0  0  0  0  0  0  0  0  0  0  0  0
   -1.9297   -2.2032    0.0880 O   0  0  0  0  0  0  0  0  0  0  0  0
   -1.7395    0.0420   -0.1209 N   0  0  0  0  0  0  0  0  0  0  0  0
   -0.9924    1.1572   -0.1845 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.3562    1.0737   -0.0813 C   0  0  0  0  0  0  0  0  0  0  0  0
    2.5840   -1.3393    0.3055 H   0  0  0  0  0  0  0  0  0  0  0  0
    2.8683    0.3685    0.1239 H   0  0  0  0  0  0  0  0  0  0  0  0
   -2.7466    0.0782   -0.1969 H   0  0  0  0  0  0  0  0  0  0  0  0
   -1.5103    2.0975   -0.3187 H   0  0  0  0  0  0  0  0  0  0  0  0
    0.9881    1.9432   -0.1268 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  1  0
  2  3  2  0
  3  4  1  0
  4  5  2  0
  4  6  1  0
  6  7  1  0
  2  8  1  0
  7  8  2  0
  1  9  1  0
  1 10  1  0
  6 11  1  0
  7 12  1  0
  8 13  1  0
M  END''') == ''' xtb: 6.6.0 (conda-forge)
     RDKit          3D

 13 13  0  0  0  0  0  0  0  0999 V2000
    2.2330   -0.4018    0.2211 N   0  0  0  0  0  0  0  0  0  0  0  0
    0.9032   -0.2381    0.0932 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.1635   -1.3176    0.1489 N   0  0  0  0  0  0  0  0  0  0  0  0
   -1.1777   -1.2603    0.0486 C   0  0  0  0  0  0  0  0  0  0  0  0
   -1.9297   -2.2032    0.0880 O   0  0  0  0  0  0  0  0  0  0  0  0
   -1.7395    0.0420   -0.1209 N   0  0  0  0  0  0  0  0  0  0  0  0
   -0.9924    1.1572   -0.1845 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.3562    1.0737   -0.0813 C   0  0  0  0  0  0  0  0  0  0  0  0
    2.5840   -1.3393    0.3055 H   0  0  0  0  0  0  0  0  0  0  0  0
    2.8683    0.3685    0.1239 H   0  0  0  0  0  0  0  0  0  0  0  0
   -2.7466    0.0782   -0.1969 H   0  0  0  0  0  0  0  0  0  0  0  0
   -1.5103    2.0975   -0.3187 H   0  0  0  0  0  0  0  0  0  0  0  0
    0.9881    1.9432   -0.1268 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  1  0
  2  3  4  0
  3  4  4  0
  4  5  2  0
  4  6  4  0
  6  7  4  0
  2  8  4  0
  7  8  4  0
  1  9  1  0
  1 10  1  0
  6 11  1  0
  7 12  1  0
  8 13  1  0
M  END
'''


class TestKekulizeMol:

    def test_C4H5N3O(self):
        assert kekulize_mol(''' xtb: 6.6.0 (conda-forge)
          06202314263D

 13 13  0     0  0            999 V2000
    2.2330   -0.4018    0.2211 N   0  0  0  0  0  0  0  0  0  0  0  0
    0.9032   -0.2381    0.0932 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.1635   -1.3176    0.1489 N   0  0  0  0  0  0  0  0  0  0  0  0
   -1.1777   -1.2603    0.0486 C   0  0  0  0  0  0  0  0  0  0  0  0
   -1.9297   -2.2032    0.0880 O   0  0  0  0  0  0  0  0  0  0  0  0
   -1.7395    0.0420   -0.1209 N   0  0  0  0  0  0  0  0  0  0  0  0
   -0.9924    1.1572   -0.1845 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.3562    1.0737   -0.0813 C   0  0  0  0  0  0  0  0  0  0  0  0
    2.5840   -1.3393    0.3055 H   0  0  0  0  0  0  0  0  0  0  0  0
    2.8683    0.3685    0.1239 H   0  0  0  0  0  0  0  0  0  0  0  0
   -2.7466    0.0782   -0.1969 H   0  0  0  0  0  0  0  0  0  0  0  0
   -1.5103    2.0975   -0.3187 H   0  0  0  0  0  0  0  0  0  0  0  0
    0.9881    1.9432   -0.1268 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  4  0  0  0  0
  2  3  4  0  0  0  0
  3  4  4  0  0  0  0
  4  5  2  0  0  0  0
  4  6  1  0  0  0  0
  6  7  4  0  0  0  0
  2  8  1  0  0  0  0
  7  8  2  0  0  0  0
  1  9  1  0  0  0  0
  1 10  1  0  0  0  0
  6 11  1  0  0  0  0
  7 12  1  0  0  0  0
  8 13  1  0  0  0  0
M  END''') == ''' xtb: 6.6.0 (conda-forge)
     RDKit          3D

 13 13  0  0  0  0  0  0  0  0999 V2000
    2.2330   -0.4018    0.2211 N   0  0  0  0  0  0  0  0  0  0  0  0
    0.9032   -0.2381    0.0932 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.1635   -1.3176    0.1489 N   0  0  0  0  0  0  0  0  0  0  0  0
   -1.1777   -1.2603    0.0486 C   0  0  0  0  0  0  0  0  0  0  0  0
   -1.9297   -2.2032    0.0880 O   0  0  0  0  0  0  0  0  0  0  0  0
   -1.7395    0.0420   -0.1209 N   0  0  0  0  0  0  0  0  0  0  0  0
   -0.9924    1.1572   -0.1845 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.3562    1.0737   -0.0813 C   0  0  0  0  0  0  0  0  0  0  0  0
    2.5840   -1.3393    0.3055 H   0  0  0  0  0  0  0  0  0  0  0  0
    2.8683    0.3685    0.1239 H   0  0  0  0  0  0  0  0  0  0  0  0
   -2.7466    0.0782   -0.1969 H   0  0  0  0  0  0  0  0  0  0  0  0
   -1.5103    2.0975   -0.3187 H   0  0  0  0  0  0  0  0  0  0  0  0
    0.9881    1.9432   -0.1268 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  1  0
  2  3  2  0
  3  4  1  0
  4  5  2  0
  4  6  1  0
  6  7  1  0
  2  8  1  0
  7  8  2  0
  1  9  1  0
  1 10  1  0
  6 11  1  0
  7 12  1  0
  8 13  1  0
M  END
'''


class TestEnumerateEZ:

    def test_CC(self):
        assert enumerate_ez('CC') == ['CC']

    def test_C6H6(self):
        assert enumerate_ez('C1=CC=CC=C1') == ['C1=CC=CC=C1']

    def test_C2H2(self):
        assert enumerate_ez('C#C') == ['C#C']

    def test_C4H8(self):
        assert enumerate_ez('CC=CC') == ['C/C=C/C', r'C/C=C\C']

    def test_C4H8_symmetric(self):
        assert enumerate_ez('C=C(C)C') == ['C=C(C)C']

    def test_C6H12(self):
        assert enumerate_ez('CC=C(C)CC') == ['C/C=C(/C)CC', r'C/C=C(\C)CC']

    def test_C6H10(self):
        assert enumerate_ez('CC=CC=CC') == ['C/C=C/C=C/C', r'C/C=C\C=C/C', r'C/C=C\C=C\C']

    def test_C3H7N(self):
        assert enumerate_ez('CC=NC') == ['C/C=N/C', r'C/C=N\C']

    def test_C2H6N2(self):
        assert enumerate_ez('CN=NC') == ['C/N=N/C', r'C/N=N\C']

    def test_C12H12N2_preserve_kekule(self):
        assert enumerate_ez('C1=CC=CC=C1N=NC1=CC=CC=C1') == \
               ['C1=CC=C(/N=N/C2=CC=CC=C2)C=C1', r'C1=CC=C(/N=N\C2=CC=CC=C2)C=C1']

    def test_C12H12N2_preserve_aromatic(self):
        assert enumerate_ez('c1ccccc1N=Nc1ccccc1') == \
               ['c1ccc(/N=N/c2ccccc2)cc1', r'c1ccc(/N=N\c2ccccc2)cc1']

    def test_C4H8_replace_ez_stereo(self):
        assert enumerate_ez('C/C=C/C') == ['C/C=C/C', r'C/C=C\C']

    def test_C5H10O_preserve_chiral_stereo(self):
        assert enumerate_ez('CC=C[C@@H](C)O') == ['C/C=C/[C@@H](C)O', r'C/C=C\[C@@H](C)O']

    def test_C7H14O2_ez_through_chirality(self):
        assert enumerate_ez('C/C=C([C@@H](C)O)/[C@@H](O)C') == \
               ['C/C=C(/[C@H](C)O)[C@@H](C)O', r'C/C=C(\[C@H](C)O)[C@@H](C)O']

    def test_C7H14O2_ez_detection_bug(self):
        assert enumerate_ez('CC=C([C@H](C)O)[C@H](O)C') == ['CC=C([C@H](C)O)[C@H](O)C']

    def test_C4H8_remove_index(self):
        assert enumerate_ez('CC=CC |atomProp:0.p.0:1.p.1:2.p.2:3.p.3|') == \
               ['C/C=C/C', r'C/C=C\C']

    def test_C4H8_preserve_index(self):
        assert enumerate_ez('CC=CC |atomProp:0.p.0:1.p.1:2.p.2:3.p.3|', index=True) == \
               ['C/C=C/C |atomProp:0.p.0:1.p.1:2.p.2:3.p.3|',
                r'C/C=C\C |atomProp:0.p.0:1.p.1:2.p.2:3.p.3|']


class TestEnumerateChiral:

    def test_CC(self):
        assert enumerate_chiral('CC') == ['CC']

    def test_C8H16_cyclic_achiral(self):
        assert enumerate_chiral('CC1CCC(C)CC1') == ['CC1CCC(C)CC1']

    def test_C4H10O(self):
        assert enumerate_chiral('CC(O)CC') == ['CC[C@@H](C)O']

    def test_C6H14O(self):
        assert enumerate_chiral('CC(O)C(C)CC') == ['CC[C@@H](C)[C@@H](C)O', 'CC[C@H](C)[C@@H](C)O']

    def test_C4H10O2_symmetric(self):
        assert enumerate_chiral('CC(O)C(O)C') == \
               ['C[C@@H](O)[C@@H](C)O', 'C[C@H](O)[C@@H](C)O']

    def test_C3H6O_cyclic(self):
        assert enumerate_chiral('CC1CO1') == ['C[C@H]1CO1']

    def test_C7H14_cyclic_symmetric(self):
        assert enumerate_chiral('CC1C(C)CCC1') == ['C[C@@H]1CCC[C@@H]1C', 'C[C@@H]1CCC[C@H]1C']

    def test_C6H10_bicyclic(self):
        assert enumerate_chiral('C12CCCC1C2') == \
               ['C1C[C@@H]2C[C@@H]2C1', 'C1C[C@@H]2C[C@H]2C1']

    def test_C7H10_bicyclic(self):
        assert enumerate_chiral('OC1C(C2)CCC2C1') == \
               ['O[C@@H]1C[C@@H]2CC[C@@H]1C2', 'O[C@@H]1C[C@@H]2CC[C@H]1C2',
                'O[C@@H]1C[C@H]2CC[C@@H]1C2', 'O[C@H]1C[C@@H]2CC[C@@H]1C2']

    def test_C10H18_bicyclic_bug(self):
        assert enumerate_chiral('C12CCCCC1CCCC2') == ['C12CCCCC1CCCC2']

    def test_C4H10O_enantiomers(self):
        assert enumerate_chiral('CC(O)CC', enantiomers=True) == ['CC[C@@H](C)O', 'CC[C@H](C)O']

    def test_C6H14O_enantiomers(self):
        assert enumerate_chiral('CC(O)C(C)CC', enantiomers=True) == \
               ['CC[C@@H](C)[C@@H](C)O', 'CC[C@@H](C)[C@H](C)O',
                'CC[C@H](C)[C@@H](C)O', 'CC[C@H](C)[C@H](C)O']

    def test_C4H10O2_symmetric_enantiomers(self):
        assert enumerate_chiral('CC(O)C(O)C', enantiomers=True) == \
               ['C[C@@H](O)[C@@H](C)O', 'C[C@H](O)[C@@H](C)O', 'C[C@H](O)[C@H](C)O']

    def test_C3H6O_cyclic_enantiomers(self):
        assert enumerate_chiral('CC1CO1', enantiomers=True) == \
               ['C[C@@H]1CO1', 'C[C@H]1CO1']

    def test_C7H14_cyclic_symmetric_enantiomers(self):
        assert enumerate_chiral('CC1C(C)CCC1', enantiomers=True) == \
               ['C[C@@H]1CCC[C@@H]1C', 'C[C@@H]1CCC[C@H]1C', 'C[C@H]1CCC[C@@H]1C']

    def test_C4H10O_replace_chiral_stereo(self):
        assert enumerate_chiral('C[C@H](O)CC') == ['CC[C@@H](C)O']

    def test_C4H10O_replace_chiral_stereo_enantiomers(self):
        assert enumerate_chiral('C[C@H](O)CC', enantiomers=True) == \
               ['CC[C@@H](C)O', 'CC[C@H](C)O']

    def test_C5H10O_preserve_ez_stereo(self):
        assert enumerate_chiral('C/C=C/[C@H](C)O') == ['C/C=C/[C@@H](C)O']

    def test_C5H10O_preserve_ez_stereo_enantiomers(self):
        assert enumerate_chiral('C/C=C/[C@H](C)O', enantiomers=True) == \
               ['C/C=C/[C@@H](C)O', 'C/C=C/[C@H](C)O']

    def test_C8H10O_preserve_kekule(self):
        assert enumerate_chiral('OC(C)C1=CC=CC=C1') == ['C[C@H](O)C1=CC=CC=C1']

    def test_C8H10O_preserve_aromatic(self):
        assert enumerate_chiral('OC(C)c1ccccc1') == ['C[C@H](O)c1ccccc1']

    def test_C4H10O_remove_index(self):
        assert enumerate_chiral('CCC(C)O |atomProp:0.p.0:1.p.1:2.p.2:3.p.3:4.p.4|') == \
               ['CC[C@@H](C)O']

    def test_C4H10O_remove_index_enantiomers(self):
        assert enumerate_chiral('CCC(C)O |atomProp:0.p.0:1.p.1:2.p.2:3.p.3:4.p.4|',
                                enantiomers=True) == \
               ['CC[C@@H](C)O', 'CC[C@H](C)O']

    def test_C4H10O_preserve_index(self):
        assert enumerate_chiral('CCC(C)O |atomProp:0.p.0:1.p.1:2.p.2:3.p.3:4.p.4|', index=True) == \
               ['CC[C@@H](C)O |atomProp:0.p.0:1.p.1:2.p.2:3.p.3:4.p.4|']

    def test_C4H10O_preserve_index_enantiomers(self):
        assert enumerate_chiral('CCC(C)O |atomProp:0.p.0:1.p.1:2.p.2:3.p.3:4.p.4|',
                                index=True, enantiomers=True) == \
               ['CC[C@@H](C)O |atomProp:0.p.0:1.p.1:2.p.2:3.p.3:4.p.4|',
                'CC[C@H](C)O |atomProp:0.p.0:1.p.1:2.p.2:3.p.3:4.p.4|']


class TestEnumerateStereo:

    def test_CO(self):
        assert enumerate_stereo('CO') == ['CO']

    def test_C4H8(self):
        assert enumerate_stereo('CC=CC') == ['C/C=C/C', r'C/C=C\C']

    def test_C6H12O(self):
        assert enumerate_stereo('CC=CC(O)CC') == ['C/C=C/[C@@H](O)CC', r'C/C=C\[C@@H](O)CC']

    def test_C6H12_replace_stereo(self):
        assert enumerate_stereo('C/C=C/[C@@H](O)CC') == ['C/C=C/[C@@H](O)CC', r'C/C=C\[C@@H](O)CC']

    def test_C4H10O_enantiomers(self):
        assert enumerate_stereo('CC(O)CC', enantiomers=True) == ['CC[C@@H](C)O', 'CC[C@H](C)O']

    def test_C4H10O_remove_index(self):
        assert enumerate_stereo('CCC(C)O |atomProp:0.p.0:1.p.1:2.p.2:3.p.3:4.p.4|') == \
               ['CC[C@@H](C)O']

    def test_C4H10O_preserve_index(self):
        assert enumerate_stereo('CCC(C)O |atomProp:0.p.0:1.p.1:2.p.2:3.p.3:4.p.4|', index=True) == \
               ['CC[C@@H](C)O |atomProp:0.p.0:1.p.1:2.p.2:3.p.3:4.p.4|']


class TestAdjCompress:

    def test_CH3NH2(self):
        assert adj_compress(
            [[1], [0, 2, 3, 4], [1, 5, 6], [1], [1], [2], [2]]
        ) == ';0;1;1;1;2;2'

    def test_CH2O_H2O(self):
        assert adj_compress(
            [[1], [0, 3, 4], [5, 6], [1], [1], [2], [2]]
        ) == ';0;;1;1;2;2'

    def test_C3H6(self):
        assert adj_compress(
            [[1, 2, 3, 4], [0, 2, 5, 6], [0, 1, 7, 8], [0], [0], [1], [1], [2],
             [2]]
        ) == ';0;0,1;0;0;1;1;2;2'

    def test_empty(self):
        assert adj_compress([[]]) == ''

    def test_invalid_input(self):
        with pytest.raises(ValueError, match="empty adjacency list"):
            adj_compress([])


class TestIsConnected:

    def test_CH3NH2(self):
        assert is_connected(
            [[1], [0, 2, 3, 4], [1, 5, 6], [1], [1], [2], [2]]
        ) is True

    def test_NH3_H2O(self):
        assert is_connected(
            [[1], [0, 3, 4], [5, 6], [1], [1], [2], [2]]
        ) is False

    def test_C3H6(self):
        assert is_connected(
            [[1, 2, 3, 4], [0, 2, 5, 6], [0, 1, 7, 8], [0], [0], [1], [1], [2],
             [2]]
        ) is True

    def test_empty(self):
        assert is_connected([]) is True

    def test_atom(self):
        assert is_connected([[0]]) is True


class TestComposition:

    def test_CH2O(self):
        assert composition([(0, 'C'), (1, 'O'), (2, 'H'), (3, 'H')]) == 'C 1,H 2,O 1'

    def test_Cl(self):
        assert composition([(0, 'Cl')]) == 'Cl 1'

    def test_empty(self):
        assert composition([]) == ''


class TestBondCompress:

    def test_C4H4(self):
        assert bond_compress(
            [(0, 1, 'TRIPLE'), (1, 2, 'SINGLE'), (2, 3, 'DOUBLE'), (0, 4, 'SINGLE'),
             (2, 5, 'SINGLE'), (3, 6, 'SINGLE'), (3, 7, 'SINGLE')]) == '0#1;0-4;1-2;2=3;2-5;3-6;3-7'

    def test_Cl(self):
        assert bond_compress([]) == ''


class TestPathwayhash:

    def test_forward(self):
        assert pathwayhash(
            ['COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF-AAAAA',
             'NPRPHDSQZBFINE-UHFFFAOYSA-O-DVBOZSIUCD-AAAAA',
             'NHPCRZRPQZSTTA-UHFFFAOYSA-N-UBZNVBZCUR-AAAAA',
             'DRECBBFIAREDAS-UHFFFAOYSA-N-LYEFCTAKDG-AAAAA',
             'DRECBBFIAREDAS-UHFFFAOYSA-N-XMJDFJRPND-AAAAA']
        ) == 'ALBIFQFHLPBELU-HWULFHTPAK-Q-CPNGBZGRWJ-EPKQB'

    def test_backward(self):
        assert pathwayhash(
            ['DRECBBFIAREDAS-UHFFFAOYSA-N-XMJDFJRPND-AAAAA',
             'DRECBBFIAREDAS-UHFFFAOYSA-N-LYEFCTAKDG-AAAAA',
             'NHPCRZRPQZSTTA-UHFFFAOYSA-N-UBZNVBZCUR-AAAAA',
             'NPRPHDSQZBFINE-UHFFFAOYSA-O-DVBOZSIUCD-AAAAA',
             'COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF-AAAAA']
        ) == 'ALBIFQFHLPBELU-HWULFHTPAK-J-CPNGBZGRWJ-EPKQB'

    def test_roundtrip(self):
        assert pathwayhash(
            ['DRECBBFIAREDAS-UHFFFAOYSA-N-XMJDFJRPND-AAAAA',
             'DRECBBFIAREDAS-UHFFFAOYSA-N-LYEFCTAKDG-AAAAA',
             'DRECBBFIAREDAS-UHFFFAOYSA-N-XMJDFJRPND-AAAAA']
        ) == 'QTHTDIZKLHJZNU-XZIRWGZUBF-N-YBUBPYIENY-AQHOC'

    def test_single(self):
        assert pathwayhash(
            ['OKKJLVBELUTLKV-UHFFFAOYSA-N-NEMZRWRQOB-AAAAA']
        ) == 'OKKJLVBELUTLKV-UHFFFAOYSA-N-NEMZRWRQOB-AAAAA'

    def test_empty(self):
        with pytest.raises(ValueError, match="empty input key list"):
            pathwayhash([])
