import pytest
from pytest import approx

from colibri2.config import LocalConfig
from colibri2.task import XTBEnergyPropertyTask
from colibri2.data import Geometry, Property


class TestXTBEnergyPropertyTask:

    @pytest.fixture(scope="class")
    def xtb_energy_property_task(self, request):
        config = LocalConfig()
        config.update({
            'PROPERTY__EXECUTABLE': request.config.getini('xtb_exec'),
            'PROPERTY__VERSION': '',
            'PROPERTY__OPTIONS': '',
            'PROPERTY__METHOD': 'GFN2',
        })
        return XTBEnergyPropertyTask(config)

    @pytest.fixture(scope="class")
    def xtb_energy_property_task_cation(self, request):
        config = LocalConfig()
        config.update({
            'PROPERTY__EXECUTABLE': request.config.getini('xtb_exec'),
            'PROPERTY__VERSION': '',
            'PROPERTY__OPTIONS': '',
            'PROPERTY__METHOD': 'GFN2',
            'PROPERTY__SET_CHARGE': 1,
            'PROPERTY__SET_MULT': 2,
        })
        return XTBEnergyPropertyTask(config)

    @pytest.fixture(scope="class")
    def xtb_energy_property_task_anion(self, request):
        config = LocalConfig()
        config.update({
            'PROPERTY__EXECUTABLE': request.config.getini('xtb_exec'),
            'PROPERTY__VERSION': '',
            'PROPERTY__OPTIONS': '',
            'PROPERTY__METHOD': 'GFN2',
            'PROPERTY__SET_CHARGE': -1,
            'PROPERTY__SET_MULT': 2,
        })
        return XTBEnergyPropertyTask(config)

    def test_H2O(self, xtb_energy_property_task):
        out = xtb_energy_property_task.process(Geometry(''' xtb: 6.6.0 (conda-forge)
     RDKit          3D

  3  2  0  0  0  0  0  0  0  0999 V2000
    0.9275    0.0562    0.0375 O   0  0  0  0  0  0  0  0  0  0  0  0
    1.8865    0.0692    0.0502 H   0  0  0  0  0  0  0  0  0  0  0  0
    0.6250    0.7044    0.6765 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  1  0
  1  3  1  0
M  END
'''))
        assert type(out) is Property
        assert out.properties['energy'] == approx(-137.9765)

    def test_H2O_cation(self, xtb_energy_property_task_cation):
        out = xtb_energy_property_task_cation.process(Geometry(''' xtb: 6.6.0 (conda-forge)
     RDKit          3D

  3  2  0  0  0  0  0  0  0  0999 V2000
    0.9275    0.0562    0.0375 O   0  0  0  0  0  0  0  0  0  0  0  0
    1.8865    0.0692    0.0502 H   0  0  0  0  0  0  0  0  0  0  0  0
    0.6250    0.7044    0.6765 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  1  0
  1  3  1  0
M  END
'''))
        assert type(out) is Property
        assert out.properties['energy'] == approx(-119.6836)

    def test_H2O_anion(self, xtb_energy_property_task_anion):
        out = xtb_energy_property_task_anion.process(Geometry(''' xtb: 6.6.0 (conda-forge)
     RDKit          3D

  3  2  0  0  0  0  0  0  0  0999 V2000
    0.9275    0.0562    0.0375 O   0  0  0  0  0  0  0  0  0  0  0  0
    1.8865    0.0692    0.0502 H   0  0  0  0  0  0  0  0  0  0  0  0
    0.6250    0.7044    0.6765 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  1  0
  1  3  1  0
M  END
'''))
        assert type(out) is Property
        assert out.properties['energy'] == approx(-131.3075)
