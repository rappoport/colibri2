import pytest
from psycopg2.extras import Json
from pytest_postgresql import factories
from sqlalchemy.engine.url import URL

from colibri2.config import LocalConfig
from colibri2.data import Formula, Superformula, Transformation, Rule
from colibri2.db import Psycopg2DB

postgresql_proc = factories.postgresql_proc()
postgresql = factories.postgresql('postgresql_proc')


@pytest.mark.db
class TestPsycopg2DB:

    @pytest.fixture(scope="function")
    def psycopg2db(self, postgresql):
        config = LocalConfig()
        url = URL.create(drivername="postgresql",
                         username=postgresql.info.user,
                         password=postgresql.info.password,
                         host=postgresql.info.host,
                         port=postgresql.info.port,
                         database=postgresql.info.dbname)
        config.update({'DB__URL': repr(url),
                       'DB__MOLECULE_TABLES__FORMULA': 'formulas',
                       'DB__FLASK_TABLES__SUPERFORMULA': 'superformulas',
                       'DB__TRANSFORMATION_TABLE': 'transformations',
                       'DB__RULE_TABLE': 'rules'})
        return Psycopg2DB(config)
    
    def test_get_number_molecules(self, psycopg2db):
        key1 = 'OTMSDBZUPAUEDD-UHFFFAOYSA-N-PGWBAZAUGG-AAAAA'
        data1 = {'index': False, 'method': None, 'options': None, 'program': None,
                 'smiles': 'CC', 'version': None, 'tag': 'd4590140-2e92-44f1-87b2-de6334bd7ce8'}
        key2 = 'YQHHCMVUMULAPZ-UHFFFAOYSA-N-GYZSABJQRP-AAAAA'
        data2 = {'index': False, 'method': None, 'options': None, 'program': None,
                 'smiles': '[CH2+]C', 'version': None, 'tag': 'd2c9c0de-62b4-4cea-9298-f455ed98b819'}
        key3 = 'CREMABGTGYGIQB-UHFFFAOYSA-N-NQPRJIHSGQ-AAAAA'
        data3 = {'index': False, 'method': None, 'options': None, 'program': None,
                 'smiles': 'CO', 'version': None, 'tag': 'e1a6c4f0-5b25-4c8d-a5b2-12e2c1541c2f'}
        psycopg2db.db_cursor.executemany("INSERT INTO formulas (key, data) VALUES (%s, %s);",
                                         [(key1, Json(data1)), (key2, Json(data2)), (key3, Json(data3))])
        number_molecules = psycopg2db.get_number_molecules('Formula')
        assert number_molecules == 3

    def test_get_number_flasks(self, psycopg2db):
        key1 = 'CREMABGTGYGIQB-UHFFFAOYSA-N-NQPRJIHSGQ-AAAAA'
        data1 = {'smiles': 'C.C', 'index': False, 'program': None, 'version': None,
                 'method': None, 'options': None, 'tag': '53d01b6a-0837-4d61-bb71-3db3d076df90'}
        key2 = 'NZNCTFHRZHZECW-UHFFFAOYSA-O-DXRORDCSRN-AAAAA'
        data2 = {'smiles': 'C.[CH3-].[H+]', 'index': False, 'program': None, 'version': None,
                 'method': None, 'options': None, 'tag': 'c02fb084-d8ab-4e4e-b1de-b5d1e9b78f86'}
        key3 = 'OTMSDBZUPAUEDD-UHFFFAOYSA-N-PGWBAZAUGG-AAAAA'
        data3 = {'smiles': 'CC', 'index': False, 'program': None, 'version': None,
                 'method': None, 'options': None, 'tag': '33271b52-2ecc-4875-9e08-5df161d387f3'}
        psycopg2db.db_cursor.executemany("INSERT INTO superformulas (key, data) VALUES (%s, %s);",
                                         [(key1, Json(data1)), (key2, Json(data2)), (key3, Json(data3))])
        number_flasks = psycopg2db.get_number_flasks('Superformula')
        assert number_flasks == 3

    def test_get_number_transformations(self, psycopg2db):
        ruleid1 = 0
        rule1data = {'rule': '[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2]'}
        key1 = 'COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF-AAAAA'
        id1 = 1
        key1data = {'smiles': 'CO.CO', 'index': False, 'program': None, 'version': None,
                 'method': None, 'options': None, 'tag': '4f6a9b4d-5d7b-4ac7-bae1-04d29a4b9ec1'}
        key2 = 'NPRPHDSQZBFINE-UHFFFAOYSA-O-DVBOZSIUCD-AAAAA'
        id2 = 2
        key2data = {'smiles': 'CO.C[O-].[H+]', 'index': False, 'program': None, 'version': None,
                 'method': None, 'options': None, 'tag': 'f8c7a77a-6b9f-4b92-b619-3cb65030cb23'} 
        data1 = {'index': True, 'tag': '44d51bdb-d0db-410f-95ac-62927fb632e3'}
        ruleid2 = 1
        rule2data = {'rule': '[O+0:1]-[H+0:2]>>[O-1:1].[H+1:2]'}
        key3 = 'DGPBUJDVYFAAJG-UHFFFAOYSA-O-SHIORNWQYZ-AAAAA'
        id3 = 3
        key3data = {'smiles': 'O.[CH2-]CO.[H+]', 'index': False, 'program': None, 'version': None,
                 'method': None, 'options': None, 'tag': 'a2b0e315-cd4d-49b9-bd3e-12d05e10e5be'}
        key4 = 'JKFINNLPWXKAFG-UHFFFAOYSA-P-JECILUDYKV-AAAAA'
        id4 = 4
        key4data = {'smiles': 'O.[CH2-]C[O-].[H+].[H+]', 'index': False, 'program': None, 'version': None,
                 'method': None, 'options': None, 'tag': 'd9354c32-b4fa-45eb-99f3-507de84fa0a5'}
        data2 = {'index': True, 'tag': 'f6b1c834-1e5f-4b07-9e49-9b3c57e8822d'}
        ruleid3 = 2
        rule3data = {'rule': '[O+1:1]-[H+0:2]>>[O+0:1].[H+1:2]'}
        key5 = 'MEAIUODGYOPHBA-UHFFFAOYSA-P-XSHLRVETKP-AAAAA'
        id5 = 5
        key5data = {'smiles': '[CH-]=[CH-].[OH3+].[OH3+]', 'index': False, 'program': None, 'version': None,
                 'method': None, 'options': None, 'tag': 'c73e4975-3301-451f-8303-e7d4c9b35184'}
        key6 = 'MEAIUODGYOPHBA-UHFFFAOYSA-P-TRAFAUYQXH-AAAAA'
        id6 = 6
        key6data = {'smiles': 'O.[CH-]=[CH-].[H+].[OH3+]', 'index': False, 'program': None, 'version': None,
                 'method': None, 'options': None, 'tag': '8e61991f-e0a0-49ad-9d3f-dcd94db6e5db'}
        data3 = {'index': True, 'tag': 'a3e0c8c5-7f3f-4c60-8d1e-e5009e3e25bc'}
        psycopg2db.db_cursor.executemany("INSERT INTO superformulas (key, data) VALUES (%s, %s);",
                                         [(key1, Json(key1data)), (key2, Json(key2data)), (key3, Json(key3data)),
                                          (key4, Json(key4data)), (key5, Json(key5data)), (key6, Json(key6data))])
        psycopg2db.db_cursor.executemany("INSERT INTO rules (id, data) VALUES (%s, %s);",
                                     [(ruleid1, Json(rule1data)),(ruleid2, Json(rule2data)),(ruleid3, Json(rule3data))])
        psycopg2db.db_cursor.executemany("INSERT INTO transformations (id1, id2, ruleid, data) VALUES (%s, %s, %s, %s);",
                                     [(id1, id2, ruleid1, Json(data1)),(id3, id4, ruleid2, Json(data2)),(id5, id6, ruleid3, Json(data3))])
        number_transformations = psycopg2db.get_number_transformations()
        assert number_transformations == 3

    def test_get_number_rules(self, psycopg2db):
        ruleid1 = 0
        rule1data = {'rule': '[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2]'}
        ruleid2 = 1
        rule2data = {'rule': '[O+0:1]-[H+0:2]>>[O-1:1].[H+1:2]'}
        ruleid3 = 2
        rule3data = {'rule': '[O+1:1]-[H+0:2]>>[O+0:1].[H+1:2]'}
        psycopg2db.db_cursor.executemany("INSERT INTO rules (id, data) VALUES (%s, %s);",
                                     [(ruleid1, Json(rule1data)),(ruleid2, Json(rule2data)),(ruleid3, Json(rule3data))])
        number_rules = psycopg2db.get_number_rules()
        assert number_rules == 3

    def test_fetch_molecule(self, psycopg2db):
        key = 'OTMSDBZUPAUEDD-UHFFFAOYSA-N-PGWBAZAUGG-AAAAA'
        data = {'index': False, 'method': None, 'options': None, 'program': None,
                'smiles': 'CC', 'version': None, 'tag': '33271b52-2ecc-4875-9e08-5df161d387f3'}
        psycopg2db.db_cursor.execute("INSERT INTO formulas (key, data) VALUES (%s, %s);",
                                     (key, Json(data)))
        mol = psycopg2db.fetch_molecule('Formula', key)
        assert mol == Formula(smiles='CC', index=False)

    def test_fetch_molecule_raw(self, psycopg2db):
        key = 'OTMSDBZUPAUEDD-UHFFFAOYSA-N-PGWBAZAUGG-AAAAA'
        data = {'index': False, 'method': None, 'options': None, 'program': None,
                'smiles': 'CC', 'version': None, 'tag': '33271b52-2ecc-4875-9e08-5df161d387f3'}
        psycopg2db.db_cursor.execute("INSERT INTO formulas (key, data) VALUES (%s, %s);",
                                     (key, Json(data)))
        mol = psycopg2db.fetch_molecule('Formula', key, raw=True)
        assert mol == (key, data)

    def test_fetch_flask(self, psycopg2db):
        key = 'CREMABGTGYGIQB-UHFFFAOYSA-N-NQPRJIHSGQ-AAAAA'
        data = {'smiles': 'C.C', 'index': False, 'program': None, 'version': None,
                'method': None, 'options': None, 'tag': '53d01b6a-0837-4d61-bb71-3db3d076df90'}
        psycopg2db.db_cursor.execute("INSERT INTO superformulas (key, data) VALUES (%s, %s);",
                                     (key, Json(data)))
        flask = psycopg2db.fetch_flask('Superformula', key)
        assert flask == Superformula(smiles='C.C', index=False)

    def test_fetch_flask_raw(self, psycopg2db):
        key = 'CREMABGTGYGIQB-UHFFFAOYSA-N-NQPRJIHSGQ-AAAAA'
        id = 1
        data = {'smiles': 'C.C', 'index': False, 'program': None, 'version': None,
                'method': None, 'options': None, 'tag': '53d01b6a-0837-4d61-bb71-3db3d076df90'}
        psycopg2db.db_cursor.execute("INSERT INTO superformulas (key, data) VALUES (%s, %s);",
                                     (key, Json(data)))
        flask_raw = psycopg2db.fetch_flask('Superformula', key, raw=True)
        assert flask_raw == (id, key, data)

    def test_fetch_transformation(self, psycopg2db):
        ruleid = 10
        ruledata = {'rule': '[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2]'}
        key1 = 'COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF-AAAAA'
        id1 = 1
        key1data = {'smiles': 'CO.CO', 'index': False, 'program': None, 'version': None,
                 'method': None, 'options': None, 'tag': '4f6a9b4d-5d7b-4ac7-bae1-04d29a4b9ec1'}
        key2 = 'NPRPHDSQZBFINE-UHFFFAOYSA-O-DVBOZSIUCD-AAAAA'
        id2 = 2
        key2data = {'smiles': 'CO.C[O-].[H+]', 'index': False, 'program': None, 'version': None,
                 'method': None, 'options': None, 'tag': 'f8c7a77a-6b9f-4b92-b619-3cb65030cb23'} 
        data = {'index': True, 'tag': '0ef086c5-3294-4377-8848-1b5182172dc9'}
        psycopg2db.db_cursor.executemany("INSERT INTO superformulas (key, data) VALUES (%s, %s);",
                                         [(key1, Json(key1data)), (key2, Json(key2data))])
        psycopg2db.db_cursor.execute("INSERT INTO rules (id, data) VALUES (%s, %s);",
                                     (ruleid, Json(ruledata)))
        psycopg2db.db_cursor.execute("INSERT INTO transformations (id1, id2, ruleid, data) VALUES (%s, %s, %s, %s);",
                                     (id1, id2, ruleid, Json(data)))
        trans = psycopg2db.fetch_transformation(key1, key2)
        assert trans == Transformation(
            'CO.CO |atomProp:0.p.0:1.p.1:2.p.2:3.p.3|',
            'CO.C[O-].[H+] |atomProp:0.p.0:1.p.1:2.p.2:3.p.3|',
            '[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2]', index=True)

    def test_fetch_transformation_raw(self, psycopg2db):
        ruleid = 10
        ruledata = {'rule': '[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2]'}
        key1 = 'COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF-AAAAA'
        id1 = 1
        key1data = {'smiles': 'CO.CO', 'index': False, 'program': None, 'version': None,
                 'method': None, 'options': None, 'tag': '4f6a9b4d-5d7b-4ac7-bae1-04d29a4b9ec1'}
        key2 = 'NPRPHDSQZBFINE-UHFFFAOYSA-O-DVBOZSIUCD-AAAAA'
        id2 = 2
        key2data = {'smiles': 'CO.C[O-].[H+]', 'index': False, 'program': None, 'version': None,
                 'method': None, 'options': None, 'tag': 'f8c7a77a-6b9f-4b92-b619-3cb65030cb23'} 
        data = {'index': True, 'tag': '0ef086c5-3294-4377-8848-1b5182172dc9'}
        psycopg2db.db_cursor.executemany("INSERT INTO superformulas (key, data) VALUES (%s, %s);",
                                         [(key1, Json(key1data)), (key2, Json(key2data))])
        psycopg2db.db_cursor.execute("INSERT INTO rules (id, data) VALUES (%s, %s);",
                                     (ruleid, Json(ruledata)))
        psycopg2db.db_cursor.execute("INSERT INTO transformations (id1, id2, ruleid, data) VALUES (%s, %s, %s, %s);",
                                     (id1, id2, ruleid, Json(data)))
        trans_raw = psycopg2db.fetch_transformation(key1, key2, raw=True)
        assert trans_raw == (key1, key2, id1, id2, key1data['smiles'], key2data['smiles'], ruledata, data)

    def test_fetch_rule(self, psycopg2db):
        ruleid = 10
        ruledata = {'rule': '[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2]'}
        psycopg2db.db_cursor.execute("INSERT INTO rules (id, data) VALUES (%s, %s);",
                                     (ruleid, Json(ruledata)))
        rule = psycopg2db.fetch_rule(ruleid)
        assert rule == Rule('[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2]', 10)

    def test_fetch_rule_raw(self, psycopg2db):
        ruleid = 10
        ruledata = {'rule': '[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2]'}
        psycopg2db.db_cursor.execute("INSERT INTO rules (id, data) VALUES (%s, %s);",
                                     (ruleid, Json(ruledata)))
        rule_raw = psycopg2db.fetch_rule(ruleid, raw=True)
        assert rule_raw == (ruleid, ruledata)

    def test_fetch_molecules(self, psycopg2db):
        key1 = 'OTMSDBZUPAUEDD-UHFFFAOYSA-N-PGWBAZAUGG-AAAAA'
        data1 = {'index': False, 'method': None, 'options': None, 'program': None,
                 'smiles': 'CC', 'version': None, 'tag': 'd4590140-2e92-44f1-87b2-de6334bd7ce8'}
        key2 = 'YQHHCMVUMULAPZ-UHFFFAOYSA-N-GYZSABJQRP-AAAAA'
        data2 = {'smiles': '[CH2+]C', 'index': False, 'program': None, 'version': None,
                 'method': None, 'options': None, 'tag': 'd2c9c0de-62b4-4cea-9298-f455ed98b819'}
        psycopg2db.db_cursor.executemany("INSERT INTO formulas (key, data) VALUES (%s, %s);",
                                         [(key1, Json(data1)), (key2, Json(data2))])
        molecules = psycopg2db.fetch_molecules('Formula', [key1, key2])
        assert molecules == [Formula(smiles='CC', index=False), Formula(smiles='[CH2+]C', index=False)]

    def test_fetch_molecules_raw(self, psycopg2db):
        key1 = 'OTMSDBZUPAUEDD-UHFFFAOYSA-N-PGWBAZAUGG-AAAAA'
        data1 = {'index': False, 'method': None, 'options': None, 'program': None,
                 'smiles': 'CC', 'version': None, 'tag': 'd4590140-2e92-44f1-87b2-de6334bd7ce8'}
        key2 = 'YQHHCMVUMULAPZ-UHFFFAOYSA-N-GYZSABJQRP-AAAAA'
        data2 = {'smiles': '[CH2+]C', 'index': False, 'program': None, 'version': None,
                 'method': None, 'options': None, 'tag': 'd2c9c0de-62b4-4cea-9298-f455ed98b819'}
        psycopg2db.db_cursor.executemany("INSERT INTO formulas (key, data) VALUES (%s, %s);",
                                         [(key1, Json(data1)), (key2, Json(data2))])
        molecules = psycopg2db.fetch_molecules('Formula', [key1, key2], raw=True)
        assert molecules == [(key1, data1), (key2, data2)]

    def test_fetch_flasks(self, psycopg2db):
        key1 = 'CREMABGTGYGIQB-UHFFFAOYSA-N-NQPRJIHSGQ-AAAAA'
        data1 = {'smiles': 'C.C', 'index': False, 'program': None, 'version': None,
                 'method': None, 'options': None, 'tag': '53d01b6a-0837-4d61-bb71-3db3d076df90'}
        key2 = 'NZNCTFHRZHZECW-UHFFFAOYSA-O-DXRORDCSRN-AAAAA'
        data2 = {'smiles': 'C.[CH3-].[H+]', 'index': False, 'program': None, 'version': None,
                 'method': None, 'options': None, 'tag': 'c02fb084-d8ab-4e4e-b1de-b5d1e9b78f86'}
        psycopg2db.db_cursor.executemany("INSERT INTO superformulas (key, data) VALUES (%s, %s);",
                                         [(key1, Json(data1)), (key2, Json(data2))])
        flasks = psycopg2db.fetch_flasks('Superformula', [key1, key2])
        assert flasks == [Superformula(smiles='C.C', index=False),
                          Superformula(smiles='C.[CH3-].[H+]', index=False)]

    def test_fetch_flasks_raw(self, psycopg2db):
        key1 = 'CREMABGTGYGIQB-UHFFFAOYSA-N-NQPRJIHSGQ-AAAAA'
        id1 = 1
        data1 = {'smiles': 'C.C', 'index': False, 'program': None, 'version': None,
                 'method': None, 'options': None,
                 'tag': '02bf0fcd-897f-4ed7-8c9d-952a0e68e80a'}
        key2 = 'NZNCTFHRZHZECW-UHFFFAOYSA-O-DXRORDCSRN-AAAAA'
        id2 = 2
        data2 = {'smiles': 'C.[CH3-].[H+]', 'index': False, 'program': None, 'version': None,
                 'method': None, 'options': None,
                 'tag': 'b3c94bbb-de49-48e7-9225-9b48ee191400'}
        psycopg2db.db_cursor.executemany("INSERT INTO superformulas (key, data) VALUES (%s, %s);",
                                         [(key1, Json(data1)), (key2, Json(data2))])
        flasks_raw = psycopg2db.fetch_flasks('Superformula', ['CREMABGTGYGIQB-UHFFFAOYSA-N-NQPRJIHSGQ-AAAAA',
                                                              'NZNCTFHRZHZECW-UHFFFAOYSA-O-DXRORDCSRN-AAAAA'],
                                             raw=True)
        assert flasks_raw == [(id1, key1, data1), (id2, key2, data2)]

    def test_fetch_transformations(self, psycopg2db):
        ruleid = 10
        ruledata = {'rule': '[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2]'}
        key1 = 'COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF-AAAAA'
        id1 = 1
        key1data = {'smiles': 'CO.CO', 'index': False, 'program': None, 'version': None,
                 'method': None, 'options': None, 'tag': '4f6a9b4d-5d7b-4ac7-bae1-04d29a4b9ec1'}
        key2 = 'NPRPHDSQZBFINE-UHFFFAOYSA-O-DVBOZSIUCD-AAAAA'
        id2 = 2
        key2data = {'smiles': 'CO.C[O-].[H+]', 'index': False, 'program': None, 'version': None,
                 'method': None, 'options': None, 'tag': 'f8c7a77a-6b9f-4b92-b619-3cb65030cb23'} 
        data = {'index': True, 'tag': '368e53f9-8e50-4fb3-a9b1-f40d31a24cf5'}
        psycopg2db.db_cursor.executemany("INSERT INTO superformulas (key, data) VALUES (%s, %s);",
                                         [(key1, Json(key1data)), (key2, Json(key2data))])
        psycopg2db.db_cursor.execute("INSERT INTO rules (id, data) VALUES (%s, %s);",
                                     (ruleid, Json(ruledata)))
        psycopg2db.db_cursor.execute("INSERT INTO transformations (id1, id2, ruleid, data) VALUES (%s, %s, %s, %s);",
                                     (id1, id2, ruleid, Json(data)))
        transformations = psycopg2db.fetch_transformations([(key1, key2)])
        assert transformations == [Transformation(reactantsmiles='CO.CO |atomProp:0.p.0:1.p.1:2.p.2:3.p.3|',
                                                  productsmiles='CO.C[O-].[H+] |atomProp:0.p.0:1.p.1:2.p.2:3.p.3|',
                                                  rule='[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2]', index=True)]

    def test_fetch_transformations_raw(self, psycopg2db):
        ruleid = 10
        ruledata = {'rule': '[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2]'}
        key1 = 'COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF-AAAAA'
        id1 = 1
        key1data = {'smiles': 'CO.CO', 'index': False, 'program': None, 'version': None,
                 'method': None, 'options': None, 'tag': '4f6a9b4d-5d7b-4ac7-bae1-04d29a4b9ec1'}
        key2 = 'NPRPHDSQZBFINE-UHFFFAOYSA-O-DVBOZSIUCD-AAAAA'
        id2 = 2
        key2data = {'smiles': 'CO.C[O-].[H+]', 'index': False, 'program': None, 'version': None,
                 'method': None, 'options': None, 'tag': 'f8c7a77a-6b9f-4b92-b619-3cb65030cb23'} 
        data = {'index': True, 'tag': '368e53f9-8e50-4fb3-a9b1-f40d31a24cf5'}
        psycopg2db.db_cursor.executemany("INSERT INTO superformulas (key, data) VALUES (%s, %s);",
                                         [(key1, Json(key1data)), (key2, Json(key2data))])
        psycopg2db.db_cursor.execute("INSERT INTO rules (id, data) VALUES (%s, %s);",
                                     (ruleid, Json(ruledata)))
        psycopg2db.db_cursor.execute("INSERT INTO transformations (id1, id2, ruleid, data) VALUES (%s, %s, %s, %s);",
                                     (id1, id2, ruleid, Json(data)))
        transformations_raw = psycopg2db.fetch_transformations([(key1, key2)], raw=True)
        assert transformations_raw == [(key1, key2, id1, id2, key1data['smiles'], key2data['smiles'], ruledata, data)]

    def test_fetch_rules(self, psycopg2db):
        ruleid1 = 1
        rule1data = {'rule': '[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2]'}
        ruleid2 = 2
        rule2data = {'rule': '[O+0:1]-[H+0:2]>>[O-1:1].[H+1:2]'}
        psycopg2db.db_cursor.executemany("INSERT INTO rules (id, data) VALUES (%s, %s);",
                                     [(ruleid1, Json(rule1data)),(ruleid2, Json(rule2data))])
        rules = psycopg2db.fetch_rules([ruleid1, ruleid2])
        assert rules == [Rule('[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2]', 1),
                        Rule('[O+0:1]-[H+0:2]>>[O-1:1].[H+1:2]', 2)]

    def test_fetch_rules_raw(self, psycopg2db):
        ruleid1 = 1
        rule1data = {'rule': '[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2]'}
        ruleid2 = 2
        rule2data = {'rule': '[O+0:1]-[H+0:2]>>[O-1:1].[H+1:2]'}
        psycopg2db.db_cursor.executemany("INSERT INTO rules (id, data) VALUES (%s, %s);",
                                     [(ruleid1, Json(rule1data)),(ruleid2, Json(rule2data))])
        rules = psycopg2db.fetch_rules([ruleid1, ruleid2], raw=True)
        assert rules == [(ruleid1, rule1data), (ruleid2, rule2data)]
        
    def test_fetch_molecules_range(self, psycopg2db):
        key1 = 'OTMSDBZUPAUEDD-UHFFFAOYSA-N-PGWBAZAUGG-AAAAA'
        data1 = {'index': False, 'method': None, 'options': None, 'program': None,
                 'smiles': 'CC', 'version': None, 'tag': 'd4590140-2e92-44f1-87b2-de6334bd7ce8'}
        key2 = 'YQHHCMVUMULAPZ-UHFFFAOYSA-N-GYZSABJQRP-AAAAA'
        data2 = {'index': False, 'method': None, 'options': None, 'program': None,
                 'smiles': '[CH2+]C', 'version': None, 'tag': 'd2c9c0de-62b4-4cea-9298-f455ed98b819'}
        key3 = 'CREMABGTGYGIQB-UHFFFAOYSA-N-NQPRJIHSGQ-AAAAA'
        data3 = {'index': False, 'method': None, 'options': None, 'program': None,
                 'smiles': 'CO', 'version': None, 'tag': 'e1a6c4f0-5b25-4c8d-a5b2-12e2c1541c2f'}
        psycopg2db.db_cursor.executemany("INSERT INTO formulas (key, data) VALUES (%s, %s);",
                                         [(key1, Json(data1)), (key2, Json(data2)), (key3, Json(data3))])
        molecules = list(psycopg2db.fetch_molecules_range('Formula',start_id=1,end_id=3))
        assert molecules == [Formula(smiles='[CH2+]C', index=False),
                             Formula(smiles='CO', index=False)]

    def test_fetch_molecules_range_raw(self, psycopg2db):
        key1 = 'OTMSDBZUPAUEDD-UHFFFAOYSA-N-PGWBAZAUGG-AAAAA'
        data1 = {'index': False, 'method': None, 'options': None, 'program': None,
                 'smiles': 'CC', 'version': None, 'tag': 'd4590140-2e92-44f1-87b2-de6334bd7ce8'}
        key2 = 'YQHHCMVUMULAPZ-UHFFFAOYSA-N-GYZSABJQRP-AAAAA'
        data2 = {'index': False, 'method': None, 'options': None, 'program': None,
                 'smiles': '[CH2+]C', 'version': None, 'tag': 'd2c9c0de-62b4-4cea-9298-f455ed98b819'}
        key3 = 'CREMABGTGYGIQB-UHFFFAOYSA-N-NQPRJIHSGQ-AAAAA'
        data3 = {'index': False, 'method': None, 'options': None, 'program': None,
                 'smiles': 'CO', 'version': None, 'tag': 'e1a6c4f0-5b25-4c8d-a5b2-12e2c1541c2f'}
        psycopg2db.db_cursor.executemany("INSERT INTO formulas (key, data) VALUES (%s, %s);",
                                         [(key1, Json(data1)), (key2, Json(data2)), (key3, Json(data3))])
        molecules_raw = list(psycopg2db.fetch_molecules_range('Formula',start_id=1,end_id=3,raw=True))
        assert molecules_raw == [(key2, data2), (key3, data3)]

    def test_fetch_flasks_range(self, psycopg2db):
        key1 = 'CREMABGTGYGIQB-UHFFFAOYSA-N-NQPRJIHSGQ-AAAAA'
        data1 = {'smiles': 'C.C', 'index': False, 'program': None, 'version': None,
                 'method': None, 'options': None, 'tag': '53d01b6a-0837-4d61-bb71-3db3d076df90'}
        key2 = 'NZNCTFHRZHZECW-UHFFFAOYSA-O-DXRORDCSRN-AAAAA'
        data2 = {'smiles': 'C.[CH3-].[H+]', 'index': False, 'program': None, 'version': None,
                 'method': None, 'options': None, 'tag': 'c02fb084-d8ab-4e4e-b1de-b5d1e9b78f86'}
        key3 = 'OTMSDBZUPAUEDD-UHFFFAOYSA-N-PGWBAZAUGG-AAAAA'
        data3 = {'smiles': 'CC', 'index': False, 'program': None, 'version': None,
                 'method': None, 'options': None, 'tag': '33271b52-2ecc-4875-9e08-5df161d387f3'}
        psycopg2db.db_cursor.executemany("INSERT INTO superformulas (key, data) VALUES (%s, %s);",
                                         [(key1, Json(data1)), (key2, Json(data2)), (key3, Json(data3))])
        flasks = list(psycopg2db.fetch_flasks_range('Superformula',start_id=1,end_id=3))
        assert flasks == [Superformula(smiles='C.[CH3-].[H+]', index=False),
                          Superformula(smiles='CC', index=False)]

    def test_fetch_flasks_range_raw(self, psycopg2db):
        key1 = 'CREMABGTGYGIQB-UHFFFAOYSA-N-NQPRJIHSGQ-AAAAA'
        data1 = {'smiles': 'C.C', 'index': False, 'program': None, 'version': None,
                 'method': None, 'options': None, 'tag': '53d01b6a-0837-4d61-bb71-3db3d076df90'}
        key2 = 'NZNCTFHRZHZECW-UHFFFAOYSA-O-DXRORDCSRN-AAAAA'
        data2 = {'smiles': 'C.[CH3-].[H+]', 'index': False, 'program': None, 'version': None,
                 'method': None, 'options': None, 'tag': 'c02fb084-d8ab-4e4e-b1de-b5d1e9b78f86'}
        key3 = 'OTMSDBZUPAUEDD-UHFFFAOYSA-N-PGWBAZAUGG-AAAAA'
        data3 = {'smiles': 'CC', 'index': False, 'program': None, 'version': None,
                 'method': None, 'options': None, 'tag': '33271b52-2ecc-4875-9e08-5df161d387f3'}
        psycopg2db.db_cursor.executemany("INSERT INTO superformulas (key, data) VALUES (%s, %s);",
                                         [(key1, Json(data1)), (key2, Json(data2)), (key3, Json(data3))])
        flasks_raw = list(psycopg2db.fetch_flasks_range('Superformula',start_id=1,end_id=3,raw=True))
        assert flasks_raw == [(2, key2, data2), (3, key3, data3)]

    def test_fetch_transformations_range(self, psycopg2db):
        ruleid1 = 0
        rule1data = {'rule': '[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2]'}
        key1 = 'COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF-AAAAA'
        id1 = 1
        key1data = {'smiles': 'CO.CO', 'index': False, 'program': None, 'version': None,
                 'method': None, 'options': None, 'tag': '4f6a9b4d-5d7b-4ac7-bae1-04d29a4b9ec1'}
        key2 = 'NPRPHDSQZBFINE-UHFFFAOYSA-O-DVBOZSIUCD-AAAAA'
        id2 = 2
        key2data = {'smiles': 'CO.C[O-].[H+]', 'index': False, 'program': None, 'version': None,
                 'method': None, 'options': None, 'tag': 'f8c7a77a-6b9f-4b92-b619-3cb65030cb23'} 
        data1 = {'index': True, 'tag': '44d51bdb-d0db-410f-95ac-62927fb632e3'}
        ruleid2 = 1
        rule2data = {'rule': '[O+0:1]-[H+0:2]>>[O-1:1].[H+1:2]'}
        key3 = 'DGPBUJDVYFAAJG-UHFFFAOYSA-O-SHIORNWQYZ-AAAAA'
        id3 = 3
        key3data = {'smiles': 'O.[CH2-]CO.[H+]', 'index': False, 'program': None, 'version': None,
                 'method': None, 'options': None, 'tag': 'a2b0e315-cd4d-49b9-bd3e-12d05e10e5be'}
        key4 = 'JKFINNLPWXKAFG-UHFFFAOYSA-P-JECILUDYKV-AAAAA'
        id4 = 4
        key4data = {'smiles': 'O.[CH2-]C[O-].[H+].[H+]', 'index': False, 'program': None, 'version': None,
                 'method': None, 'options': None, 'tag': 'd9354c32-b4fa-45eb-99f3-507de84fa0a5'}
        data2 = {'index': True, 'tag': 'f6b1c834-1e5f-4b07-9e49-9b3c57e8822d'}
        ruleid3 = 2
        rule3data = {'rule': '[O+1:1]-[H+0:2]>>[O+0:1].[H+1:2]'}
        key5 = 'MEAIUODGYOPHBA-UHFFFAOYSA-P-XSHLRVETKP-AAAAA'
        id5 = 5
        key5data = {'smiles': '[CH-]=[CH-].[OH3+].[OH3+]', 'index': False, 'program': None, 'version': None,
                 'method': None, 'options': None, 'tag': 'c73e4975-3301-451f-8303-e7d4c9b35184'}
        key6 = 'MEAIUODGYOPHBA-UHFFFAOYSA-P-TRAFAUYQXH-AAAAA'
        id6 = 6
        key6data = {'smiles': 'O.[CH-]=[CH-].[H+].[OH3+]', 'index': False, 'program': None, 'version': None,
                 'method': None, 'options': None, 'tag': '8e61991f-e0a0-49ad-9d3f-dcd94db6e5db'}
        data3 = {'index': True, 'tag': 'a3e0c8c5-7f3f-4c60-8d1e-e5009e3e25bc'}
        psycopg2db.db_cursor.executemany("INSERT INTO superformulas (key, data) VALUES (%s, %s);",
                                         [(key1, Json(key1data)), (key2, Json(key2data)), (key3, Json(key3data)),
                                          (key4, Json(key4data)), (key5, Json(key5data)), (key6, Json(key6data))])
        psycopg2db.db_cursor.executemany("INSERT INTO rules (id, data) VALUES (%s, %s);",
                                     [(ruleid1, Json(rule1data)),(ruleid2, Json(rule2data)),(ruleid3, Json(rule3data))])
        psycopg2db.db_cursor.executemany("INSERT INTO transformations (id1, id2, ruleid, data) VALUES (%s, %s, %s, %s);",
                                     [(id1, id2, ruleid1, Json(data1)),(id3, id4, ruleid2, Json(data2)),(id5, id6, ruleid3, Json(data3))])
        transformations = list(psycopg2db.fetch_transformations_range(start_id=1,end_id=3))
        assert transformations == [Transformation(reactantsmiles='O.[CH2-]CO.[H+]',productsmiles='O.[CH2-]C[O-].[H+].[H+]',
                                                  rule='[O+0:1]-[H+0:2]>>[O-1:1].[H+1:2]', index=True),
                                                  Transformation(reactantsmiles='[CH-]=[CH-].[OH3+].[OH3+]',productsmiles='O.[CH-]=[CH-].[H+].[OH3+]',
                                                  rule='[O+1:1]-[H+0:2]>>[O+0:1].[H+1:2]', index=True)]

    def test_fetch_transformations_range_raw(self, psycopg2db):
        ruleid1 = 0
        rule1data = {'rule': '[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2]'}
        key1 = 'COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF-AAAAA'
        id1 = 1
        key1data = {'smiles': 'CO.CO', 'index': False, 'program': None, 'version': None,
                 'method': None, 'options': None, 'tag': '4f6a9b4d-5d7b-4ac7-bae1-04d29a4b9ec1'}
        key2 = 'NPRPHDSQZBFINE-UHFFFAOYSA-O-DVBOZSIUCD-AAAAA'
        id2 = 2
        key2data = {'smiles': 'CO.C[O-].[H+]', 'index': False, 'program': None, 'version': None,
                 'method': None, 'options': None, 'tag': 'f8c7a77a-6b9f-4b92-b619-3cb65030cb23'} 
        data1 = {'index': True, 'tag': '44d51bdb-d0db-410f-95ac-62927fb632e3'}
        ruleid2 = 1
        rule2data = {'rule': '[O+0:1]-[H+0:2]>>[O-1:1].[H+1:2]'}
        key3 = 'DGPBUJDVYFAAJG-UHFFFAOYSA-O-SHIORNWQYZ-AAAAA'
        id3 = 3
        key3data = {'smiles': 'O.[CH2-]CO.[H+]', 'index': False, 'program': None, 'version': None,
                 'method': None, 'options': None, 'tag': 'a2b0e315-cd4d-49b9-bd3e-12d05e10e5be'}
        key4 = 'JKFINNLPWXKAFG-UHFFFAOYSA-P-JECILUDYKV-AAAAA'
        id4 = 4
        key4data = {'smiles': 'O.[CH2-]C[O-].[H+].[H+]', 'index': False, 'program': None, 'version': None,
                 'method': None, 'options': None, 'tag': 'd9354c32-b4fa-45eb-99f3-507de84fa0a5'}
        data2 = {'index': True, 'tag': 'f6b1c834-1e5f-4b07-9e49-9b3c57e8822d'}
        ruleid3 = 2
        rule3data = {'rule': '[O+1:1]-[H+0:2]>>[O+0:1].[H+1:2]'}
        key5 = 'MEAIUODGYOPHBA-UHFFFAOYSA-P-XSHLRVETKP-AAAAA'
        id5 = 5
        key5data = {'smiles': '[CH-]=[CH-].[OH3+].[OH3+]', 'index': False, 'program': None, 'version': None,
                 'method': None, 'options': None, 'tag': 'c73e4975-3301-451f-8303-e7d4c9b35184'}
        key6 = 'MEAIUODGYOPHBA-UHFFFAOYSA-P-TRAFAUYQXH-AAAAA'
        id6 = 6
        key6data = {'smiles': 'O.[CH-]=[CH-].[H+].[OH3+]', 'index': False, 'program': None, 'version': None,
                 'method': None, 'options': None, 'tag': '8e61991f-e0a0-49ad-9d3f-dcd94db6e5db'}
        data3 = {'index': True, 'tag': 'a3e0c8c5-7f3f-4c60-8d1e-e5009e3e25bc'}
        psycopg2db.db_cursor.executemany("INSERT INTO superformulas (key, data) VALUES (%s, %s);",
                                         [(key1, Json(key1data)), (key2, Json(key2data)), (key3, Json(key3data)),
                                          (key4, Json(key4data)), (key5, Json(key5data)), (key6, Json(key6data))])
        psycopg2db.db_cursor.executemany("INSERT INTO rules (id, data) VALUES (%s, %s);",
                                     [(ruleid1, Json(rule1data)),(ruleid2, Json(rule2data)),(ruleid3, Json(rule3data))])
        psycopg2db.db_cursor.executemany("INSERT INTO transformations (id1, id2, ruleid, data) VALUES (%s, %s, %s, %s);",
                                     [(id1, id2, ruleid1, Json(data1)),(id3, id4, ruleid2, Json(data2)),(id5, id6, ruleid3, Json(data3))])
        transformations = list(psycopg2db.fetch_transformations_range(start_id=1,end_id=3,raw=True))
        assert transformations == [(key3, key4, id3, id4, key3data['smiles'], key4data['smiles'], rule2data, data2),
                                   (key5, key6, id5, id6, key5data['smiles'], key6data['smiles'], rule3data, data3)]

    def test_fetch_rules_range(self, psycopg2db):
        ruleid1 = 0
        rule1data = {'rule': '[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2]'}
        ruleid2 = 1
        rule2data = {'rule': '[O+0:1]-[H+0:2]>>[O-1:1].[H+1:2]'}
        ruleid3 = 2
        rule3data = {'rule': '[O+1:1]-[H+0:2]>>[O+0:1].[H+1:2]'}
        psycopg2db.db_cursor.executemany("INSERT INTO rules (id, data) VALUES (%s, %s);",
                                     [(ruleid1, Json(rule1data)),(ruleid2, Json(rule2data)),(ruleid3, Json(rule3data))])
        rules = list(psycopg2db.fetch_rules_range(start_id=1,end_id=3))
        assert rules == [Rule(rule='[O+0:1]-[H+0:2]>>[O-1:1].[H+1:2]', ruleid=ruleid2), Rule(rule='[O+1:1]-[H+0:2]>>[O+0:1].[H+1:2]', ruleid=ruleid3)]

    def test_fetch_rules_range_raw(self, psycopg2db):
        ruleid1 = 0
        rule1data = {'rule': '[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2]'}
        ruleid2 = 1
        rule2data = {'rule': '[O+0:1]-[H+0:2]>>[O-1:1].[H+1:2]'}
        ruleid3 = 2
        rule3data = {'rule': '[O+1:1]-[H+0:2]>>[O+0:1].[H+1:2]'}
        psycopg2db.db_cursor.executemany("INSERT INTO rules (id, data) VALUES (%s, %s);",
                                     [(ruleid1, Json(rule1data)),(ruleid2, Json(rule2data)),(ruleid3, Json(rule3data))])
        rules = list(psycopg2db.fetch_rules_range(start_id=1,end_id=3,raw=True))
        assert rules == [(ruleid2, rule2data),(ruleid3, rule3data)]

    def test_fetch_all_molecules(self, psycopg2db):
        key1 = 'OTMSDBZUPAUEDD-UHFFFAOYSA-N-PGWBAZAUGG-AAAAA'
        data1 = {'index': False, 'method': None, 'options': None, 'program': None,
                 'smiles': 'CC', 'version': None, 'tag': 'd4590140-2e92-44f1-87b2-de6334bd7ce8'}
        key2 = 'YQHHCMVUMULAPZ-UHFFFAOYSA-N-GYZSABJQRP-AAAAA'
        data2 = {'smiles': '[CH2+]C', 'index': False, 'program': None, 'version': None,
                 'method': None, 'options': None, 'tag': 'd2c9c0de-62b4-4cea-9298-f455ed98b819'}
        psycopg2db.db_cursor.executemany("INSERT INTO formulas (key, data) VALUES (%s, %s);",
                                         [(key1, Json(data1)), (key2, Json(data2))])
        molecules = list(psycopg2db.fetch_all_molecules('Formula'))
        assert molecules == [Formula(smiles='CC', index=False), Formula(smiles='[CH2+]C', index=False)]

    def test_fetch_all_molecules_raw(self, psycopg2db):
        key1 = 'OTMSDBZUPAUEDD-UHFFFAOYSA-N-PGWBAZAUGG-AAAAA'
        data1 = {'index': False, 'method': None, 'options': None, 'program': None,
                 'smiles': 'CC', 'version': None, 'tag': 'd4590140-2e92-44f1-87b2-de6334bd7ce8'}
        key2 = 'YQHHCMVUMULAPZ-UHFFFAOYSA-N-GYZSABJQRP-AAAAA'
        data2 = {'smiles': '[CH2+]C', 'index': False, 'program': None, 'version': None,
                 'method': None, 'options': None, 'tag': 'd2c9c0de-62b4-4cea-9298-f455ed98b819'}
        psycopg2db.db_cursor.executemany("INSERT INTO formulas (key, data) VALUES (%s, %s);",
                                         [(key1, Json(data1)), (key2, Json(data2))])
        molecules = list(psycopg2db.fetch_all_molecules('Formula', raw=True))
        assert molecules == [(key1, data1), (key2, data2)]

    def test_fetch_all_flasks(self, psycopg2db):
        key1 = 'CREMABGTGYGIQB-UHFFFAOYSA-N-NQPRJIHSGQ-AAAAA'
        data1 = {'smiles': 'C.C', 'index': False, 'program': None, 'version': None,
                 'method': None, 'options': None, 'tag': '53d01b6a-0837-4d61-bb71-3db3d076df90'}
        key2 = 'NZNCTFHRZHZECW-UHFFFAOYSA-O-DXRORDCSRN-AAAAA'
        data2 = {'smiles': 'C.[CH3-].[H+]', 'index': False, 'program': None, 'version': None,
                 'method': None, 'options': None, 'tag': 'c02fb084-d8ab-4e4e-b1de-b5d1e9b78f86'}
        psycopg2db.db_cursor.executemany("INSERT INTO superformulas (key, data) VALUES (%s, %s);",
                                         [(key1, Json(data1)), (key2, Json(data2))])
        flasks = list(psycopg2db.fetch_all_flasks('Superformula'))
        assert flasks == [Superformula(smiles='C.C', index=False),
                          Superformula(smiles='C.[CH3-].[H+]', index=False)]

    def test_fetch_all_flasks_raw(self, psycopg2db):
        key1 = 'CREMABGTGYGIQB-UHFFFAOYSA-N-NQPRJIHSGQ-AAAAA'
        data1 = {'smiles': 'C.C', 'index': False, 'program': None, 'version': None,
                 'method': None, 'options': None, 'tag': '53d01b6a-0837-4d61-bb71-3db3d076df90'}
        key2 = 'NZNCTFHRZHZECW-UHFFFAOYSA-O-DXRORDCSRN-AAAAA'
        data2 = {'smiles': 'C.[CH3-].[H+]', 'index': False, 'program': None, 'version': None,
                 'method': None, 'options': None, 'tag': 'c02fb084-d8ab-4e4e-b1de-b5d1e9b78f86'}
        psycopg2db.db_cursor.executemany("INSERT INTO superformulas (key, data) VALUES (%s, %s);",
                                         [(key1, Json(data1)), (key2, Json(data2))])
        flasks_raw = list(psycopg2db.fetch_all_flasks('Superformula', raw=True))
        assert flasks_raw == [(1, key1, data1), (2, key2, data2)]

    def test_fetch_all_transformations(self, psycopg2db):
        ruleid = 10
        ruledata = {'rule': '[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2]'}
        key1 = 'COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF-AAAAA'
        id1 = 1
        key1data = {'smiles': 'CO.CO', 'index': False, 'program': None, 'version': None,
                 'method': None, 'options': None, 'tag': '4f6a9b4d-5d7b-4ac7-bae1-04d29a4b9ec1'}
        key2 = 'NPRPHDSQZBFINE-UHFFFAOYSA-O-DVBOZSIUCD-AAAAA'
        id2 = 2
        key2data = {'smiles': 'CO.C[O-].[H+]', 'index': False, 'program': None, 'version': None,
                 'method': None, 'options': None, 'tag': 'f8c7a77a-6b9f-4b92-b619-3cb65030cb23'} 
        data = {'index': True, 'tag': '368e53f9-8e50-4fb3-a9b1-f40d31a24cf5'}
        psycopg2db.db_cursor.executemany("INSERT INTO superformulas (key, data) VALUES (%s, %s);",
                                         [(key1, Json(key1data)), (key2, Json(key2data))])
        psycopg2db.db_cursor.execute("INSERT INTO rules (id, data) VALUES (%s, %s);",
                                     (ruleid, Json(ruledata)))
        psycopg2db.db_cursor.execute("INSERT INTO transformations (id1, id2, ruleid, data) VALUES (%s, %s, %s, %s);",
                                     (id1, id2, ruleid, Json(data)))
        transformations = list(psycopg2db.fetch_all_transformations())
        assert transformations == [Transformation(reactantsmiles='CO.CO |atomProp:0.p.0:1.p.1:2.p.2:3.p.3|',
                                                  productsmiles='CO.C[O-].[H+] |atomProp:0.p.0:1.p.1:2.p.2:3.p.3|',
                                                  rule='[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2]', index=True)]

    def test_fetch_all_transformations_raw(self, psycopg2db):
        ruleid = 10
        ruledata = {'rule': '[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2]'}
        key1 = 'COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF-AAAAA'
        id1 = 1
        key1data = {'smiles': 'CO.CO', 'index': False, 'program': None, 'version': None,
                 'method': None, 'options': None, 'tag': '4f6a9b4d-5d7b-4ac7-bae1-04d29a4b9ec1'}
        key2 = 'NPRPHDSQZBFINE-UHFFFAOYSA-O-DVBOZSIUCD-AAAAA'
        id2 = 2
        key2data = {'smiles': 'CO.C[O-].[H+]', 'index': False, 'program': None, 'version': None,
                 'method': None, 'options': None, 'tag': 'f8c7a77a-6b9f-4b92-b619-3cb65030cb23'} 
        data = {'index': True, 'tag': '368e53f9-8e50-4fb3-a9b1-f40d31a24cf5'}
        psycopg2db.db_cursor.executemany("INSERT INTO superformulas (key, data) VALUES (%s, %s);",
                                         [(key1, Json(key1data)), (key2, Json(key2data))])
        psycopg2db.db_cursor.execute("INSERT INTO rules (id, data) VALUES (%s, %s);",
                                     (ruleid, Json(ruledata)))
        psycopg2db.db_cursor.execute("INSERT INTO transformations (id1, id2, ruleid, data) VALUES (%s, %s, %s, %s);",
                                     (id1, id2, ruleid, Json(data)))
        transformations_raw = list(psycopg2db.fetch_all_transformations(raw=True))
        assert transformations_raw == [(key1, key2, id1, id2, key1data['smiles'], key2data['smiles'], ruledata, data)]

    def test_fetch_all_rules(self, psycopg2db):
        ruleid = 10
        ruledata = {'rule': '[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2]'}
        psycopg2db.db_cursor.execute("INSERT INTO rules (id, data) VALUES (%s, %s);",
                                     (ruleid, Json(ruledata)))
        rules = list(psycopg2db.fetch_all_rules())
        assert rules == [Rule(rule='[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2]', ruleid=ruleid)]

    def test_fetch_all_rules_raw(self, psycopg2db):
        ruleid = 10
        ruledata = {'rule': '[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2]'}
        psycopg2db.db_cursor.execute("INSERT INTO rules (id, data) VALUES (%s, %s);",
                                     (ruleid, Json(ruledata)))
        rules = list(psycopg2db.fetch_all_rules(raw=True))
        assert rules == [(ruleid, ruledata)]

    def test_store_molecule(self, psycopg2db):
        mol = Formula('CC', tag='782cc1ce-70cb-48b7-bc2c-9e8c45029a0c')
        ret = psycopg2db.store_molecule('Formula', mol.key, mol)
        assert ret is True
        psycopg2db.db_cursor.execute("SELECT key, data FROM formulas;")
        key, data = psycopg2db.db_cursor.fetchone()
        assert key == 'OTMSDBZUPAUEDD-UHFFFAOYSA-N-PGWBAZAUGG-AAAAA'
        assert data == {'index': False, 'method': None, 'options': None, 'program': None,
                        'smiles': 'CC', 'stereo': False, 'version': None,
                        'tag': '782cc1ce-70cb-48b7-bc2c-9e8c45029a0c'}

    def test_store_flask(self, psycopg2db):
        flask = Superformula('C.C', tag='f82a8ec3-edc9-4c1a-a369-bdbbdc02230b')
        ret = psycopg2db.store_flask('Superformula', flask.key, flask)
        assert ret is True
        psycopg2db.db_cursor.execute("SELECT key, data FROM superformulas;")
        key, data = psycopg2db.db_cursor.fetchone()
        assert key == 'CREMABGTGYGIQB-UHFFFAOYSA-N-NQPRJIHSGQ-AAAAA'
        assert data == {'index': False, 'method': None, 'options': None, 'program': None,
                        'smiles': 'C.C', 'stereo': False, 'version': None,
                        'tag': 'f82a8ec3-edc9-4c1a-a369-bdbbdc02230b'}
            
    def test_store_transformation(self, psycopg2db):
        ruleid = 10
        ruledata = {'rule': '[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2]'}
        key1 = 'COTNUBDHGSIOTA-UHFFFAOYSA-N-BGEJERMAWF-AAAAA'
        id1 = 1
        key1data = {'smiles': 'CO.CO', 'index': False, 'program': None, 'version': None,
                 'method': None, 'options': None, 'tag': '4f6a9b4d-5d7b-4ac7-bae1-04d29a4b9ec1'}
        key2 = 'NPRPHDSQZBFINE-UHFFFAOYSA-O-DVBOZSIUCD-AAAAA'
        id2 = 2
        key2data = {'smiles': 'CO.C[O-].[H+]', 'index': False, 'program': None, 'version': None,
                 'method': None, 'options': None, 'tag': 'f8c7a77a-6b9f-4b92-b619-3cb65030cb23'} 
        psycopg2db.db_cursor.executemany("INSERT INTO superformulas (key, data) VALUES (%s, %s);",
                                         [(key1, Json(key1data)), (key2, Json(key2data))])
        psycopg2db.db_cursor.execute("INSERT INTO rules (id, data) VALUES (%s, %s);",
                                     (ruleid, Json(ruledata)))
        trans = Transformation('CO.CO |atomProp:0.p.0:1.p.1:2.p.2:3.p.3|',
                                'CO.C[O-].[H+] |atomProp:0.p.0:1.p.1:2.p.2:3.p.3|',
                                '[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2]',
                                index=True,
                                tag='d91190d1-1cd4-406f-a2cf-e96e5dffc21d')
        ret = psycopg2db.store_transformation(key1, key2, ruleid, trans)
        assert ret is True
        psycopg2db.db_cursor.execute("SELECT id1, id2, ruleid, data FROM transformations;")
        id1, id2, ruleid, data = psycopg2db.db_cursor.fetchone()
        assert id1 == 1
        assert id2 == 2
        assert ruleid == 10
        assert data == {'index': True, 'stereo': False,
                        'tag': 'd91190d1-1cd4-406f-a2cf-e96e5dffc21d'}

    def test_store_rule(self, psycopg2db):
        rule = Rule(rule='[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2]', ruleid=10,
                    tag='a2e14b6d-a20e-48e2-987e-5e531509313b')
        ret = psycopg2db.store_rule(rule.ruleid, rule)
        assert ret is True
        psycopg2db.db_cursor.execute("SELECT id, data FROM rules;")
        ruleid, data = psycopg2db.db_cursor.fetchone()
        assert ruleid == 10
        assert data == {'rule': '[O+0:1]-[#1+0:2]>>[O-1:1].[#1+1:2]',
                        'tag': 'a2e14b6d-a20e-48e2-987e-5e531509313b'}

    def test_clear_molecules(self, psycopg2db):
        key = 'OTMSDBZUPAUEDD-UHFFFAOYSA-N-PGWBAZAUGG-AAAAA'
        data = {'index': False, 'method': None, 'options': None, 'program': None,
                'smiles': 'CC', 'version': None, 'tag': '33271b52-2ecc-4875-9e08-5df161d387f3'}
        psycopg2db.db_cursor.execute("INSERT INTO formulas (key, data) VALUES (%s, %s);",
                                     (key, Json(data)))
        psycopg2db.clear_molecules('Formula')
        psycopg2db.db_cursor.execute("SELECT COUNT(*) FROM formulas;")
        result = psycopg2db.db_cursor.fetchone()
        assert result[0] == 0
