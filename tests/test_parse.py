import pytest
from colibri2.parse import parse_xyz, format_xyz, parse_mol, format_mol, format_mopac, \
    parse_mopac, parse_cml
from pytest import approx


class TestParseXyz:
    def test_H(self):
        assert parse_xyz("""\
1

h 0.0 0.0 0.0
""") == [{'el': 'h', 'idx': 0, 'x': 0.0, 'y': 0.0, 'z': 0.0}]

    def test_CH4(self):
        assert parse_xyz("""\
5

C          1.06001       -0.05185        0.06091
H          2.15221       -0.05185        0.06091
H          0.69595        0.75381        0.70222
H          0.69594       -1.01006        0.43798
H          0.69594        0.10071       -0.95746
""") == [{'el': 'C', 'idx': 0, 'x': approx(2.0031, abs=1e-4),
          'y': approx(-0.0980, abs=1e-4), 'z': approx(0.1151, abs=1e-4)},
         {'el': 'H', 'idx': 1, 'x': approx(4.0671, abs=1e-4),
          'y': approx(-0.0980, abs=1e-4), 'z': approx(0.1151, abs=1e-4)},
         {'el': 'H', 'idx': 2, 'x': approx(1.3152, abs=1e-4),
          'y': approx(1.4245, abs=1e-4), 'z': approx(1.3270, abs=1e-4)},
         {'el': 'H', 'idx': 3, 'x': approx(1.3151, abs=1e-4),
          'y': approx(-1.9087, abs=1e-4), 'z': approx(0.8277, abs=1e-4)},
         {'el': 'H', 'idx': 4, 'x': approx(1.3151, abs=1e-4),
          'y': approx(0.1903, abs=1e-4), 'z': approx(-1.8093, abs=1e-4)}]


class TestFormatXyz:
    def test_H(self):
        assert format_xyz([{'el': 'h', 'idx': 0, 'x': 0.0, 'y': 0.0, 'z': 0.0}]) == \
               """1

 h        0.00000        0.00000        0.00000
"""


class TestParseMol:
    def test_H(self):
        assert parse_mol("""\

     RDKit          3D

  1  0  0  0  0  0  0  0  0  0999 V2000
    0.0000    0.0000    0.0000 H   0  0  0  0  0 15  0  0  0  0  0  0
M  CHG  1   1   1
M  END
""") == ([{'el': 'H', 'formalCharge': 1, 'idx': 0, 'x': approx(0.0), 'y': approx(0.0),
           'z': approx(0.0)}], [], {'charge': 1, 'mult': 1})

    def test_H2O(self):
        assert parse_mol("""\

     RDKit          3D

  3  2  0  0  0  0  0  0  0  0999 V2000
    0.0000    0.0543    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0
    0.7667    0.6419    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0
   -0.7667    0.6419    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  1  0
  1  3  1  0
M  END
""") == ([{'el': 'O', 'idx': 0, 'x': 0.0, 'y': approx(0.1026121), 'z': 0.0},
          {'el': 'H', 'idx': 1, 'x': approx(1.448853), 'y': approx(1.213015), 'z': 0.0},
          {'el': 'H', 'idx': 2, 'x': approx(-1.448853), 'y': approx(1.213015), 'z': 0.0}],
         [{'at1': 0, 'at2': 1, 'idx': 0, 'type': 'SINGLE'},
          {'at1': 0, 'at2': 2, 'idx': 1, 'type': 'SINGLE'}],
         {'charge': 0, 'mult': 1})

    def test_NH2(self):
        assert parse_mol("""\

     RDKit          3D

  3  2  0  0  0  0  0  0  0  0999 V2000
    0.0000    0.0000    0.0059 N   0  0  0  0  0  2  0  0  0  0  0  0
    0.0000    0.8078    0.5888 H   0  0  0  0  0  0  0  0  0  0  0  0
    0.0000   -0.8078    0.5888 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  1  0
  1  3  1  0
M  RAD  1   1   2
M  END
    """) == ([{'el': 'N', 'idx': 0, 'spinMultiplicity': 2, 'x': 0.0, 'y': 0.0, 'z': approx(0.01114938)},
              {'el': 'H', 'idx': 1, 'x': 0.0, 'y': approx(1.526521), 'z': approx(1.112671)},
              {'el': 'H', 'idx': 2, 'x': 0.0, 'y': approx(-1.526521), 'z': approx(1.112671)}],
             [{'at1': 0, 'at2': 1, 'idx': 0, 'type': 'SINGLE'},
              {'at1': 0, 'at2': 2, 'idx': 1, 'type': 'SINGLE'}],
             {'charge': 0, 'mult': 2})

    def test_C(self):
        assert parse_mol("""\



  1  0  0  0  0  0  0  0  0  0999 V2000
    0.0000    0.0000    0.0000 C   0  0  0  0  0 15  0  0  0  0  0  0
M  END
""") == ([{'el': 'C', 'idx': 0, 'spinMultiplicity': 5, 'x': 0.0, 'y': 0.0, 'z': 0.0}], [],
         {'charge': 0, 'mult': 5})


class TestFormatMol:

    def test_H(self):
        assert format_mol([
            {'el': 'H', 'idx': 0, 'x': 0.0, 'y': 0.0, 'z': 0.0}
        ], [], {'charge': 0, 'mult': 1}) == """\

     RDKit          3D

  1  0  0  0  0  0  0  0  0  0999 V2000
    0.0000    0.0000    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0
M  END
"""

    def test_H2O(self):
        assert format_mol([{'el': 'O', 'idx': 0, 'x': 0.0, 'y': 0.1026121, 'z': 0.0},
                           {'el': 'H', 'idx': 1, 'x': 1.448853, 'y': 1.213015, 'z': 0.0},
                           {'el': 'H', 'idx': 2, 'x': -1.448853, 'y': 1.213015, 'z': 0.0}],
                          [{'at1': 0, 'at2': 1, 'idx': 0, 'type': 'SINGLE'},
                           {'at1': 0, 'at2': 2, 'idx': 1, 'type': 'SINGLE'}],
                          {'charge': 0, 'mult': 1}) == """\

     RDKit          3D

  3  2  0  0  0  0  0  0  0  0999 V2000
    0.0000    0.0543    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0
    0.7667    0.6419    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0
   -0.7667    0.6419    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  1  0
  1  3  1  0
M  END
"""

    def test_NH2(self):
        assert format_mol([{'el': 'N', 'idx': 0, 'spinMultiplicity': 2, 'x': 0.0, 'y': 0.0, 'z': 0.01114938},
                           {'el': 'H', 'idx': 1, 'x': 0.0, 'y': 1.526521, 'z': 1.112671},
                           {'el': 'H', 'idx': 2, 'x': 0.0, 'y': -1.526521, 'z': 1.112671}],
                          [{'at1': 0, 'at2': 1, 'idx': 0, 'type': 'SINGLE'},
                           {'at1': 0, 'at2': 2, 'idx': 1, 'type': 'SINGLE'}],
                          {'charge': 0, 'mult': 2}) == """\

     RDKit          3D

  3  2  0  0  0  0  0  0  0  0999 V2000
    0.0000    0.0000    0.0059 N   0  0  0  0  0  2  0  0  0  0  0  0
    0.0000    0.8078    0.5888 H   0  0  0  0  0  0  0  0  0  0  0  0
    0.0000   -0.8078    0.5888 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  1  0
  1  3  1  0
M  RAD  1   1   2
M  END
"""

    def test_H2O_determine_bonds(self):
        assert format_mol([{'el': 'O', 'idx': 0, 'x': 0.0, 'y': 0.1026121, 'z': 0.0},
                           {'el': 'H', 'idx': 1, 'x': 1.448853, 'y': 1.213015, 'z': 0.0},
                           {'el': 'H', 'idx': 2, 'x': -1.448853, 'y': 1.213015, 'z': 0.0}],
                          [], {'charge': 0, 'mult': 1},
                          determine_bonds=True) == """\

     RDKit          3D

  3  2  0  0  0  0  0  0  0  0999 V2000
    0.0000    0.0543    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0
    0.7667    0.6419    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0
   -0.7667    0.6419    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  1  0
  1  3  1  0
M  END
"""

    def test_C4H2N2O2_determine_bonds(self):
        assert format_mol(
            [{'el': 'O', 'idx': 0, 'x': 4.496981258627478, 'y': 2.185090317834581, 'z': 0.14210740456729265},
             {'el': 'C', 'idx': 1, 'x': 2.3527090250835023, 'y': 1.1565123882338177, 'z': 0.07407726408295043},
             {'el': 'N', 'idx': 2, 'x': 0.10941514261231708, 'y': 2.66489178086165, 'z': -0.04478650915219196},
             {'el': 'C', 'idx': 3, 'x': -2.1183829856374343, 'y': 1.6215739874892796, 'z': -0.1160291840482948},
             {'el': 'C', 'idx': 4, 'x': -2.400897041259911, 'y': -1.1663389640815562, 'z': -0.07596699020751549},
             {'el': 'N', 'idx': 5, 'x': -0.427456049376617, 'y': -2.632766436744044, 'z': 0.031369453667780024},
             {'el': 'C', 'idx': 6, 'x': 2.073029558647873, 'y': -1.602298781018716, 'z': 0.11376151269881672},
             {'el': 'O', 'idx': 7, 'x': 3.967668971136804, 'y': -3.037923717850793, 'z': 0.21731850432498212},
             {'el': 'H', 'idx': 8, 'x': -3.7837986192166233, 'y': 2.8030307605673563, 'z': -0.20541322974022225},
             {'el': 'H', 'idx': 9, 'x': -4.2696472058423005, 'y': -1.9919603079040318, 'z': -0.13643822619359747}],
            [], {'charge': 0, 'mult': 1}, determine_bonds=True) == """\

     RDKit          3D

 10 10  0  0  0  0  0  0  0  0999 V2000
    2.3797    1.1563    0.0752 O   0  0  0  0  0  0  0  0  0  0  0  0
    1.2450    0.6120    0.0392 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.0579    1.4102   -0.0237 N   0  0  0  0  0  0  0  0  0  0  0  0
   -1.1210    0.8581   -0.0614 C   0  0  0  0  0  0  0  0  0  0  0  0
   -1.2705   -0.6172   -0.0402 C   0  0  0  0  0  0  0  0  0  0  0  0
   -0.2262   -1.3932    0.0166 N   0  0  0  0  0  0  0  0  0  0  0  0
    1.0970   -0.8479    0.0602 C   0  0  0  0  0  0  0  0  0  0  0  0
    2.0996   -1.6076    0.1150 O   0  0  0  0  0  0  0  0  0  0  0  0
   -2.0023    1.4833   -0.1087 H   0  0  0  0  0  0  0  0  0  0  0  0
   -2.2594   -1.0541   -0.0722 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  2  0
  2  3  1  0
  2  7  1  0
  3  4  2  0
  4  5  1  0
  4  9  1  0
  5  6  2  0
  5 10  1  0
  6  7  1  0
  7  8  2  0
M  END
"""


class TestFormatMopac:

    @classmethod
    def setup_class(cls):
        cls.H = [{'el': 'H', 'x': 0.0, 'y': 0.0, 'z': 0.0}]

        cls.H2O = [{'el': 'O', 'idx': 0, 'x': 0.0000079, 'y': 0.1025478, 'z': 0.0},
                   {'el': 'H', 'idx': 1, 'x': 1.4489219, 'y': 1.2129982, 'z': 0.0},
                   {'el': 'H', 'idx': 2, 'x': -1.448953, 'y': 1.2130183, 'z': 0.0}]

        cls.NH2 = [{'el': 'N', 'idx': 0, 'x': 0.0, 'y': -0.0001634, 'z': 0.01120547},
                   {'el': 'H', 'idx': 1, 'x': 0.0, 'y': 1.52706261, 'z': 1.1131731},
                   {'el': 'H', 'idx': 2, 'x': 0.0, 'y': -1.5264533, 'z': 1.1127259}]

    def test_H(self):
        assert format_mopac(self.H, {'charge': 1, 'mult': 1}, "proton", 50, "eps=78.4") == """\
xyz disp eps=78.4 singlet charge=1 cycles=50
proton

H   0.00000 1  0.00000 1  0.00000 1
"""

    def test_H2O(self):
        assert format_mopac(self.H2O, {'charge': 0, 'mult': 1}, "water", 50, "eps=78.4") == """\
xyz disp eps=78.4 singlet charge=0 cycles=50
water

O   0.00000 1  0.05427 1  0.00000 1
H   0.76674 1  0.64189 1  0.00000 1
H  -0.76675 1  0.64190 1  0.00000 1
"""

    def test_NH2(self):
        assert format_mopac(self.NH2, {'charge': 0, 'mult': 2}, "amide", 50, "eps=78.4") == """\
xyz disp eps=78.4 doublet charge=0 uhf cycles=50
amide

N   0.00000 1 -0.00009 1  0.00593 1
H   0.00000 1  0.80809 1  0.58907 1
H   0.00000 1 -0.80776 1  0.58883 1
"""


class TestParseMopac:

    @pytest.fixture(scope="class")
    def inputs_path(self, request):
        return request.fspath / '..' / 'inputs'

    @pytest.fixture(scope="class")
    def mopac_output_H(self, inputs_path):
        path = inputs_path / 'H.mopout'
        return path.read_text('utf-8')

    @pytest.fixture(scope="class")
    def mopac_output_H2O(self, inputs_path):
        path = inputs_path / 'H2O.mopout'
        return path.read_text('utf-8')

    @pytest.fixture(scope="class")
    def mopac_output_NH2(self, inputs_path):
        path = inputs_path / 'NH2.mopout'
        return path.read_text('utf-8')

    def test_H(self, mopac_output_H):
        atoms, properties, flags = parse_mopac(mopac_output_H)
        assert atoms == [
            {'el': 'H', 'idx': 0, 'x': approx(0.0), 'y': approx(0.0), 'z': approx(0.0)}
        ]
        assert properties == {'charge': 1, 'mult': 1, 'energy': 0.0}
        assert flags == {'converged': True}

    def test_H2O(self, mopac_output_H2O):
        atoms, properties, flags = parse_mopac(mopac_output_H2O)
        assert atoms == [
            {'el': 'O', 'idx': 0, 'x': approx(0.0000079, abs=1e-7), 'y': approx(0.1025478), 'z': approx(0.0)},
            {'el': 'H', 'idx': 1, 'x': approx(1.4489219), 'y': approx(1.2129982), 'z': approx(0.0)},
            {'el': 'H', 'idx': 2, 'x': approx(-1.448953), 'y': approx(1.2130183), 'z': approx(0.0)}
        ]
        assert properties == {'charge': 0, 'mult': 1, 'energy': approx(-323.00305)}
        assert flags == {'converged': True}

    def test_NH2(self, mopac_output_NH2):
        atoms, properties, flags = parse_mopac(mopac_output_NH2)
        assert atoms == [
            {'el': 'N', 'idx': 0, 'x': approx(0.0), 'y': approx(-0.0001634, abs=1e-7), 'z': approx(0.01120547)},
            {'el': 'H', 'idx': 1, 'x': approx(0.0), 'y': approx(1.52706261), 'z': approx(1.1131731)},
            {'el': 'H', 'idx': 2, 'x': approx(0.0), 'y': approx(-1.5264533), 'z': approx(1.1127259)}
        ]
        assert properties == {'charge': 0, 'mult': 2, 'energy': approx(-211.71849)}


class TestParseCML:

    @pytest.fixture(scope="class")
    def inputs_path(self, request):
        return request.fspath / '..' / 'inputs'

    # OpenBabel output
    @pytest.fixture(scope="class")
    def cml_H(self, inputs_path):
        path = inputs_path / 'H.cml'
        return path.read_text('utf-8')

    @pytest.fixture(scope="class")
    def cml_H2O(self, inputs_path):
        path = inputs_path / 'H2O.cml'
        return path.read_text('utf-8')

    @pytest.fixture(scope="class")
    def cml_NH2(self, inputs_path):
        path = inputs_path / 'NH2.cml'
        return path.read_text('utf-8')

    def test_H(self, cml_H):
        assert parse_cml(cml_H) == (
            1, 1,
            [
                {'charge': 1, 'el': 'H', 'h_count': 0, 'idx': 0, 'x': 0.0, 'y': 0.0, 'z': 0.0}
            ],
            [])

    def test_H2O(self, cml_H2O):
        assert parse_cml(cml_H2O) == (
            0, 1,
            [
                {'el': 'O', 'h_count': 2, 'idx': 0, 'x': 0.0, 'y': 0.0, 'z': 0.1173},
                {'el': 'H', 'h_count': 0, 'idx': 1, 'x': 0.0, 'y': 0.7572, 'z': -0.4692},
                {'el': 'H', 'h_count': 0, 'idx': 2, 'x': 0.0, 'y': -0.7572, 'z': -0.4692}
            ],
            [
                {'at1': 0, 'at2': 1, 'idx': 0, 'type': 'SINGLE'},
                {'at1': 0, 'at2': 2, 'idx': 1, 'type': 'SINGLE'}
            ]
        )

    def test_NH2(self, cml_NH2):
        assert parse_cml(cml_NH2) == (
            0, 2,
            [
                {'charge': 0, 'el': 'N', 'mult': 2, 'h_count': 2, 'idx': 0, 'x': -0.0057, 'y': 0.4060, 'z': 0.0},
                {'charge': 0, 'el': 'H', 'h_count': 0, 'idx': 1, 'x': -0.8403, 'y': -0.2051, 'z': 0.0},
                {'charge': 0, 'el': 'H', 'h_count': 0, 'idx': 2, 'x': 0.8460, 'y': -0.2009, 'z': 0.0}
            ],
            [
                {'at1': 0, 'at2': 1, 'idx': 0, 'type': 'SINGLE'},
                {'at1': 0, 'at2': 2, 'idx': 1, 'type': 'SINGLE'}
            ]
        )
