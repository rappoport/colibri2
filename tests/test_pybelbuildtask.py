import pytest

from colibri2.config import LocalConfig
from colibri2.data import Formula, Configuration, FailedFormula
from colibri2.task import PybelBuildTask


class TestPybelBuildTask:

    @pytest.fixture(scope="class")
    def pybel_builder_task(self):
        config = LocalConfig()
        config.update({
            'CONFIGURATION__EMBED_METHOD': "UFF",
        })
        return PybelBuildTask(config)

    @pytest.fixture(scope="class")
    def pybel_builder_task_uff(self):
        config = LocalConfig()
        config.update({
            'CONFIGURATION__EMBED_METHOD': "UFF",
            'CONFIGURATION__FF_METHOD': "UFF",
            'CONFIGURATION__FF_ITERATIONS': 1000,
        })
        return PybelBuildTask(config)

    @pytest.fixture(scope="class")
    def pybel_builder_task_stereo(self):
        config = LocalConfig()
        config.update({
            'CONFIGURATION__EMBED_METHOD': "UFF",
            'CONFIGURATION__STEREO': True,
        })
        return PybelBuildTask(config)

    def test_H(self, pybel_builder_task):
        out = pybel_builder_task.process(Formula('[H+]'))
        assert type(out) is Configuration
        assert out.charge == 1
        assert out.mult == 1
        assert out.smiles == '[H+]'

    def test_H2O(self, pybel_builder_task):
        out = pybel_builder_task.process(Formula('O'))
        assert type(out) is Configuration
        assert out.charge == 0
        assert out.mult == 1
        assert out.smiles == 'O'

    def test_SO42m(self, pybel_builder_task):
        out = pybel_builder_task.process(Formula('O=S(=O)([O-])[O-]'))
        assert type(out) == Configuration
        assert out.charge == -2
        assert out.mult == 1
        assert out.smiles == 'O=S(=O)([O-])[O-]'

    def test_Clm(self, pybel_builder_task):
        out = pybel_builder_task.process(Formula('[Cl-]'))
        assert type(out) == Configuration
        assert out.charge == -1
        assert out.mult == 1
        assert out.smiles == '[Cl-]'

    def test_O2m(self, pybel_builder_task):
        out = pybel_builder_task.process(Formula('[O-2]'))
        assert type(out) == Configuration
        assert out.charge == -2
        assert out.mult == 1
        assert out.smiles == '[O-2]'

    def test_Nap(self, pybel_builder_task):
        out = pybel_builder_task.process(Formula('[Na+]'))
        assert type(out) == Configuration
        assert out.charge == 1
        assert out.mult == 1
        assert out.smiles == '[Na+]'

    def test_Brp(self, pybel_builder_task):
        out = pybel_builder_task.process(Formula('[Br+]'))
        assert type(out) == Configuration
        assert out.charge == 1
        assert out.mult == 3
        assert out.smiles == '[Br+]'

    def test_Mg2p(self, pybel_builder_task):
        out = pybel_builder_task.process(Formula('[Mg+2]'))
        assert type(out) == Configuration
        assert out.charge == 2
        assert out.mult == 1
        assert out.smiles == '[Mg+2]'

    def test_N2H62p(self, pybel_builder_task):
        out = pybel_builder_task.process(Formula('[NH3+][NH3+]'))
        assert type(out) == Configuration
        assert out.charge == 2
        assert out.mult == 1
        assert out.smiles == '[NH3+][NH3+]'

    def test_H3Op(self, pybel_builder_task):
        out = pybel_builder_task.process(Formula('[OH3+]'))
        assert type(out) == Configuration
        assert out.charge == 1
        assert out.mult == 1
        assert out.smiles == '[OH3+]'

    def test_C4H8O_remove_stereo(self, pybel_builder_task):
        out = pybel_builder_task.process(Formula('C=C[C@@H](C)O', stereo=True))
        assert type(out) == Configuration
        assert out.smiles == 'C=CC(C)O'

    def test_C4H8O_preserve_stereo(self, pybel_builder_task_stereo):
        out = pybel_builder_task_stereo.process(Formula('C=C[C@@H](C)O', stereo=True))
        assert type(out) == Configuration
        assert out.smiles == 'C=C[C@@H](C)O'

    def test_C4H9N5_ff_error_uff(self, pybel_builder_task_uff):
        out = pybel_builder_task_uff.process(Formula('NC1=C(N)C(N)=C(N)N1'))
        assert type(out) == Configuration
        assert out.smiles == 'NC1=C(N)C(N)=C(N)N1'
