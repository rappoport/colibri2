import os

import pytest

from colibri2.api import Network2Command
from colibri2.config import LocalConfig
from colibri2.result import Success


class TestNetwork2Command:

    @pytest.fixture(scope="function")
    def network2_command_plain(self):
        config = LocalConfig()
        config.update({'NETWORK2__INITIAL_FLASK': 'CC |atomProp:0.p.0:1.p.1|',
                       'WRITER__OUTPUT_PATH': '/tmp/network.graphml',
                       'REACTION__RULES': ['[C+0:1]-[C+0:2]>>[C+1:1].[C-1:2]',
                                           '[C+1:1].[C-1:2]>>[C+0:1]-[C+0:2]']})
        return Network2Command(config)

    @pytest.fixture(scope="function")
    def network2_command_index(self):
        config = LocalConfig()
        config.update({'NETWORK2__INITIAL_FLASK': 'CC |atomProp:0.p.0:1.p.1|',
                       'WRITER__OUTPUT_PATH': '/tmp/network.graphml',
                       'REACTION__RULES': ['[C+0:1]-[C+0:2]>>[C+1:1].[C-1:2]',
                                           '[C+1:1].[C-1:2]>>[C+0:1]-[C+0:2]'],
                       'REACTION__INDEX': True})
        return Network2Command(config)

    def teardown_method(self, method):
        if os.path.exists('/tmp/network.graphml'):
            os.remove('/tmp/network.graphml')

    def test_CC_plain(self, network2_command_plain):
        result = network2_command_plain.execute()
        assert result == Success
        assert len(network2_command_plain.queue) == 0
        assert len(network2_command_plain.flasks) == 2
        assert len(network2_command_plain.invalid_flasks) == 0
        assert len(network2_command_plain.transformations) == 2

    def test_CC_index(self, network2_command_index):
        result = network2_command_index.execute()
        assert result == Success
        assert len(network2_command_index.queue) == 0
        assert len(network2_command_index.flasks) == 3
        assert len(network2_command_index.invalid_flasks) == 0
        assert len(network2_command_index.transformations) == 4
