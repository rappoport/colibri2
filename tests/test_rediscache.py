import json

import pytest
from pytest_redis import factories

from colibri2.cache import RedisCache
from colibri2.config import LocalConfig
from colibri2.data import Formula, Superformula

redis_proc = factories.redis_proc()


@pytest.mark.cache
class TestRedisCache:

    @pytest.fixture(scope="function")
    def rediscache(self, redis_proc):
        config = LocalConfig()
        config.update({'CACHE__URL': f'redis://@{redis_proc.host}:{redis_proc.port}/0',
                       'CACHE__MOLECULE_STORES__FORMULA': 'formulas',
                       'CACHE__FLASK_STORES__SUPERFORMULA': 'superformulas'})
        return RedisCache(config)

    def test_get_molecule(self, rediscache):
        rediscache.redis.hset(
            'formulas',
            'OTMSDBZUPAUEDD-UHFFFAOYSA-N-PGWBAZAUGG-AAAAA',
            json.dumps(
                {'smiles': 'CC', 'index': False, 'program': None, 'stereo': False, 'version': None,
                 'method': None, 'options': None, 'tag': '8d3755c5-df6d-4386-a360-ac7945b93595'})
        )
        mol = rediscache.get_molecule('Formula', 'OTMSDBZUPAUEDD-UHFFFAOYSA-N-PGWBAZAUGG-AAAAA')
        assert mol == Formula('CC')

    def test_get_flask(self, rediscache):
        rediscache.redis.hset(
            'superformulas',
            'CREMABGTGYGIQB-UHFFFAOYSA-N-NQPRJIHSGQ-AAAAA',
            json.dumps(
                {'smiles': 'C.C', 'index': False, 'stereo': False, 'program': None, 'version': None,
                 'method': None, 'options': None, 'tag': '21b05b70-12be-4688-b6bc-d914a7d16e65'})
        )
        flask = rediscache.get_flask('Superformula', 'CREMABGTGYGIQB-UHFFFAOYSA-N-NQPRJIHSGQ-AAAAA')
        assert flask == Superformula('C.C', index=False)

    def test_set_molecule(self, rediscache):
        rediscache.set_molecule('Formula', 'OTMSDBZUPAUEDD-UHFFFAOYSA-N-PGWBAZAUGG-AAAAA',
                                Formula('CC', tag='0ea773cc-2006-4e09-8404-fb5d6a1a7a54'))
        result = json.loads(rediscache.redis.hget('formulas', 'OTMSDBZUPAUEDD-UHFFFAOYSA-N-PGWBAZAUGG-AAAAA'))
        assert result == {'smiles': 'CC', 'index': False, 'program': None, 'stereo': False, 'version': None,
                          'method': None, 'options': None, 'tag': '0ea773cc-2006-4e09-8404-fb5d6a1a7a54'}

    def test_set_flask(self, rediscache):
        rediscache.set_flask('Superformula', 'CREMABGTGYGIQB-UHFFFAOYSA-N-NQPRJIHSGQ-AAAAA',
                             Superformula('C.C', tag='60f246ca-4559-4945-ba62-67a5d4baf017'))
        result = json.loads(rediscache.redis.hget('superformulas', 'CREMABGTGYGIQB-UHFFFAOYSA-N-NQPRJIHSGQ-AAAAA'))
        assert result == {'index': False, 'stereo': False, 'method': None, 'options': None, 'program': None,
                          'smiles': 'C.C', 'version': None, 'tag': '60f246ca-4559-4945-ba62-67a5d4baf017'}

    def test_clear_molecules(self, rediscache):
        rediscache.redis.hset(
            'formulas',
            'OTMSDBZUPAUEDD-UHFFFAOYSA-N-PGWBAZAUGG-AAAAA',
            json.dumps(
                {'smiles': 'CC', 'index': False, 'stereo': False, 'program': None, 'version': None,
                 'method': None, 'options': None, 'tag': '9d26f216-1027-4158-876b-ed4d667a2acd'})
        )
        rediscache.clear_molecules('Formula')
        count = rediscache.redis.hlen('formulas')
        assert count == 0
