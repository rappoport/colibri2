FROM continuumio/anaconda

WORKDIR /app

COPY environment.yml .

ENV PATH=$PATH:/opt/conda/bin:/opt/conda/envs/colibri2/bin

RUN rm -f /opt/conda/pkgs

RUN --mount=type=cache,target=/opt/conda/pkgs \
     conda env create -f environment.yml \
    && echo "conda activate colibri2" >> ~/.bashrc

COPY . .

RUN pip3 install -e . \
    && cp docker/pytest-docker.ini pytest.ini
