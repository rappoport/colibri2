"""Exceptions and signal handlers for colibri2.

Exception Classes:
       (Exception)
      |
      + -- DataError
      |    |
      |    + -- MoleculeError
      |    + -- FlaskError
      |    + -- TransformationError
      |    + -- ReactionRuleError
      |
      + -- TaskError
      |    |
      |    + -- PreconditionError
      |    + -- ExecutionError
      |    + -- IncompleteError
      |    + -- ValidationError
      |
      + -- CommandError


Signal Handlers:
    timeout_handler
    interrupt_handler
"""

__all__ = ['DataError', 'MoleculeError', 'FlaskError', 'TransformationError',
           'ReactionRuleError', 'TaskError', 'PreconditionError', 'ExecutionError',
           'IncompleteError', 'ValidationError', 'CommandError']


class DataError(Exception):
    def __init__(self, input: str = None, message: str = None):
        self.input = input
        self.message = message

    def __str__(self):
        if self.input is not None:
            output = f"{self.input[0:32]}...: {self.message}"
        else:
            output = self.message
        return output


class MoleculeError(DataError):
    pass


class FlaskError(DataError):
    pass


class TransformationError(DataError):
    pass


class ReactionRuleError(DataError):
    pass


class TaskError(Exception):
    def __init__(self, message: str = None):
        self.message = message

    def __str__(self):
        return str(self.message)


class PreconditionError(TaskError):
    pass


class ExecutionError(TaskError):
    pass


class IncompleteError(TaskError):
    pass


class ValidationError(TaskError):
    pass


class CommandError(Exception):
    def __init__(self, message: str = None):
        self.message = message

    def __str__(self):
        return str(self.message)
