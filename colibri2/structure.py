"""Structure functions for colibri2.

Functions:
    canonicalize: Canonicalize (CX)SMILES string
    split_smiles: Split (CX)SMILES string into molecules
    combine_smiles: Combine (CX)SMILES strings into single (super)molecule
    is_smiles_aromatic: Check if (CX)SMILES contains aromatic atoms
    is_mol_aromatic: Check if MOL structure definition contains aromatic bonds
    aromatize_smiles: Convert (CX)SMILES from Kekule to aromatic form
    kekulize_smiles: Convert (CX)SMILES from aromatic to Kekule form
    cleanup_mol: Remove some known problems of MOL structure definitions
    aromatize_mol: Convert MOL structure definition from Kekule to aromatic form
    kekulize_mol: Convert MOL structure definition from aromatic to Kekule form
    enumerate_ez: Enumerate stereo double bond isomers
    enumerate_chiral: Enumerate tetrahedral chiral isomers
    enumerate_stereo: Enumerate stereo isomers
    adj_compress: Create compressed string representation of adjacency list
    is_connected: Determine using BFS if adjacency list is connected
    composition: Format atom composition dict as comma-separated string
    bond_compress: Create compressed string representation of list of bonds
    pathwayhash: Compute 14-10-1-10-5 hash key for a pathway from a list of keys
    merge_atoms: Merge atom definitions
"""

import copy
import itertools as it
from collections import deque, Counter
from typing import Any, Dict, Tuple
from typing import List

from rdkit import rdBase, Chem
from rdkit.Chem import rdDetermineBonds, MolSanitizeException

from .chemistry import Element
from .exceptions import MoleculeError
from .utils import controlhash, stringhash, invert_letter

__all__ = ['canonicalize', 'split_smiles', 'combine_smiles', 'is_smiles_aromatic', 'is_mol_aromatic',
           'aromatize_smiles', 'kekulize_smiles', 'cleanup_mol', 'remove_bonds', 'aromatize_mol',
           'kekulize_mol', 'enumerate_ez', 'enumerate_chiral', 'enumerate_stereo', 'adj_compress',
           'is_connected', 'composition', 'bond_compress', 'pathwayhash', 'merge_atoms']

# Shut up RDKit
rdBase.DisableLog('rdApp.warning')
rdBase.DisableLog('rdApp.error')

# Bond symbols for compressed representation
_BOND_SYMBOLS = {
    'SINGLE': '-',
    'DOUBLE': '=',
    'TRIPLE': '#',
    'AROMATIC': ':',
}


def canonicalize(smiles: str, index: bool = False, stereo: bool = False) -> str:
    """Canonicalize (CX)SMILES string.

    Args:
        smiles (str): Input (CX)SMILES string
        index (bool): Use permutation indices
        stereo (bool): Preserve stereochemical information

    Returns:
        Canonicalized (CX)SMILES string
    """

    # Get sanitization flags from the raw SMILES strings
    flags_list = []
    for smiles_frag in smiles.split(' ')[0].split('.'):
        flags = Chem.SANITIZE_ALL
        if not is_smiles_aromatic(smiles_frag):
            flags &= ~Chem.SANITIZE_SETAROMATICITY
        flags_list.append(flags)

    # Canonicalize
    rdmol_in = Chem.MolFromSmiles(smiles, sanitize=False)
    if rdmol_in is None:
        raise MoleculeError(message="invalid SMILES string")
    if not stereo:
        Chem.RemoveStereochemistry(rdmol_in)
    rdmol_non_h = Chem.RemoveHs(rdmol_in, sanitize=False)

    frags = Chem.GetMolFrags(rdmol_non_h, asMols=True, sanitizeFrags=False)
    if len(frags) != len(flags_list):
        raise MoleculeError(message="invalid fragment definition in SMILES string")

    # Preserve both Kekule and aromatic definitions
    for frag, flags in zip(frags, flags_list):
        error = Chem.SanitizeMol(frag, sanitizeOps=flags, catchErrors=True)
        if error != 0:
            raise MoleculeError(message="sanitization error")

    # Recombine the fragments
    rdmol_out = frags[0]
    for frag in frags[1:]:
        rdmol_out = Chem.CombineMols(rdmol_out, frag)

    # Add permutation indices if necessary
    if index:
        # Need to bring in canonical order before adding indices
        smiles_can = Chem.MolToCXSmiles(rdmol_out, canonical=True)
        rdmol_can = Chem.MolFromSmiles(smiles_can, sanitize=False)
        if rdmol_can is None:
            raise MoleculeError(message="invalid SMILES string")
        atoms = [atom for atom in rdmol_can.GetAtoms() if atom.GetSymbol() != 'H']
        indices = [atom.GetUnsignedProp('p') for atom in atoms if atom.HasProp('p')]
        if indices:
            if len(indices) != len(atoms):
                raise MoleculeError(message="invalid permutation indices in SMILES string")
        else:
            for p, atom in enumerate(atoms):
                atom.SetUnsignedProp('p', p)

        # Select automorphism with the minimal sequence of indices
        # (in lexicographic order)
        min_indices = None
        atoms = [atom for atom in rdmol_can.GetAtoms() if atom.GetSymbol() != 'H']
        for auto in rdmol_can.GetSubstructMatches(rdmol_can, uniquify=False):
            auto_atoms = [rdmol_can.GetAtomWithIdx(idx) for idx in auto]
            indices = tuple([atom.GetUnsignedProp('p') for atom in auto_atoms if atom.HasProp('p')])
            if min_indices is None or indices < min_indices:
                min_indices = indices
        for atom, idx in zip(atoms, min_indices):
            atom.SetUnsignedProp('p', idx)
        smiles_out = Chem.MolToCXSmiles(rdmol_can, canonical=True)

    # Remove indices and atomic properties
    else:
        smiles_out = Chem.MolToSmiles(rdmol_out, canonical=True)

    return smiles_out


def split_smiles(smiles: str, index: bool = False, stereo: bool = False) -> List[str]:
    """Split (CX)SMILES string into molecules.

    Args:
        smiles (str): Input (CX)SMILES string
        index (bool): Preserve permutation indices
        stereo (bool): Preserve stereochemical information

    Returns:
        List[str]: List of (CX)SMILES strings
    """

    if smiles is None or smiles == '':
        raise ValueError("empty input SMILES string")

    # Get sanitization flags from the raw SMILES strings
    flags_list = []
    for smiles_frag in smiles.split(' ')[0].split('.'):
        flags = Chem.SANITIZE_ALL
        if not is_smiles_aromatic(smiles_frag):
            flags &= ~Chem.SANITIZE_SETAROMATICITY
        flags_list.append(flags)

    rdmol_in = Chem.MolFromSmiles(smiles, sanitize=False)
    if rdmol_in is None:
        raise MoleculeError(message="invalid SMILES string")
    if not stereo:
        Chem.RemoveStereochemistry(rdmol_in)
    rdmol_non_h = Chem.RemoveHs(rdmol_in, sanitize=False)

    frags = Chem.GetMolFrags(rdmol_non_h, asMols=True, sanitizeFrags=False)
    if len(frags) != len(flags_list):
        raise MoleculeError(message="invalid fragment definition in SMILES string")

    # Sanitize fragments using flags
    for frag, flags in zip(frags, flags_list):
        error = Chem.SanitizeMol(frag, sanitizeOps=flags, catchErrors=True)
        if error != 0:
            raise MoleculeError(message="sanitization error")

    if index:
        smiles_list = [Chem.MolToCXSmiles(frag, canonical=True) for frag in frags]
    else:
        smiles_list = [Chem.MolToSmiles(frag, canonical=True) for frag in frags]
    smiles_list_can = [canonicalize(smiles_frag, index, stereo) for smiles_frag in smiles_list]

    return smiles_list_can


def combine_smiles(smiles_list: List[str], index: bool = False, stereo: bool = False) -> str:
    """Combine (CX)SMILES strings into single (super)molecule.

    Args:
        smiles_list  (List[str]): List of (CX)SMILES strings
        index (bool): Preserve permutation indices
        stereo (bool): Preserve stereochemical information

    Returns:
        str: Output (CX)SMILES string
    """

    if not smiles_list:
        raise ValueError("empty input list")

    smiles_list_can = [canonicalize(smiles, index, stereo) for smiles in smiles_list]

    # Sanitization flags
    flags_list = []
    for smiles_frag in smiles_list_can:
        flags = Chem.SANITIZE_ALL
        if not is_smiles_aromatic(smiles_frag):
            flags &= ~Chem.SANITIZE_SETAROMATICITY
        flags_list.append(flags)

    rdmol = Chem.MolFromSmiles(smiles_list_can[0], sanitize=False)
    if rdmol is None:
        raise MoleculeError(message="invalid SMILES string")
    error = Chem.SanitizeMol(rdmol, sanitizeOps=flags_list[0], catchErrors=True)
    if error != 0:
        raise MoleculeError(message="sanitization error")

    for smiles, flags in zip(smiles_list_can[1:], flags_list[1:]):
        rdmol_mol = Chem.MolFromSmiles(smiles, sanitize=False)
        if rdmol_mol is None:
            raise MoleculeError(message="invalid SMILES string")
        error = Chem.SanitizeMol(rdmol_mol, sanitizeOps=flags, catchErrors=True)
        if error != 0:
            raise MoleculeError(message="sanitization error")
        rdmol = Chem.CombineMols(rdmol, rdmol_mol)

    if index:
        smiles_out = Chem.MolToCXSmiles(rdmol, canonical=True)
    else:
        smiles_out = Chem.MolToSmiles(rdmol, canonical=True)

    return smiles_out


def is_smiles_aromatic(smiles: str) -> bool:
    """Check if (CX)SMILES string contains aromatic atoms.

    Args:
        smiles (str): Input (CX)SMILES string

    Returns:
        bool: True if aromatic atoms are found
    """

    rdmol = Chem.MolFromSmiles(smiles, sanitize=False)
    if rdmol is None:
        raise MoleculeError(message="invalid SMILES string")
    return any(at.GetIsAromatic() for at in rdmol.GetAtoms())


def is_mol_aromatic(coord: str) -> bool:
    """Check if MOL structure definition contains aromatic bonds.

    Args:
         coord (str): Input MOL structure definition

    Returns:
        bool: True if aromatic bonds are found
    """
    rdmol = Chem.MolFromMolBlock(coord, sanitize=False)

    return any(bd.GetBondType() == Chem.BondType.AROMATIC for bd in rdmol.GetBonds())


def aromatize_smiles(smiles: str, index: bool = False, stereo: bool = False) -> str:
    """Convert (CX)SMILES from Kekule to aromatic form.

    Args:
        smiles (str): Input (CX)SMILES string
        index (bool): Preserve permutation indices
        stereo (bool): Preserve stereochemical information

    Returns:
        str: Output (CX)SMILES string
    """

    rdmol = Chem.MolFromSmiles(smiles, sanitize=False)
    if rdmol is None:
        raise MoleculeError(message="invalid SMILES string")
    error = Chem.SanitizeMol(rdmol, sanitizeOps=Chem.SANITIZE_ALL, catchErrors=True)
    if error != 0:
        raise MoleculeError(message="sanitization error")

    if index:
        smiles_can = Chem.MolToCXSmiles(rdmol, canonical=True)
    else:
        smiles_can = Chem.MolToSmiles(rdmol, canonical=True)

    smiles_out = canonicalize(smiles_can, index, stereo)
    return smiles_out


def kekulize_smiles(smiles: str, index: bool = False, stereo: bool = False) -> str:
    """Convert (CX)SMILES from aromatic to Kekule form.

    The roundtrip conversion aromatize_smiles -> kekulize_smiles is not necessarily invariant.

    Args:
        smiles (str): Input (CX)SMILES string
        index (bool): Preserve permutation indices
        stereo (bool): Preserve stereochemical information

    Returns:
        str: Output (CX)SMILES string
    """

    rdmol = Chem.MolFromSmiles(smiles, sanitize=False)
    if rdmol is None:
        raise MoleculeError(message="invalid SMILES string")
    error = Chem.SanitizeMol(rdmol, sanitizeOps=(Chem.SANITIZE_ALL & ~Chem.SANITIZE_SETAROMATICITY),
                             catchErrors=True)
    if error != 0:
        raise MoleculeError(message="sanitization error")

    Chem.Kekulize(rdmol, clearAromaticFlags=True)

    if index:
        smiles_can = Chem.MolToCXSmiles(rdmol, canonical=True)
    else:
        smiles_can = Chem.MolToSmiles(rdmol, canonical=True)

    smiles_out = canonicalize(smiles_can, index, stereo)
    return smiles_out


def cleanup_mol(coord: str) -> str:
    """Re-perceive the bonds if MOL structure definitions do not pass sanitization.
    Targets MOL files generated by XTB using Wiberg bond orders.

    Args:
        coord (str): Input MOL structure definition

    Returns:
        str: Output MOL structure definition
    """

    rdmol = Chem.MolFromMolBlock(coord, sanitize=False)

    # Check for spurious rings and exocyclic aromatic bonds
    spurious_rings = False
    exo_aromatic = False
    for bd in rdmol.GetBonds():
        at1 = bd.GetBeginAtom()
        at2 = bd.GetEndAtom()
        type_ = bd.GetBondType()

        if type_ == Chem.BondType.AROMATIC and not bd.IsInRing():
            exo_aromatic = True

        # TODO (dr): Add fix for boron: additional bonds produce negative charge
        # val = at.GetTotalValence() + at.GetFormalCharge()
        val1 = at1.GetTotalValence() - at1.GetFormalCharge()
        val2 = at2.GetTotalValence() - at2.GetFormalCharge()

        req1 = Element(at1.GetSymbol()).valence
        req2 = Element(at2.GetSymbol()).valence

        if val1 == req1 + 1 and val2 == req2 + 1 and type_ == Chem.BondType.SINGLE:
            spurious_rings = True

    error = Chem.SanitizeMol(rdmol, sanitizeOps=Chem.SANITIZE_ALL, catchErrors=True)
    if spurious_rings or exo_aromatic or error != 0:
        charge = Chem.GetFormalCharge(rdmol)
        try:
            rdmol_out = Chem.Mol(rdmol)
            rdDetermineBonds.DetermineBonds(rdmol_out, useHueckel=False, charge=charge)
        except (ValueError, MolSanitizeException):
            try:
                rdmol_out = Chem.RWMol(rdmol)
                for bd in rdmol_out.GetBonds():
                    bd.SetBondType(Chem.BondType.SINGLE)
                rdDetermineBonds.DetermineBondOrders(rdmol_out, charge=charge)
            except (ValueError, MolSanitizeException):
                raise MoleculeError(message="cannot determine bonds")
        coord_out = Chem.MolToMolBlock(rdmol_out)
    else:
        coord_out = coord

    return coord_out


def remove_bonds(coord: str) -> str:
    """Remove all bond information from MOL structure definition.

    Args:
        coord (str): Input MOL structure definition

    Returns:
        str: Output MOL structure definition
    """

    rdmol = Chem.MolFromMolBlock(coord, sanitize=False)
    rdmol_out = Chem.RWMol(rdmol)
    bonds = [(bd.GetBeginAtomIdx(), bd.GetEndAtomIdx()) for bd in rdmol_out.GetBonds()]
    for begin_idx, end_idx in bonds:
        rdmol_out.RemoveBond(begin_idx, end_idx)
    coord_out = Chem.MolToMolBlock(rdmol_out)
    return coord_out


def aromatize_mol(coord: str) -> str:
    """Convert MOL structure definition from Kekule to aromatic form.

    Args:
        coord (str): Input MOL structure definition

    Returns:
        str: Output MOL structure definition
    """

    rdmol = Chem.MolFromMolBlock(coord, sanitize=False)
    error = Chem.SanitizeMol(rdmol, sanitizeOps=Chem.SANITIZE_ALL, catchErrors=True)
    if error != 0:
        raise MoleculeError(message="sanitization error")

    coord_out = Chem.MolToMolBlock(rdmol, kekulize=False)
    return coord_out


def kekulize_mol(coord: str) -> str:
    """Convert MOL structure definition from aromatic to Kekule form.

    Args:
        coord (str): Input MOL structure definition

    Returns:
        str: Output MOL structure definition
    """

    rdmol = Chem.MolFromMolBlock(coord, sanitize=False)
    error = Chem.SanitizeMol(rdmol, sanitizeOps=(Chem.SANITIZE_ALL & ~Chem.SANITIZE_SETAROMATICITY),
                             catchErrors=True)
    if error != 0:
        raise MoleculeError(message="sanitization error")

    Chem.Kekulize(rdmol, clearAromaticFlags=True)

    coord_out = Chem.MolToMolBlock(rdmol, kekulize=False)
    return coord_out


def enumerate_ez(smiles: str, index: bool = False) -> List[str]:
    """Enumerate stereo double bond isomers for (CX)SMILES.
    Existing E/Z information is replaced.

    Args:
        smiles (str): Input (CX)SMILES string
        index (bool): Preserve permutation indices

    Returns:
        List[str]: List of (CX)SMILES strings
    """

    rdmol_in = Chem.MolFromSmiles(smiles, sanitize=False)
    if rdmol_in is None:
        raise MoleculeError(message="invalid (CX)SMILES string")

    flags = Chem.SANITIZE_ALL
    if not is_smiles_aromatic(smiles):
        flags &= ~Chem.SANITIZE_SETAROMATICITY
    error = Chem.SanitizeMol(rdmol_in, sanitizeOps=flags, catchErrors=True)
    if error != 0:
        raise MoleculeError(message="sanitization error")

    # Determine stereo double bonds
    Chem.AssignStereochemistry(rdmol_in)
    Chem.FindPotentialStereoBonds(rdmol_in)
    indices = set()
    for bond in rdmol_in.GetBonds():
        if bond.IsInRing():
            continue
        if bond.GetStereo() == Chem.BondStereo.STEREONONE:
            continue
        if bond.GetBondType() != Chem.BondType.DOUBLE:
            raise ValueError(f"cannot process stereo bond in {smiles}")
        indices.add(bond.GetIdx())

    # No stereo double bonds
    if not indices:
        return [smiles]

    # Get all subsets of indices. Subset is assigned Z, complementary subset E
    isomers = set()
    for z_indices in it.chain.from_iterable(it.combinations(indices, n) for n in range(len(indices) + 1)):
        z_indices = set(z_indices)
        e_indices = indices - z_indices
        rdmol_iso = copy.deepcopy(rdmol_in)
        for idx_z in z_indices:
            bond = rdmol_iso.GetBondWithIdx(idx_z)
            bond.SetStereo(Chem.BondStereo.STEREOZ)
        for idx_e in e_indices:
            bond = rdmol_iso.GetBondWithIdx(idx_e)
            bond.SetStereo(Chem.BondStereo.STEREOE)

        # Needed for the SMILES string to reflect stereochemistry
        Chem.AssignStereochemistry(rdmol_iso)
        if index:
            smiles_iso = Chem.MolToCXSmiles(rdmol_iso, canonical=True)
        else:
            smiles_iso = Chem.MolToSmiles(rdmol_iso, canonical=True)
        isomers.add(smiles_iso)

    return list(sorted(isomers))


def enumerate_chiral(smiles: str, index: bool = False, enantiomers: bool = False) -> List[str]:
    """Enumerate stereoisomers due to tetrahedral centers.
    Existing chiral information is replaced.

    Args:
        smiles (str): Input (CX)SMILES string
        index (bool): Preserve permutation indices
        enantiomers (bool): Generate enantiomeric pairs

    Returns:
        List[str]: List of (CX)SMILES strings
    """

    rdmol_in = Chem.MolFromSmiles(smiles, sanitize=False)
    if rdmol_in is None:
        raise MoleculeError(message="invalid (CX)SMILES string")

    flags = Chem.SANITIZE_ALL
    if not is_smiles_aromatic(smiles):
        flags &= ~Chem.SANITIZE_SETAROMATICITY
    error = Chem.SanitizeMol(rdmol_in, sanitizeOps=flags, catchErrors=True)
    if error != 0:
        raise MoleculeError(message="sanitization error")

    # Determine stereo centers
    Chem.AssignStereochemistry(rdmol_in, flagPossibleStereoCenters=True)
    Chem.FindPotentialStereo(rdmol_in)
    indices = set()
    for atom in rdmol_in.GetAtoms():
        if atom.HasProp('_ChiralityPossible'):
            indices.add(atom.GetIdx())

    # No stereo centers
    if not indices:
        return [smiles]

    # Get all subsets of indices. Subset is assigned R, complementary subset S
    # If enantiomers = False, favor enantiomers with more R configurations
    isomers = set()
    subsets = set()
    for r_indices in it.chain.from_iterable(
            it.combinations(indices, r) for r in reversed(range(len(indices) + 1))):
        subsets.add(r_indices)
        s_indices = tuple([idx for idx in indices if idx not in r_indices])
        if not enantiomers and s_indices in subsets:
            continue
        rdmol_iso = copy.deepcopy(rdmol_in)
        for idx_r in r_indices:
            atom = rdmol_iso.GetAtomWithIdx(idx_r)
            atom.SetChiralTag(Chem.CHI_TETRAHEDRAL_CW)
        for idx_s in s_indices:
            atom = rdmol_iso.GetAtomWithIdx(idx_s)
            atom.SetChiralTag(Chem.CHI_TETRAHEDRAL_CCW)
        if index:
            smiles_iso = Chem.MolToCXSmiles(rdmol_iso, canonical=True)
        else:
            smiles_iso = Chem.MolToSmiles(rdmol_iso, canonical=True)
        isomers.add(smiles_iso)

    return list(sorted(isomers))


def enumerate_stereo(smiles: str, index: bool = False, enantiomers: bool = False) -> List[str]:
    """Enumerate stereoisomers (E/Z and tetrahedral centers) from (CX)SMILES string.
    Existing stereochemical information is replaced.

    Args:
        smiles (str): Input (CX)SMILES string
        index (bool): Preserve permutation indices
        enantiomers (bool): Generate enantiomeric pairs

    Returns:
        List[str]: List of (CX)SMILES strings
    """

    smiles_ez = enumerate_ez(smiles, index)
    smiles_stereo = [smi for smi_ez in smiles_ez for smi in enumerate_chiral(smi_ez, index, enantiomers)]

    return smiles_stereo


def adj_compress(adj: List[List[int]]) -> str:
    """Create compressed string representation of adjacency list.

    Args:
        adj (List[List[int]]): Adjacency list

    Returns:
        str: Compressed string representation
    """

    if len(adj) < 1:
        raise ValueError("empty adjacency list")
    return ';'.join(','.join(
        [str(m) for m in nlist if m < n]) for n, nlist in enumerate(adj))


def is_connected(adj: List[List[int]]) -> bool:
    """Determine using BFS if adjacency list is connected.

    Args:
        adj (List[List[int]]): Adjacency list

    Returns:
        bool: True if adjacency list is connected
    """

    # Number of nodes
    length = len(adj)

    # Consider single node or empty list as connected
    if length < 2:
        return True

    # Accumulate component nodes on comp using queue for BFS
    queue = deque([0])
    comp = set()
    while queue:
        n = queue.popleft()
        comp.add(n)
        for m in adj[n]:
            if m not in comp:
                queue.append(m)

    # If all nodes in comp, the adjacency list is connected
    return len(comp) == length


def composition(atoms: List[Tuple[int, str]]) -> str:
    """Format atom composition dict as comma-separated string.

    Args:
        atoms (List[Tuple[int, str]]): Atom list

    Returns:
        str: Sum formula representation
    """

    # Get atom counts
    counts = dict(Counter(at for _, at in atoms))

    # Text accumulator
    s = []

    # Put carbon first, then hydrogen
    if 'C' in counts:
        s.append(f"C {counts['C']}")
    if 'H' in counts:
        s.append(f"H {counts['H']}")

    # All other elements in alphabetic order
    for at in sorted(counts.keys()):
        if at in ['C', 'H']:
            continue
        s.append(f'{at} {counts[at]}')

    return ','.join(s)


def bond_compress(bonds: List[Tuple[int, int, str]]) -> str:
    """Create compressed string representation of list of bonds.

    Args:
        bonds (List[Tuple[int, int, str]]): List of bonds

    Returns:
        str: Compressed string representation
    """

    if len(bonds) < 1:
        return ''

    bonds_ordered = [(min(idx1, idx2), max(idx1, idx2), type_)
                     for idx1, idx2, type_ in bonds]
    bonds_sorted = sorted(bonds_ordered, key=lambda bd: (bd[0], bd[1]))
    return ";".join([f"{min(idx1, idx2)}{_BOND_SYMBOLS[type_]}{max(idx1, idx2)}"
                     for idx1, idx2, type_ in bonds_sorted])


def pathwayhash(keys: List[str]) -> str:
    """Compute 14-10-1-10-5 key for a pathway from a list of keys.

    NOTE: value of pathway_hash differs from colibri due to difference in hashing
    https://docs.python.org/3.3/using/cmdline.html#envvar-PYTHONHASHSEED

    Args:
        keys (List[str]): List of keys in 14-10-1-10-5 format

    Returns:
        str: 14-10-1-10-5 pathway key hash
    """

    if not keys:
        raise ValueError('empty input key list')

    # Only one key
    if len(keys) == 1:
        return keys[0]

    # More than one key. Ensure last key is larger or equal than first key
    invert = False
    if stringhash(keys[0]) > stringhash(keys[-1]):
        invert = True
        keys = reversed(keys)

    # Split keys into components
    c1, c2, c3, c4, c5 = zip(*[key.split('-') for key in keys])

    # Hash components in 14-10-1-10-5 pattern
    h1 = controlhash('>>'.join(c1), pos=14)
    h2 = controlhash('>>'.join(c2), pos=10)
    h3 = controlhash('>>'.join(c3), pos=1)
    h4 = controlhash('>>'.join(c4), pos=10)
    h5 = controlhash('>>'.join(c5), pos=5)

    # Invert h3 if necessary, join components
    return '-'.join([h1, h2, invert_letter(h3) if invert else h3, h4, h5])


def merge_atoms(atoms1: List[Dict[str, Any]], atoms2: List[Dict[str, Any]]) -> List[Dict[str, Any]]:
    """Merge atom definitions. In case of conflict, later definitions replace earlier ones.

    Args:
        atoms1 (List[Dict[str, Any]]): Atom definitions 1
        atoms2 (List[Dict[str, Any]]): Atom definitions 2

    Returns:
         List[Dict[str, Any]]: Output atom definitions
    """

    if len(atoms1) != len(atoms2):
        raise ValueError(f"atom lists differ in length {len(atoms1)} != {len(atoms2)}")

    elements1 = ','.join(atom['el'] for atom in atoms1)
    elements2 = ','.join(atom['el'] for atom in atoms2)
    if elements1 != elements2:
        raise ValueError(f"element definitions are incompatible {elements1} != {elements2}")

    atoms = []
    for atom1, atom2 in zip(atoms1, atoms2):
        atom = {**atom1, **atom2}
        atoms.append(atom)

    return atoms
