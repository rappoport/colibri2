"""Network streamwriter interface for colibri2.

Classes:
    StreamWriter: Abstract base class for network streamwriter interface
"""

from abc import ABC, abstractmethod
from typing import Any, Dict

from ..config import Config
from ..utils import ensure_logger

__all__ = ['StreamWriter']


class StreamWriter(ABC):
    """Abstract network writer interface."""

    def __init__(self, config: Config):
        self.config = config
        self.logger = ensure_logger("writer", self.config.LOGGING.LEVEL.upper())

    @abstractmethod
    def write_node(self, key: str, attributes: Dict[str, Any]):
        """Add single node with attributes.

        Args:
            key (str): Node key
            attributes (Dict[str, Any]): Node attributes
        """
        pass

    @abstractmethod
    def write_edge(self, from_: str, to: str, attributes: Dict[str, Any]):
        """Add single edge with attributes.

        Args:
            from_ (str): Predecessor node key
            to (str): Successor node key
            attributes (Dict[str, Any]): Edge attributes
        """
        pass

    @abstractmethod
    def write_header(self):
        """Add header to file.

        """
        pass

    @abstractmethod
    def write_footer(self):
        """Add footer to file.

        """
        pass
