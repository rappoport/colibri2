"""Direct writer to file.

Classes:
    GraphMLStreamWriter: Network writer to file
"""

from typing import Dict, Any
from bz2 import open as bzopen

from .streamwriter import StreamWriter
from ..config import Config

__all__ = ['GraphMLStreamWriter']


class GraphMLStreamWriter(StreamWriter):
    """Network writer to file."""

    def __init__(self, config: Config):
        super().__init__(config)

    def __del__(self):
        self.file.close()

    def open_file(self):
        if '.graphml' in self.config.WRITER.OUTPUT_PATH:
            if '.bz2' in self.config.WRITER.OUTPUT_PATH:
                self.file = bzopen(self.config.WRITER.OUTPUT_PATH, "wb")
            else:
                self.file = open(self.config.WRITER.OUTPUT_PATH, "wb")
        else:
            raise NotImplementedError("network output format is not .graphml")

    def write_node(self, key: str, attributes: Dict[str, Any]):
        """Add single node with attributes.

        Args:
            key(str): Node key
            attributes (Dict[str, Any]): Node attributes
        """
        self.file.write(('    <node id="' + key + '">\n').encode('utf8'))
        self.file.write(('      <data key="d0">' + attributes['smiles'] + '</data>\n').encode('utf8'))
        self.file.write(('    </node>\n').encode('utf8'))

    def write_edge(self, from_: str, to: str, attributes: Dict[str, Any]):
        """Add single edge with attributes.

        Args:
            from_ (str): Predecessor node key
            to (str): Successor node key
            attributes (Dict[str, Any]): Edge attributes
        """
        self.file.write(('    <edge source="' + from_ + '" target="' + to + '">\n').encode('utf8'))
        self.file.write(('      <data key="d1">' + attributes['rule'] + '</data>\n').encode('utf8'))
        self.file.write(('    </edge>\n').encode('utf8'))

    def write_header(self):
        """Add header to file.

        """
        self.file.write(("<?xml version='1.0' encoding='utf-8'?>\n").encode('utf8'))
        self.file.write((
            '<graphml xmlns="http://graphml.graphdrawing.org/xmlns" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://graphml.graphdrawing.org/xmlns http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd">\n').encode(
            'utf8'))
        self.file.write(('  <key id="d1" for="edge" attr.name="rule" attr.type="string" />\n').encode('utf8'))
        self.file.write(('  <key id="d0" for="node" attr.name="smiles" attr.type="string" />\n').encode('utf8'))
        self.file.write(('  <graph edgedefault="directed">\n').encode('utf8'))

    def write_footer(self):
        """Add footer to file.

        """
        self.file.write(('  </graph>\n').encode('utf8'))
        self.file.write(('</graphml>\n').encode('utf8'))
