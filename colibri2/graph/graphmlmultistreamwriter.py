"""Direct writer to file with multiple writers.

Classes:
    GraphMLMultiStreamWriter: Network writer to file for multiple writers
"""

from typing import Dict, Any
from bz2 import open as bzopen

from .graphmlstreamwriter import GraphMLStreamWriter
from ..config import Config

__all__ = ['GraphMLMultiStreamWriter']


class GraphMLMultiStreamWriter(GraphMLStreamWriter):
    """Network writer to file."""

    def __init__(self, config: Config):
        super().__init__(config)

    def open_file(self):
        if '.graphml' in self.config.WRITER.OUTPUT_PATH:
            if '.bz2' in self.config.WRITER.OUTPUT_PATH:
                self.file = bzopen(self.config.WRITER.OUTPUT_PATH.replace('.graphml.bz2',f"_{self.config.WRITER.WORKER_NUMBER}.graphml.bz2"), "wb")
            else:
                self.file = open(self.config.WRITER.OUTPUT_PATH.replace('.graphml',f"_{self.config.WRITER.WORKER_NUMBER}.graphml"), "wb")
        else:
            raise NotImplementedError("network output format is not .graphml")
