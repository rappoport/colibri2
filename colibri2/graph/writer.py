"""Network writer interface for colibri2.

Classes:
    Writer: Abstract base class for network writer interface
"""

from abc import ABC, abstractmethod
from typing import Any, Dict

from ..config import Config
from ..utils import ensure_logger

__all__ = ['Writer']


class Writer(ABC):
    """Abstract network writer interface."""

    def __init__(self, config: Config):
        self.config = config
        self.logger = ensure_logger("writer", self.config.LOGGING.LEVEL.upper())

    @abstractmethod
    def add_node(self, key: str, attributes: Dict[str, Any]):
        """Add single node with attributes.

        Args:
            key (str): Node key
            attributes (Dict[str, Any]): Node attributes
        """
        pass

    @abstractmethod
    def add_edge(self, from_: str, to: str, attributes: Dict[str, Any]):
        """Add single edge with attributes.

        Args:
            from_ (str): Predecessor node key
            to (str): Successor node key
            attributes (Dict[str, Any]): Edge attributes
        """
        pass

    @abstractmethod
    def write(self, format: str, path: str):
        """Write network to file.

        Args:
            format (str): Output format
            path (str): Output path
        """
        pass
