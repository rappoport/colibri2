"""Network writer interface using NetworkX library.

Classes:
    NetworkXWriter: Network writer using NetworkX
"""

from typing import Dict, Any

import networkx as nx

from .writer import Writer
from ..config import Config

__all__ = ['NetworkXWriter']


class NetworkXWriter(Writer):
    """Network writer using NetworkX library."""

    def __init__(self, config: Config):
        super().__init__(config)

        self.graph = nx.DiGraph()

    def add_node(self, key: str, attributes: Dict[str, Any]):
        """Add single node with attributes.

        Args:
            key(str): Node key
            attributes (Dict[str, Any]): Node attributes
        """
        self.graph.add_node(key, **attributes)

    def add_edge(self, from_: str, to: str, attributes: Dict[str, Any]):
        """Add single edge with attributes.

        Args:
            from_ (str): Predecessor node key
            to (str): Successor node key
            attributes (Dict[str, Any]): Edge attributes
        """
        self.graph.add_edge(from_, to, **attributes)

    def write(self, format: str, path: str):
        """Write network to GraphML file.

        Args:
            format (str): Output format
            path (str): Output path
        """
        if format == 'graphml':
            nx.write_graphml(self.graph, path)
        else:
            raise NotImplementedError(f"unknown network output format {format}")
