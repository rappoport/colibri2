"""Parse functions for colibri2.

Functions:
    parse_xyz: Parse coordinates in XYZ format
    format_xyz: Format coordinates in XYZ format
    parse_mol: Parse molecule definition in MOL format
    format_mol: Format molecule definition in MOL Format
    format_mopac: Format MOPAC program input
    parse_mopac: Parse MOPAC program output
    parse_xtb_opt: Parse XTB program output
    parse_cml: Parse molecule definition in CML format
"""

__all__ = ['parse_xyz', 'format_xyz', 'parse_mol', 'format_mol', 'format_mopac', 'parse_mopac',
           'parse_xtb_opt', 'parse_xtb_en', 'parse_cml']

import re
from collections import OrderedDict
from typing import List, Dict, Any, Tuple
from xml.etree import ElementTree as ET

from rdkit import Chem
from rdkit.Chem import Descriptors, rdDetermineBonds

from .exceptions import MoleculeError

# au (Bohr) -> Angstrom conversion
_AU2AA = 0.52917721092

# Spin states
_SPIN_STATES = ['', 'singlet', 'doublet', 'triplet', 'quartet',
                'quintet', 'sextet', 'septet', 'octet', 'nonet']

# Spin multiplicities
_SPIN_MULT = {'SINGLET': 1, 'DOUBLET': 2, 'TRIPLET': 3, 'QUARTET': 4,
              'QUINTET': 5, 'SEXTET': 6, 'SEPTET': 7, 'OCTET': 8,
              'NONET': 9}

# Bond order parameters for CML
# https://github.com/rdkit/rdkit/tree/master/Code/GraphMol/Bond.h
# https://github.com/openbabel/openbabel/blob/master/src/formats/xml/cmlformat.cpp
_BOND_TYPES = {
    # RDKit nomenclature
    'S': 'SINGLE',
    'D': 'DOUBLE',
    'T': 'TRIPLE',
    'A': 'AROMATIC',

    # OpenBabel nomenclature
    '1': 'SINGLE',
    '2': 'DOUBLE',
    '3': 'TRIPLE',
    '5': 'AROMATIC',
}

# Regex for XYZ atom labels
_RE_XYZ_ATOM = re.compile(r'^\s*[a-zA-z]+')

# Regex for floating-point numbers
_RE_XYZ_NUMBER = re.compile(r'-?\d+\.\d+')

# Regex for MOPAC no-op message
_RE_MOPAC_NO_OP = re.compile(r'^\s*GRADIENTS WERE INITIALLY ACCEPTABLY SMALL\s*$',
                             re.MULTILINE)

# Regex for MOPAC energy evaluation (no optimization)
_RE_MOPAC_NO_OPT = re.compile(r'^\s*NO PARAMETERS MARKED FOR OPTIMIZATION, SO 1SCF WAS USED\s*$',
                              re.MULTILINE)

# Regex for MOPAC convergence
_RE_MOPAC_CONV = re.compile(r'^\s*GEOMETRY OPTIMISED USING EIGENVECTOR FOLLOWING \(EF\)\.\s*$',
                            re.MULTILINE)

# Regex for MOPAC total energy in eV
_RE_MOPAC_TOTAL_E = re.compile(r'^\s*TOTAL ENERGY\s*=\s*(\S+)\s*EV\s*$', re.MULTILINE)

# Regex for floating-point numbers
_RE_NUMBER = r'[+-]?(?:\d+\.\d+|\d+|\.\d+)(?:[EDed][+-]?\d+)?'

# Regex for chemical element symbols
_RE_ELEMENT = r'[A-Z][a-z]?'

# Regex for molecular charge
_RE_MOPAC_CHARGE = re.compile(r'^\s*\*\s*CHARGE ON SYSTEM\s*=\s*([-+]?\d+)\s*$', re.MULTILINE)

# Regex for spin multiplicity
_RE_MOPAC_MULT = re.compile(r'^\s*(SINGLET|DOUBLET|TRIPLET|QUARTET' +
                            r'|QUINTET|SEXTET|SEPTET|OCTET|NONET) STATE CALCULATION\s*$',
                            re.MULTILINE)

# Regex for empty line
_RE_EMPTY_LINE = re.compile(r'^\s*$', re.MULTILINE)

# Regex for Cartesian coordinates (convergence)
_RE_MOPAC_COORD_TITLE_CONV = re.compile(r'^\s*CARTESIAN COORDINATES\s*$\n', re.MULTILINE)
_RE_MOPAC_COORD_XYZ_CONV = re.compile(r'^\s*\d+\s*(' + _RE_ELEMENT + r')\s+(' +
                                      _RE_NUMBER + r')\s+(' + _RE_NUMBER + r')\s+(' +
                                      _RE_NUMBER + r')\s*$', re.MULTILINE)

# Regex for Cartesian coordinates (no convergence)
_RE_MOPAC_COORD_TITLE_NOCONV = re.compile(r'^\s*CURRENT VALUE OF GEOMETRY\s*$\n.*\n\n\n',
                                          re.MULTILINE)
_RE_MOPAC_COORD_XYZ_NOCONV = re.compile(r'^\s*(' + _RE_ELEMENT + r')\s+(' + _RE_NUMBER +
                                        r')\s+\+1\s+(' + _RE_NUMBER + r')\s+\+1\s+(' +
                                        _RE_NUMBER + r')\s+\+1\s*$', re.MULTILINE)

# Regexes for XTB
_RE_XTB_CONV = re.compile(
    r'^\s*\*\*\* GEOMETRY OPTIMIZATION CONVERGED AFTER\s*\d+\s*ITERATIONS \*\*\*\s*$',
    re.MULTILINE)

_RE_XTB_TOTAL_E = re.compile(
    r'^\s*\| TOTAL ENERGY\s+(' + _RE_NUMBER + r') Eh\s+\|\s*$', re.MULTILINE)

_RE_XTB_POINT_GROUP = re.compile(
    r'^\s*(\S+) symmetry found \(for desy threshold:\s+' + _RE_NUMBER + r'\s*\) used in thermo\s*$',
    re.MULTILINE)

_RE_XTB_IMAG_FREQ = re.compile(
    r'^\s*found\s+\d\s+significant imaginary frequenc(?:y|ies)\s*$', re.MULTILINE)

_RE_XTB_FREE_E = re.compile(
    r'^\s*\| TOTAL FREE ENERGY\s+(' + _RE_NUMBER + r') Eh\s+\|\s*$', re.MULTILINE)

# Conversion factor for Hartree to eV (CODATA 2018)
_CONV_H_EV = 27.211386245988


def parse_xyz(xyz) -> List[Dict[str, Any]]:
    """Parse coordinates in XYZ format.

    Args:
        xyz (str): Input coordinates in XYZ format

    Returns:
        Atom definitions
    """

    # Skip the first two lines
    _, _, lines = xyz.split('\n', 2)

    atoms = []

    # Iterate over atoms
    for idx, line in enumerate(lines.splitlines()):
        m = _RE_XYZ_ATOM.match(line)
        if m:
            el = m.group(0).strip()
            x, y, z = [float(number) for number in _RE_XYZ_NUMBER.findall(line)]
        else:
            raise ValueError('Invalid XYZ coordinate input')

        atoms.append({
            'el': el,
            'idx': idx,
            'x': x / _AU2AA,
            'y': y / _AU2AA,
            'z': z / _AU2AA,
        })

    return atoms


def format_xyz(atoms: List[Dict[str, Any]]) -> str:
    """Write coordinates in XYZ format to a string.

    Args:
        atoms (List[Dict[str, Any]]): Atom definitions

    Returns:
        str: String in XYZ format
    """

    # Write atom symbols and coordinates
    lines = [f'{atom["el"]:>2s} {(atom["x"] * _AU2AA):14.5f} '
             f'{(atom["y"] * _AU2AA):14.5f} {(atom["z"] * _AU2AA):14.5f}' for atom in atoms]
    return f'{len(atoms)}\n\n' + '\n'.join(lines) + '\n'


def parse_mol(mol) -> Tuple[List[Dict[str, Any]], List[Dict[str, Any]], Dict[str, Any]]:
    """Parse molecule definition in MOL format.

    Args:
        mol (str): Input molecule definition in MOL format

    Returns:
        List[Dict[str, Any]], List[Dict[str, Any]], Dict[str, Any]: atoms, bonds, properties
    """

    rdmol = Chem.MolFromMolBlock(mol, removeHs=False)
    if rdmol is None:
        raise ValueError("invalid molecule")

    atoms = []
    conf = rdmol.GetConformer()
    for idx, pos in enumerate(conf.GetPositions()):
        x, y, z = pos
        rdat = rdmol.GetAtomWithIdx(idx)
        el = rdat.GetSymbol()
        charge = rdat.GetFormalCharge()
        mult = rdat.GetNumRadicalElectrons() + 1
        atom = {'el': el, 'idx': idx, 'x': x / _AU2AA, 'y': y / _AU2AA, 'z': z / _AU2AA}
        if charge != 0:
            atom['formalCharge'] = charge
        if mult != 1:
            atom['spinMultiplicity'] = mult
        atoms.append(atom)

    bonds = []
    for idx, bond in enumerate(rdmol.GetBonds()):
        at1 = bond.GetBeginAtomIdx()
        at2 = bond.GetEndAtomIdx()
        type_ = bond.GetBondType()
        bonds.append({'at1': at1, 'at2': at2, 'idx': idx, 'type': str(type_)})

    properties = {
        'charge': Chem.GetFormalCharge(rdmol),
        'mult': Descriptors.NumRadicalElectrons(rdmol) + 1,
    }

    return atoms, bonds, properties


def format_mol(atoms: List[Dict[str, Any]], bonds: List[Dict[str, Any]], properties: Dict[str, Any],
               determine_bonds=False) -> str:
    """Format molecule definition in MOL format.

    Args:
        atoms (List[Dict[str, Any]]): Atom data
        bonds (List[Dict[str, Any]]): Bond data
        properties (Dict[str, Any]): Molecular properties
        determine_bonds (bool): Assign bonds from coordinates

    Returns:
        str: Molecule definition in MOL format
    """

    rdmol = Chem.RWMol()
    conf = Chem.Conformer(len(atoms))
    for idx, atom in enumerate(atoms):
        rdat = Chem.Atom(atom['el'])
        formal_charge = atom.get('formalCharge')
        if formal_charge is not None:
            rdat.SetFormalCharge(formal_charge)
        spin_multiplicity = atom.get('spinMultiplicity')
        if spin_multiplicity is not None:
            rdat.SetNumRadicalElectrons(spin_multiplicity - 1)
        rdmol.AddAtom(rdat)
        conf.SetAtomPosition(idx, [atom['x'] * _AU2AA, atom['y'] * _AU2AA, atom['z'] * _AU2AA])
    rdmol.AddConformer(conf)

    if determine_bonds:
        try:
            charge = properties.get('charge', 0)
            rdDetermineBonds.DetermineBonds(rdmol, useHueckel=False, charge=charge)
        except ValueError:
            raise MoleculeError(message="cannot determine bonds")
    else:
        for bond in bonds:
            rdmol.AddBond(bond['at1'], bond['at2'], getattr(Chem.BondType, bond['type']))

    return Chem.MolToMolBlock(rdmol.GetMol())


def format_mopac(atoms: List[Dict[str, Any]], properties: Dict[str, Any],
                 title: str, cycles: int, options: str, no_opt: bool = False) -> str:
    """Format MOPAC program input.

    Based on https://github.com/openbabel/openbabel/blob/master/src/formats/mopacformat.cpp
    (MOPAC Cartesian format)

    Args:
        atoms (List[Dict[str, Any]): Atom coordinates (in AA, keys: 'el', 'x', 'y', 'z')
        properties (Dict[str, Any]): Molecular properties
        title (str): (Optional) title for the calculation
        cycles (int): Number of optimization cycles
        options (str): MOPAC program options
        no_opt (bool): Perform energy evaluation, do not optimize

    Returns:
        str: MOPAC program input
    """

    # 'disp' option is necessary for printing energies
    # Do not optimize
    if no_opt:
        optimization_str = "noopt disp"
    else:
        # Optimization in Cartesian coordinates
        optimization_str = "xyz disp"

    # Geometry options
    options_str = options

    # Spin state designation
    mult = properties.get('mult', 1)
    spin_str = _SPIN_STATES[mult]

    # Charge
    charge = properties.get('charge', 0)
    charge_str = f"charge={charge}"

    # Open-shell
    uhf_str = "uhf" if mult > 1 else ""

    # Number of iterations
    cycles_str = f"cycles={cycles:d}"

    # Line 1 contains the keywords
    line1 = " ".join([s for s in [
        optimization_str, options_str, spin_str, charge_str, uhf_str, cycles_str] if s])

    # Lines 2 and 3 contain the title and comment, can both be empty
    line2 = title
    line3 = ""

    lines = [line1, line2, line3]

    # Atom lines
    for atom in atoms:
        lines.append(f"{atom['el']:<3s}{atom['x'] * _AU2AA:8.5f} 1 "
                     f"{atom['y'] * _AU2AA:8.5f} 1 {atom['z'] * _AU2AA:8.5f} 1")

    # Append empty line
    lines.append("")

    return "\n".join(lines)


def parse_mopac(mopout: str) -> Tuple[List[Dict[str, Any]], Dict[str, Any], Dict[str, Any]]:
    """Parse MOPAC program output.

    Args:
        mopout (str): MOPAC program output

    Returns:
        List[Dict[str, Any]], Dict[str, Any], Dict[str, Any]: atoms, properties, flags
    """

    # Match total energy
    m = _RE_MOPAC_TOTAL_E.search(mopout)
    if m:
        energy = float(m.group(1))
    else:
        energy = 0.0

    # Molecular charge
    m = _RE_MOPAC_CHARGE.search(mopout)
    if m:
        charge = int(m.group(1))
    else:
        charge = 0

    # Spin multiplicity
    m = _RE_MOPAC_MULT.search(mopout)
    if m:
        mult = _SPIN_MULT[m.group(1)]
    else:
        mult = 1

    # Initialize flags and position variables
    converged = False
    no_opt = False
    has_coord = False
    start_pos = 0
    coordtitle_pos = 0
    emptyline_pos = 0

    # No optimization necessary
    m = _RE_MOPAC_NO_OP.search(mopout)
    if m:
        converged = True
        start_pos = m.end()

    else:

        # Energy evaluation (no optimization)
        m = _RE_MOPAC_NO_OPT.search(mopout)
        if m:
            no_opt = True
            start_pos = m.end()

        else:

            # Check convergence
            m = _RE_MOPAC_CONV.search(mopout)

            # Optimization converged
            if m:
                converged = True
                start_pos = m.end()

    # Get coordinates
    if converged or no_opt:

        # Find start of Cartesian coordinates
        m = _RE_MOPAC_COORD_TITLE_CONV.search(mopout, start_pos)

        if m:
            has_coord = True
            coordtitle_pos = m.end()

            # Find end of Cartesian coordinates
            m = _RE_EMPTY_LINE.search(mopout, coordtitle_pos)
            emptyline_pos = m.start()

        # Search for converged output
        _RE_COORD_XYZ = _RE_MOPAC_COORD_XYZ_CONV

    # Optimization failed, use last coordinates
    else:
        start_pos = 0

        # Find start of Cartesian coordinates
        m = _RE_MOPAC_COORD_TITLE_NOCONV.search(mopout, start_pos)

        if m:
            has_coord = True
            coordtitle_pos = m.end()

            # Find end of Cartesian coordinates
            m = _RE_EMPTY_LINE.search(mopout, coordtitle_pos)
            emptyline_pos = m.start()

        # Search for no-convergence output
        _RE_COORD_XYZ = _RE_MOPAC_COORD_XYZ_NOCONV

    # Extract XYZ coordinates in Angstrom
    atoms = []
    if has_coord:
        for idx, m in enumerate(_RE_COORD_XYZ.finditer(mopout, coordtitle_pos, emptyline_pos)):
            el, x, y, z = m.group(1), m.group(2), m.group(3), m.group(4)
            atoms.append({'idx': idx, 'el': el, 'x': float(x) / _AU2AA,
                          'y': float(y) / _AU2AA, 'z': float(z) / _AU2AA})

    # Collect properties and flags
    properties = {
        'charge': charge,
        'mult': mult,
        'energy': energy,
    }

    flags = {
        'converged': converged
    }

    return atoms, properties, flags


def parse_xtb_opt(xtbout: str, thermochem: bool = False) -> Tuple[Dict[str, Any], Dict[str, Any]]:
    """Parse XTB optimization output.

    Args:
        xtbout (str): XTB optimization output
        thermochem (bool): Extract thermochemistry output

    Returns:
        Dict[str, Any], Dict[str, Any]:  properties, flags
    """

    energy = 0.0
    converged = False
    start_pos = 0

    # Check convergence
    m = _RE_XTB_CONV.search(xtbout)
    if m:
        converged = True
        start_pos = m.end()

    # Match total energy in Hartree, convert to eV
    m = _RE_XTB_TOTAL_E.search(xtbout, start_pos)
    if m:
        energy = float(m.group(1)) * _CONV_H_EV

    # Collect properties and flags
    properties = {
        'energy': energy,
    }

    flags = {
        'converged': converged,
    }

    if thermochem:
        free_energy = 0.0
        is_stable = True
        point_group = 'c1'
        start_pos = 0

        # Match point group output
        m = _RE_XTB_POINT_GROUP.search(xtbout)
        if m:
            point_group = m.group(1)
            start_pos = m.end()

        # Check for imaginary frequencies
        m = _RE_XTB_IMAG_FREQ.search(xtbout, start_pos)
        if m:
            is_stable = False

        # Match free energy at 298.15 K in Hartree, convert to eV
        m = _RE_XTB_FREE_E.search(xtbout, start_pos)
        if m:
            free_energy = float(m.group(1)) * _CONV_H_EV

        # Add to properties and flags
        properties.update({
            'point_group': point_group,
            'free_energy': free_energy,
        })
        flags['is_stable'] = is_stable

    return properties, flags


def parse_xtb_en(xtbout: str) -> Tuple[Dict[str, Any], Dict[str, Any]]:
    """Parse XTB single-point energy output.

    Args:
        xtbout (str): XTB single-point energy output

    Returns:
        Dict[str, Any], Dict[str, Any]:  properties, flags
    """

    energy = 0.0
    start_pos = 0

    # Match total energy in Hartree, convert to eV
    m = _RE_XTB_TOTAL_E.search(xtbout, start_pos)
    if m:
        energy = float(m.group(1)) * _CONV_H_EV

    # Collect properties and flags
    properties = {
        'energy': energy,
    }

    flags = {}

    return properties, flags


def parse_cml(cml: str) -> Tuple[int, int, List[Dict[str, Any]], List[Dict[str, Any]]]:
    """Parse molecular structure in CML format.

    Args:
        cml (str): Molecular definition in CML format

    Returns:
        (int, int, List[Dict[str, Any]], List[Dict[str, Any]]): charge, mult, atoms, bonds
    """

    namespaces = {'cml': 'http://www.xml-cml.org/schema'}
    cml = ET.fromstring(cml)
    if cml.tag == '{' + namespaces['cml'] + '}molecule':
        mol = cml
    else:
        mol = cml.find('cml:molecule', namespaces=namespaces)

    if mol is None:
        raise ValueError("invalid CML input")

    charge_str = mol.get('formalCharge', 0)
    mult_str = mol.get('spinMultiplicity', 1)
    atom_array = mol.find('cml:atomArray', namespaces=namespaces)
    bond_array = mol.find('cml:bondArray', namespaces=namespaces)

    if charge_str is None or mult_str is None or atom_array is None:
        raise ValueError("invalid CML input")

    charge = int(charge_str)
    mult = int(mult_str)

    atom_dict = OrderedDict()
    for idx, atom in enumerate(atom_array.iterfind('cml:atom', namespaces=namespaces)):
        id_ = atom.get('id')
        if id_ is None:
            id_ = f'a{idx}'
        el = atom.get('elementType')
        if el is None:
            raise ValueError(f"missing atom type for atom {idx}")
        atom_charge = atom.get('formalCharge')
        h_count = atom.get('hydrogenCount')
        spin_mult = atom.get('spinMultiplicity')
        x = atom.get('x3') or atom.get('x2')
        y = atom.get('y3') or atom.get('y2')
        z = atom.get('z3') or atom.get('z2') or 0.0
        atom_dict[id_] = {'idx': idx, 'el': el, 'x': float(x), 'y': float(y), 'z': float(z)}
        if atom_charge is not None:
            atom_dict[id_]['charge'] = int(atom_charge)
        if h_count is not None:
            atom_dict[id_]['h_count'] = int(h_count)
        if spin_mult is not None:
            atom_dict[id_]['mult'] = int(spin_mult)

    bond_dict = OrderedDict()
    if bond_array is not None:
        for idx, bond in enumerate(bond_array.iterfind('cml:bond', namespaces=namespaces)):
            id_ = bond.get('id')
            if id_ is None:
                id_ = f'b{idx}'
            id1, id2 = bond.get('atomRefs2').split()
            order = bond.get('order')
            idx1 = atom_dict[id1]['idx']
            idx2 = atom_dict[id2]['idx']
            bond_dict[id_] = {'idx': idx, 'at1': int(idx1), 'at2': int(idx2),
                              'type': _BOND_TYPES.get(order, 'UNSPECIFIED')}

    atoms = list(atom_dict.values())
    bonds = list(bond_dict.values())

    return charge, mult, atoms, bonds
