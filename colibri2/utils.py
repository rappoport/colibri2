"""Utility variables and methods for colibri2.

Functions:
    string_hash: Compute deterministic string hash
    base26: Encode positive number in base 26 and returns last pos digits
    invert_letter: Compute the inverted letter in alphabet with respect to basis
    controlhash: Fixed-size hash function for strings
    random_id: Random valid identifier of given length pos
    ensure_logger: Create logger if necessary and (re)set logging level
    camelcase_to_uppercase: Convert CamelCase string to UPPERCASE_STRING
"""

import hashlib
import logging
import random
import re
import string
from math import factorial
from typing import List

from .config import Config

__all__ = ['stringhash', 'base26', 'invert_letter', 'controlhash', 'lexicographic_index',
           'random_id', 'ensure_logger', 'camelcase_to_uppercase']

_ALPHABET = string.ascii_uppercase

_BASE = 26

_RE_PART = re.compile(r'[A-Z][a-z]*')


def stringhash(s: str) -> int:
    """Compute deterministic string hash instead of hash() builtin.

    Args:
        s (str): Input string

    Returns:
        int: String hash
    """

    return int(hashlib.sha256(s.encode('ascii')).hexdigest(), 16)


def base26(num: int, pos: int = 10) -> str:
    """Encode num > 0 in base 26 in a fixed-length string.

    Args:
        num (int): Number to encode
        pos (int, optional): Hash length

    Returns:
        str: Encoded number

    Raises:
        ValueError: Negative number
    """

    # Number has to be non-negative
    if num < 0:
        raise ValueError("input must be non-negative")

    # Perform conversion
    digits = []

    while num:
        digits.append(_ALPHABET[num % _BASE])
        num //= _BASE

    # Pad with letter 'A' from left up to required length
    padding = ['A'] * (pos - min(pos, len(digits)))

    return ''.join(padding + digits[pos - 1::-1])


def invert_letter(letter: str) -> str:
    """Compute the inverted letter in alphabet with respect to basis.

    Args:
        letter (str): Input letter

    Returns:
        str: Output letter
    """

    return _ALPHABET[-(_ALPHABET.index(letter) + 1)]


def controlhash(s: str, pos: int = 10) -> str:
    """Compute fixed-size hash of a string.

    Args:
        s (str): Input string
        pos (int, optional): Hash length

    Returns:
        str: Hash value
    """

    return base26(stringhash(s), pos)


def lexicographic_index(indices: List[int]) -> int:
    """Compute index of the permutation in lexicographic order.

    From https://stackoverflow.com/questions/12146910/finding-the-lexicographic-index-of-a-permutation-of-a-given-array

    Args:
        indices (List[int]): Array of permutation indices

    Returns:
        int: Index of the permutation in lexicographic order
    """

    result = 0
    num = len(indices)
    for j in range(num):
        k = sum(1 for i in indices[j + 1:] if i < indices[j])
        result += k * factorial(num - j - 1)
    return result


def random_id(pos: int = 10) -> str:
    """Generate random valid identifier of given length pos (default: 10).

    Args:
        pos (int): Identifier length

    Returns:
        str: Identifier
    """

    return (''.join([random.choice(
        string.ascii_letters + string.digits) for _ in range(pos)]))


def ensure_logger(name: str = None,
                  level: str = None,
                  fmt: str = None) -> logging.Logger:
    """Create new logger if necessary and (re)set logging level.

    Args:
        name (str, optional): Logger name
        level (str, optional): Logging level
        fmt (str, optional): Format string

    Returns:
        logger (logging.Logger): New logger
    """

    if name is None:
        name = 'colibri2'

    if level is None:
        level = Config().LOGGING.LEVEL

    logger = logging.getLogger(name)
    logger.setLevel(getattr(logging, level, logging.INFO))

    return logger


def camelcase_to_uppercase(s: str) -> str:
    """Convert CamelCase string to underscore-separated UPPERCASE_STRING.

    Args:
        s (str): Input CamelCase string

    Returns:
        s: Underscore-separated UPPERCASE_STRING
    """

    return '_'.join(match.group(0).upper() for match in _RE_PART.finditer(s))
