"""Abstract cache interaction class for colibri2.

Classes:
    Cache: Abstract cache interaction class
"""

from abc import ABC, abstractmethod
from typing import List, Tuple

from ..config import Config
from ..data import Molecule, Flask
from ..utils import ensure_logger

__all__ = ['Cache']


class Cache(ABC):
    """Abstract cache interaction class."""

    def __init__(self, config: Config):
        self.config = config
        self.logger = ensure_logger("cache", self.config.LOGGING.LEVEL.upper())
        self.url = None

    @abstractmethod
    def get_molecule(self, type_: str, key: str) -> Molecule:
        """Get molecule by key from cache."""
        pass

    @abstractmethod
    def get_flask(self, type_: str, key: str) -> Flask:
        """Get flask by key from cache."""
        pass

    @abstractmethod
    def set_molecule(self, type_: str, key: str, mol: Molecule):
        """Set molecule in cache."""
        pass

    @abstractmethod
    def set_flask(self, type_: str, key: str, flask: Flask):
        """Set flask in cache."""
        pass

    def get_molecules(self, type_: str, keys: List[str]) -> List[Molecule]:
        """Get list of molecules from cache."""
        return [self.get_molecule(type_, key) for key in keys]

    def get_flasks(self, type_: str, keys: List[str]) -> List[Flask]:
        """Get list of flasks from cache."""
        return [self.get_flask(type_, key) for key in keys]

    def set_molecules(self, type_: str, mols: List[Tuple[str, Molecule]]):
        """Set list of molecules in cache."""
        for key, mol in mols:
            self.set_molecule(type_, key, mol)

    def set_flasks(self, type_: str, flasks: List[Tuple[str, Flask]]):
        """Set list of flasks in cache."""
        for key, flask in flasks:
            self.set_flask(type_, key, flask)

    @abstractmethod
    def clear_molecules(self, type_: str):
        """Clear molecules from cache."""
        pass

    @abstractmethod
    def clear_flasks(self, type_: all):
        """Clear flasks from cache."""
        pass
