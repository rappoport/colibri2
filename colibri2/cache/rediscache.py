"""Redis-based cache interaction class.

Classes:
    RedisCache: Redis-based cache interaction class
"""

import json

from redis import Redis

from .cache import Cache
from ..config import Config
from ..data import Flask, Molecule, MOLECULE_TYPES, FLASK_TYPES
from ..utils import camelcase_to_uppercase


class RedisCache(Cache):

    def __init__(self, config: Config):
        super().__init__(config)

        self.redis = Redis.from_url(self.config.CACHE.URL)
        self.url = self.config.CACHE.URL

        self.molecule_stores = {}
        for type_ in MOLECULE_TYPES:
            conf_name = camelcase_to_uppercase(type_)
            if conf_name in config.CACHE.MOLECULE_STORES:
                self.molecule_stores[type_] = config.CACHE.MOLECULE_STORES[conf_name]

        self.flask_stores = {}
        for type_ in FLASK_TYPES:
            conf_name = camelcase_to_uppercase(type_)
            if conf_name in config.CACHE.FLASK_STORES:
                self.flask_stores[type_] = config.CACHE.FLASK_STORES[conf_name]

    def get_molecule(self, type_: str, key: str) -> Molecule:
        mol = None
        store = self.molecule_stores.get(type_)
        if store:
            value = self.redis.hget(store, key)
            if value:
                mol = MOLECULE_TYPES[type_].fromdict(json.loads(value))
                self.logger.debug(f"retrieved molecule {mol} from cache {store}")
            else:
                self.logger.debug(f"molecule not found in cache {store} for key {key}")
        else:
            self.logger.debug(f"molecule cache not found for type {type_}")
        return mol

    def get_flask(self, type_: str, key: str) -> Flask:
        flask = None
        store = self.flask_stores.get(type_)
        if store:
            value = self.redis.hget(store, key)
            if value:
                flask = FLASK_TYPES[type_].fromdict(json.loads(value))
                self.logger.debug(f"retrieved flask {flask} from cache {store}")
            else:
                self.logger.debug(f"flask not found in cache {store} for key {key}")
        else:
            self.logger.debug(f"molecule cache not found for type {type_}")
        return flask

    def set_molecule(self, type_: str, key: str, mol: Molecule):
        store = self.molecule_stores.get(type_)
        if store:
            skip_none_values = self.config.CACHE.get('SKIP_NONE_VALUES', False)
            skip_keys = self.config.CACHE.get('SKIP_KEYS',[])
            count = self.redis.hset(store, key, json.dumps(mol.todict(skip_none_values, skip_keys)))
            if count:
                self.logger.debug(f"stored molecule {mol} to cache {store}")
            else:
                self.logger.debug(f"could not store molecule {mol} to cache {store}")
        else:
            self.logger.debug(f"molecule cache not found for type {type_}")

    def set_flask(self, type_: str, key: str, flask: Flask):
        store = self.flask_stores.get(type_)
        if store:
            skip_none_values = self.config.CACHE.get('SKIP_NONE_VALUES', False)
            skip_keys = self.config.CACHE.get('SKIP_KEYS',[])
            count = self.redis.hset(store, key, json.dumps(flask.todict(skip_none_values, skip_keys)))
            if count:
                self.logger.debug(f"stored flask {flask} to cache {store}")
            else:
                self.logger.debug(f"could not store flask {flask} to cache {store}")
        else:
            self.logger.debug(f"flask cache not found for type {type_}")

    def clear_molecules(self, type_: str):
        store = self.molecule_stores.get(type_)
        if store:
            self.redis.delete(store)
            self.logger.debug(f"cleared molecules from cache {store}")
        else:
            self.logger.error(f"molecule table not found for type {type_}")

    def clear_flasks(self, type_: str):
        store = self.flask_stores.get(type_)
        if store:
            self.redis.delete(store)
            self.logger.debug(f"cleared flasks from cache {store}")
        else:
            self.logger.error(f"flask table not found for type {type_}")
