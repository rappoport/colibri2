"""Data types for command results.

Classes:
    Abstract base class for command results
    Success: Successful command
    Failure: Unsuccessful command
"""

from abc import ABC, abstractmethod

__all__ = ['Result', 'Success', 'Failure']


class Result(ABC):
    @abstractmethod
    def code(self) -> int:
        return 255


class _Success(Result):
    def __str__(self):
        return "Success"

    def code(self) -> int:
        return 0


Success = _Success()


class Failure(Result):

    def __init__(self, message):
        self.message = message

    def __str__(self):
        return f"Failure({self.message})"

    def code(self) -> int:
        return 1
