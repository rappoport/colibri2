"""Redis-based task queue interaction class.

Classes:
    RedisQueue: Redis-based task queue interaction class
"""

import json

from redis import Redis

from .taskqueue import TaskQueue
from ..data import Flask, Molecule, MOLECULE_TYPES, FLASK_TYPES
from ..utils import camelcase_to_uppercase

__all__ = ['RedisQueue']


class RedisQueue(TaskQueue):

    def __init__(self, config):
        super().__init__(config)

        self.redis = Redis.from_url(self.config.QUEUE.URL)
        self.url = self.config.QUEUE.URL

        self.molecule_queues = {}
        for type_ in MOLECULE_TYPES:
            conf_name = camelcase_to_uppercase(type_)
            if conf_name in config.QUEUE.MOLECULE_QUEUES:
                self.molecule_queues[type_] = config.QUEUE.MOLECULE_QUEUES[conf_name]

        self.flask_queues = {}
        for type_ in FLASK_TYPES:
            conf_name = camelcase_to_uppercase(type_)
            if conf_name in config.QUEUE.FLASK_QUEUES:
                self.flask_queues[type_] = config.QUEUE.FLASK_QUEUES[conf_name]

    def remove_molecule(self, type_: str) -> Molecule:
        mol = None
        queue = self.molecule_queues.get(type_)
        if queue:
            value = self.redis.lpop(queue)
            if value:
                mol = MOLECULE_TYPES[type_].fromdict(json.loads(value))
                self.logger.debug(f"removed molecule {mol} from queue {queue}")
            else:
                self.logger.debug(f"molecule queue {queue} is empty")
        else:
            self.logger.error(f"molecule queue not found for type {type_}")
        return mol

    def remove_flask(self, type_: str) -> Flask:
        flask = None
        queue = self.flask_queues.get(type_)
        if queue:
            value = self.redis.lpop(queue)
            if value:
                flask = FLASK_TYPES[type_].fromdict(json.loads(value))
                self.logger.debug(f"removed flask {flask} from queue {queue}")
            else:
                self.logger.debug(f"flask queue {queue} is empty")
        else:
            self.logger.error(f"flask queue not found for type {type_}")
        return flask

    def append_molecule(self, type_: str, mol: Molecule):
        queue = self.molecule_queues.get(type_)
        if queue:
            skip_none_values = self.config.QUEUE.get('SKIP_NONE_VALUES', False)
            skip_keys = self.config.QUEUE.get('SKIP_KEYS',[])
            self.redis.rpush(queue, json.dumps(mol.todict(skip_none_values, skip_keys)))
            self.logger.debug(f"appended molecule {mol} to queue {queue}")
        else:
            self.logger.error(f"molecule queue not found for type {type_}")

    def append_flask(self, type_: str, flask: Flask):
        queue = self.flask_queues.get(type_)
        if queue:
            skip_none_values = self.config.QUEUE.get('SKIP_NONE_VALUES', False)
            skip_keys = self.config.QUEUE.get('SKIP_KEYS',[])
            self.redis.rpush(queue, json.dumps(flask.todict(skip_none_values, skip_keys)))
            self.logger.debug(f"appended flask {flask} to queue {queue}")
        else:
            self.logger.error(f"flask queue not found for type {type_}")

    def clear_molecules(self, type_: str):
        queue = self.molecule_queues.get(type_)
        if queue:
            self.redis.delete(queue)
            self.logger.debug(f"cleared molecules from queue {queue}")
        else:
            self.logger.error(f"molecule queue not found for type {type_}")

    def clear_flasks(self, type_: str):
        queue = self.flask_queues.get(type_)
        if queue:
            self.redis.delete(queue)
            self.logger.debug(f"cleared flasks from queue {queue}")
        else:
            self.logger.error(f"flask queue not found for type {type_}")

