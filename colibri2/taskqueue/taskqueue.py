"""Abstract queue interaction class for colibri2.

Classes:
    TaskQueue: Abstract queue interaction class
"""

from abc import ABC, abstractmethod
from typing import List

from ..config import Config
from ..data import Molecule, Flask
from ..utils import ensure_logger

__all__ = ['TaskQueue']


class TaskQueue(ABC):
    """Abstract task queue interaction class."""

    def __init__(self, config: Config):
        self.config = config
        self.logger = ensure_logger("queue", self.config.LOGGING.LEVEL.upper())
        self.url = None

    @abstractmethod
    def remove_molecule(self, type_: str) -> Molecule:
        """Remove molecule from queue."""
        pass

    @abstractmethod
    def remove_flask(self, type_: str) -> Flask:
        """Remove flask from queue."""
        pass

    @abstractmethod
    def append_molecule(self, type_: str, mol: Molecule):
        """Append molecule to queue."""
        pass

    @abstractmethod
    def append_flask(self, type_: str, flask: Flask):
        """Append flask to queue."""
        pass

    def remove_molecules(self, type_: str, number: int) -> List[Molecule]:
        """Remove molecules from queue."""
        return [self.remove_molecule(type_) for _ in range(number)]

    def remove_flasks(self, type_: str, number: int) -> List[Flask]:
        """Remove flasks from queue."""
        return [self.remove_flask(type_) for _ in range(number)]

    def append_molecules(self, type_: str, mols: List[Molecule]):
        """Append molecules to queue."""
        for mol in mols:
            self.append_molecule(type_, mol)

    def append_flasks(self, type_: str, flasks: List[Flask]):
        """Append flasks to queue."""
        for flask in flasks:
            self.append_flask(type_, flask)

    @abstractmethod
    def clear_molecules(self, type_: str):
        """Clear molecules from queue."""
        pass

    @abstractmethod
    def clear_flasks(self, type_: str):
        """Clear flasks from queue."""
        pass
