"""CLI for export3 command.

Functions:
    export3cli: export3 CLI
"""

import sys

import click
import toml

from colibri2.api import export3
from colibri2.config import Config, add_option, ensure_option

__all__ = ['export3cli']


@click.command()
@click.option('-o', '--output-path', type=click.Path(dir_okay=False, writable=True), default=None,
              help="Output network file path")
@click.option('-d', '--db-url', type=str, default=None, metavar='URL', help="PostgreSQL database URL")
@click.option('-f', '--flask-table', type=str, default=None, help="Table for valid flasks")
@click.option('-t', '--trans-table', type=str, default=None, help="Table for transformations")
@click.option('-R', '--rule-table', type=str, default=None, help="Table for rules")
@click.option('-D', '--db-driver', type=str, default=None, help="PostgreSQL database driver method")
@click.option('-w', '--writer-method', type=str, default=None, help="Output writer method")
@click.option('-N', '--number-workers', type=int, default=None, help="Number of output writers if multi_graphml_stream method used")
@click.option('-n', '--worker-number', type=int, default=None, help="Writer number if multi_graphml_stream method used")
@click.option('-Z', '--settings-file', type=str, default=None, help="Custom settings file")
@click.option('--info', is_flag=True, default=None, help="Dump configuration info and exit")
def export3cli(output_path: str, db_url: str, flask_table: str, trans_table: str, rule_table: str, 
               db_driver: str, writer_method: str, number_workers: int, worker_number: int,
               settings_file: str, info: bool):
    config = Config(settings_file=settings_file)
    add_option(config, 'WRITER__OUTPUT_PATH', output_path)
    add_option(config, 'DB__URL', db_url)
    add_option(config, 'DB__FLASK_TABLES__SUPERFORMULA', flask_table)
    add_option(config, 'DB__TRANSFORMATION_TABLE', trans_table)
    add_option(config, 'DB__RULE_TABLE', rule_table)
    add_option(config, 'DB__DRIVER', db_driver)
    add_option(config, 'WRITER__METHOD', writer_method)
    add_option(config, 'WRITER__NUMBER_WORKERS', number_workers)
    add_option(config, 'WRITER__WORKER_NUMBER', worker_number)


    ensure_option(config, 'WRITER__OUTPUT_PATH')
    ensure_option(config, 'DB__URL')

    print(toml.dumps(config.as_dict()))
    if info:
        sys.exit(1)

    sys.exit(export3(config).code())


if __name__ == '__main__':
    export3cli()
