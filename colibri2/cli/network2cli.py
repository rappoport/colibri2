"""CLI for network2 command.

Functions:
    network2cli: network2 CLI
"""

import sys

import click
import toml

from colibri2.api import network2
from colibri2.config import Config, add_option, ensure_option

__all__ = ['network2cli']


@click.command()
@click.option('-i', '--initial-flask', type=str, default=None, metavar='SMILES', help="Initial flask SMILES")
@click.option('-x', '--index', is_flag=True, default=None, help="Use permutational index")
@click.option('-r', '--rules-path', type=click.Path(dir_okay=False), default=None, help="Rules file path")
@click.option('-o', '--output-path', type=click.Path(dir_okay=False, writable=True), default=None,
              help="Output network file path")
@click.option('-s', '--single-step', is_flag=True, default=None, help="Perform only one step")
@click.option('-Z', '--settings-file', type=str, default=None, help="Custom settings file")
@click.option('--info', is_flag=True, default=None, help="Dump configuration info and exit")
def network2cli(initial_flask: str, index: bool, rules_path: str, output_path: str, single_step: bool,
                settings_file: str, info: bool):
    config = Config(settings_file=settings_file)
    add_option(config, 'NETWORK2__INITIAL_FLASK', initial_flask)
    add_option(config, 'REACTION__INDEX', index)
    add_option(config, 'REACTION__RULES_PATH', rules_path)
    add_option(config, 'WRITER__OUTPUT_PATH', output_path)
    add_option(config, 'NETWORK2__SINGLE_STEP', single_step)

    print(toml.dumps(config.as_dict()))
    if info:
        sys.exit(1)

    sys.exit(network2(config).code())


if __name__ == '__main__':
    network2cli()
