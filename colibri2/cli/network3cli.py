"""CLI for network3 command.

Functions:
    network3cli: network3 CLI
"""

import sys

import click
import toml

from colibri2.api import network3
from colibri2.config import Config, add_option, ensure_option

__all__ = ['network3cli']


@click.command()
@click.option('-r', '--rules-path', type=click.Path(dir_okay=False), default=None, help="Rules file path")
@click.option('-d', '--db-url', type=str, default=None, metavar='URL', help="PostgreSQL database URL")
@click.option('-c', '--cache-url', type=str, default=None, metavar='URL', help="Redis cache URL")
@click.option('-q', '--queue-url', type=str, default=None, metavar='URL', help="Redis queue URL")
@click.option('-x', '--index', is_flag=True, default=None, help="Use permutational index")
@click.option('-f', '--flask-table', type=str, default=None, help="Table for valid flasks")
@click.option('-I', '--invalid-table', type=str, default=None, help="Table for invalid flasks")
@click.option('-t', '--trans-table', type=str, default=None, help="Table for transformations")
@click.option('-R', '--rule-table', type=str, default=None, help="Table for rules")
@click.option('-F', '--flask-queue', type=str, default=None, help="Queue for valid flasks")
@click.option('-C', '--flask-cache', type=str, default=None, help="Cache for valid flasks")
@click.option('-S', '--skip-none-values', is_flag=True, default=False, help="Skip None values")
@click.option('-Z', '--settings-file', type=str, default=None, help="Custom settings file")
@click.option('--info', is_flag=True, default=None, help="Dump configuration info and exit")
def network3cli(rules_path: str, index: bool, db_url: str, cache_url: str, queue_url: str,
                flask_table: str, invalid_table: str, trans_table: str, rule_table: str,
                flask_queue: str, flask_cache: str, skip_none_values: bool, settings_file: str, 
                info: bool):
    config = Config(settings_file=settings_file)
    add_option(config, 'REACTION__RULES_PATH', rules_path)
    add_option(config, 'DB__URL', db_url)
    add_option(config, 'QUEUE__URL', queue_url)
    add_option(config, 'CACHE__URL', cache_url)
    add_option(config, 'REACTION__INDEX', index)
    add_option(config, 'DB__FLASK_TABLES__SUPERFORMULA', flask_table)
    add_option(config, 'DB__FLASK_TABLES__INVALID_SUPERFORMULA', invalid_table)
    add_option(config, 'DB__TRANSFORMATION_TABLE', trans_table)
    add_option(config, 'DB__RULE_TABLE', rule_table)
    add_option(config, 'DB__SKIP_NONE_VALUES', skip_none_values)
    add_option(config, 'QUEUE__FLASK_QUEUES__SUPERFORMULA', flask_queue)
    add_option(config, 'CACHE__FLASK_STORES__SUPERFORMULA', flask_cache)

    ensure_option(config, 'REACTION__RULES_PATH')
    ensure_option(config, 'DB__URL')
    ensure_option(config, 'CACHE__URL')
    ensure_option(config, 'QUEUE__URL')

    print(toml.dumps(config.as_dict()))
    if info:
        sys.exit(1)

    sys.exit(network3(config).code())


if __name__ == '__main__':
    network3cli()
