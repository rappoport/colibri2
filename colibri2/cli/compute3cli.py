"""CLI for compute3 command.

Functions:
    compute3cli: compute3 CLI
"""

import sys

import click
import toml

from colibri2.api import compute3
from colibri2.config import Config, add_option, ensure_option

__all__ = ['compute3cli']


@click.command()
@click.option('-d', '--db-url', type=str, default=None, metavar='URL', help="PostgreSQL database URL")
@click.option('-q', '--queue-url', type=str, default=None, metavar='URL', help="Redis queue URL")
@click.option('-g', '--geometry-table', type=str, default=None, help="Table for valid geometries")
@click.option('-y', '--property-table', type=str, default=None, help="Table for valid properties")
@click.option('-F', '--failed-formula-table', type=str, default=None, help="Table for failed formulas")
@click.option('-X', '--failed-configuration-table', type=str, default=None, help="Table for failed configurations")
@click.option('-G', '--failed-geometry-table', type=str, default=None, help="Table for failed geometries")
@click.option('-l', '--failed-property-table', type=str, default=None, help="Table for failed properties")
@click.option('-I', '--invalid-configuration-table', type=str, default=None, help="Table for invalid configurations")
@click.option('-Y', '--invalid-geometry-table', type=str, default=None, help="Table for invalid geometries")
@click.option('-L', '--invalid-property-table', type=str, default=None, help="Table for invalid properties")
@click.option('-m', '--molecule-queue', type=str, default=None, help="Queue for valid molecules")
@click.option('-p', '--optimizer', type=str, default=None, help="Geometry optimizer")
@click.option('-x', '--executable', type=str, default=None, help="Geometry executable")
@click.option('-P', '--property', is_flag=True, default=None, help="Perform property calculation")
@click.option('-t', '--property-type', type=str, default=None, help="Property type")
@click.option('-c', '--property-calculator', type=str, default=None, help="Property calculator")
@click.option('-e', '--property-executable', type=str, default=None, help="Property executable")
@click.option('-Z', '--settings-file', type=str, default=None, help="Custom settings file")
@click.option('--info', is_flag=True, default=None, help="Dump configuration info and exit")
def compute3cli(db_url: str, queue_url: str, geometry_table: str, property_table: str,
                failed_formula_table: str, failed_configuration_table: str, failed_geometry_table: str,
                failed_property_table: str, invalid_configuration_table: str, invalid_geometry_table: str, 
                invalid_property_table: str, molecule_queue: str, optimizer: str, executable: str, 
                property: str, property_type: str, property_calculator: str, property_executable: str,
                settings_file: str, info: bool):
    config = Config(settings_file=settings_file)
    add_option(config, 'DB__URL', db_url)
    add_option(config, 'QUEUE__URL', queue_url)
    add_option(config, 'COMPUTE3__OPTIMIZER', optimizer)
    add_option(config, 'GEOMETRY__EXECUTABLE', executable)
    add_option(config, 'COMPUTE3__PROPERTY', property)
    add_option(config, 'COMPUTE3__PROPERTY_TYPE', property_type)
    add_option(config, 'COMPUTE3__PROPERTY_CALCULATOR', property_calculator)
    add_option(config, 'PROPERTY__EXECUTABLE', property_executable)
    add_option(config, 'DB__MOLECULE_TABLES__GEOMETRY', geometry_table)
    add_option(config, 'DB__MOLECULE_TABLES__PROPERTY', property_table)
    add_option(config, 'DB__MOLECULE_TABLES__FAILED_FORMULA', failed_formula_table)
    add_option(config, 'DB__MOLECULE_TABLES__FAILED_CONFIGURATION', failed_configuration_table)
    add_option(config, 'DB__MOLECULE_TABLES__FAILED_GEOMETRY', failed_geometry_table)
    add_option(config, 'DB__MOLECULE_TABLES__FAILED_PROPERTY', failed_property_table)
    add_option(config, 'DB__MOLECULE_TABLES__INVALID_CONFIGURATION', invalid_configuration_table)
    add_option(config, 'DB__MOLECULE_TABLES__INVALID_GEOMETRY', invalid_geometry_table)
    add_option(config, 'DB__MOLECULE_TABLES__INVALID_PROPERTY', invalid_property_table)
    add_option(config, 'QUEUE__MOLECULE_QUEUES__FORMULA', molecule_queue)

    ensure_option(config, 'DB__URL')
    ensure_option(config, 'QUEUE__URL')

    print(toml.dumps(config.as_dict()))
    if info:
        sys.exit(1)

    sys.exit(compute3(config).code())


if __name__ == '__main__':
    compute3cli()
