"""CLI for compute2 command.

Functions:
    compute2cli: compute2 CLI
"""

import sys

import click
import toml
from colibri2.api import compute2
from colibri2.config import Config, add_option, ensure_option

__all__ = ['compute2cli']


@click.command()
@click.option('-i', '--input-mol', type=str, default=None, metavar='SMILES', help="Initial molecule SMILES")
@click.option('-S', '--stereo', is_flag=True, default=None, help="Preserve stereochemical information")
@click.option('-p', '--optimizer', type=str, default=None, help="Geometry optimizer")
@click.option('-x', '--executable', type=str, default=None, help="Geometry executable")
@click.option('-P', '--property', is_flag=True, default=None, help="Perform property calculation")
@click.option('-t', '--property-type', type=str, default=None, help="Property type")
@click.option('-c', '--property-calculator', type=str, default=None, help="Property calculator")
@click.option('-e', '--property-executable', type=str, default=None, help="Property executable")
@click.option('-Z', '--settings-file', type=str, default=None, help="Custom settings file")
@click.option('--info', is_flag=True, default=None, help="Dump configuration info and exit")
def compute2cli(input_mol: str, stereo: bool, optimizer: str, executable: str, property: bool,
                property_type: str, property_calculator: str, property_executable: str,
                settings_file: str, info: bool):
    config = Config(settings_file=settings_file)
    add_option(config, 'COMPUTE2__INPUT_MOLECULE', input_mol)
    add_option(config, 'COMPUTE2__STEREO', stereo)
    add_option(config, 'COMPUTE2__OPTIMIZER', optimizer)
    add_option(config, 'GEOMETRY__EXECUTABLE', executable)
    add_option(config, 'COMPUTE2__PROPERTY', property)
    add_option(config, 'COMPUTE2__PROPERTY_TYPE', property_type)
    add_option(config, 'COMPUTE2__PROPERTY_CALCULATOR', property_calculator)
    add_option(config, 'PROPERTY__EXECUTABLE', property_executable)

    ensure_option(config, 'COMPUTE2__INPUT_MOLECULE')

    print(toml.dumps(config.as_dict()))
    if info:
        sys.exit(1)

    sys.exit(compute2(config).code())


if __name__ == '__main__':
    compute2cli()
