"""CLI for collect3 command.

Functions:
    collect3cli: collect3 CLI
"""

import sys

import click
import toml
from colibri2.api import collect3
from colibri2.config import Config, add_option, ensure_option

__all__ = ['collect3cli']


@click.command()
@click.option('-o', '--output-path', type=click.Path(dir_okay=False, writable=True), default=None,
              help="Energy output file path")
@click.option('-T', '--free-energy', is_flag=True, default=None, help="Output free energy")
@click.option('-d', '--db-url', type=str, default=None, metavar='URL', help="PostgreSQL database URL")
@click.option('-g', '--geometry-table', type=str, default=None, help="Table for valid geometries")
@click.option('-y', '--property-table', type=str, default=None, help="Table for valid properties")
@click.option('-P', '--property', is_flag=True, default=None, help="Perform property calculation")
@click.option('-t', '--property-type', type=str, default=None, help="Property type")
@click.option('-D', '--db-driver', type=str, default=None, help="PostgreSQL database driver method")
@click.option('-Z', '--settings-file', type=str, default=None, help="Custom settings file")
@click.option('--info', is_flag=True, default=None, help="Dump configuration info and exit")
def collect3cli(output_path: str, free_energy: bool, db_url: str, geometry_table: str, property_table: str,
                property: bool, property_type: str, db_driver: str, settings_file: str, info: bool):
    config = Config(settings_file=settings_file)
    add_option(config, 'COLLECT3__OUTPUT_PATH', output_path)
    add_option(config, 'COLLECT3__FREE_ENERGY', free_energy)
    add_option(config, 'COLLECT3__PROPERTY', property)
    add_option(config, 'COLLECT3__PROPERTY_TYPE', property_type)
    add_option(config, 'DB__URL', db_url)
    add_option(config, 'DB__MOLECULE_TABLES__GEOMETRY', geometry_table)
    add_option(config, 'DB__MOLECULE_TABLES__PROPERTY', property_table)
    add_option(config, 'DB__DRIVER', db_driver)

    ensure_option(config, 'COLLECT3__OUTPUT_PATH')
    ensure_option(config, 'DB__URL')

    print(toml.dumps(config.as_dict()))
    if info:
        sys.exit(1)

    sys.exit(collect3(config).code())


if __name__ == '__main__':
    collect3cli()
