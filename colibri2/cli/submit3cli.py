"""CLI for the submit3 command.

Functions:
    submit3cli: submit3 CLI
"""

import sys

import click
import toml

from colibri2.api import submit3
from colibri2.config import Config, add_option, ensure_option

__all__ = ['submit3cli']


@click.command()
@click.option('-i', '--input-file', type=click.Path(dir_okay=False, exists=True), default=None,
              help="Input molecule file")
@click.option('-C', '--clear-data', is_flag=True, default=None, help="Clear DB and queue data")
@click.option('-A', '--append', is_flag=True, default=None, help="Do not insert molecules already in DB")
@click.option('-U', '--unique', is_flag=True, default=None, help="Deduplicate molecules prior to insertion")
@click.option('-S', '--stereo', is_flag=True, default=None, help="Preserve stereochemical information")
@click.option('-d', '--db-url', type=str, default=None, metavar='URL', help="PostgreSQL database URL")
@click.option('-q', '--queue-url', type=str, default=None, metavar='URL', help="Redis queue URL")
@click.option('-f', '--formula-table', type=str, default=None, help="Table for input formulas")
@click.option('-g', '--geometry-table', type=str, default=None, help="Table for valid geometries")
@click.option('-y', '--property-table', type=str, default=None, help="Table for valid properties")
@click.option('-F', '--failed-formula-table', type=str, default=None, help="Table for failed formulas")
@click.option('-X', '--failed-configuration-table', type=str, default=None, help="Table for failed configurations")
@click.option('-G', '--failed-geometry-table', type=str, default=None, help="Table for failed geometries")
@click.option('-l', '--failed-property-table', type=str, default=None, help="Table for failed properties")
@click.option('-I', '--invalid-configuration-table', type=str, default=None, help="Table for invalid configurations")
@click.option('-Y', '--invalid-geometry-table', type=str, default=None, help="Table for invalid geometries")
@click.option('-L', '--invalid-property-table', type=str, default=None, help="Table for invalid properties")
@click.option('-m', '--molecule-queue', type=str, default=None, help="Queue for valid molecules")
@click.option('-P', '--property', is_flag=True, default=None, help="Initialize property calculation")
@click.option('-Z', '--settings-file', type=str, default=None, help="Custom settings file")
@click.option('--info', is_flag=True, default=None, help="Dump configuration info and exit")
def submit3cli(input_file: str, clear_data: bool, append: bool, unique: bool, stereo: bool,
               db_url: str, queue_url: str, formula_table: str, geometry_table: str, property_table: str,
               failed_formula_table: str, failed_configuration_table: str, failed_geometry_table: str,
               failed_property_table: str, invalid_configuration_table: str, invalid_geometry_table: str,
               invalid_property_table: str, molecule_queue: str, property: str, settings_file: str, info: bool):
    config = Config(settings_file=settings_file)
    add_option(config, 'SUBMIT3__INPUT_FILE', input_file)
    add_option(config, 'SUBMIT3__CLEAR_DATA', clear_data)
    add_option(config, 'SUBMIT3__APPEND', append)
    add_option(config, 'SUBMIT3__UNIQUE', unique)
    add_option(config, 'SUBMIT3__STEREO', stereo)
    add_option(config, 'SUBMIT3__PROPERTY', property)
    add_option(config, 'DB__URL', db_url)
    add_option(config, 'QUEUE__URL', queue_url)
    add_option(config, 'DB__MOLECULE_TABLES__FORMULA', formula_table)
    add_option(config, 'DB__MOLECULE_TABLES__GEOMETRY', geometry_table)
    add_option(config, 'DB__MOLECULE_TABLES__PROPERTY', property_table)
    add_option(config, 'DB__MOLECULE_TABLES__FAILED_FORMULA', failed_formula_table)
    add_option(config, 'DB__MOLECULE_TABLES__FAILED_CONFIGURATION', failed_configuration_table)
    add_option(config, 'DB__MOLECULE_TABLES__FAILED_GEOMETRY', failed_geometry_table)
    add_option(config, 'DB__MOLECULE_TABLES__FAILED_PROPERTY', failed_property_table)
    add_option(config, 'DB__MOLECULE_TABLES__INVALID_CONFIGURATION', invalid_configuration_table)
    add_option(config, 'DB__MOLECULE_TABLES__INVALID_GEOMETRY', invalid_geometry_table)
    add_option(config, 'DB__MOLECULE_TABLES__INVALID_PROPERTY', invalid_property_table)
    add_option(config, 'QUEUE__MOLECULE_QUEUES__FORMULA', molecule_queue)

    ensure_option(config, 'SUBMIT3__INPUT_FILE')
    ensure_option(config, 'DB__URL')
    ensure_option(config, 'QUEUE__URL')

    print(toml.dumps(config.as_dict()))
    if info:
        sys.exit(1)

    sys.exit(submit3(config).code())


if __name__ == '__main__':
    submit3cli()
