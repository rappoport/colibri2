"""Colibri submit3 command.

Functions:
    submit3: submit3 functional API

Classes:
    Submit3Command: submit3 command API
"""

import csv

from .command import Command
from ..config import Config
from ..data import Formula
from ..db import SQLAlchemyDB
from ..exceptions import CommandError
from ..result import Result, Success
from ..taskqueue import RedisQueue

__all__ = ['submit3', 'Submit3Command']


def submit3(config: Config) -> Result:
    return Submit3Command(config).execute()


class Submit3Command(Command):
    def __init__(self, config: Config):
        super().__init__(config)

        self.queue = RedisQueue(self.config)
        self.logger.debug(f"connected to task queue {self.queue.url}")

        self.db = SQLAlchemyDB(self.config)
        self.logger.debug(f"connected to database {self.db.url}")

    def execute(self) -> Result:
        self.logger.debug("starting execution")

        if self.config.SUBMIT3.CLEAR_DATA:
            self.db.clear_molecules('Formula')
            self.db.clear_molecules('Geometry')
            self.db.clear_molecules('FailedFormula')
            self.db.clear_molecules('FailedConfiguration')
            self.db.clear_molecules('InvalidConfiguration')
            self.db.clear_molecules('InvalidGeometry')
            self.queue.clear_molecules('Formula')
            if self.config.SUBMIT3.PROPERTY:
                self.db.clear_molecules('Property')
                self.db.clear_molecules('FailedGeometry')
                self.db.clear_molecules('InvalidProperty')
                self.db.clear_molecules('FailedProperty')

        input_file = self.config.SUBMIT3.INPUT_FILE
        if not input_file:
            raise CommandError("submit3 command needs input path")

        input_format = self.config.SUBMIT3.INPUT_FORMAT
        index = self.config.SUBMIT3.INDEX
        stereo = self.config.SUBMIT3.STEREO

        molecules = []
        if input_format == 'csv':
            with open(input_file, newline='') as f:
                for row in csv.DictReader(f):
                    smiles = row['smiles']
                    tag = row.get('tag')
                    molecules.append(Formula(smiles, index=index, stereo=stereo, tag=tag))
        else:
            raise CommandError(message=f"invalid input format {input_format} in submit3 command")

        self.logger.info(f"read {len(molecules)} molecules from file")
        if self.config.SUBMIT3.APPEND:
            self.logger.debug("appending molecules not available in the DB")
            batch_size = self.config.SUBMIT3.CHECK_BATCH_SIZE
            type_ = 'Property' if self.config.SUBMIT3.PROPERTY else 'Geometry'
            new_molecules = []
            for index in range(0, len(molecules), batch_size):
                batch_molecules = molecules[index:index + batch_size]
                old_molecules = self.db.fetch_molecules(type_, [mol.tag for mol in batch_molecules])
                old_keys = {mol.tag for mol in old_molecules}
                new_molecules.extend([mol for mol in batch_molecules if mol.tag not in old_keys])
        else:
            new_molecules = molecules

        if self.config.SUBMIT3.UNIQUE:
            submit_molecules = sorted(set(new_molecules))
            self.logger.info(f"submitting {len(submit_molecules)} unique molecules")
        else:
            submit_molecules = new_molecules
            self.logger.info(f"submitting {len(submit_molecules)} molecules")

        self.queue.append_molecules('Formula', submit_molecules)
        self.db.store_molecules('Formula', [(mol.tag, mol) for mol in submit_molecules])

        self.logger.debug("finished execution")

        return Success
