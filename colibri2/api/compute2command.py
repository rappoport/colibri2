"""Colibri2 compute2 command (structure building and optimization).

Functions:
    compute2: compute2 functional API

Classes:
    Compute2Command: compute2 command API
"""

from .command import Command
from ..config import Config
from ..data import Formula, Configuration, Geometry, FailedConfiguration, InvalidGeometry, \
    FailedFormula, InvalidConfiguration, Property, FailedProperty, InvalidProperty
from ..exceptions import CommandError
from ..result import Result, Success
from ..task import RDKitBuildTask, PybelBuildTask, MOPACGeometryTask, XTBGeometryTask, XTBEnergyPropertyTask

__all__ = ['compute2', 'Compute2Command']


def compute2(config: Config):
    return Compute2Command(config).execute()


class Compute2Command(Command):
    def __init__(self, config: Config):
        super().__init__(config)

        builder = self.config.CONFIGURATION.PROGRAM.lower()

        if builder == 'rdkit':
            self.build_task = RDKitBuildTask(self.config)
        elif builder == 'pybel':
            self.build_task = PybelBuildTask(self.config)
        else:
            raise CommandError(message="invalid configuration program in compute2 command")

        optimizer = self.config.COMPUTE2.OPTIMIZER.lower()

        if optimizer == 'mopac':
            self.geometry_task = MOPACGeometryTask(self.config)
        elif optimizer == 'xtb':
            self.geometry_task = XTBGeometryTask(self.config)
        else:
            raise CommandError(message="invalid optimizer in compute2 command")

        self.property_task = None
        if self.config.COMPUTE2.PROPERTY:
            property_type = self.config.COMPUTE2.PROPERTY_TYPE.lower()
            property_calculator = self.config.COMPUTE2.PROPERTY_CALCULATOR.lower()

            if property_type == 'energy' and property_calculator == 'xtb':
                self.property_task = XTBEnergyPropertyTask(self.config)
            else:
                raise CommandError(message="invalid property calculator in compute2 command")

    def execute(self) -> Result:
        self.logger.debug("starting execution")

        input_mol = self.config.COMPUTE2.INPUT_MOLECULE
        if not input_mol:
            raise CommandError(message="compute2 command needs initial molecule")
        index = self.config.COMPUTE2.INDEX
        stereo = self.config.COMPUTE2.STEREO
        inp = Formula(input_mol, index=index, stereo=stereo)

        self.logger.debug(f"<{inp}>: received input molecule")

        conf = self.build_task.process(inp)
        self.logger.debug(f"<{conf}>: finished building")

        if isinstance(conf, Configuration):

            self.logger.debug(f"<{conf}>: successful molecule building")

            geom = self.geometry_task.process(conf)
            self.logger.debug(f"<{geom}>: finished optimization")

            if isinstance(geom, Geometry):

                self.logger.debug(f"<{geom}>: successful molecule optimization")
                print(geom.coord)
                print(f"Geomerty optimization energy: {geom.energy}")

                if self.property_task:

                    prop = self.property_task.process(geom)
                    self.logger.debug(f"<{prop}>: finished property calculation")

                    if isinstance(prop, Property):

                        self.logger.debug(f"<{prop}>: successful property calculation")
                        for key, val in prop.properties.items():
                            print(f"{key}: {val}")

                    elif isinstance(prop, FailedProperty):

                        self.logger.debug(f"<{prop}>: failed property calculation")

                    elif isinstance(prop, InvalidProperty):

                        self.logger.debug(f"<{prop}>: invalid property calculation")

                    else:
                        raise CommandError(message=f"Got unexpected type {type(prop)}")

            elif isinstance(geom, FailedConfiguration):

                self.logger.debug(f"<{geom}>: failed molecule optimization")
                print(geom.coord)

            elif isinstance(geom, InvalidGeometry):

                self.logger.debug(f"<{geom}>: invalid molecule optimization")
                print(geom.coord)

            else:
                raise CommandError(message=f"Got unexpected type {type(geom)}")

        elif isinstance(conf, FailedFormula):

            self.logger.debug(f"<{conf}>: failed molecule building")
            print(conf.smiles)

        elif isinstance(conf, InvalidConfiguration):

            self.logger.debug(f"<{conf}>: invalid molecule building")
            print(conf.coord)

        else:
            raise CommandError(message=f"Got unexpected type {type(conf)}")

        self.logger.debug("finished execution")

        return Success
