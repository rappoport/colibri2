"""Colibri initialize3 command.

Functions:
    initialize3: initialize3 functional API

Classes:
    Initialize3Command: initialize3 command API
"""

from .command import Command
from ..cache import RedisCache
from ..config import Config
from ..data import Superformula
from ..db import SQLAlchemyDB
from ..exceptions import CommandError
from ..result import Result, Success
from ..taskqueue import RedisQueue

__all__ = ['initialize3', 'Initialize3Command']


def initialize3(config: Config) -> Result:
    return Initialize3Command(config).execute()


class Initialize3Command(Command):
    def __init__(self, config: Config):
        super().__init__(config)

        self.queue = RedisQueue(self.config)
        self.logger.debug(f"connected to task queue {self.queue.url}")

        self.cache = RedisCache(self.config)
        self.logger.debug(f"connected to cache {self.cache.url}")

        self.db = SQLAlchemyDB(self.config)
        self.logger.debug(f"connected to database {self.db.url}")

    def execute(self) -> Result:
        self.logger.debug("starting execution")

        self.db.clear_transformations()
        self.db.clear_flasks('Superformula')
        self.db.clear_flasks('InvalidSuperformula')
        self.db.clear_rules()
        self.queue.clear_flasks('Superformula')
        self.cache.clear_flasks('Superformula')

        initial_flask = self.config.NETWORK3.INITIAL_FLASK
        if not initial_flask:
            raise CommandError("initialize3 command needs initial flask")

        index = self.config.REACTION.get('INDEX')
        stereo = self.config.REACTION.get('STEREO')
        if index:
            self.logger.debug("using permutational index")
        initial_flask = Superformula(initial_flask, stereo=stereo, index=index)

        self.logger.debug(f"inserting initial flask {initial_flask}")
        self.queue.append_flask('Superformula', initial_flask)
        self.cache.set_flask('Superformula', initial_flask.key, initial_flask)

        self.logger.debug("finished execution")

        return Success
