"""Colibri2 compute3 command (distributed energy computation).

Functions:
    compute3: compute3 functional API

Classes:
    Compute3Command: compute3 command API
"""

import sys
import time

from .command import Command
from ..config import Config
from ..data import Configuration, Geometry, Property, FailedFormula, FailedConfiguration, \
    FailedGeometry, FailedProperty, InvalidConfiguration, InvalidGeometry, InvalidProperty
from ..db import SQLAlchemyDB
from ..exceptions import CommandError
from ..result import Result, Success
from ..task import RDKitBuildTask, PybelBuildTask, MOPACGeometryTask, XTBGeometryTask, XTBEnergyPropertyTask
from ..taskqueue import RedisQueue

__all__ = ['compute3', 'Compute3Command']


def compute3(config: Config):
    return Compute3Command(config).execute()


class Compute3Command(Command):
    def __init__(self, config: Config):
        super().__init__(config)

        builder = self.config.CONFIGURATION.PROGRAM.lower()

        if builder == 'rdkit':
            self.build_task = RDKitBuildTask(self.config)
        elif builder == 'pybel':
            self.build_task = PybelBuildTask(self.config)
        else:
            raise CommandError(message="invalid configuration program in compute2 command")

        optimizer = self.config.COMPUTE3.OPTIMIZER.lower()

        if optimizer == 'mopac':
            self.geometry_task = MOPACGeometryTask(self.config)
        elif optimizer == 'xtb':
            self.geometry_task = XTBGeometryTask(self.config)
        else:
            raise CommandError(message="invalid optimizer in compute3 command")

        self.property_task = None
        if self.config.COMPUTE3.PROPERTY:
            property_type = self.config.COMPUTE3.PROPERTY_TYPE.lower()
            property_calculator = self.config.COMPUTE3.PROPERTY_CALCULATOR.lower()

            if property_type == 'energy' and property_calculator == 'xtb':
                self.property_task = XTBEnergyPropertyTask(self.config)
            else:
                raise CommandError(message="invalid property calculator in compute3 command")

        self.queue = RedisQueue(self.config)
        self.logger.debug(f"connected to task queue {self.queue.url}")

        self.db = SQLAlchemyDB(self.config)
        self.logger.debug(f"connected to database {self.db.url}")

    def execute(self) -> Result:
        self.logger.debug("starting execution")

        tasks_per_command = self.config.COMPUTE3.TASKS_PER_COMMAND
        empty_tasks_to_stop = self.config.COMPUTE3.EMPTY_TASKS_TO_STOP

        tasks = 0
        empty_tasks = 0

        if self.config.LOGGING.SCREEN_LOG:
            print("Starting execution")
            sys.stdout.flush()

        while True:
            if 0 <= tasks_per_command <= tasks:
                self.logger.debug(f"executed {tasks} tasks, stopping")
                if self.config.LOGGING.SCREEN_LOG:
                    print("Stopping execution")
                    sys.stdout.flush()
                break

            if 0 <= empty_tasks_to_stop <= empty_tasks:
                self.logger.debug(f"received {empty_tasks} empty tasks, stopping")
                if self.config.LOGGING.SCREEN_LOG:
                    print("Stopping execution")
                    sys.stdout.flush()
                break

            if self.config.LOGGING.SCREEN_LOG:
                if tasks > 0 and tasks % self.config.LOGGING.SCREEN_LOG_INTERVAL == 0:
                    print(f"Processed {tasks} tasks")
                    sys.stdout.flush()

            inp = self.queue.remove_molecule('Formula')

            if not inp:
                empty_tasks += 1
                self.logger.debug(f"empty task, {empty_tasks_to_stop - empty_tasks} empty tasks to stop")
                time.sleep(self.config.COMPUTE3.EMPTY_TASK_INTERVAL)
                continue

            self.logger.debug(f"<{inp}>: retrieved from queue")

            conf = self.build_task.process(inp)
            self.logger.debug(f"<{conf}>: finished building")

            if isinstance(conf, Configuration):

                self.logger.debug(f"<{conf}>: successful molecule building")

                geom = self.geometry_task.process(conf)
                self.logger.debug(f"<{geom}>: finished optimization")

                if isinstance(geom, Geometry):

                    self.logger.debug(f"<{geom}: successful molecule optimization")

                    if self.property_task:

                        prop = self.property_task.process(geom)
                        self.logger.debug(f"<{prop}>: finished property calculation")

                        if isinstance(prop, Property):

                            self.logger.debug(f"<{prop}>: successful property calculation")
                            self.db.store_molecule('Property', prop.tag, prop)
                        
                        elif isinstance(prop, FailedProperty):

                            self.logger.debug(f"<{prop}>: failed property calculation")
                            self.db.store_molecule('FailedProperty', prop.tag, prop)

                        elif isinstance(prop, FailedGeometry):

                            self.logger.debug(f"<{prop}>: failed property calculation")
                            self.db.store_molecule('FailedGeometry', prop.tag, prop)

                        elif isinstance(prop, InvalidProperty):

                            self.logger.debug(f"<{prop}>: invalid property calculation")
                            self.db.store_molecule('InvalidProperty', prop.tag, prop)

                        else:
                            raise CommandError(message=f"Got unexpected type {type(prop)}")

                    else:
                        self.db.store_molecule('Geometry', geom.tag, geom)

                elif isinstance(geom, FailedConfiguration):

                    self.logger.debug(f"<{geom}>: failed molecule optimization")
                    self.db.store_molecule('FailedConfiguration', geom.tag, geom)

                elif isinstance(geom, InvalidGeometry):

                    self.logger.debug(f"<{geom}>: invalid molecule optimization")
                    self.db.store_molecule('InvalidGeometry', geom.tag, geom)

                else:
                    raise CommandError(message=f"Got unexpected type {type(geom)}")

            elif isinstance(conf, FailedFormula):

                self.logger.debug(f"<{conf}>: failed molecule building")
                self.db.store_molecule('FailedFormula', conf.tag, conf)

            elif isinstance(conf, InvalidConfiguration):

                self.logger.debug(f"<{conf}>: invalid molecule building")
                self.db.store_molecule('InvalidConfiguration', conf.tag, conf)

            else:
                raise CommandError(message=f"Got unexpected type {type(conf)}")

            tasks += 1

        self.logger.debug("finished execution")

        return Success
