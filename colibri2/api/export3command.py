"""Colibri2 export3 command.

Functions:
    export3: export3 functional API

Classes:
    Export3Command: export3 command API
"""
import numpy as np
from .command import Command
from ..config import Config
from ..db import SQLAlchemyDB, Psycopg2DB
from ..exceptions import CommandError
from ..graph import NetworkXWriter, GraphMLStreamWriter, GraphMLMultiStreamWriter
from ..result import Result, Success

__all__ = ['export3', 'Export3Command']


def export3(config: Config) -> Result:
    return Export3Command(config).execute()


class Export3Command(Command):
    def __init__(self, config: Config):
        super().__init__(config)

        if self.config.WRITER.METHOD == "graphml_stream":
            self.stream_writer = GraphMLStreamWriter(self.config)
            self.logger.debug("initialized graph writer")
            self.writer = None
            self.multi_stream_writer = None
        elif self.config.WRITER.METHOD == "multi_graphml_stream":
            self.multi_stream_writer = GraphMLMultiStreamWriter(self.config)
            self.logger.debug("initialized graph writer")
            self.writer = None
            self.stream_writer = None
        elif self.config.WRITER.METHOD == "networkx":
            self.writer = NetworkXWriter(self.config)
            self.logger.debug("initialized graph writer")
            self.stream_writer = None
            self.multi_stream_writer = None
        else:
            self.logger.error(f"{config.WRITER.METHOD} is not a valid writer method")

        if self.config.DB.DRIVER == "psycopg2":
            self.db = Psycopg2DB(self.config)
            self.logger.debug(f"connected to database {self.db.url} using psycopg2 driver")
        elif self.config.DB.DRIVER == "sqlalchemy":
            self.db = SQLAlchemyDB(self.config)
            self.logger.debug(f"connected to database {self.db.url} using sqlalchemy driver")
        else:
            self.logger.error(f"invalid export3 database driver {config.DB.DRIVER}")

    def execute(self) -> Result:
        self.logger.debug("started execution")

        output_path = self.config.WRITER.OUTPUT_PATH
        if not output_path:
            raise CommandError(message="export3 command needs output path")

        output_format = self.config.WRITER.OUTPUT_FORMAT
        if output_format != 'graphml':
            raise CommandError(f"invalid export3 output format {output_format}")
        
        node_id_type = self.config.WRITER.NODE_ID_TYPE
        if  node_id_type not in ['key', 'id']:
            raise CommandError(f"invalid export3 node id format {output_format}")

        if self.stream_writer is not None:
            self.stream_writer.open_file()
            self.stream_writer.write_header()
            if node_id_type == 'id':
                for id, _, data in self.db.fetch_all_flasks('Superformula', raw=True):
                    self.stream_writer.write_node(str(id), {'smiles': data['smiles']})
                for _, _, reactantid, productid, _, _, rule, _ in self.db.fetch_all_transformations(raw=True):
                    self.stream_writer.write_edge(str(reactantid), str(productid), rule)
            elif node_id_type == 'key':
                for _, key, data in self.db.fetch_all_flasks('Superformula', raw=True):
                    self.stream_writer.write_node(key, {'smiles': data['smiles']})
                for reactantkey, productkey, _, _, _, _, rule, _ in self.db.fetch_all_transformations(raw=True):
                    self.stream_writer.write_edge(reactantkey, productkey, rule)
            self.stream_writer.write_footer()
        
        elif self.multi_stream_writer is not None:
            self.multi_stream_writer.open_file()
            num_nodes = self.db.get_number_flasks('Superformula')
            num_edges = self.db.get_number_transformations()
            num_workers = self.config.WRITER.NUMBER_WORKERS
            worker_num = self.config.WRITER.WORKER_NUMBER

            if num_workers < 2:
                raise CommandError(f"minimum number of workers is 2, but {num_workers} provided")

            # Calculate the number of node workers 
            if num_nodes >= num_edges:
                num_node_workers = int(num_workers * num_nodes / (num_nodes + num_edges))
            else:
                num_node_workers = num_workers - int(num_workers * num_edges / (num_edges + num_nodes))
            
            # Ensure at least one node worker
            if num_node_workers < 1:
                num_node_workers = 1

            if worker_num == 0:
                self.multi_stream_writer.write_header()

            if worker_num < num_node_workers:
                row_ids = np.linspace(0, num_nodes, num_node_workers + 1, dtype=int)
                range_num = worker_num
                if node_id_type == 'id':
                    for id, _, data in self.db.fetch_flasks_range('Superformula', row_ids[range_num], row_ids[range_num+1], raw=True):
                        self.multi_stream_writer.write_node(str(id), {'smiles': data['smiles']})
                elif node_id_type == 'key':
                    for _, key, data in self.db.fetch_flasks_range('Superformula', row_ids[range_num], row_ids[range_num+1], raw=True):
                        self.multi_stream_writer.write_node(key, {'smiles': data['smiles']})
            else:
                row_ids = np.linspace(0, num_edges, num_workers - num_node_workers + 1, dtype=int)
                range_num = worker_num - num_node_workers
                if node_id_type == 'id':
                    for _, _, reactantid, productid, _, _, rule, _ in self.db.fetch_transformations_range(row_ids[range_num], row_ids[range_num+1],raw=True):
                        self.multi_stream_writer.write_edge(str(reactantid), str(productid), rule)
                elif node_id_type == 'key':
                    for reactantkey, productkey, _, _, _, _, rule, _ in self.db.fetch_transformations_range(row_ids[range_num], row_ids[range_num+1],raw=True):
                        self.multi_stream_writer.write_edge(reactantkey, productkey, rule)

            if worker_num == num_workers-1:
                self.multi_stream_writer.write_footer()

        elif self.writer is not None:
            if node_id_type == 'id':
                for id, _, data in self.db.fetch_all_flasks('Superformula', raw=True):
                    self.writer.add_node(str(id), {'smiles': data['smiles']})
                for _, _, reactantid, productid, _, _, rule, _ in self.db.fetch_all_transformations(raw=True):
                    self.writer.add_edge(str(reactantid), str(productid), rule)
            elif node_id_type == 'key':
                for flask in self.db.fetch_all_flasks('Superformula'):
                    self.writer.add_node(flask.key, {'smiles': flask.smiles})
                for trans in self.db.fetch_all_transformations():
                    self.writer.add_edge(trans.reactantkey, trans.productkey, {'rule': trans.rule})
            self.writer.write('graphml', output_path)

        else:
            self.logger.error(f"invalid export3 writer method {self.config.WRITER.METHOD}")

        self.logger.debug("finished execution")

        return Success
