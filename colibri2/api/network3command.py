"""Colibri2 network3 command (distributed network construction).

Functions:
    network3: network3 functional API

Classes:
    Network3Command: network3 command API
"""

import time

from .command import Command
from ..cache import RedisCache
from ..config import Config
from ..db import SQLAlchemyDB
from ..result import Result, Success
from ..task import RDKitFlaskReactionTask
from ..taskqueue import RedisQueue
from ..utils import controlhash

__all__ = ['network3', 'Network3Command']


def network3(config: Config) -> Result:
    return Network3Command(config).execute()


class Network3Command(Command):
    def __init__(self, config: Config):
        super().__init__(config)

        self.task = RDKitFlaskReactionTask(self.config)

        self.queue = RedisQueue(self.config)
        self.logger.debug(f"connected to task queue {self.queue.url}")

        self.cache = RedisCache(self.config)
        self.logger.debug(f"connected to cache {self.cache.url}")

        self.db = SQLAlchemyDB(self.config)
        self.logger.debug(f"connected to database {self.db.url}")

    def execute(self) -> Result:
        self.logger.debug("starting execution")


        self.db.store_rules([(rule.ruleid, rule) for rule in self.task.reaction_rules])
        reaction_ruleids = {rule.rule: rule.ruleid for rule in self.task.reaction_rules}
        tasks_per_command = self.config.NETWORK3.TASKS_PER_COMMAND
        empty_tasks_to_stop = self.config.NETWORK3.EMPTY_TASKS_TO_STOP

        tasks = 0
        empty_tasks = 0

        inp = self.queue.remove_flask('Superformula')
        if inp:
            self.db.store_flask('Superformula', inp.key, inp)
        
        while True:
            if 0 <= tasks_per_command <= tasks:
                self.logger.debug(f"executed {tasks} tasks, stopping")
                break

            if 0 <= empty_tasks_to_stop <= empty_tasks:
                self.logger.debug(f"received {empty_tasks} empty tasks, stopping")
                break

            if not inp:
                empty_tasks += 1
                self.logger.debug(f"empty task, {empty_tasks_to_stop - empty_tasks} empty tasks to stop")
                time.sleep(self.config.NETWORK3.EMPTY_TASK_INTERVAL)
                continue

            self.logger.debug(f"<{inp}: retrieved from queue")
            out, valid, invalid, transformations = self.task.process(inp)
            self.logger.debug(f"<{out}: finished processing")
            for flask in valid:
                self.logger.debug(f"<{flask}: generated valid flask")
            for flask in invalid:
                self.logger.debug(f"<{flask}: generated invalid flask: {flask.error}")
            for trans in transformations:
                self.logger.debug(f"<{trans}>: generated transformation")

            valid = set(valid)
            cached = self.cache.get_flasks('Superformula', [flask.key for flask in valid])
            new = valid - set(cached)
            for flask in new:
                self.db.store_flask('Superformula', flask.key, flask)
            self.db.store_flasks('InvalidSuperformula', [((controlhash(flask.smiles), 44), flask) for flask in invalid])
            self.db.store_transformations([(trans.reactantkey, trans.productkey, reaction_ruleids.get(trans.rule), trans) for trans in transformations])

            self.cache.set_flasks('Superformula', [(flask.key, flask) for flask in new])
            self.queue.append_flasks('Superformula', list(new))

            tasks += 1
            inp = self.queue.remove_flask('Superformula')

        self.logger.debug("finished execution")

        return Success
