"""Colibri2 network2 command (single-threaded network construction).

Functions:
    network2: network2 functional API

Classes:
    Network2Command: network2 command API
"""

from collections import deque

from .command import Command
from ..config import Config
from ..data import Superformula
from ..exceptions import CommandError
from ..graph import NetworkXWriter
from ..result import Result, Success
from ..task import RDKitFlaskReactionTask

__all__ = ['network2', 'Network2Command']


def network2(config: Config) -> Result:
    return Network2Command(config).execute()


class Network2Command(Command):
    def __init__(self, config: Config):
        super().__init__(config)

        self.task = RDKitFlaskReactionTask(self.config)
        self.writer = NetworkXWriter(self.config)
        self.queue = deque()
        self.cache = set()
        self.flasks = set()
        self.invalid_flasks = set()
        self.transformations = set()

    def execute(self) -> Result:

        self.logger.debug("starting execution")

        output_path = self.config.WRITER.OUTPUT_PATH
        if not output_path:
            raise CommandError(message="network2 command needs output path")

        initial_flask = self.config.NETWORK2.INITIAL_FLASK
        if not initial_flask:
            raise CommandError(message="network2 command needs initial flask")
        index = self.config.REACTION.get('INDEX')
        stereo = self.config.REACTION.get('STEREO')
        initial_flask = Superformula(initial_flask, index=index, stereo=stereo)

        self.logger.debug(f"<{initial_flask}>: received initial flask")

        if self.config.NETWORK2.SINGLE_STEP:
            out, valid, invalid, transformations = self.task.process(initial_flask)
            for flask in valid:
                self.logger.debug(f"<{flask}>: generated valid flask")
            for flask in invalid:
                self.logger.debug(f"<{flask}>: generated invalid flask: {flask.error}")
            for trans in transformations:
                self.logger.debug(f"<{trans}>: generated transformation")
            self.flasks.add(out)
            self.flasks.update(valid)
            self.invalid_flasks.update(invalid)
            self.transformations.update(transformations)
        else:
            self.logger.debug(f"inserting initial flask {initial_flask}")
            self.queue.append(initial_flask)
            while self.queue:
                inp = self.queue.popleft()
                self.logger.debug(f"<{inp}>: retrieved from queue ({len(self.queue)} left)")
                out, valid, invalid, transformations = self.task.process(inp)
                self.logger.debug(f"<{out}>: finished processing")
                for flask in valid:
                    self.logger.debug(f"<{flask}>: generated valid flask")
                for flask in invalid:
                    self.logger.debug(f"<{flask}>: generated invalid flask: {flask.error}")
                for trans in transformations:
                    self.logger.debug(f"<{trans}>: generated transformation")
                self.flasks.add(out)
                self.invalid_flasks.update(invalid)
                self.transformations.update(transformations)
                self.queue.extend(set(valid) - self.cache)
                self.cache.update(valid)

        self.logger.debug(f"storing {len(self.flasks)} nodes and {len(self.transformations)} "
                          f"edges to {output_path}")
        for flask in self.flasks:
            self.writer.add_node(flask.key, {'smiles': flask.smiles})
        for trans in self.transformations:
            self.writer.add_edge(trans.reactantkey, trans.productkey, {'rule': trans.rule})
        self.writer.write('graphml', output_path)

        for flask in self.invalid_flasks:
            self.logger.debug(f"<{flask}>: collected invalid flask")

        self.logger.debug("finishing execution")

        return Success
