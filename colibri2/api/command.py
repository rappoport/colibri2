"""API commands for colibri2.

Classes:
    Command: Abstract base class for colibri2 API commands
"""
import logging
import pathlib
from abc import ABC, abstractmethod

from ..config import Config
from ..result import Result
from ..utils import ensure_logger, random_id

__all__ = ['Command']


class Command(ABC):
    def __init__(self, config: Config):
        self.config = config
        self.logger = ensure_logger(str(self), self.config.LOGGING.LEVEL.upper())
        if self.config.LOGGING.FILENAME:
            base_path = pathlib.Path(self.config.LOGGING.FILENAME)
            filename = base_path.with_stem(base_path.stem + '-' + str(self))
            logging.basicConfig(filename=filename)
        else:
            logging.basicConfig()


    def __str__(self):
        return type(self).__name__.replace('Command', '').lower() + '-' + random_id(6)

    @abstractmethod
    def execute(self) -> Result:
        pass
