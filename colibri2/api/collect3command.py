"""Colibri2 collect3 command.

Functions:
    collect3: collect3 functional API

Classes:
    Collect3Command: collect3 command API
"""

import csv

from .command import Command
from ..config import Config
from ..db import SQLAlchemyDB, Psycopg2DB
from ..exceptions import CommandError
from ..result import Result, Success

__all__ = ['collect3', 'Collect3Command']


def collect3(config: Config) -> Result:
    return Collect3Command(config).execute()


class Collect3Command(Command):
    def __init__(self, config: Config):
        super().__init__(config)

        if self.config.DB.DRIVER == "psycopg2":
            self.db = Psycopg2DB(self.config)
            self.logger.debug(f"connected to database {self.db.url} using psycopg2 driver")
        elif self.config.DB.DRIVER == "sqlalchemy":
            self.db = SQLAlchemyDB(self.config)
            self.logger.debug(f"connected to database {self.db.url} using sqlalchemy driver")
        else:
            self.logger.error(f"invalid collect3 database driver {config.DB.DRIVER}")

    def execute(self) -> Result:
        self.logger.debug("started execution")

        output_path = self.config.COLLECT3.OUTPUT_PATH
        if not output_path:
            raise CommandError("collect3 command needs output path")

        output_format = self.config.COLLECT3.OUTPUT_FORMAT
        free_energy = self.config.COLLECT3.FREE_ENERGY
        property = self.config.COLLECT3.PROPERTY

        if free_energy and property:
            raise CommandError("property and free energy flags are incompatible")

        mol_type = 'Property' if property else 'Geometry'

        if output_format == 'csv':
            with open(output_path, 'w', newline='') as f:
                field_names = ['smiles', 'formula', 'inchikey', 'tag', 'energy']
                if free_energy:
                    field_names.append('free_energy')
                writer = csv.DictWriter(f, fieldnames=field_names)
                writer.writeheader()
                for mol in self.db.fetch_all_molecules(mol_type):
                    data = {
                        'smiles': mol.smiles,
                        'inchikey': mol.inchikey,
                        'formula': mol.sumformula,
                        'tag': mol.tag,
                    }
                    if property:
                        data['energy'] = mol.properties['energy']
                    else:
                        data['energy'] = mol.energy
                        if free_energy:
                            data['free_energy'] = mol.free_energy
                    writer.writerow(data)
        else:
            raise CommandError(f"invalid collect3 output format {output_format}")

        self.logger.debug("finished execution")

        return Success
