"""Data types for colibri2.

Classes use graph representations based on (CX)SMILES strings,
https://docs.chemaxon.com/display/docs/ChemAxon_Extended_SMILES_and_SMARTS_-_CXSMILES_and_CXSMARTS.html
or 3D structure representations based on MOL format strings
https://www.3dsbiovia.com/products/collaborative-science/biovia-draw/ctfile-no-fee.html
It would be preferable to use non-proprietary CML format,
https://xml-cml.org, however due to a bug in RDKit, the numbers of unpaired
electrons per atom are not exported.

Objects:
    MOLECULE_TYPES: Mapping from molecule data class names to types
    FLASK_TYPES: Mapping from flask data class names to types

Classes:
    Molecule: Abstract base class for molecular structures
    ValidMolecule: Abstract base class for valid molecular structures
    Structure: Valid molecules with graph representations ((CX)SMILES)
    Formula: Valid molecules with graph representations
    Structure3D: Valid molecules with 3D structure representations (MOL)
    Configuration: Valid molecules from heuristic builders
    Geometry: Valid molecules from energy-based structure optimizations
    Property: Valid molecules with structure-dependent properties
    PendingMolecule: Abstract base class for molecules waiting for results
    IncompleteMolecule: Abstract base class for molecules lacking upstream results
    InvalidMolecule: Abstract base class for invalid molecular structures
    InvalidStructure: Invalid molecules graph representations
    InvalidFormula: Invalid molecules graph representations
    FailedFormula: Failed molecules graph representations
    InvalidStructure3D: Invalid molecules having 3D structure representations
    InvalidConfiguration: Invalid molecules from heuristic builders
    FailedConfiguration: Failed molecules from heuristic builders
    InvalidGeometry: Invalid molecules from structure optimizations
    FailedGeometry: Failed molecules from structure optimizations
    InvalidProperty: Invalid molecules with structure-dependent properties
    FailedProperty: Failed molecules with structure-dependent properties
    Flask: Abstract base class for molecular collections
    ValidFlask: Abstract base class for valid molecular collections
    Superstructure: Valid flasks with graph representations ((CX)SMILES)
    Superformula: Valid flasks with graph representations
    Supergeometry: Valid flasks with graph representations and energies
    Superproperty: Valid flasks with structure-dependent properties
    PendingFlask: Abstract base class for flasks waiting for results
    PendingSupergeometry: Pending flask, waiting for energy evaluations
    IncompleteFlask: Abstract base class for flasks lacking upstream results
    IncompleteSupergeometry: Incomplete flask lacking energy result
    InvalidFlask: Abstract base class for invalid molecular collections
    InvalidSuperstructure: Invalid flask with graph representations
    InvalidSuperformula: Invalid flasks with graph representations
    FailedSuperformula: Failed flasks with graph representations
    InvalidSupergeometry: Invalid flasks with graph representations and energies
    FailedSupergeometry: Failed flasks with graph representations and energies
    InvalidSuperproperty: Failed flasks with structure-dependent properties
    FailedSuperproperty: Failed flasks with structure-dependent properties
    Transformation: Flask transformation object
    Rule: Transformation rule 
"""
import uuid
from abc import ABC, abstractmethod
from dataclasses import dataclass, field, asdict, replace
from typing import Dict, Any, List, Optional, Tuple

from rdkit import Chem, rdBase
from rdkit.Chem import AllChem, Descriptors

from .exceptions import MoleculeError, FlaskError, TransformationError, \
    ReactionRuleError
from .structure import canonicalize, split_smiles, combine_smiles, \
    adj_compress, composition, bond_compress
from .utils import stringhash, lexicographic_index, base26, controlhash

__all__ = ['Molecule', 'ValidMolecule', 'Formula', 'Configuration', 'Geometry',
           'Property', 'PendingMolecule', 'IncompleteMolecule', 'InvalidMolecule',
           'InvalidFormula', 'FailedFormula', 'InvalidConfiguration',
           'FailedConfiguration', 'InvalidGeometry', 'FailedGeometry',
           'InvalidProperty', 'FailedProperty',
           'Flask', 'ValidFlask', 'Superstructure', 'Superformula',
           'Supergeometry', 'Superproperty', 'PendingFlask', 'PendingSupergeometry',
           'IncompleteFlask', 'IncompleteSupergeometry', 'InvalidFlask',
           'InvalidSuperstructure', 'InvalidSuperformula', 'FailedSuperformula',
           'InvalidSupergeometry', 'FailedSupergeometry',
           'Transformation', 'Rule', 'MOLECULE_TYPES', 'FLASK_TYPES']

# Shut up RDKit
rdBase.DisableLog('rdApp.warning')
rdBase.DisableLog('rdApp.error')


class Molecule(ABC):
    @abstractmethod
    def todict(self, skip_none_values: bool = False, skip_keys: list[str] = []) -> Dict[str, Any]:
        pass

    @classmethod
    @abstractmethod
    def fromdict(cls, d: Dict[str, Any]) -> 'Molecule':
        pass


class ValidMolecule(Molecule, ABC):
    _rdmol: Chem.Mol = None
    smiles: str = None
    stereo: bool = None

    @property
    def plainsmiles(self) -> str:
        return Chem.MolToSmiles(Chem.RemoveHs(self._rdmol, sanitize=False), canonical=True)

    @property
    def charge(self) -> int:
        return Chem.GetFormalCharge(self._rdmol)

    @property
    def mult(self) -> int:
        return Descriptors.NumRadicalElectrons(self._rdmol) + 1

    @property
    def key(self) -> str:
        return f"{self.inchikey}-{self.smileskey}-{self.permkey}"

    @property
    def plainkey(self) -> str:
        return f"{self.inchikey}-{self.smileskey}"

    @property
    def inchi(self) -> str:
        options = ['/DoNotAddH']
        if not self.stereo:
            options.append('/SNon')
        options = ' '.join(options)
        return Chem.MolToInchi(self._rdmol, options)

    @property
    def inchikey(self) -> str:
        options = ['/DoNotAddH']
        if not self.stereo:
            options.append('/SNon')
        options = ' '.join(options)
        return Chem.MolToInchiKey(self._rdmol, options)

    @property
    def smileskey(self) -> str:
        return controlhash(self.plainsmiles, 10)

    @property
    def permindices(self) -> List[int]:
        return [at.GetUnsignedProp('p') for at in Chem.RemoveHs(self._rdmol, sanitize=False).GetAtoms()
                if at.GetSymbol() != 'H' and at.HasProp('p')]

    @property
    def perm(self) -> str:
        return ",".join(str(p) for p in self.permindices)

    @property
    def permkey(self) -> str:
        return base26(lexicographic_index(self.permindices), 5)

    @property
    def sumformula(self) -> str:
        return AllChem.CalcMolFormula(self._rdmol)

    @property
    def adjacency(self) -> List[List[int]]:
        return [[j for j, conn_ij in enumerate(conn_i) if conn_ij]
                for i, conn_i in enumerate(
                Chem.GetAdjacencyMatrix(self._rdmol).tolist())]

    @property
    def adjkey(self) -> str:
        return adj_compress(self.adjacency)

    @property
    def atoms(self) -> List[Tuple[int, str]]:
        return [(at.GetIdx(), at.GetSymbol()) for at in self._rdmol.GetAtoms()]

    @property
    def atomkey(self) -> str:
        return composition(self.atoms)

    @property
    def bonds(self) -> List[Tuple[int, int, str]]:
        return [(bd.GetBeginAtomIdx(), bd.GetEndAtomIdx(), str(bd.GetBondType()).split('.')[-1])
                for bd in self._rdmol.GetBonds()]

    @property
    def bondkey(self) -> str:
        return bond_compress(self.bonds)

    @property
    def ez_stereo(self) -> List[Tuple[int, str]]:
        return [(bd.GetIdx(), str(bd.GetStereo()).split('.')[-1].replace('STEREO', ''))
                for bd in self._rdmol.GetBonds()
                if bd.GetStereo() != Chem.BondStereo.STEREONONE]

    @property
    def chiral_stereo(self) -> List[Tuple[int, str]]:
        return [(at.GetIdx(), str(at.GetChiralTag()).split('.')[-1].replace('CHI_TETRAHEDRAL_', ''))
                for at in self._rdmol.GetAtoms()
                if at.GetChiralTag() in
                {Chem.ChiralType.CHI_TETRAHEDRAL_CW, Chem.CHI_TETRAHEDRAL_CCW}]

    @property
    def priority(self) -> float:
        return round(Descriptors.MolWt(self._rdmol))

    def __str__(self):
        return f"{self.plainsmiles} ({self.key}/{self.priority})"

    def __hash__(self):
        return stringhash(self.key)


@dataclass(frozen=True, order=True)
class Structure(ValidMolecule):
    smiles: str = field(repr=True, compare=True)
    index: bool = field(default=False, repr=True, compare=True)
    stereo: bool = field(default=False, repr=True, compare=True)
    tag: str = field(default=None, repr=False, compare=False)
    program: str = field(default=None, repr=False, compare=False)
    version: str = field(default=None, repr=False, compare=False)
    method: str = field(default=None, repr=False, compare=False)
    options: str = field(default=None, repr=False, compare=False)
    _rdmol: Chem.Mol = field(default=None, init=False, repr=False, compare=False)

    def __post_init__(self):
        if self.tag is None:
            object.__setattr__(self, 'tag', str(uuid.uuid4()))

        # Check if SMILES is valid
        if self.smiles is None or self.smiles == "":
            raise MoleculeError(message="empty SMILES string")
        rdmol_in = Chem.MolFromSmiles(self.smiles, sanitize=True)
        if rdmol_in is None:
            raise MoleculeError(message="reading SMILES string failed")
        if len(Chem.GetMolFrags(rdmol_in)) > 1:
            raise MoleculeError(message="multiple molecules in SMILES string")

        # Canonicalize and sanitize SMILES string
        smiles_out = canonicalize(self.smiles, self.index, self.stereo)
        rdmol_can = Chem.MolFromSmiles(smiles_out, sanitize=False)
        if rdmol_can is None:
            raise MoleculeError(message="invalid SMILES string")
        aromatic = any(bd.GetBondType() == Chem.BondType.AROMATIC for bd in rdmol_can.GetBonds())
        flags = Chem.SANITIZE_ALL
        if not aromatic:
            flags &= ~Chem.SANITIZE_SETAROMATICITY
        error = Chem.SanitizeMol(rdmol_can, sanitizeOps=flags, catchErrors=True)
        if error != 0:
            raise MoleculeError(message="sanitization error")
        if self.stereo:
            Chem.AssignStereochemistry(rdmol_can)
        rdmol_out = Chem.AddHs(rdmol_can)

        object.__setattr__(self, '_rdmol', rdmol_out)
        object.__setattr__(self, 'smiles', smiles_out)

    def __hash__(self):
        return super().__hash__()

    @classmethod
    def fromdict(cls, d: Dict[str, Any]) -> 'Structure':
        return cls(**d)

    def todict(self, skip_none_values: bool = False, skip_keys: list[str] = []) -> Dict[str, Any]:
        return {key: value for key, value in asdict(self).items() 
            if not key.startswith("_") and (not skip_none_values or value is not None) and (key not in skip_keys)
        }

    def copy(self, **kwargs) -> 'Structure':
        return replace(self, **kwargs)


@dataclass(frozen=True, order=True)
class Formula(Structure):
    pass

    def __hash__(self):
        return super().__hash__()


@dataclass(frozen=True, order=True)
class Structure3D(ValidMolecule):
    coord: str = field(repr=True, compare=True)
    stereo: bool = field(default=False, repr=True, compare=True)
    tag: str = field(default=None, repr=False, compare=False)
    program: str = field(default=None, repr=False, compare=False)
    version: str = field(default=None, repr=False, compare=False)
    method: str = field(default=None, repr=False, compare=False)
    options: str = field(default=None, repr=False, compare=False)
    _rdmol: Chem.Mol = field(default=None, init=False, repr=False, compare=False)

    def __post_init__(self):
        if self.tag is None:
            object.__setattr__(self, 'tag', str(uuid.uuid4()))
        if self.coord is None or self.coord == "":
            raise MoleculeError(message="empty MOL format string")
        rdmol = Chem.MolFromMolBlock(self.coord, sanitize=False, removeHs=False)
        if rdmol is None:
            raise MoleculeError(message="reading MOL format failed")

        if rdmol.GetNumBonds() == 0:
            # No bonds, all atoms must be explicit
            for at in rdmol.GetAtoms():
                at.SetNoImplicit(True)

        # MOL structure has bonds, sanitize structure definitions
        aromatic = any(bd.GetBondType() == Chem.BondType.AROMATIC for bd in rdmol.GetBonds())
        flags = Chem.SANITIZE_ALL
        if not aromatic:
            flags &= ~Chem.SANITIZE_SETAROMATICITY
        error = Chem.SanitizeMol(rdmol, sanitizeOps=flags, catchErrors=True)
        if error != 0:
            raise MoleculeError(message="sanitization error")

        if not self.stereo:
            Chem.RemoveStereochemistry(rdmol)

        object.__setattr__(self, '_rdmol', rdmol)

    def __hash__(self):
        return super().__hash__()

    @property
    def smiles(self) -> str:
        flags = {'canonical': True}
        if self._rdmol.GetNumBonds() == 0:
            flags['allHsExplicit'] = True
        return Chem.MolToSmiles(Chem.RemoveHs(self._rdmol, sanitize=False), **flags)

    @classmethod
    def fromdict(cls, d: Dict[str, Any]) -> 'Structure3D':
        return cls(**d)

    def todict(self, skip_none_values: bool = False, skip_keys: list[str] = []) -> Dict[str, Any]:
        return {key: value for key, value in asdict(self).items() 
            if not key.startswith("_") and (not skip_none_values or value is not None) and (key not in skip_keys)
        }

    def copy(self, **kwargs) -> 'Structure3D':
        return replace(self, **kwargs)


@dataclass(frozen=True, order=True)
class Configuration(Structure3D):
    def __hash__(self):
        return super().__hash__()


@dataclass(frozen=True, order=True)
class Geometry(Structure3D):
    energy: float = field(default=None, repr=True, compare=False)
    converged: bool = field(default=None, repr=True, compare=False)
    free_energy: float = field(default=None, repr=True, compare=False)
    stable: bool = field(default=None, repr=True, compare=False)
    raw_output: str = field(default=None, repr=True, compare=False)

    def __hash__(self):
        return super().__hash__()


@dataclass(frozen=True, order=True)
class Property(Structure3D):
    properties: dict = field(default_factory=dict, repr=True, compare=False)

    def __hash__(self):
        return super().__hash__()


class PendingMolecule(ValidMolecule, ABC):
    pass


class IncompleteMolecule(ValidMolecule, ABC):
    pass


class InvalidMolecule(Molecule, ABC):
    pass


@dataclass(frozen=True, order=True)
class InvalidStructure(InvalidMolecule):
    smiles: str = field(repr=True, compare=True)
    error: str = field(repr=True, compare=False)
    tag: str = field(default=None, repr=True, compare=False)
    program: str = field(default=None, repr=False, compare=False)
    version: str = field(default=None, repr=False, compare=False)
    method: str = field(default=None, repr=False, compare=False)
    options: str = field(default=None, repr=False, compare=False)

    def __post_init__(self):
        if self.tag is None:
            object.__setattr__(self, 'tag', str(uuid.uuid4()))

    @classmethod
    def fromdict(cls, d: Dict[str, Any]) -> 'InvalidStructure':
        return cls(**d)

    def todict(self, skip_none_values: bool = False, skip_keys: list[str] = []) -> Dict[str, Any]:
        return {key: value for key, value in asdict(self).items() 
            if not key.startswith("_") and (not skip_none_values or value is not None) and (key not in skip_keys)
        }

    def copy(self, **kwargs) -> 'InvalidStructure':
        return replace(self, **kwargs)

    @property
    def key(self):
        return self.tag

    def __str__(self):
        return f"{self.smiles} ({self.error})"


@dataclass(frozen=True, order=True)
class InvalidFormula(InvalidStructure):
    pass


@dataclass(frozen=True, order=True)
class FailedFormula(InvalidStructure):
    pass


@dataclass(frozen=True, order=True)
class InvalidStructure3D(InvalidMolecule):
    coord: str = field(repr=True, compare=True)
    error: str = field(repr=True, compare=False)
    tag: str = field(default=None, repr=True, compare=False)
    program: str = field(default=None, repr=False, compare=False)
    version: str = field(default=None, repr=False, compare=False)
    method: str = field(default=None, repr=False, compare=False)
    options: str = field(default=None, repr=False, compare=False)

    def __post_init__(self):
        if self.tag is None:
            object.__setattr__(self, 'tag', str(uuid.uuid4()))

    @classmethod
    def fromdict(cls, d: Dict[str, Any]) -> 'InvalidStructure3D':
        return cls(**d)

    def todict(self, skip_none_values: bool = False, skip_keys: list[str] = []) -> Dict[str, Any]:
        return {key: value for key, value in asdict(self).items() 
            if not key.startswith("_") and (not skip_none_values or value is not None) and (key not in skip_keys)
        }

    def copy(self, **kwargs) -> 'InvalidStructure3D':
        return replace(self, **kwargs)

    @property
    def smiles(self):
        rdmol = Chem.MolFromMolBlock(self.coord, sanitize=True, removeHs=False)
        if rdmol is None:
            smiles = "**INVALID**"
        else:
            try:
                smiles = Chem.MolToSmiles(Chem.RemoveHs(rdmol, sanitize=True), canonical=True)
            except Chem.KekulizeException as e:
                smiles = str(e)
        return smiles

    @property
    def key(self):
        return self.tag

    def __str__(self):
        return f"{self.smiles} ({self.error})"


@dataclass(frozen=True, order=True)
class InvalidConfiguration(InvalidStructure3D):
    pass


@dataclass(frozen=True, order=True)
class FailedConfiguration(InvalidStructure3D):
    pass


@dataclass(frozen=True, order=True)
class InvalidGeometry(InvalidStructure3D):
    pass


@dataclass(frozen=True, order=True)
class FailedGeometry(InvalidStructure3D):
    pass


@dataclass(frozen=True, order=True)
class InvalidProperty(InvalidStructure3D):
    pass


@dataclass(frozen=True, order=True)
class FailedProperty(InvalidStructure3D):
    pass


class Flask(ABC):
    @abstractmethod
    def todict(self, skip_none_values: bool = False, skip_keys: list[str] = []):
        pass

    @classmethod
    @abstractmethod
    def fromdict(cls, d):
        pass


class ValidFlask(Flask, ABC):
    _rdmol: Chem.Mol = None
    smiles: str = None
    stereo: bool = None

    @property
    def plainsmiles(self) -> str:
        return Chem.MolToSmiles(Chem.RemoveHs(self._rdmol, sanitize=False), canonical=True)

    @property
    def charge(self) -> int:
        return Chem.GetFormalCharge(self._rdmol)

    @property
    def mult(self) -> int:
        return int(Descriptors.NumRadicalElectrons(self._rdmol)) + 1

    @property
    def key(self) -> str:
        return f"{self.inchikey}-{self.smileskey}-{self.permkey}"

    @property
    def plainkey(self) -> str:
        return f"{self.inchikey}-{self.smileskey}"

    @property
    def inchi(self) -> str:
        options = ['/DoNotAddH']
        if not self.stereo:
            options.append('/SNon')
        options = ' '.join(options)
        return Chem.MolToInchi(self._rdmol, options)

    @property
    def inchikey(self) -> str:
        options = ['/DoNotAddH']
        if not self.stereo:
            options.append('/SNon')
        options = ' '.join(options)
        return Chem.MolToInchiKey(self._rdmol, options)

    @property
    def smileskey(self) -> str:
        return controlhash(self.plainsmiles, 10)

    @property
    def permindices(self) -> List[int]:
        return [at.GetUnsignedProp('p') for at in Chem.RemoveHs(self._rdmol, sanitize=False).GetAtoms()
                if at.GetSymbol() != 'H' and at.HasProp('p')]

    @property
    def perm(self) -> str:
        return ",".join(str(p) for p in self.permindices)

    @property
    def permkey(self) -> str:
        return base26(lexicographic_index(self.permindices), 5)

    @property
    def sumformula(self) -> str:
        return AllChem.CalcMolFormula(self._rdmol)

    def __str__(self):
        return f"{self.plainsmiles} ({self.key}/{self.priority})"

    def __hash__(self):
        return stringhash(self.key)

    @property
    def priority(self) -> float:
        return sum(round(Descriptors.MolWt(Chem.MolFromSmiles(s))) ** 2 for s in split_smiles(self.smiles, False))


@dataclass(frozen=True, order=True)
class Superstructure(ValidFlask):
    smiles: str = field(repr=True, compare=True)
    index: bool = field(default=False, repr=True, compare=True)
    stereo: bool = field(default=False, repr=True, compare=True)
    tag: str = field(default=None, repr=False, compare=False)
    program: str = field(default=None, repr=False, compare=False)
    version: str = field(default=None, repr=False, compare=False)
    method: str = field(default=None, repr=False, compare=False)
    options: str = field(default=None, repr=False, compare=False)
    _rdmol: Chem.Mol = field(default=None, init=False, repr=False, compare=False)

    def __post_init__(self):
        if self.tag is None:
            object.__setattr__(self, 'tag', str(uuid.uuid4()))

        # Check if SMILES is valid
        if self.smiles is None or self.smiles == "":
            raise FlaskError(message="empty SMILES string")
        rdmol_in = Chem.MolFromSmiles(self.smiles, sanitize=True)
        if rdmol_in is None:
            raise FlaskError(message="reading SMILES string failed")

        smiles_can = canonicalize(self.smiles, self.index, self.stereo)
        smiles_frags = split_smiles(smiles_can, self.index, self.stereo)
        mols = sorted([Formula(frag, self.index, self.stereo) for frag in smiles_frags])
        smiles_out = combine_smiles([mol.smiles for mol in mols], self.index, self.stereo)

        rdmol_can = Chem.MolFromSmiles(smiles_out, sanitize=False)
        if rdmol_can is None:
            raise MoleculeError(message="invalid SMILES string")
        aromatic_atoms = any(at.GetIsAromatic() for at in rdmol_can.GetAtoms())
        aromatic_bonds = any(bd.GetBondType() == Chem.BondType.AROMATIC for bd in rdmol_can.GetBonds())
        aromatic = aromatic_atoms | aromatic_bonds
        flags = Chem.SANITIZE_ALL
        if not aromatic:
            flags &= ~Chem.SANITIZE_SETAROMATICITY
        error = Chem.SanitizeMol(rdmol_can, sanitizeOps=flags, catchErrors=True)
        if error != 0:
            raise MoleculeError(message="sanitization error")
        if self.stereo:
            Chem.AssignStereochemistry(rdmol_can)
        rdmol_out = Chem.AddHs(rdmol_can)

        object.__setattr__(self, '_rdmol', rdmol_out)
        object.__setattr__(self, 'smiles', smiles_out)

    def count(self) -> int:
        return len(Chem.GetMolFrags(self._rdmol, asMols=True))

    def split(self) -> List['Formula']:
        return [Formula(smiles, self.index, self.stereo) for smiles in
                split_smiles(self.smiles, self.index, self.stereo)]

    @classmethod
    def fromdict(cls, d: Dict[str, Any]) -> 'Superstructure':
        return cls(**d)

    def todict(self, skip_none_values: bool = False, skip_keys: list[str] = []) -> Dict[str, Any]:
        return {key: value for key, value in asdict(self).items() 
            if not key.startswith("_") and (not skip_none_values or value is not None) and (key not in skip_keys)
        }

    def copy(self, **kwargs) -> 'Superstructure':
        return replace(self, **kwargs)

    @classmethod
    def combine(cls, smiles_list: List[str], index: bool = False, stereo: bool = False, **kwargs) -> 'Superstructure':
        if not smiles_list:
            raise FlaskError(message="empty input list")
        mol1 = Chem.MolFromSmiles(smiles_list[0], sanitize=False)
        if mol1 is None:
            raise FlaskError(message="invalid SMILES string")
        for smiles in smiles_list[1:]:
            mol2 = Chem.MolFromSmiles(smiles, sanitize=False)
            if mol2 is None:
                raise FlaskError(message="invalid SMILES string")
            mol1 = Chem.CombineMols(mol1, mol2)

        return cls(Chem.MolToCXSmiles(mol1, canonical=True), index, stereo, **kwargs)


@dataclass(frozen=True, order=True)
class Superformula(Superstructure):
    pass


@dataclass(frozen=True, order=True)
class Supergeometry(Superstructure):
    energy: float = field(default=None, repr=True, compare=False)
    converged: bool = field(default=None, repr=True, compare=False)
    free_energy: float = field(default=None, repr=True, compare=False)
    stable: bool = field(default=None, repr=True, compare=False)


@dataclass(frozen=True, order=True)
class Superproperty(Superstructure):
    properties: dict = field(default_factory=dict, repr=True, compare=False)


class PendingFlask(ValidFlask, ABC):
    pass


@dataclass(frozen=True, order=True)
class PendingSupergeometry(PendingFlask, Superstructure):
    attempts_left: int = field(default=None, repr=True, compare=False)


class IncompleteFlask(ValidFlask, ABC):
    pass


@dataclass(frozen=True, order=True)
class IncompleteSupergeometry(IncompleteFlask, Superstructure):
    pass


class InvalidFlask(Flask, ABC):
    pass


@dataclass(frozen=True, order=True)
class InvalidSuperstructure(InvalidFlask):
    smiles: str = field(repr=True, compare=True)
    error: str = field(repr=True, compare=False)
    tag: str = field(default=None, repr=True, compare=False)
    program: str = field(default=None, compare=False)
    version: str = field(default=None, compare=False)
    method: str = field(default=None, compare=False)
    options: str = field(default=None, compare=False)

    def __post_init__(self):
        if self.tag is None:
            object.__setattr__(self, 'tag', str(uuid.uuid4()))

    @classmethod
    def fromdict(cls, d: Dict[str, Any]) -> 'InvalidSuperstructure':
        return cls(**d)

    def todict(self, skip_none_values: bool = False, skip_keys: list[str] = []) -> Dict[str, Any]:
        return {key: value for key, value in asdict(self).items() 
            if not key.startswith("_") and (not skip_none_values or value is not None) and (key not in skip_keys)
        }

    def copy(self, **kwargs) -> 'InvalidSuperstructure':
        return replace(self, **kwargs)


@dataclass(frozen=True, order=True)
class InvalidSuperformula(InvalidSuperstructure):
    pass


@dataclass(frozen=True, order=True)
class FailedSuperformula(InvalidSuperstructure):
    pass


@dataclass(frozen=True, order=True)
class InvalidSupergeometry(InvalidSuperstructure):
    pass


@dataclass(frozen=True, order=True)
class FailedSupergeometry(InvalidSuperstructure):
    pass


@dataclass(frozen=True, order=True)
class InvalidSuperproperty(InvalidSuperstructure):
    pass


@dataclass(frozen=True, order=True)
class FailedSuperproperty(InvalidSuperstructure):
    pass


@dataclass(frozen=True, order=True)
class Rule:
    rule: str = field(repr=True, compare=True)
    ruleid: int = field(repr=True, compare=False)
    tag: str = field(default=None, repr=False, compare=False)

    def __post_init__(self):
        if self.tag is None:
            object.__setattr__(self, 'tag', str(uuid.uuid4()))

        if self.rule is None or self.rule == "":
            raise ReactionRuleError(message="empty SMARTS rule")
        try:
            rdrule = AllChem.ReactionFromSmarts(self.rule)
        except ValueError as e:
            raise ReactionRuleError(message=e)
        if rdrule is None:
            raise ReactionRuleError(message="reading SMARTS rule failed")
    
    @classmethod
    def fromdict(cls, d: Dict[str, Any]) -> 'Rule':
        return cls(**d)

    def todict(self, skip_none_values: bool = False, skip_keys: list[str] = []) -> Dict[str, Any]:
        return {key: value for key, value in asdict(self).items() 
            if not key.startswith("_") and (not skip_none_values or value is not None) and (key not in skip_keys)
        }

    def copy(self, **kwargs) -> 'Rule':
        return replace(self, **kwargs)

    def __str__(self):
        return f"{self.rule} ({self.ruleid})"
    

@dataclass(frozen=True, order=True)
class Transformation:
    reactantsmiles: str = field(repr=True, compare=True)
    productsmiles: str = field(repr=True, compare=True)
    rule: str = field(repr=True, compare=False)
    index: bool = field(default=False, repr=True, compare=False)
    stereo: bool = field(default=False, repr=True, compare=False)
    tag: str = field(default=None, repr=False, compare=False)
    _reactant: Superformula = field(default=None, init=False, repr=False, compare=False)
    _product: Superformula = field(default=None, init=False, repr=False, compare=False)

    def __post_init__(self):
        if self.tag is None:
            object.__setattr__(self, 'tag', str(uuid.uuid4()))

        if self.rule is None or self.rule == "":
            raise TransformationError(message="empty SMARTS rule")
        try:
            rdrule = AllChem.ReactionFromSmarts(self.rule)
        except ValueError as e:
            raise TransformationError(message=e)
        if rdrule is None:
            raise TransformationError(message="reading SMARTS rule failed")

        if self.reactantsmiles is None or self.reactantsmiles == "":
            raise TransformationError(message="empty reactant SMILES string")
        reactant = Superformula(self.reactantsmiles, self.index, self.stereo)

        if self.productsmiles is None or self.productsmiles == "":
            raise TransformationError(message="empty product SMILES string")
        product = Superformula(self.productsmiles, self.index, self.stereo)

        object.__setattr__(self, '_reactant', reactant)
        object.__setattr__(self, 'reactantsmiles', reactant.smiles)
        object.__setattr__(self, '_product', product)
        object.__setattr__(self, 'productsmiles', product.smiles)

    @property
    def reactantkey(self) -> str:
        return self._reactant.key

    @property
    def reactantplainsmiles(self) -> str:
        return self._reactant.plainsmiles

    @property
    def reactantplainkey(self) -> str:
        return self._reactant.plainkey

    @property
    def productkey(self) -> str:
        return self._product.key

    @property
    def productplainsmiles(self) -> str:
        return self._product.plainsmiles

    @property
    def productplainkey(self) -> str:
        return self._product.plainkey

    @property
    def key(self) -> str:
        return self.reactantkey + ">>" + self.productkey

    @classmethod
    def fromdict(cls, d: Dict[str, Any]) -> 'Transformation':
        return cls(**d)

    def todict(self, skip_none_values: bool = False, skip_keys: list[str] = []) -> Dict[str, Any]:
        return {key: value for key, value in asdict(self).items() 
            if not key.startswith("_") and (not skip_none_values or value is not None) and (key not in skip_keys)
        }

    def copy(self, **kwargs) -> 'Transformation':
        return replace(self, **kwargs)

    def __str__(self):
        return f"{self.reactantplainsmiles}->{self.productplainsmiles} ({self.key})"

    def __hash__(self):
        return stringhash(self.key)


MOLECULE_TYPES = {
    'Formula': Formula,
    'Configuration': Configuration,
    'Geometry': Geometry,
    'Property': Property,
    'InvalidFormula': InvalidFormula,
    'FailedFormula': FailedFormula,
    'InvalidConfiguration': InvalidConfiguration,
    'FailedConfiguration': FailedConfiguration,
    'InvalidGeometry': InvalidGeometry,
    'FailedGeometry': FailedGeometry,
    'InvalidProperty': InvalidProperty,
    'FailedProperty': FailedProperty
}

FLASK_TYPES = {
    'Superformula': Superformula,
    'Supergeometry': Supergeometry,
    'Superproperty': Superproperty,
    'PendingSupergeometry': PendingSupergeometry,
    'IncompleteSupergeometry': IncompleteSupergeometry,
    'InvalidSuperformula': InvalidSuperformula,
    'FailedSuperformula': FailedSuperformula,
    'InvalidSupergeometry': InvalidSupergeometry,
    'FailedSupergeometry': FailedSupergeometry,
    'InvalidSuperproperty': InvalidSuperproperty,
    'FailedSuperproperty': FailedSuperproperty,
}
