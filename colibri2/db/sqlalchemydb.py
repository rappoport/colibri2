"""SQLAlchemy-backed database interaction class.

Classes:
    SQLAlchemyDB: SQLAlchemy-backed database interaction class
"""

from typing import List, Generator, Tuple, Union

from sqlalchemy import MetaData, Table, Column, String, create_engine, select, \
    delete, Integer, ForeignKey, func, bindparam
from sqlalchemy.dialects.postgresql import JSONB, insert, dialect
from sqlalchemy.engine.url import make_url
from sqlalchemy.sql import alias
from sqlalchemy.schema import CreateTable
from sqlalchemy.exc import DataError as SQLDataError

from .db import DB
from ..config import Config
from ..data import Flask, Molecule, Transformation, Rule, MOLECULE_TYPES, \
    FLASK_TYPES
from ..utils import camelcase_to_uppercase
from ..exceptions import DataError


class SQLAlchemyDB(DB):

    def __init__(self, config: Config):
        super().__init__(config)

        url = make_url(self.config.DB.URL)
        self.url = repr(url)
        self.metadata = MetaData()

        self.logger.debug(f"connected to db {self.config.DB.URL} using sqlalchemy driver")

        self.molecule_tables = {}
        for type_ in MOLECULE_TYPES:
            conf_name = camelcase_to_uppercase(type_)
            if conf_name in config.DB.MOLECULE_TABLES:
                table = Table(
                    config.DB.MOLECULE_TABLES[conf_name],
                    self.metadata,
                    Column('key', String(44), primary_key=True),
                    Column('data', JSONB, nullable=False)
                )
                ddl = str(CreateTable(table)).replace('\n', ' ')
                self.logger.debug(f"created table {table.name} with schema {ddl}")
                self.molecule_tables[type_] = table

        self.flask_tables = {}
        for type_ in FLASK_TYPES:
            conf_name = camelcase_to_uppercase(type_)
            if conf_name in config.DB.FLASK_TABLES:
                table = Table(
                    self.config.DB.FLASK_TABLES[conf_name],
                    self.metadata,
                    Column('id', Integer, primary_key=True, autoincrement=True),
                    Column('key', String(44), unique=True),
                    Column('data', JSONB, nullable=False)
                )
                ddl = str(CreateTable(table)).replace('\n', ' ')
                self.logger.debug(f"created table {table.name} with schema {ddl}")
                self.flask_tables[type_] = table

        self.rule_table = None
        if config.DB.RULE_TABLE:
            table = Table(
                self.config.DB.RULE_TABLE,
                self.metadata,
                Column('id', Integer, primary_key=True),
                Column('data', JSONB, nullable=False),
            )
            ddl = str(CreateTable(table)).replace('\n', ' ')
            self.logger.debug(f"created table {table.name} with schema {ddl}")
            self.rule_table = table

        self.transformation_table = None
        if config.DB.TRANSFORMATION_TABLE:
            table = Table(
                self.config.DB.TRANSFORMATION_TABLE,
                self.metadata,
                Column('id1', Integer, ForeignKey(f'{self.flask_tables.get("Superformula")}.id'), primary_key=True),
                Column('id2', Integer, ForeignKey(f'{self.flask_tables.get("Superformula")}.id'), primary_key=True),
                Column('ruleid', Integer, ForeignKey(f'{self.config.DB.RULE_TABLE}.id')),
                Column('data', JSONB, nullable=False),
            )
            ddl = str(CreateTable(table)).replace('\n', ' ')
            self.logger.debug(f"created table {table.name} with schema {ddl}")
            self.transformation_table = table

        self.engine = create_engine(url)
        self.metadata.create_all(self.engine)

    def get_number_molecules(self, type_: str):
        number_rows = None
        with self.engine.connect() as conn:
            table = self.molecule_tables.get(type_)
            if table is not None:
                sel = select([func.count()]).select_from(table)
                self.logger.debug(f"executing statement {sel}")
                number_rows = conn.execute(sel).scalar_one()
                self.logger.debug(f"Number of entries in molecule table for type {type_}: {number_rows}")
            else:
                self.logger.debug(f"molecule table not found for type {type_}")
        return number_rows

    def get_number_flasks(self, type_: str):
        number_rows = None
        with self.engine.connect() as conn:
            table = self.flask_tables.get(type_)
            if table is not None:
                sel = select([func.count()]).select_from(table)
                self.logger.debug(f"executing statement {sel}")
                number_rows = conn.execute(sel).scalar_one()
                self.logger.debug(f"Number of entries in flask table for type {type_}: {number_rows}")
            else:
                self.logger.debug(f"flask table not found for type {type_}")
        return number_rows

    def get_number_transformations(self):
        number_rows = None
        with self.engine.connect() as conn:
            table = self.transformation_table
            if table is not None:
                sel = select([func.count()]).select_from(table)
                self.logger.debug(f"executing statement {sel}")
                number_rows = conn.execute(sel).scalar_one()
                self.logger.debug(f"Number of entries in transformation table: {number_rows}")
            else:
                self.logger.debug(f"transformation table not found")
        return number_rows

    def get_number_rules(self):
        number_rows = None
        with self.engine.connect() as conn:
            table = self.rule_table
            if table is not None:
                sel = select([func.count()]).select_from(table)
                self.logger.debug(f"executing statement {sel}")
                number_rows = conn.execute(sel).scalar_one()
                self.logger.debug(f"Number of entries in rule table: {number_rows}")
            else:
                self.logger.debug(f"rule table not found")
        return number_rows

    def fetch_molecule(self, type_: str, key: str, raw: bool=False) -> Union[Molecule, Tuple]:
        mol = None
        with self.engine.connect() as conn:
            table = self.molecule_tables.get(type_)
            if table is not None:
                sel = select([table]) \
                    .where(table.columns.key == key)
                self.logger.debug(f"executing statement {sel}")
                rp = conn.execute(sel)
                row = rp.first()
                if raw and row:
                    mol = row
                elif row:
                    mol = MOLECULE_TYPES[type_].fromdict(row['data'])
                    self.logger.debug(f"fetched molecule {mol} from db")
                else:
                    self.logger.debug(f"molecule not found in db for key {key}")
            else:
                self.logger.error(f"molecule table not found for type {type_}")
        return mol

    def fetch_flask(self, type_: str, key: str, raw: bool=False) -> Union[Flask, Tuple]:
        flask = None
        with self.engine.connect() as conn:
            table = self.flask_tables.get(type_)
            if table is not None:
                sel = select([table]) \
                    .where(table.columns.key == key)
                self.logger.debug(f"executing statement {sel}")
                rp = conn.execute(sel)
                row = rp.first()
                if raw and row:
                    flask = row
                elif row:
                    flask = FLASK_TYPES[type_].fromdict(row['data'])
                    self.logger.debug(f"fetched flask {flask} from db")
                else:
                    self.logger.debug(f"flask not found in db  for key {key}")
            else:
                self.logger.error(f"flask table not found for type {type_}")
        return flask

    def fetch_transformation(self, key1: str, key2: str, raw: bool=False) -> Union[Transformation, Tuple]:
        trans = None
        with self.engine.connect() as conn:
            table = self.transformation_table
            rule_table = self.rule_table
            if table is not None:
                superformula_subquery = select([
                    self.flask_tables.get('Superformula').c.id.label('id'),
                    self.flask_tables.get('Superformula').c.key,
                    self.flask_tables.get('Superformula').c.data
                ]).where(
                    self.flask_tables.get('Superformula').c.key.in_([key1, key2])
                )

                superformula_subquery_key1 = superformula_subquery.alias('superformula_subquery_key1')
                superformula_subquery_key2 = superformula_subquery.alias('superformula_subquery_key2')

                sel = select([
                    superformula_subquery_key1.c.key.label('reactantkey'),
                    superformula_subquery_key2.c.key.label('productkey'),
                    superformula_subquery_key1.c.id.label('reactantid'),
                    superformula_subquery_key2.c.id.label('productid'),
                    superformula_subquery_key1.c.data['smiles'].label('reactantsmiles'),
                    superformula_subquery_key2.c.data['smiles'].label('productsmiles'),
                    rule_table.c.data.label('rule'),
                    table.c.data
                ]) \
                .join(rule_table, table.c.ruleid == rule_table.c.id) \
                .join(superformula_subquery_key1, superformula_subquery_key1.c.key == key1)  \
                .join(superformula_subquery_key2, superformula_subquery_key2.c.key == key2)  \
                .where(
                    (table.c.id1 == superformula_subquery_key1.c.id) & 
                    (table.c.id2 == superformula_subquery_key2.c.id)
                )

                self.logger.debug(f"executing statement {sel}")
                rp = conn.execute(sel)
                row = rp.first()
                if raw and row:
                    trans = row
                elif row:
                    trans = Transformation.fromdict({**row['data'], **row['rule'], 'reactantsmiles': row['reactantsmiles'], 'productsmiles': row['productsmiles']})
                    self.logger.debug(f"fetched transformation {trans} from db")
                else:
                    self.logger.debug(f"transformation not found in db for key pair {key1}>>{key2}")
            else:
                self.logger.error("transformation table not found")
        return trans

    def fetch_rule(self, ruleid: int, raw: bool=False) -> Union[Rule, Tuple]:
        rule = None
        with self.engine.connect() as conn:
            table = self.rule_table
            if table is not None:
                sel = select([table]) \
                    .where((table.columns.id == ruleid))
                self.logger.debug(f"executing statement {sel}")
                rp = conn.execute(sel)
                row = rp.first()
                if raw and row:
                    rule = row
                elif row:
                    rule = Rule.fromdict({**row['data'], 'ruleid': row['id']})
                    self.logger.debug(f"fetched rule {rule} from db")
                else:
                    self.logger.debug(f"rule not found in db for id {ruleid}")
            else:
                self.logger.error("rule table not found")
        return rule

    def fetch_molecules(self, type_: str, keys: List[str], raw: bool=False) -> Union[List[Molecule], List[Tuple]]:
        molecules = []
        with self.engine.connect() as conn:
            table = self.molecule_tables.get(type_)
            if table is not None:
                sel = select([table]) \
                    .where(table.columns.key.in_(keys))
                self.logger.debug(f"executing statement {sel}")
                rp = conn.execute(sel)
                if raw:
                    return rp.fetchall()
                else:
                    molecules = [MOLECULE_TYPES[type_].fromdict(row['data']) for row in rp.fetchall()]
                    self.logger.debug(f"fetched molecules {[str(mol) for mol in molecules]} from db "
                                      f"({len(molecules)}/{len(keys)} requested)")
            else:
                self.logger.error(f"molecule table not found for type {type_}")
        return molecules

    def fetch_flasks(self, type_: str, keys: List[str], raw: bool=False) -> Union[List[Flask], List[Tuple]]:
        flasks = []
        with self.engine.connect() as conn:
            table = self.flask_tables.get(type_)
            if table is not None:
                sel = select([table]) \
                    .where(table.columns.key.in_(keys))
                self.logger.debug(f"executing statement {sel}")
                rp = conn.execute(sel)
                if raw:
                    return rp.fetchall()
                else:
                    flasks = [FLASK_TYPES[type_].fromdict(row['data']) for row in rp.fetchall()]
                    self.logger.debug(f"fetched flasks {[str(flask) for flask in flasks]} from db "
                                      f"({len(flasks)}/{len(keys)} requested)")
            else:
                self.logger.error(f"flask table not found for type {type_}")
        return flasks

    def fetch_transformations(self, keys: List[Tuple[str, str]], raw: bool=False):
        transformations = []
        with self.engine.connect() as conn:
            table = self.transformation_table
            rule_table = self.rule_table
            if table is not None:
                superformula_subquery = select([
                    self.flask_tables.get('Superformula').c.id.label('id'),
                    self.flask_tables.get('Superformula').c.key,
                    self.flask_tables.get('Superformula').c.data
                ]).where(
                    self.flask_tables.get('Superformula').c.key.in_([key for pair in keys for key in pair])
                )

                superformula_subquery_key1 = superformula_subquery.alias('superformula_subquery_key1')
                superformula_subquery_key2 = superformula_subquery.alias('superformula_subquery_key2')

                sel = select([
                    superformula_subquery_key1.c.key.label('reactantkey'),
                    superformula_subquery_key2.c.key.label('productkey'),
                    superformula_subquery_key1.c.id.label('reactantid'),
                    superformula_subquery_key2.c.id.label('productid'),
                    superformula_subquery_key1.c.data['smiles'].label('reactantsmiles'),
                    superformula_subquery_key2.c.data['smiles'].label('productsmiles'),
                    rule_table.c.data.label('rule'),
                    table.c.data
                ]) \
                .join(rule_table, table.c.ruleid == rule_table.c.id) \
                .join(superformula_subquery_key1, superformula_subquery_key1.c.id == table.c.id1) \
                .join(superformula_subquery_key2, superformula_subquery_key2.c.id == table.c.id2) \
                .where(
                    superformula_subquery_key1.c.key.in_([key[0] for key in keys]) &  
                    superformula_subquery_key2.c.key.in_([key[1] for key in keys]) 
                )

                self.logger.debug(f"executing statement {sel}")
                rp = conn.execute(sel)
                if raw:
                    return rp.fetchall()
                else:
                    transformations = [Transformation.fromdict({**row['data'], **row['rule'], 'reactantsmiles': row['reactantsmiles'], 'productsmiles': row['productsmiles']}) for row in rp.fetchall()]
                    self.logger.debug(f"fetched transformations {[str(trans) for trans in transformations]} from db "
                                      f"({len(transformations)}/{len(keys)} requested)")
            else:
                self.logger.error("transformation table not found")
        return transformations

    def fetch_rules(self, ruleids: List[int], raw: bool=False):
        rules = []
        with self.engine.connect() as conn:
            table = self.rule_table
            if table is not None:
                sel = select([table]) \
                    .where(table.columns.id.in_(ruleids))
                self.logger.debug(f"executing statement {sel}")
                rp = conn.execute(sel)
                if raw:
                    return rp.fetchall()
                else:
                    rules = [Rule.fromdict({**row['data'], 'ruleid': row['id']}) for row in rp.fetchall()]
                    self.logger.debug(f"fetched rules {[str(rule) for rule in rules]} from db "
                                      f"({len(rules)}/{len(ruleids)} requested)")
            else:
                self.logger.error("rule table not found")
        return rules

    def fetch_molecules_range(self, type_: str, start_id: int, end_id: int, raw: bool = False) -> Generator[Union[Molecule, Tuple], None, None]:
        with self.engine.connect() as conn:
            table = self.molecule_tables.get(type_)
            if table is not None:
                limit = end_id - start_id
                sel = select([table]).offset(start_id).limit(limit)
                self.logger.debug(f"executing statement {sel}")
                rp = conn.execute(sel)
                if raw:
                    yield from rp.fetchall()
                else:
                    for row in rp.fetchall():
                        mol = MOLECULE_TYPES[type_].fromdict(row['data'])
                        self.logger.debug(f"yielding molecule {mol} from db in range {start_id} - {end_id}")
                        yield mol
            else:
                self.logger.error(f"molecule table not found for type {type_}")

    def fetch_flasks_range(self, type_: str, start_id: int, end_id: int, raw: bool = False) -> Generator[Union[Flask, Tuple], None, None]:
        with self.engine.connect() as conn:
            table = self.flask_tables.get(type_)
            if table is not None:
                limit = end_id - start_id
                sel = select([table]).offset(start_id).limit(limit)
                self.logger.debug(f"executing statement {sel}")
                rp = conn.execute(sel)
                if raw:
                    yield from rp.fetchall()
                else:
                    for row in rp.fetchall():
                        flask = FLASK_TYPES[type_].fromdict(row['data'])
                        self.logger.debug(f"yielding flask {flask} from db in range {start_id} - {end_id}")
                        yield flask
            else:
                self.logger.error(f"flask table not found for type {type_}")

    def fetch_transformations_range(self, start_id: int, end_id: int, raw: bool = False) -> Generator[Union[Transformation, Tuple], None, None]:
        table = self.transformation_table
        rule_table = self.rule_table
        product_table = self.flask_tables.get('Superformula')
        reactant_table = alias(product_table)
        if table is not None:
            limit = end_id - start_id
            with self.engine.connect() as conn:
                sel = select([
                    reactant_table.c.key.label('reactantkey'),
                    product_table.c.key.label('productkey'),
                    reactant_table.c.id.label('reactantid'), 
                    product_table.c.id.label('productid'), 
                    reactant_table.c.data['smiles'].label('reactantsmiles'), 
                    product_table.c.data['smiles'].label('productsmiles'), 
                    rule_table.c.data.label('rule'), 
                    table.c.data
                    ]) \
                    .join(rule_table, table.c.ruleid == rule_table.c.id) \
                    .join(reactant_table, table.c.id1 == reactant_table.c.id) \
                    .join(product_table, table.c.id2 == product_table.c.id) \
                    .offset(start_id).limit(limit)          
                self.logger.debug(f"executing statement {sel}")
                rp = conn.execute(sel)
                if raw:
                    yield from rp.fetchall()
                else:
                    for row in rp.fetchall():
                        trans = Transformation.fromdict({**row['data'], **row['rule'], 'reactantsmiles': row['reactantsmiles'], 'productsmiles': row['productsmiles']})
                        self.logger.debug(f"yielding transformation {trans} from db in range {start_id} - {end_id}")
                        yield trans
        else:
            self.logger("transformation table not found")

    def fetch_rules_range(self, start_id: int, end_id: int, raw: bool = False) -> Generator[Union[Rule, Tuple], None, None]:
        table = self.rule_table
        if table is not None:
            limit = end_id - start_id
            with self.engine.connect() as conn:
                sel = select([self.rule_table]).offset(start_id).limit(limit)
                self.logger.debug(f"executing statement {sel}")
                rp = conn.execute(sel)
                if raw:
                    yield from rp.fetchall()
                else:
                    for row in rp.fetchall():
                        rule = Rule.fromdict({**row['data'], 'ruleid': row['id']})
                        self.logger.debug(f"yielding rule {rule} from db in range {start_id} - {end_id}")
                        yield rule
        else:
            self.logger("rule table not found")

    # TODO(dr): Add optimization using window functions
    # https://github.com/sqlalchemy/sqlalchemy/wiki/RangeQuery-and-WindowedRangeQuery
    def fetch_all_molecules(self, type_: str, raw: bool=False) -> Generator[Union[Molecule, Tuple], None, None]:
        with self.engine.connect() as conn:
            table = self.molecule_tables.get(type_)
            if table is not None:
                sel = select([table])
                self.logger.debug(f"executing statement {sel}")
                rp = conn.execute(sel)
                if raw:
                    yield from rp.fetchall()
                else:
                    for row in rp.fetchall():
                        mol = MOLECULE_TYPES[type_].fromdict(row['data'])
                        self.logger.debug(f"yielding molecule {mol} from db")
                        yield mol
            else:
                self.logger.error(f"molecule table not found for type {type_}")

    def fetch_all_flasks(self, type_: str, raw: bool=False) -> Generator[Union[Flask, Tuple], None, None]:
        with self.engine.connect() as conn:
            table = self.flask_tables.get(type_)
            if table is not None:
                sel = select([table])
                self.logger.debug(f"executing statement {sel}")
                rp = conn.execute(sel)
                if raw:
                    yield from rp.fetchall()
                else:
                    for row in rp.fetchall():
                        flask = FLASK_TYPES[type_].fromdict(row['data'])
                        self.logger.debug(f"yielding flask {flask} from db")
                        yield flask
            else:
                self.logger.error(f"flask table not found for type {type_}")

    def fetch_all_transformations(self, raw: bool=False) -> Generator[Union[Transformation, Tuple], None, None]:
        table = self.transformation_table
        rule_table = self.rule_table
        product_table = self.flask_tables.get('Superformula')
        reactant_table = alias(product_table)
        if table is not None:
            with self.engine.connect() as conn:
                sel = select([
                    reactant_table.c.key.label('reactantkey'),
                    product_table.c.key.label('productkey'),
                    reactant_table.c.id.label('reactantid'), 
                    product_table.c.id.label('productid'), 
                    reactant_table.c.data['smiles'].label('reactantsmiles'), 
                    product_table.c.data['smiles'].label('productsmiles'), 
                    rule_table.c.data.label('rule'), 
                    table.c.data
                    ]) \
                    .join(rule_table, table.c.ruleid == rule_table.c.id) \
                    .join(reactant_table, table.c.id1 == reactant_table.c.id) \
                    .join(product_table, table.c.id2 == product_table.c.id)
                self.logger.debug(f"executing statement {sel}")
                rp = conn.execute(sel)
                if raw:
                    yield from rp.fetchall()
                else:
                    for row in rp.fetchall():
                        trans = Transformation.fromdict({**row['data'], **row['rule'], 'reactantsmiles': row['reactantsmiles'], 'productsmiles': row['productsmiles']})
                        self.logger.debug(f"yielding transformation {trans} from db")
                        yield trans
        else:
            self.logger("transformation table not found")

    def fetch_all_rules(self, raw: bool=False) -> Generator[Union[Rule, Tuple], None, None]:
        table = self.rule_table
        if table is not None:
            with self.engine.connect() as conn:
                sel = select([self.rule_table])
                self.logger.debug(f"executing statement {sel}")
                rp = conn.execute(sel)
                if raw:
                    yield from rp.fetchall()
                else:
                    for row in rp.fetchall():
                        rule = Rule.fromdict({**row['data'], 'ruleid': row['id']})
                        self.logger.debug(f"yielding rule {rule} from db")
                        yield rule
        else:
            self.logger("rule table not found")

    def store_molecule(self, type_: str, key: str, mol: Molecule) -> bool:
        stored = False
        with self.engine.connect() as conn:
            table = self.molecule_tables.get(type_)
            if table is not None:
                try:
                    skip_none_values = self.config.DB.get('SKIP_NONE_VALUES', False)
                    skip_keys = self.config.DB.get('SKIP_KEYS',[])
                    ins = insert(table) \
                        .values(
                        key=key,
                        data=mol.todict(skip_none_values, skip_keys)
                    ).on_conflict_do_nothing(
                        index_elements=['key']
                    )
                except SQLDataError as e:
                    raise DataError(message=f"{key}: {e.args[0]}")
                self.logger.debug(f"executing statement {ins.compile(dialect=dialect())}")
                rp = conn.execute(ins)
                stored = rp.rowcount == 1
                if stored:
                    self.logger.debug(f"stored molecule {mol} to db")
                else:
                    self.logger.debug(f"did not store molecule {mol} to db")
            else:
                self.logger.error(f"molecule table not found for type {type_}")
        return stored

    def store_flask(self, type_: str, key: str, flask: Flask) -> bool:
        stored = False
        with self.engine.connect() as conn:
            table = self.flask_tables.get(type_)
            if table is not None:
                try:
                    skip_none_values = self.config.DB.get('SKIP_NONE_VALUES', False)
                    skip_keys = self.config.DB.get('SKIP_KEYS',[])
                    ins = insert(table) \
                        .values(
                        key=key,
                        data=flask.todict(skip_none_values, skip_keys)
                    ).on_conflict_do_nothing(
                        index_elements=['key']
                    )
                except SQLDataError as e:
                    raise DataError(message=f"{key} : {e.args[0]}")
                self.logger.debug(f"executing statement {ins.compile(dialect=dialect())}")
                rp = conn.execute(ins)
                stored = rp.rowcount == 1
                if stored:
                    self.logger.debug(f"stored flask {flask} to db")
                else:
                    self.logger.debug(f"did not store flask {flask} to db")
            else:
                self.logger.error(f"flask table not found for type {type_}")
        return stored

    def store_transformation(self, key1: str, key2: str, ruleid: int, trans: Transformation) -> bool:
        stored = False
        with self.engine.connect() as conn:
            if self.transformation_table is not None:
                try:
                    cids = select([
                        self.flask_tables.get('Superformula').c.id,
                        self.flask_tables.get('Superformula').c.key
                    ]).where(self.flask_tables.get('Superformula').c.key.in_([key1, key2])).subquery()
                    
                    skip_none_values = self.config.DB.get('SKIP_NONE_VALUES', False)
                    skip_keys = self.config.DB.get('SKIP_KEYS',[]) + ['rule', 'productsmiles', 'reactantsmiles']
                    data = trans.todict(skip_none_values, skip_keys)

                    key1_id = select([cids.c.id]).where(cids.c.key == key1)
                    key2_id = select([cids.c.id]).where(cids.c.key == key2)

                    ins = insert(self.transformation_table) \
                        .values(
                            id1=key1_id.label('id1'), 
                            id2=key2_id.label('id2'),
                            ruleid=ruleid,
                            data=bindparam('data', value=data, type_=JSONB)
                    ).on_conflict_do_nothing(
                        index_elements=['id1', 'id2']
                    )

                except SQLDataError as e:
                    raise DataError(message=f"{key1}>>{key2}: {e.args[0]}")
                self.logger.debug(f"executing statement {ins.compile(dialect=dialect())}")
                rp = conn.execute(ins)
                stored = rp.rowcount == 1
                if stored:
                    self.logger.debug(f"stored transformation {trans} to db")
                else:
                    self.logger.debug(f"did not store transformation {trans} to db")
            else:
                self.logger.error("transformation table not found")
        return stored

    def store_rule(self, ruleid: int, rule: Rule) -> bool:
        stored = False
        with self.engine.connect() as conn:
            if self.rule_table is not None:
                try:
                    skip_none_values = self.config.DB.get('SKIP_NONE_VALUES', False)
                    skip_keys = self.config.DB.get('SKIP_KEYS',[]) + ['ruleid']
                    ins = insert(self.rule_table) \
                        .values(
                        id=ruleid,
                        data=rule.todict(skip_none_values, skip_keys)
                    ).on_conflict_do_nothing(
                        index_elements=['id']
                    )
                except SQLDataError as e:
                    raise DataError(message=f"{ruleid}: {e.args[0]}")
                self.logger.debug(f"executing statement {ins.compile(dialect=dialect())}")
                rp = conn.execute(ins)
                stored = rp.rowcount == 1
                if stored:
                    self.logger.debug(f"stored rule {rule} to db")
                else:
                    self.logger.debug(f"did not store rule {rule} to db")
            else:
                self.logger.error("rule table not found")
        return stored

    def clear_molecules(self, type_: str):
        with self.engine.connect() as conn:
            table = self.molecule_tables.get(type_)
            if table is not None:
                del_ = delete(table)
                self.logger.debug(f"executing statement {del_}")
                conn.execute(del_)
                self.logger.debug(f"cleared molecules from table {table}")
            else:
                self.logger.error(f"molecule table not found for type {type_}")

    def clear_flasks(self, type_: str):
        with self.engine.connect() as conn:
            table = self.flask_tables.get(type_)
            if table is not None:
                del_ = delete(table)
                self.logger.debug(f"executing statement {del_}")
                conn.execute(del_)
                self.logger.debug(f"cleared flasks from table {table}")
            else:
                self.logger.error(f"flask table not found for type {type_}")

    def clear_transformations(self):
        with self.engine.connect() as conn:
            if self.transformation_table is not None:
                del_ = delete(self.transformation_table)
                self.logger.debug(f"executing statement {del_}")
                conn.execute(del_)
                self.logger.debug("cleared transformation table")
            else:
                self.logger.error("transformation table not found")

    def clear_rules(self):
        with self.engine.connect() as conn:
            if self.rule_table is not None:
                del_ = delete(self.rule_table)
                self.logger.debug(f"executing statement {del_}")
                conn.execute(del_)
                self.logger.debug("cleared rule table")
            else:
                self.logger.error("rule table not found")

