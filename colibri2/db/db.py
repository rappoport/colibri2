"""Abstract database interaction class for colibri2.

Classes:
    DB: Abstract database interaction class
"""

from abc import ABC, abstractmethod
from typing import List, Tuple, Generator, Union

from ..config import Config
from ..data import Molecule, Flask, Transformation, Rule
from ..utils import ensure_logger

__all__ = ['DB']


class DB(ABC):
    """Abstract database interaction class."""

    def __init__(self, config: Config):
        self.config = config
        self.logger = ensure_logger("db", self.config.LOGGING.LEVEL.upper())
        self.url = None

    @abstractmethod
    def get_number_molecules(self) -> int:
        """Get number of molecules in DB."""
        pass

    @abstractmethod
    def get_number_flasks(self, type_: str) -> int:
        """Get number of flasks in DB."""
        pass

    @abstractmethod
    def get_number_transformations(self) -> int:
        """Get number of transformations in DB."""
        pass

    @abstractmethod
    def get_number_rules(self) -> int:
        """Get number of rules in DB."""
        pass

    @abstractmethod
    def fetch_molecule(self, type_: str, key: str, raw: bool) -> Union[Molecule, Tuple]:
        """Fetch molecule by key from DB."""
        pass

    @abstractmethod
    def fetch_flask(self, type_: str, key: str, raw: bool) -> Union[Flask, Tuple]:
        """Fetch flask by key from DB."""
        pass

    @abstractmethod
    def fetch_transformation(self, key1: str, key2: str, raw: bool) -> Union[Transformation, Tuple]:
        """Fetch transformation by key pair from DB."""
        pass

    @abstractmethod
    def fetch_rule(self, id: int, raw: bool) -> Union[Rule, Tuple]:
        """Fetch rule by key from DB."""
        pass

    def fetch_molecules(self, type_: str, keys: List[str], raw: bool) -> Union[List[Molecule], List[Tuple]]:
        """Fetch list of molecules by key from DB."""
        molecules = [self.fetch_molecule(type_, key) for key in keys]
        return [mol for mol in molecules if mol is not None]

    def fetch_flasks(self, type_: str, keys: List[str], raw: bool) -> Union[List[Flask], List[Tuple]]:
        """Fetch list of flasks by key from DB."""
        flasks = [self.fetch_flask(type_, key) for key in keys]
        return [flask for flask in flasks if flask is not None]

    def fetch_transformations(self, keys: List[Tuple[str, str]], raw: bool):
        """Fetch list of transformations by key pair from DB."""
        transformations = [self.fetch_transformation(key1, key2) for key1, key2 in keys]
        return [trans for trans in transformations if trans is not None]

    def fetch_rules(self, ids: List[int], raw: bool) -> Union[List[Rule], List[Tuple]]:
        """Fetch list of rules by key from DB."""
        rules = [self.fetch_rule(id) for id in ids]
        return [rule for rule in rules if rule is not None]
     
    @abstractmethod
    def fetch_molecules_range(self, start_id: int, end_id: int, raw: bool = False) -> Generator[Union[Molecule, Tuple], None, None]:
        """Get molecules in a range from the DB."""
        pass    
    @abstractmethod
    def fetch_flasks_range(self, type_: str, start_id: int, end_id: int, raw: bool = False) -> Generator[Union[Flask, Tuple], None, None]:
        """Get flasks in a range from the DB."""
        pass

    @abstractmethod
    def fetch_transformations_range(self, start_id: int, end_id: int, raw: bool = False) -> Generator[Union[Transformation, Tuple], None, None]:
        """Get transformations in a range from the DB."""
        pass    

    @abstractmethod
    def fetch_rules_range(self, start_id: int, end_id: int, raw: bool = False) -> Generator[Union[Rule, Tuple], None, None]:
        """Get rules in a range from the DB."""
        pass    

    @abstractmethod
    def fetch_all_molecules(self, type_: str, raw: bool) -> Generator[Union[Molecule, Tuple], None, None]:
        """Fetch all molecules from DB as a generator."""
        pass

    @abstractmethod
    def fetch_all_flasks(self, type_: str, raw: bool) -> Generator[Union[Flask, Tuple], None, None]:
        """Fetch all flasks from DB as a generator."""
        pass

    @abstractmethod
    def fetch_all_transformations(self, raw: bool) -> Generator[Union[Transformation, Tuple], None, None]:
        """Fetch all transformations from DB as a generator."""
        pass

    @abstractmethod
    def fetch_all_rules(self, raw: bool) -> Generator[Union[Rule, Tuple], None, None]:
        """Fetch all rules from DB as a generator."""
        pass

    @abstractmethod
    def store_molecule(self, type_: str, key: str, mol: Molecule) -> bool:
        """Store molecule to DB."""
        pass

    @abstractmethod
    def store_flask(self, type_: str, key: str, flask: Flask) -> bool:
        """Store flask to DB."""
        pass

    @abstractmethod
    def store_transformation(self, key1: str, key2: str, ruleid: int, trans: Transformation) -> bool:
        """Store transformation to DB."""
        pass

    @abstractmethod
    def store_rule(self, id: int, rule: Rule) -> bool:
        """Store rule to DB."""
        pass

    def store_molecules(self, type_: str, mols: List[Tuple[str, Molecule]]) -> int:
        """Store list of molecules to DB."""
        return sum(self.store_molecule(type_, key, mol) for key, mol in mols)

    def store_flasks(self, type_: str, flasks: List[Tuple[str, Flask]]) -> int:
        """Store list of flasks to DB."""
        return sum(self.store_flask(type_, key, flask) for key, flask in flasks)

    def store_transformations(self, transformations: List[Tuple[str, str, int, Transformation]]) -> int:
        """Store list of transformations to DB."""
        return sum(self.store_transformation(key1, key2, ruleid, trans)
                   for key1, key2, ruleid, trans in transformations)

    def store_rules(self, rules: List[Tuple[int, Rule]]) -> int:
        """Store list of rules to DB."""
        return sum(self.store_rule(ruleid, rule)
                   for ruleid, rule in rules)

    @abstractmethod
    def clear_molecules(self, type_: str):
        """Clear molecules from DB."""
        pass

    @abstractmethod
    def clear_flasks(self, type_: str):
        """Clear flasks from DB."""
        pass

    @abstractmethod
    def clear_transformations(self):
        """Clear transformations from DB."""
        pass
  
    @abstractmethod
    def clear_rules(self):
        """Clear rules from DB."""
        pass
