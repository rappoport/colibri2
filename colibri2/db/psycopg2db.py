"""Psycopg2-backed database interaction class.

Classes:
    Psycopg2DB: Psycopg2-backed database interaction class
"""

from typing import List, Generator, Tuple, Union
from urllib.parse import urlparse

from psycopg2 import connect, sql
from psycopg2.extras import Json

from .db import DB
from ..config import Config
from ..data import Flask, Molecule, Transformation, Rule, FLASK_TYPES, MOLECULE_TYPES
from ..utils import camelcase_to_uppercase


class Psycopg2DB(DB):

    def __init__(self, config: Config):
        super().__init__(config)

        url_data = urlparse(self.config.DB.URL)
        self.conn = connect(database=url_data.path.replace('/', ''),
                            user=url_data.username,
                            password=url_data.password,
                            host=url_data.hostname,
                            port=url_data.port)
        self.db_cursor = self.conn.cursor()
        self.logger.debug(f"connected to db {self.config.DB.URL} using psycopg2 driver")

        self.molecule_tables = {}
        for type_ in MOLECULE_TYPES:
            conf_name = camelcase_to_uppercase(type_)
            table = config.DB.MOLECULE_TABLES.get(conf_name)
            if table is not None:
                ddl = sql.SQL("""CREATE TABLE IF NOT EXISTS {table} (
                                    key VARCHAR(44) NOT NULL PRIMARY KEY,
                                    data JSONB NOT NULL);"""
                              ).format(table=sql.Identifier(table))
                self.db_cursor.execute(ddl)
                self.logger.debug(f"created table {table} with schema {ddl}")
                self.molecule_tables[type_] = table

        self.flask_tables = {}
        for type_ in FLASK_TYPES:
            conf_name = camelcase_to_uppercase(type_)
            table = config.DB.FLASK_TABLES.get(conf_name)
            if table is not None:
                ddl = sql.SQL("""CREATE TABLE IF NOT EXISTS {table} (
                                    id SERIAL PRIMARY KEY,
                                    key VARCHAR(44) NOT NULL UNIQUE,
                                    data JSONB NOT NULL);"""
                            ).format(table=sql.Identifier(table))
                self.db_cursor.execute(ddl)
                self.logger.debug(f"created table {table} with schema {ddl}")
                self.flask_tables[type_] = table

        self.rule_table = None
        if config.DB.RULE_TABLE:
            table = config.DB.RULE_TABLE
            if table:
                ddl = sql.SQL("""CREATE TABLE IF NOT EXISTS {table} (
                                    id INTEGER NOT NULL PRIMARY KEY,
                                    data JSONB NOT NULL);"""
                              ).format(table=sql.Identifier(table))
                self.db_cursor.execute(ddl)
                self.logger.debug(f"created table {table} with schema {ddl}")
                self.rule_table = table

        self.transformation_table = None
        if config.DB.TRANSFORMATION_TABLE:
            table = config.DB.TRANSFORMATION_TABLE
            superformula_table = self.flask_tables.get('Superformula')
            if table:
                ddl = sql.SQL("""CREATE TABLE IF NOT EXISTS {table} (
                                    id1 INTEGER REFERENCES {superformula_table}(id),
                                    id2 INTEGER REFERENCES {superformula_table}(id),
                                    ruleid INTEGER REFERENCES {rule_table}(id),
                                    data JSONB NOT NULL,
                                    PRIMARY KEY (id1, id2));"""
                              ).format(table=sql.Identifier(table),
                                       rule_table=sql.Identifier(config.DB.RULE_TABLE),
                                       superformula_table=sql.Identifier(superformula_table))
                self.db_cursor.execute(ddl)
                self.logger.debug(f"created table {table} with schema {ddl}")
                self.transformation_table = table

    def __del__(self):
        self.conn.close()
        self.logger.info("closed connection to db")

    def get_number_molecules(self, type_: str):
        number_rows = None
        table = self.molecule_tables.get(type_)
        if table is not None:
            stmt = sql.SQL("SELECT COUNT(*) FROM {table};").format(table=sql.Identifier(table))
            self.logger.debug(f"executing statement {stmt}")
            self.db_cursor.execute(stmt)
            number_rows = self.db_cursor.fetchone()[0]
            self.logger.debug(f"Number of entries in molecule table for type {type_}: {number_rows}")
        else:
            self.logger.debug(f"molecule table not found for type {type_}")
        return number_rows

    def get_number_flasks(self, type_: str):
        number_rows = None
        table = self.flask_tables.get(type_)
        if table is not None:
            stmt = sql.SQL("SELECT COUNT(*) FROM {table};").format(table=sql.Identifier(table))
            self.logger.debug(f"executing statement {stmt}")
            self.db_cursor.execute(stmt)
            number_rows = self.db_cursor.fetchone()[0]
            self.logger.debug(f"Number of entries in flask table for type {type_}: {number_rows}")
        else:
            self.logger.debug(f"flask table not found for type {type_}")
        return number_rows

    def get_number_transformations(self):
        number_rows = None
        table = self.transformation_table
        if table is not None:
            stmt = sql.SQL("SELECT COUNT(*) FROM {table};").format(table=sql.Identifier(table))
            self.logger.debug(f"executing statement {stmt}")
            self.db_cursor.execute(stmt)
            number_rows = self.db_cursor.fetchone()[0]
            self.logger.debug(f"Number of entries in transformation table: {number_rows}")
        else:
            self.logger.debug(f"transformation table not found")
        return number_rows

    def get_number_rules(self):
        number_rows = None
        table = self.rule_table
        if table is not None:
            stmt = sql.SQL("SELECT COUNT(*) FROM {table};").format(table=sql.Identifier(table))
            self.logger.debug(f"executing statement {stmt}")
            self.db_cursor.execute(stmt)
            number_rows = self.db_cursor.fetchone()[0]
            self.logger.debug(f"Number of entries in rule table: {number_rows}")
        else:
            self.logger.debug(f"rule table not found")
        return number_rows
         
    def fetch_molecule(self, type_: str, key: str, raw: bool = False) -> Union[Molecule, Tuple]:
        mol = None
        table = self.molecule_tables.get(type_)
        if table is not None:
            query = sql.SQL("SELECT key, data FROM {table} WHERE key = %s;").format(
                table=sql.Identifier(table))
            stmt = self.db_cursor.mogrify(query, (key,))
            self.logger.debug(f"executing statement {stmt}")
            self.db_cursor.execute(stmt)
            res = self.db_cursor.fetchone()
            if raw:
                mol = res
            elif res is not None:
                mol = MOLECULE_TYPES[type_].fromdict(res[1])
                self.logger.debug(f"fetched molecule {mol} from db")
            else:
                self.logger.debug(f"molecule not found in db for key {key}")
        else:
            self.logger.error(f"molecule table not found for type {type_}")
        return mol

    def fetch_flask(self, type_: str, key: str, raw: bool = False) -> Union[Flask, Tuple]:
        flask = None
        table = self.flask_tables.get(type_)
        if table is not None:
            query = sql.SQL("SELECT id, key, data FROM {table} WHERE key = %s;").format(
                table=sql.Identifier(table))
            stmt = self.db_cursor.mogrify(query, (key,))
            self.logger.debug(f"executing statement {stmt}")
            self.db_cursor.execute(stmt)
            res = self.db_cursor.fetchone()
            if raw:
                flask = res
            elif res is not None:
                flask = FLASK_TYPES[type_].fromdict(res[2])
                self.logger.debug(f"fetched flask {flask} from db")
            else:
                self.logger.debug(f"flask not found in db for key {key}")
        else:
            self.logger.error(f"flask table not found for type {type_}")
        return flask

    def fetch_transformation(self, key1: str, key2: str, raw: bool = False) -> Union[Transformation, Tuple]:
        trans = None
        table = self.transformation_table
        rule_table = self.rule_table
        superformula_table = self.flask_tables.get('Superformula')
        if table is not None:
            query = sql.SQL("""
                SELECT
                    s1.key AS reactantkey,
                    s2.key AS productkey,
                    s1.id AS reactantid, 
                    s2.id AS productid, 
                    s1.data->>'smiles' AS reactantsmiles, 
                    s2.data->>'smiles' AS productsmiles, 
                    r.data AS rule, 
                    t.data
                FROM {table} AS t
                JOIN {rule_table} AS r ON t.ruleid = r.id
                JOIN {superformula_table} AS s1 ON s1.id = t.id1
                JOIN {superformula_table} AS s2 ON s2.id = t.id2
                WHERE s1.key = %s AND s2.key = %s;
            """).format(
                table=sql.Identifier(table),
                rule_table=sql.Identifier(rule_table),
                superformula_table=sql.Identifier(superformula_table)
            )
            stmt = self.db_cursor.mogrify(query, (key1, key2))
            self.logger.debug(f"executing statement {stmt}")
            self.db_cursor.execute(stmt)
            res = self.db_cursor.fetchone()
            if raw:
                trans = res
            elif res is not None:
                _, _, _, _, reactantsmiles, productsmiles, rule, data = res
                trans = Transformation.fromdict({'reactantsmiles': reactantsmiles, 'productsmiles': productsmiles, **rule, **data})
                self.logger.debug(f"fetched transformation {trans} from db")
            else:
                self.logger.debug(f"transformation not found in db for key pair {key1}>>{key2}")
        else:
            self.logger.error("transformation table not found")
        return trans

    def fetch_rule(self, ruleid: int, raw: bool = False) -> Union[Rule, Tuple]:
        rule = None
        table = self.rule_table
        if table is not None:
            query = sql.SQL("SELECT id, data FROM {table} WHERE id = %s;").format(
                table=sql.Identifier(table))
            stmt = self.db_cursor.mogrify(query, (ruleid,))
            self.logger.debug(f"executing statement {stmt}")
            self.db_cursor.execute(stmt)
            res = self.db_cursor.fetchone()
            if raw:
                rule = res
            elif res is not None:
                id, data = res
                rule = Rule.fromdict({**data, 'ruleid': id})
                self.logger.debug(f"fetched rule {rule} from db")
            else:
                self.logger.debug(f"rule not found in db for id {ruleid}")
        else:
            self.logger.error("rule table not found")
        return rule

    def fetch_molecules(self, type_: str, keys: List[str], raw: bool = False) -> Union[List[Molecule], List[Tuple]]:
        molecules = []
        table = self.molecule_tables.get(type_)
        if table is not None:
            query = sql.SQL("SELECT key, data FROM {table} WHERE key IN %s;").format(
                table=sql.Identifier(table))
            stmt = self.db_cursor.mogrify(query, (tuple(keys),))
            self.logger.debug(f"executing statement {stmt}")
            self.db_cursor.execute(stmt)
            if raw:
                molecules = self.db_cursor.fetchall()
            else:
                molecules = [MOLECULE_TYPES[type_].fromdict(res[1]) for res in self.db_cursor]
                self.logger.debug(f"fetched molecules {molecules} from db "
                                  "f({len(molecules)}/{len(keys)} requested)")
        else:
            self.logger.error(f"molecule table not found for type {type_}")
        return molecules

    def fetch_flasks(self, type_: str, keys: List[str], raw: bool = False) -> Union[List[Flask], List[Tuple]]:
        flasks = []
        table = self.flask_tables.get(type_)
        if table is not None:
            query = sql.SQL("SELECT id, key, data FROM {table} WHERE key IN %s;").format(
                table=sql.Identifier(table))
            stmt = self.db_cursor.mogrify(query, (tuple(keys),))
            self.logger.debug(f"executing statement {stmt}")
            self.db_cursor.execute(stmt)
            if raw:
                flasks = self.db_cursor.fetchall()
            else:
                flasks = [FLASK_TYPES[type_].fromdict(res[2]) for res in self.db_cursor]
                self.logger.debug(f"fetched flasks {flasks} from db "
                                  f"({len(flasks)}/{len(keys)} requested)")
        else:
            self.logger.error(f"flask table not found for type {type_}")
        return flasks

    def fetch_transformations(self, keys: List[Tuple[str, str]], raw: bool = False):
        transformations = []
        table = self.transformation_table
        rule_table = self.rule_table
        superformula_table = self.flask_tables.get('Superformula')
        if table is not None:
            query = sql.SQL("""
                SELECT
                    s1.key AS reactantkey,
                    s2.key AS productkey,     
                    s1.id AS reactantid, 
                    s2.id AS productid, 
                    s1.data->>'smiles' AS reactantsmiles, 
                    s2.data->>'smiles' AS productsmiles, 
                    r.data AS rule, 
                    t.data
                FROM {table} AS t
                JOIN {rule_table} AS r ON t.ruleid = r.id
                JOIN {superformula_table} AS s1 ON s1.id = t.id1
                JOIN {superformula_table} AS s2 ON s2.id = t.id2
                WHERE (s1.key, s2.key) IN %s;
            """).format(
                table=sql.Identifier(table),
                rule_table=sql.Identifier(rule_table),
                superformula_table=sql.Identifier(superformula_table)
            )
            stmt = self.db_cursor.mogrify(query, (tuple(keys),))
            self.logger.debug(f"executing statement {stmt}")
            self.db_cursor.execute(stmt)
            if raw:
                transformations = self.db_cursor.fetchall()
            else:
                transformations = [Transformation.fromdict({'reactantsmiles': reactantsmiles, 'productsmiles': productsmiles, **rule, **data}) 
                                        for _, _, _, _, reactantsmiles, productsmiles, rule, data in self.db_cursor]
                self.logger.debug(f"fetched transformations {transformations} from db "
                                  f"({len(transformations)}/{len(keys)} requested)")
        else:
            self.logger.error("transformation table not found")
        return transformations

    def fetch_rules(self, ruleids: List[int], raw: bool = False):
        rules = []
        table = self.rule_table
        if table is not None:
            query = sql.SQL("SELECT id, data FROM {table} WHERE id IN %s;").format(
                table=sql.Identifier(table))
            stmt = self.db_cursor.mogrify(query, (tuple(ruleids),))
            self.logger.debug(f"executing statement {stmt}")
            self.db_cursor.execute(stmt)
            if raw:
                rules = self.db_cursor.fetchall()
            else:
                rules = [Rule.fromdict({**data, 'ruleid': id}) for id, data in self.db_cursor]
                self.logger.debug(f"fetched rules {rules} from db "
                                  f"({len(rules)}/{len(ruleids)} requested)")
        else:
            self.logger.error("rule table not found")
        return rules

    def fetch_molecules_range(self, type_: str, start_id: int, end_id: int, raw: bool = False) -> Generator[Union[Molecule, Tuple], None, None]:
        table = self.molecule_tables.get(type_)
        if table is not None:
            limit = end_id - start_id
            stmt = sql.SQL("SELECT key, data FROM {table} OFFSET %s LIMIT %s;").format(table=sql.Identifier(table))
            self.logger.debug(f"executing statement {stmt}")
            self.db_cursor.execute(stmt, (start_id, limit))
            if raw:
                yield from self.db_cursor
            else:
                for _, data in self.db_cursor:
                    mol = MOLECULE_TYPES[type_].fromdict(data)
                    self.logger.debug(f"yielding molecule {mol} from db in range {start_id} - {end_id}")
                    yield mol
        else:
            self.logger.debug(f"molecule table not found for type {type_}")

    def fetch_flasks_range(self, type_: str, start_id: int, end_id: int, raw: bool = False) -> Generator[Union[Flask, Tuple], None, None]:
        table = self.flask_tables.get(type_)
        if table is not None:
            limit = end_id - start_id
            stmt = sql.SQL("SELECT id, key, data FROM {table} OFFSET %s LIMIT %s;").format(table=sql.Identifier(table))
            self.logger.debug(f"executing statement {stmt}")
            self.db_cursor.execute(stmt, (start_id, limit))
            if raw:
                yield from self.db_cursor
            else:
                for _, _, data in self.db_cursor:
                    flask = FLASK_TYPES[type_].fromdict(data)
                    self.logger.debug(f"yielding flask {flask} from db in range {start_id} - {end_id}")
                    yield flask
        else:
            self.logger.debug(f"flask table not found for type {type_}")

    def fetch_transformations_range(self, start_id: int, end_id: int, raw: bool = False) -> Generator[Union[Transformation, Tuple], None, None]:
        table = self.transformation_table
        rule_table = self.rule_table
        superformula_table = self.flask_tables.get('Superformula')
        if table is not None:
            limit = end_id - start_id
            stmt = sql.SQL("""SELECT
                    s1.key AS reactantkey,
                    s2.key AS productkey,
                    s1.id AS reactantid, 
                    s2.id AS productid,
                    s1.data->>'smiles' AS reactantsmiles, 
                    s2.data->>'smiles' AS productsmiles, 
                    r.data AS rule, 
                    t.data
                FROM {table} AS t
                JOIN {rule_table} AS r ON t.ruleid = r.id
                JOIN {superformula_table} AS s1 ON s1.id = t.id1
                JOIN {superformula_table} AS s2 ON s2.id = t.id2
                OFFSET %s LIMIT %s;
            """).format(
                table=sql.Identifier(table),
                rule_table=sql.Identifier(rule_table),
                superformula_table=sql.Identifier(superformula_table)
            )
            self.logger.debug(f"executing statement {stmt}")
            self.db_cursor.execute(stmt, (start_id, limit))
            if raw:
                yield from self.db_cursor
            else:
                for _, _, _, _, reactantsmiles, productsmiles, rule, data in self.db_cursor:
                    trans = Transformation.fromdict({**data, **rule, 'reactantsmiles': reactantsmiles, 'productsmiles': productsmiles})
                    self.logger.debug(f"yielding transformation {trans} from db in range {start_id} - {end_id}")
                    yield trans
        else:
            self.logger.error("transformation table not found")

    def fetch_rules_range(self, start_id: int, end_id: int, raw: bool = False) -> Generator[Union[Rule, Tuple], None, None]:
        table = self.rule_table
        if table is not None:
            limit = end_id - start_id
            stmt = sql.SQL("SELECT id, data from {table} OFFSET %s LIMIT %s;").format(table=sql.Identifier(table))
            self.logger.debug(f"executing statement {stmt}")
            self.db_cursor.execute(stmt, (start_id, limit))
            if raw:
                yield from self.db_cursor
            else:
                for id, data in self.db_cursor:
                    rule = Rule.fromdict({**data, 'ruleid': id})
                    self.logger.debug(f"yielding rule {rule} from db in range {start_id} - {end_id}")
                    yield rule
        else:
            self.logger.error("rule table not found")

    def fetch_all_molecules(self, type_: str, raw: bool = False) -> Generator[Union[Molecule, Tuple], None, None]:
        table = self.molecule_tables.get(type_)
        if table is not None:
            stmt = sql.SQL("SELECT key, data FROM {table};").format(table=sql.Identifier(table))
            self.logger.debug(f"executing statement {stmt}")
            self.db_cursor.execute(stmt)
            if raw:
                yield from self.db_cursor
            else:
                for _, data in self.db_cursor:
                    mol = MOLECULE_TYPES[type_].fromdict(data)
                    self.logger.debug(f"yielding molecule {mol} from db")
                    yield mol
        else:
            self.logger.error(f"molecule table not found for type {type_}")

    def fetch_all_flasks(self, type_: str, raw: bool = False) -> Generator[Union[Flask, Tuple], None, None]:
        table = self.flask_tables.get(type_)
        if table is not None:
            stmt = sql.SQL("SELECT id, key, data FROM {table};").format(table=sql.Identifier(table))
            self.logger.debug(f"executing statement {stmt}")
            self.db_cursor.execute(stmt)
            if raw:
                yield from self.db_cursor
            else:
                for _, _, data in self.db_cursor:
                    flask = FLASK_TYPES[type_].fromdict(data)
                    self.logger.debug(f"yielding flask {flask} from db")
                    yield flask
        else:
            self.logger.debug(f"flask table not found for type {type_}")

    def fetch_all_transformations(self, raw: bool = False) -> Generator[Union[Transformation, Tuple], None, None]:
        table = self.transformation_table
        rule_table = self.rule_table
        superformula_table = self.flask_tables.get('Superformula')
        if table is not None:
            stmt = sql.SQL("""SELECT
                    s1.key AS reactantkey,
                    s2.key AS productkey,
                    s1.id AS reactantid, 
                    s2.id AS productid, 
                    s1.data->>'smiles' AS reactantsmiles, 
                    s2.data->>'smiles' AS productsmiles,
                    r.data AS rule, 
                    t.data
                FROM {table} AS t
                JOIN {rule_table} AS r ON t.ruleid = r.id
                JOIN {superformula_table} AS s1 ON s1.id = t.id1
                JOIN {superformula_table} AS s2 ON s2.id = t.id2;
            """).format(
                table=sql.Identifier(table),
                rule_table=sql.Identifier(rule_table),
                superformula_table=sql.Identifier(superformula_table)
            )
            self.logger.debug(f"executing statement {stmt}")
            self.db_cursor.execute(stmt)
            if raw:
                yield from self.db_cursor
            else:
                for _, _, _, _, reactantsmiles, productsmiles, rule, data in self.db_cursor:
                    trans = Transformation.fromdict({**data, **rule, 'reactantsmiles': reactantsmiles, 'productsmiles': productsmiles})
                    self.logger.debug(f"yielding transformation {trans} from db")
                    yield trans
        else:
            self.logger.error("transformation table not found")

    def fetch_all_rules(self, raw: bool = False) -> Generator[Union[Rule, Tuple], None, None]:
        table = self.rule_table
        if table is not None:
            stmt = sql.SQL("SELECT id, data from {table};").format(table=sql.Identifier(table))
            self.logger.debug(f"executing statement {stmt}")
            self.db_cursor.execute(stmt)
            if raw:
                yield from self.db_cursor
            else:
                for id, data in self.db_cursor:
                    rule = Rule.fromdict({**data, 'ruleid': id})
                    self.logger.debug(f"yielding rule {rule} from db")
                    yield rule
        else:
            self.logger.error("rule table not found")

    def store_molecule(self, type_: str, key: str, mol: Molecule) -> bool:
        stored = False
        table = self.molecule_tables.get(type_)
        if table is not None:
            query = sql.SQL("INSERT INTO {table} VALUES (%s, %s) ON CONFLICT (key) DO NOTHING;").format(
                table=sql.Identifier(table))
            skip_none_values = self.config.DB.get('SKIP_NONE_VALUES', False)
            skip_keys = self.config.DB.get('SKIP_KEYS',[])
            stmt = self.db_cursor.mogrify(query, (key, Json(mol.todict(skip_none_values, skip_keys))))
            self.logger.debug(f"executing statement {stmt}")
            self.db_cursor.execute(stmt)
            stored = self.db_cursor.rowcount == 1
            if stored:
                self.logger.debug(f"stored molecule {mol} to db")
            else:
                self.logger.debug(f"did not store molecule {mol} to db")
        else:
            self.logger.error(f"molecule table not found for type {type_}")
        return stored

    def store_flask(self, type_: str, key: str, flask: Flask) -> bool:
        stored = False
        table = self.flask_tables.get(type_)
        if table is not None:
            query = sql.SQL("INSERT INTO {table} (key, data) VALUES (%s, %s) ON CONFLICT (key) DO NOTHING;").format(
                table=sql.Identifier(table))
            skip_none_values = self.config.DB.get('SKIP_NONE_VALUES', False)
            skip_keys = self.config.DB.get('SKIP_KEYS',[])
            stmt = self.db_cursor.mogrify(query, (key, Json(flask.todict(skip_none_values, skip_keys))))
            self.logger.debug(f"executing statement {stmt}")
            self.db_cursor.execute(stmt)
            stored = self.db_cursor.rowcount == 1
            if stored:
                self.logger.debug(f"stored flask {flask} to db")
            else:
                self.logger.debug(f"did not store flask {flask} to db")
        else:
            self.logger.error(f"flask table not found for type {type_}")
        return stored

    def store_transformation(self, key1: str, key2: str, ruleid: int, trans: Transformation) -> bool:
        stored = False
        table = self.transformation_table
        superformula_table = self.flask_tables.get('Superformula')
        if table is not None:
            query = sql.SQL("""
                INSERT INTO {table} 
                SELECT s1.id AS id1, s2.id AS id2, %s, %s
                FROM {superformula_table} AS s1
                CROSS JOIN {superformula_table} AS s2
                WHERE s1.key = %s
                AND s2.key = %s
                LIMIT 1
                ON CONFLICT (id1, id2) DO NOTHING;
            """).format(
                table=sql.Identifier(table),
                superformula_table=sql.Identifier(superformula_table)
            )
            skip_none_values = self.config.DB.get('SKIP_NONE_VALUES', False)
            skip_keys = self.config.DB.get('SKIP_KEYS',[]) + ['rule', 'productsmiles', 'reactantsmiles']
            stmt = self.db_cursor.mogrify(query, (ruleid, Json(trans.todict(skip_none_values, skip_keys)), key1, key2))
            self.logger.debug(f"executing statement {stmt}")
            self.db_cursor.execute(stmt)
            stored = self.db_cursor.rowcount == 1
            if stored:
                self.logger.debug(f"stored transformation {trans} to db")
            else:
                self.logger.debug(f"did not store transformation {trans} to db")
        else:
            self.logger.error("transformation table not found")
        return stored

    def store_rule(self, ruleid: int, rule: Rule) -> bool:
        stored = False
        table = self.rule_table
        if table is not None:
            query = sql.SQL("""INSERT INTO {table} VALUES (%s, %s) ON CONFLICT (id) DO NOTHING;""").format(
                table=sql.Identifier(table))
            skip_none_values = self.config.DB.get('SKIP_NONE_VALUES', False)
            skip_keys = self.config.DB.get('SKIP_KEYS',[]) + ['ruleid']
            stmt = self.db_cursor.mogrify(query, (ruleid, Json(rule.todict(skip_none_values, skip_keys))))
            self.logger.debug(f"executing statement {stmt}")
            self.db_cursor.execute(stmt)
            stored = self.db_cursor.rowcount == 1
            if stored:
                self.logger.debug(f"stored rule {rule} to db")
            else:
                self.logger.debug(f"did not store rule {rule} to db")
        else:
            self.logger.error("rule table not found")
        return stored
    
    def clear_molecules(self, type_: str):
        table = self.molecule_tables.get(type_)
        if table is not None:
            stmt = sql.SQL("DELETE FROM {table};").format(table=sql.Identifier(table))
            self.logger.debug(f"executing statement {stmt}")
            self.db_cursor.execute(stmt)
            self.logger.debug(f"cleared molecules from table {table}")
        else:
            self.logger.error(f"molecule table not found for type {type_}")

    def clear_flasks(self, type_: str):
        table = self.flask_tables.get(type_)
        if table is not None:
            stmt = sql.SQL("DELETE FROM {table};").format(table=sql.Identifier(table))
            self.logger.debug(f"executing statement {stmt}")
            self.db_cursor.execute(stmt)
            self.logger.debug(f"cleared flasks from table {table}")
        else:
            self.logger.error(f"flask table not found for type {type_}")

    def clear_transformations(self):
        table = self.transformation_table
        if table is not None:
            stmt = sql.SQL("DELETE FROM {table}").format(table=sql.Identifier(table))
            self.logger.debug(f"executing statement {stmt}")
            self.db_cursor.execute(stmt)
            self.logger.debug("cleared transformation table")
        else:
            self.logger.error("transformation table not found")

    def clear_rules(self):
        table = self.rule_table
        if table is not None:
            stmt = sql.SQL("DELETE FROM {table}").format(table=sql.Identifier(table))
            self.logger.debug(f"executing statement {stmt}")
            self.db_cursor.execute(stmt)
            self.logger.debug("cleared rule table")
        else:
            self.logger.error("rule table not found")

