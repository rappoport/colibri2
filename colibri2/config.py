"""Configuration handling for colibri2.

Classes:
     Config: Singleton wrapper for dynaconf LazySettings object
     LocalConfig: Wrapper for dynaconf LazySettings object
     add_option: Add option named key if the value is not None
     ensure_option: Raise RuntimeError if key does not exist
"""
from collections.abc import Mapping
from typing import Any

from dynaconf import Dynaconf

__all__ = ['Config', 'LocalConfig', 'add_option', 'ensure_option']


class Config:
    """Singleton class delegating to dynaconf.Dynaconf object."""
    __instance = None

    def __new__(cls, env='TESTING', settings_file=None):
        if cls.__instance is None:
            cls.__instance = LocalConfig(env, settings_file)
        return cls.__instance


class LocalConfig:
    """Wrapper for local dynaconf.Dynaconf object."""

    def __new__(cls, env='TESTING', settings_file=None):
        if settings_file is None:
            settings_file="config/settings.toml"
        return Dynaconf(
            settings_file=settings_file,
            environments=True,
            ENVVAR_PREFIX_FOR_DYNACONF='COLIB2',
            ENV_FOR_DYNACONF=env
        )


def add_option(options: Mapping, key: str, value: Any):
    """Add option named key if the value is not None.

    Args:
        options (Mapping): Options dict-like object
        key (str): Option key
        value (Any): Option value
    """
    if value is not None:
        options[key] = value


def ensure_option(options: Mapping, key: str):
    """Raise RuntimeError if key does not exist.

    Args:
        options (Mapping): Options dict-like object
        key (str): Key to check
    """
    if options.get(key, None) is None:
        raise RuntimeError(f"required option {key} not provided.")
