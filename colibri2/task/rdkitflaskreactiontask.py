"""Build tasks using RDKit.

Classes:
     RDKitFlaskReactionTask: Flask reaction task using RDKit
"""

from itertools import combinations, chain
from typing import List, Tuple, Iterator

import toml
from rdkit import Chem

from .rdkitlibgen import RDKitLibraryGenerator
from .task import Task
from ..config import Config
from ..data import Flask, Superformula, Formula, Transformation, Rule, InvalidSuperformula, \
    FailedSuperformula
from ..exceptions import ReactionRuleError, PreconditionError, ValidationError, \
    FlaskError, MoleculeError

__all__ = ['RDKitFlaskReactionTask']


class RDKitFlaskReactionTask(Task):

    def __init__(self, config: Config):
        super().__init__(config)

        reaction_rules = self.config.REACTION.RULES

        if self.config.REACTION.RULES_PATH:
            rules_file = self.config.REACTION.RULES_PATH
            try:
                with open(rules_file) as rules_f:
                    reaction_rules_from_file = toml.load(rules_f)
                    if reaction_rules_from_file:
                        reaction_rules.extend(
                            [rule for block in reaction_rules_from_file.values()
                             for rule in block.values()])

            except (FileNotFoundError, PermissionError):
                self.logger.error(
                    f"reaction rules file {rules_file} could not be read")

        self.reaction_rules = [Rule(rule, id) for id, rule in enumerate(reaction_rules)]

        self.reactivity_uni = self.config.REACTION.INCLUDE_UNIMOLECULAR
        self.reactivity_cycl = self.config.REACTION.INCLUDE_CYCLIZATION
        self.reactivity_bi = self.config.REACTION.INCLUDE_BIMOLECULAR

        self.reaction_index = self.config.REACTION.get('INDEX')

        self.generators_uni = {}
        self.generators_cycl = {}
        self.generators_bi = {}

        if self.reactivity_uni:
            for rule in self.reaction_rules:
                try:
                    if RDKitLibraryGenerator.molecularity(rule.rule) != 1:
                        continue

                    generator = RDKitLibraryGenerator(
                        rule.rule, index=self.reaction_index)

                except ReactionRuleError as e:
                    self.logger.error(f"invalid unimolecular rule: {e}")
                    continue

                self.generators_uni[rule.ruleid] = generator
                self.logger.debug(
                    f"initialized unimolecular generator for rule {generator.rule}")

        if self.reactivity_cycl:
            for rule in self.reaction_rules:
                try:
                    molecularity = RDKitLibraryGenerator.molecularity(rule.rule)
                    num_products = RDKitLibraryGenerator.num_products(rule.rule)
                    if not (molecularity == 2 and num_products == 1):
                        continue

                    if RDKitLibraryGenerator.univalent_atoms(rule.rule):
                        continue

                    generator = RDKitLibraryGenerator(
                        rule.rule, cycl=True, index=self.reaction_index)

                except ReactionRuleError as e:
                    self.logger.error(f"invalid cyclization rule: {e}")
                    continue

                self.generators_cycl[rule.ruleid] = generator
                self.logger.debug(
                    f"initialized cyclization generator for rule {generator.rule}")

        if self.reactivity_bi:
            for rule in self.reaction_rules:
                try:
                    if RDKitLibraryGenerator.molecularity(rule.rule) != 2:
                        continue

                    generator = RDKitLibraryGenerator(
                        rule.rule, index=self.reaction_index)

                except ReactionRuleError as e:
                    self.logger.error(f"invalid bimolecular rule: {e}")
                    continue

                self.generators_bi[rule.ruleid] = generator
                self.logger.debug(
                    f"initialized bimolecular generator for rule {generator.rule}")

    def validate_reactant(self, flask: Superformula):
        """Validate reactant flask.

        Args:
            flask (Superformula): Reactant flask

        Raises:
            PreconditionError: Precondition not satisfied
        """
        self.logger.info(f"<{flask}>: validated reactant")

    def react_flasks_uni(self, smiles_list: List[str]) -> Iterator[Tuple[List[str], str, str]]:
        """Run unimolecular reactions on flasks.

        Args:
            smiles_list (List[str]): List of SMILES strings in reactant flask

        Yields:
            Tuple[List[str], str, str]:
                List of SMILES strings, transformation SMARTS rule, reactivity type
        """

        reactants_seen = set()
        products_seen = set()

        for index1, smiles1 in enumerate(smiles_list):

            if smiles1 in reactants_seen:
                continue
            reactants_seen.add(smiles1)

            spectators = [smiles2 for index2, smiles2 in enumerate(smiles_list)
                          if index2 != index1]

            for ruleid, generator in self.generators_uni.items():
                for products in generator.react([smiles1]):

                    if products in products_seen:
                        continue
                    products_seen.add(products)

                    product_list = spectators + list(products)

                    self.logger.debug(f"<{generator.rule}>: reaction {smiles_list} -> {product_list}")

                    yield product_list, generator.rule, generator.reactivity

    def react_flasks_cycl(self, smiles_list: List[str]) -> Iterator[Tuple[List[str], str, str]]:
        """Run cyclization reactions on flasks.

        Args:
            smiles_list (List[str]): List of SMILES strings in reactant flask

        Yields:
            Tuple[List[str], str, str]:
                List of SMILES strings, transformation SMARTS rule, reactivity type
        """

        reactants_seen = set()
        products_seen = set()

        for index1, smiles1 in enumerate(smiles_list):

            if smiles1 in reactants_seen:
                continue
            reactants_seen.add(smiles1)

            spectators = [smiles2 for index2, smiles2 in enumerate(smiles_list)
                          if index2 != index1]

            for ruleid, generator in self.generators_cycl.items():
                for products in generator.react([smiles1]):

                    if products in products_seen:
                        continue
                    products_seen.add(products)

                    product_list = spectators + list(products)

                    self.logger.debug(f"<{generator.rule}>: reaction {smiles_list} -> {product_list}")

                    yield product_list, generator.rule, generator.reactivity

    def react_flasks_bi(self, smiles_list: List[str]) -> Iterator[Tuple[List[str], str, str]]:
        """Run bimolecular reactions on flasks.

        Args:
            smiles_list (List[str]): List of SMILES strings in reactant flask

        Yields:
            Tuple[str, str, str]:
                List of SMILES strings, transformation SMARTS rule, reactivity type
        """

        reactants_seen = set()
        products_seen = set()

        for (index1, smiles1), (index2, smiles2) in combinations(enumerate(smiles_list), 2):

            reactants = (smiles1, smiles2)
            indices = {index1, index2}

            if reactants in reactants_seen:
                continue
            reactants_seen.add(reactants)

            spectators = [smiles3 for index3, smiles3 in enumerate(smiles_list)
                          if index3 not in indices]

            for ruleid, generator in self.generators_bi.items():
                for products in chain(
                        generator.react([smiles1, smiles2]),
                        generator.react([smiles2, smiles1])):

                    if products in products_seen:
                        continue
                    products_seen.add(products)

                    product_list = spectators + list(products)

                    self.logger.debug(f"<{generator.rule}>: reaction {smiles_list} -> {product_list}")

                    yield product_list, generator.rule, generator.reactivity

    def react_flask(self, smiles_list: List[str]) -> Iterator[Tuple[List[str], str, str]]:
        """Run reactions on flasks.

        Args:
            smiles_list (List[str]): List of SMILES strings in reactant flask

        Yields:
            Tuple[List[str], str, str]:
                List of SMILES strings, transformation SMARTS rule, reactivity type
        """

        if self.generators_uni:
            yield from self.react_flasks_uni(smiles_list)

        if self.generators_cycl:
            yield from self.react_flasks_cycl(smiles_list)

        if self.generators_bi:
            yield from self.react_flasks_bi(smiles_list)

    def generate_flasks(self, inp: Superformula) -> Iterator[Tuple[Superformula, Transformation]]:
        """Generate flasks according to reaction rules.

        Args:
            inp (Superformula): Input superformula flask

        Yields:
            Tuple[Superformula, Transformation]: Output superformula flasks, transformations
        """

        smiles_list = [formula.smiles for formula in inp.split()]
        for product_list, rule, reactivity in self.react_flask(smiles_list):
            product = Superformula.combine(
                product_list,
                index=self.reaction_index,
                stereo=self.config.REACTION.get('STEREO'),
                program=self.config.REACTION.get('PROGRAM'),
                version=self.config.REACTION.get('VERSION'),
            )

            trans = Transformation(
                inp.smiles,
                product.smiles,
                rule, 
                index=self.reaction_index,
                stereo=self.config.REACTION.get('STEREO'),
            )

            self.logger.info(f"<{product}>: generated {reactivity} product")

            yield product, trans

    def validate_composition(self, inp: Superformula, product: Superformula):
        """Validate product composition.

        Args:
            inp (Superformula): Reactant superformula flask
            product (Superformula): Product superformula flask

        Raises:
            ValidationError: Invalid output
        """

        reactant_sumformula = inp.sumformula
        product_sumformula = product.sumformula

        self.logger.debug(f"<{product}>: reactant stoichiometry: {reactant_sumformula}, "
                          f"product stoichiometry: {product_sumformula}")

        if reactant_sumformula != product_sumformula:
            raise ValidationError(message="stoichiometry change")

    def validate_formula(self, formula: Formula):
        """Validate product formula.

        Args:
            formula (Formula): Product formula

        Raises:
            ValidationError: Invalid formula
        """

        # Do a round-trip conversion for testing
        # Workaround since the radical positions are not recognized
        rdmol = Chem.MolFromSmiles(formula.smiles)
        if rdmol is None:
            raise ValidationError(message="invalid SMILES")
        rdproducttmp = Chem.MolFromSmiles(Chem.MolToSmiles(Chem.RemoveHs(rdmol, sanitize=True),
                                                           canonical=True))
        if rdproducttmp is None:
            raise ValidationError(message="invalid SMILES")

        if rdproducttmp.GetNumAtoms() == 1:
            return

        charges = [at.GetFormalCharge() for at in rdproducttmp.GetAtoms()]
        abs_charges = [abs(c) for c in charges]
        radicals = [at.GetNumRadicalElectrons() for at in rdproducttmp.GetAtoms()]

        if (sum(charges) > self.config.FORMULA.MAX_CHARGE or
                sum(abs_charges) > self.config.FORMULA.MAX_ABS_CHARGE):
            raise ValidationError(message="too highly charged")

        if sum(radicals) > self.config.FORMULA.MAX_RADICALS:
            raise ValidationError(message="too many radicals")

        if len(Chem.GetSymmSSSR(rdproducttmp)) > self.config.FORMULA.MAX_RINGS:
            raise ValidationError(message="too many rings")

    def validate_product(self, reactant: Superformula, product: Superformula):
        """Validate product superformula flask.

        Args:
            reactant (Superformula): Reactant superformula flask
            product (Superformula): Product superformula flask

        Raises:
            ValidationError: Invalid output
        """

        if not self.config.FORMULA.VALIDATION:
            return

        self.validate_composition(reactant, product)
        for formula in product.split():
            self.validate_formula(formula)
        self.logger.info(f"<{product}>: validated product")

    def process(self, inp: Superformula) -> Tuple[
        Flask, List[Superformula], List[InvalidSuperformula], List[Transformation]]:

        """Generate flasks according to reaction rules using RDKit.

        Args:
            inp (Superformula): Input superformula flask

        Returns:
            Tuple[Flask, List[Superformula], List[InvalidSuperformula],  List[Transformation]]:
                Output reactant, valid products, invalid products, transformations
        """

        out = None
        valid_products = []
        invalid_products = []
        transformations = []

        try:
            self.validate_reactant(inp)

            for flask, trans in self.generate_flasks(inp):
                try:
                    self.validate_product(inp, flask)
                    valid_products.append(flask)
                    transformations.append(trans)

                except ValidationError as e:
                    self.logger.error(f"<{flask.smiles}>: {e.message}")
                    flask = InvalidSuperformula(
                        flask.smiles,
                        error=e.message,
                        program=self.config.REACTION.get('PROGRAM'),
                        version=self.config.REACTION.get('VERSION'),
                    )
                    invalid_products.append(flask)

            out = inp

        except PreconditionError as e:
            self.logger.error(f"{inp}: {e.message}")
            out = FailedSuperformula(
                inp.smiles,
                error=e.message,
                tag=inp.tag,
                program=self.config.REACTION.get('PROGRAM'),
                version=self.config.REACTION.get('VERSION'),
            )

        except (MoleculeError, FlaskError) as e:
            self.logger.error(f"<{out}>: {e.message}")
            out = InvalidSuperformula(
                out.smiles,
                error=e.message,
                tag=inp.tag,
                program=self.config.REACTION.get('PROGRAM'),
                version=self.config.REACTION.get('VERSION'),
            )

        return out, valid_products, invalid_products, transformations
