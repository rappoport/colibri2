"""Abstract geometry task.

Classes:
     GeometryTask: Abstract geometry task
"""

from abc import ABC, abstractmethod

from .task import Task
from ..data import Molecule, Configuration, Geometry, InvalidConfiguration, \
    FailedConfiguration, InvalidGeometry
from ..exceptions import PreconditionError, ExecutionError, ValidationError, \
    MoleculeError
from ..structure import is_connected

__all__ = ['GeometryTask']


class GeometryTask(Task, ABC):
    """Abstract geometry task."""

    def validate_configuration(self, mol: Configuration):
        """Validate input configuration.

        Args:
            mol (Configuration): Input configuration

        Raises:
            PreconditionError: Precondition is not satisfied
        """
        self.logger.info(f"<{mol}>: validated configuration")

    @abstractmethod
    def generate_geometry(self, inp: Configuration) -> Geometry:
        """Generate geometry from input configuration.

        Args:
            inp (Configuration): Input configuration

        Returns:
            Geometry: Output geometry
        """

    def validate_composition(self, inp: Configuration, out: Geometry):
        """Validate molecular composition.

        Args:
            inp (Configuration): Input configuration
            out (Geometry): Output geometry

        Raises:
            ValidationError: Invalid output
        """

        configuration_atomkey = inp.atomkey
        geometry_atomkey = out.atomkey

        self.logger.debug(f"<{out}>: configuration stoichiometry: {configuration_atomkey}, "
                          f"geometry stoichiometry: {geometry_atomkey}")

        if configuration_atomkey != geometry_atomkey:
            raise ValidationError(message="stoichiometry change")

    def validate_connectivity(self, inp: Configuration, out: Geometry):
        """Validate molecular connectivity.

        Args:
            inp (Configuration): Input configuration
            out (Geometry): Output geometry

        Raises:
            ValidationError: Invalid output
        """

        configuration_adjkey = inp.adjkey
        geometry_adjkey = out.adjkey

        self.logger.debug(f"<{out}>: configuration adjacency: {configuration_adjkey}, "
                          f"geometry adjacency: {geometry_adjkey}")

        # Check for fragmentation
        if not is_connected(out.adjacency):
            raise ValidationError(message="fragmentation")

        # Check for rearrangement
        if configuration_adjkey != geometry_adjkey:
            raise ValidationError(message="rearrangement")

    def validate_convergence(self, inp: Configuration, out: Geometry):
        """Validate output bonding.

        Args:
            inp (Configuration): Input configuration
            out (Geometry): Output geometry

        Raises:
            ValidationError: Invalid output
        """

        # Check for no convergence or energy missing
        if not out.converged:
            raise ValidationError(message="no convergence")
        if out.energy == 0.0:
            raise ValidationError(message="molecule has zero energy")

    def validate_bonding(self, inp: Configuration, out: Geometry):
        """Validate output bonding.

        Args:
            inp (Configuration): Input configuration
            out (Geometry): Output geometry

        Raises:
            ValidationError: Invalid output
        """

        configuration_bondkey = inp.bondkey
        geometry_bondkey = out.bondkey

        self.logger.debug(f"<{out}>: configuration bonding: {configuration_bondkey}, "
                          f"geometry bonding: {geometry_bondkey}")

        if configuration_bondkey != geometry_bondkey:
            raise ValidationError(message="bonding change")

    def validate_stereochemistry(self, inp: Configuration, out: Geometry):
        """Validate output stereochemistry.

        Args:
            inp (Configuration): Input configuration
            out (Geometry): Output geometry

        Raises:
            ValidationError: Invalid output
        """

        configuration_stereo = (inp.ez_stereo, inp.chiral_stereo)
        geometry_stereo = (out.ez_stereo, out.chiral_stereo)

        self.logger.debug(f"<{out}>: configuration stereochemistry: {configuration_stereo}, "
                          f"geometry stereochemistry: {geometry_stereo}")

        if configuration_stereo != geometry_stereo:
            raise ValidationError(message="stereochemistry change")

    def validate_geometry(self, inp: Configuration, out: Geometry):
        """Validate output geometry.

        Args:
            inp (Configuration): Input configuration
            out (Geometry): Output geometry

        Raises:
            ValidationError: Invalid output
        """

        if not self.config.GEOMETRY.VALIDATION:
            return

        self.validate_composition(inp, out)

        # Skip following validations for atoms
        if len(out.atoms) == 1:
            return

        if not self.config.GEOMETRY.IGNORE_BONDING:
            self.validate_connectivity(inp, out)
        if not self.config.GEOMETRY.NO_OPT:
            self.validate_convergence(inp, out)
        if (not self.config.GEOMETRY.IGNORE_BONDING and
                self.config.GEOMETRY.VALIDATE_BONDING):
            self.validate_bonding(inp, out)
        if (self.config.GEOMETRY.STEREO and
                self.config.GEOMETRY.VALIDATE_STEREO):
            self.validate_stereochemistry(inp, out)
        self.logger.info(f"<{out}>: validated geometry")

    def process(self, inp: Configuration) -> Molecule:
        """Optimize molecule using MOPAC.

        Args:
            inp (Configuration): Input configuration

        Returns:
            Molecule: Output molecule
        """

        out = None

        try:
            self.validate_configuration(inp)
            out = self.generate_geometry(inp)
            self.validate_geometry(inp, out)

        except PreconditionError as e:
            self.logger.error(f"<{inp}>: {e.message}")
            out = InvalidConfiguration(
                inp.coord,
                error=e.message,
                tag=inp.tag,
                program=self.config.GEOMETRY.PROGRAM,
                version=self.config.GEOMETRY.VERSION,
                method=self.config.GEOMETRY.METHOD,
                options=self.config.GEOMETRY.OPTIONS,
            )

        except (ExecutionError, MoleculeError) as e:
            self.logger.error(f"<{inp}>: {e.message}")
            out = FailedConfiguration(
                inp.coord,
                error=e.message,
                tag=inp.tag,
                program=self.config.GEOMETRY.PROGRAM,
                version=self.config.GEOMETRY.VERSION,
                method=self.config.GEOMETRY.METHOD,
                options=self.config.GEOMETRY.OPTIONS,
            )

        except ValidationError as e:
            self.logger.error(f"<{out}>: {e.message}")
            out = InvalidGeometry(
                out.coord,
                error=e.message,
                tag=inp.tag,
                program=self.config.GEOMETRY.PROGRAM,
                version=self.config.GEOMETRY.VERSION,
                method=self.config.GEOMETRY.METHOD,
                options=self.config.GEOMETRY.OPTIONS,
            )

        return out
