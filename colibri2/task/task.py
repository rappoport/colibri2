"""Abstract task class for colibri2.

Classes:
    Task: Abstract task class
"""

from abc import ABC, abstractmethod
from typing import Any

from ..config import Config
from ..utils import ensure_logger, random_id

__all__ = ['Task']


class Task(ABC):
    """Abstract task class."""

    def __init__(self, config: Config):
        """Construct Task object.

        Args:
            config (Config): Configuration object
        """

        self.config = config
        self.task_id = f"{self}-{random_id(6)}"
        self.logger = ensure_logger(self.task_id)

    def __str__(self) -> str:
        """Return string representation.

        Returns:
            str: Task type as string
        """

        return type(self).__name__.replace('Task', '').lower()

    @abstractmethod
    def process(self, inp: Any) -> Any:
        """
        Perform the task and return the result.

        Args:
            inp (Any): Task input

        Returns:
             Any: Task result
        """
        pass
