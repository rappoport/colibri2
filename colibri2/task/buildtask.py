"""Abstract build task.

Classes:
     BuildTask: Abstract build task
"""

from abc import ABC, abstractmethod
from ..data import Molecule, Formula, Configuration, InvalidFormula, FailedFormula, \
    InvalidConfiguration
from ..exceptions import ExecutionError, PreconditionError, ValidationError, \
    MoleculeError
from ..structure import is_connected

from .task import Task

__all__ = ['BuildTask']


class BuildTask(Task, ABC):
    """Abstract geometry task."""

    def validate_formula(self, mol: Formula):
        """Validate input formula.

        Args:
            mol (Formula): Input formula

        Raises:
            PreconditionError: Precondition is not satisfied
        """
        self.logger.info(f"<{mol}>: validated formula")

    @abstractmethod
    def embed_molecule(self, obmol):
        """Embed molecule in 3D.

        Args:
            obmol: Molecule to embed

        Raises:
            ExecutionError: Embedding failed
        """

    @abstractmethod
    def ff_optimize_molecule(self, obmol) -> int:
        """Run FF optimization on molecule.

        Args:
            obmol: Molecule to optimize

        Raises:
            ExecutionError: FF optimization failed
        """

    @abstractmethod
    def build_molecule(self, smiles: str) -> str:
        """Build molecule.

        Args:
            smiles (int): SMILES string

        Returns:
            str: Output atomic coordinates in MOL format

        Raises:
            MoleculeError: Invalid molecule
            ExecutionError: Build failed
        """

    def generate_configuration(self, inp: Formula) -> Configuration:
        """Generate configuration.

        Args:
            inp (Formula): Input formula

        Returns:
            Configuration: Output configuration molecule
        """

        self.logger.debug(f"<{inp}>: embedding method: {self.config.CONFIGURATION.EMBED_METHOD}")
        self.logger.debug(f"<{inp}>: embedding iterations: "
                          f"{self.config.CONFIGURATION.EMBED_ITERATIONS}")
        if self.config.CONFIGURATION.FF_OPTIMIZATION:
            self.logger.debug(f"<{inp}>: FF method: {self.config.CONFIGURATION.FF_METHOD}")
            self.logger.debug(f"<{inp}>: FF iterations: "
                              f"{self.config.CONFIGURATION.FF_ITERATIONS}")
        else:
            self.logger.debug(f"<{inp}>: no FF optimization")
        self.logger.debug(f"<{inp}>: number of conformers: "
                          f"{self.config.CONFIGURATION.CONFORMERS}")

        coord = self.build_molecule(inp.smiles)

        self.logger.debug(f"<{inp}>: built MOL structure: {coord}")

        out = Configuration(
            coord,
            stereo=self.config.CONFIGURATION.get('STEREO'),
            tag=inp.tag,
            program=self.config.CONFIGURATION.get('PROGRAM'),
            version=self.config.CONFIGURATION.get('VERSION'),
            method=self.config.CONFIGURATION.get('METHOD'),
            options=self.config.CONFIGURATION.get('OPTIONS'),
        )

        self.logger.info(f"<{out}>: generated configuration")

        return out
    
    def validate_composition(self, inp: Formula, out: Configuration):
        """Validate molecular composition.

        Args:
            inp (Formula): Input formula
            out (Configuration): Output configuration

        Raises:
            ValidationError: Invalid output
        """

        formula_atomkey = inp.atomkey
        configuration_atomkey = out.atomkey
        formula_charge = inp.charge
        configuration_charge = out.charge
        formula_mult = inp.mult
        configuration_mult = out.mult

        self.logger.debug(f"<{out}>: formula stoichiometry: {formula_atomkey}, "
                          f"configuration stoichiometry: {configuration_atomkey}")
        self.logger.debug(f"<{out}>: formula charge: {formula_charge}, "
                          f"configuration charge: {configuration_charge}")
        self.logger.debug(f"<{out}>: formula spin multiplicity: {formula_mult}, "
                          f"configuration spin multiplicity: {configuration_mult}")

        if formula_atomkey != configuration_atomkey:
            raise ValidationError(message="stoichiometry change")
        if formula_charge != configuration_charge:
            raise ValidationError(message="charge change")
        if formula_mult != configuration_mult:
            raise ValidationError(message="spin multiplicity change")

    def validate_connectivity(self, inp: Formula, out: Configuration):
        """Validate molecular connectivity.

        Args:
            inp (Formula):  Input formula
            out (Configuration): Output configuration

        Raises:
            ValidationError: Invalid output
        """

        # Check if connectivities are available
        formula_parts = inp.inchi.count('/')
        configuration_parts = out.inchi.count('/')

        if formula_parts > 1 and configuration_parts > 1:

            formula_comp, formula_conn = (inp.inchi.split('/')[1:3])
            configuration_comp, configuration_conn = (out.inchi.split('/')[1:3])

            self.logger.debug(f"<{out}>: formula composition: {formula_comp}, "
                              f"configuration composition: {configuration_comp}")
            self.logger.debug(f"<{out}>: formula connectivity: {formula_conn}, "
                              f"configuration connectivity: {configuration_conn}")

            # Check for fragmentation
            if not is_connected(out.adjacency):
                raise ValidationError(message="fragmentation")

            # Check for rearrangement
            if inp.adjkey != out.adjkey:
                raise ValidationError(message="rearrangement")

        elif formula_parts > 1 and configuration_parts == 1:
            raise ValidationError(message="atomization")

    def validate_configuration(self, inp: Formula, out: Configuration):
        """Validate output configuration.

        Args:
            inp (Formula):  Input formula
            out (Configuration): Output configuration

        Raises:
            ValidationError: Invalid output
        """

        if not self.config.CONFIGURATION.VALIDATION:
            return

        self.validate_composition(inp, out)
        self.validate_connectivity(inp, out)
        self.logger.info(f"<{out}>: validated configuration")

    def process(self, inp: Formula) -> Molecule:
        """Build molecule.

        Args:
            inp (Formula): Input formula

        Returns:
            Molecule: Output molecule
        """

        out = None

        try:
            self.validate_formula(inp)
            out = self.generate_configuration(inp)
            self.validate_configuration(inp, out)

        except PreconditionError as e:
            self.logger.error(f"<{inp}>: {e.message}")
            out = InvalidFormula(
                inp.smiles,
                error=e.message,
                tag=inp.tag,
                program=self.config.CONFIGURATION.get('PROGRAM'),
                version=self.config.CONFIGURATION.get('VERSION'),
                method=self.config.CONFIGURATION.get('METHOD'),
                options=self.config.CONFIGURATION.get('OPTIONS'),
            )

        except (ExecutionError, MoleculeError) as e:
            self.logger.error(f"<{inp}>: {e.message}")
            out = FailedFormula(
                inp.smiles,
                error=e.message,
                tag=inp.tag,
                program=self.config.CONFIGURATION.get('PROGRAM'),
                version=self.config.CONFIGURATION.get('VERSION'),
                method=self.config.CONFIGURATION.get('METHOD'),
                options=self.config.CONFIGURATION.get('OPTIONS'),
            )

        except ValidationError as e:
            self.logger.error(f"<{out}>: {e.message}")
            out = InvalidConfiguration(
                out.coord,
                error=e.message,
                tag=inp.tag,
                program=self.config.CONFIGURATION.get('PROGRAM'),
                version=self.config.CONFIGURATION.get('VERSION'),
                method=self.config.CONFIGURATION.get('METHOD'),
                options=self.config.CONFIGURATION.get('OPTIONS'),
            )

        return out
