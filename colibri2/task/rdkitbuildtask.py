"""Build task using RDKit.

Classes:
     RDKitBuildTask: Build task using RDKit
"""
import numpy as np
from rdkit import Chem
from rdkit.Chem import AllChem

from .buildtask import BuildTask
from ..exceptions import ExecutionError, MoleculeError

__all__ = ['RDKitBuildTask']


class RDKitBuildTask(BuildTask):
    """Build task using RDKit."""

    def embed_molecule(self, rdmol: Chem.Mol):
        """Embed molecule in 3D using RDKit.

        Args:
            rdmol (Chem.Mol): Molecule to embed

        Raises:
            ExecutionError: Embedding failed
        """

        embed_method = self.config.CONFIGURATION.EMBED_METHOD
        embed_iterations = self.config.CONFIGURATION.EMBED_ITERATIONS
        num_conformers = self.config.CONFIGURATION.CONFORMERS

        # Set embedding parameters
        # Explore other algorithms and parameters
        # https://www.rdkit.org/docs/source/rdkit.Chem.rdDistGeom.html
        if embed_method == 'KDG':
            params = AllChem.KDG()
        elif embed_method == 'ETDG':
            params = AllChem.ETDG()
        elif embed_method == 'ETKDG':
            params = AllChem.ETKDG()
        elif embed_method == 'ETKDGv2':
            params = AllChem.ETKDGv2()
        elif embed_method == 'ETKDGv3':
            params = AllChem.ETKDGv3()
        else:
            raise RuntimeError("invalid embedding method")

        params.maxIterations = embed_iterations

        if num_conformers > 1:
            res = AllChem.EmbedMultipleConfs(rdmol, num_conformers, params)
            if len(res) == 0:
                raise ExecutionError(message="embedding failed")
        else:
            try:
                res = AllChem.EmbedMolecule(rdmol, params)
            except RuntimeError as e:
                raise ExecutionError(message=f"embedding failed due to RuntimeError: {e}")
            if res == -1:
                raise ExecutionError(message="embedding failed")

    def ff_optimize_molecule(self, rdmol: Chem.Mol) -> int:
        """Run FF optimization on molecule using RDKit.

        Args:
            rdmol (Chem.Mol): Molecule to optimize

        Returns:
            int: Index of lowest-energy conformer (-1 is default)

        Raises:
            ExecutionError: FF optimization failed
        """

        ff_method = self.config.CONFIGURATION.FF_METHOD
        ff_iterations = self.config.CONFIGURATION.FF_ITERATIONS
        num_conformers = self.config.CONFIGURATION.CONFORMERS

        if num_conformers > 1:

            if ff_method == 'UFF':
                results = AllChem.UFFOptimizeMoleculeConfs(rdmol, maxIters=ff_iterations)
            elif ff_method in {'MMFF94', 'MMFF94s'}:
                results = AllChem.MMFFOptimizeMoleculeConfs(
                    rdmol, mmffVariant=ff_method, maxIters=ff_iterations)
            else:
                raise RuntimeError("invalid FF method")

            min_index = -1
            min_energy = np.inf
            for index, (res, energy) in enumerate(results):
                if res != 0:
                    continue
                if energy < min_energy:
                    min_index = index
                    min_energy = energy
            conformer = min_index

        else:

            if ff_method == 'UFF':
                res = AllChem.UFFOptimizeMolecule(rdmol, maxIters=ff_iterations)
                if res != 0:
                    raise ExecutionError(message="FF optimization failed")
            elif ff_method in {'MMFF94', 'MMFF94s'}:
                res = AllChem.MMFFOptimizeMolecule(
                    rdmol, mmffVariant=ff_method, maxIters=ff_iterations)
                if res != 0:
                    raise ExecutionError(message="FF optimization failed")
            else:
                raise RuntimeError("invalid FF method")

            conformer = -1

        return conformer

    def build_molecule(self, smiles: str) -> str:
        """Build molecule using RDKit.

        Args:
            smiles (int): SMILES string

        Returns:
            str: Output atomic coordinates in MOL format

        Raises:
            MoleculeError: Invalid molecule
            ExecutionError: Build failed
        """

        # Generate 3D structure from SMILES string and run FF optimization
        conformer = -1
        rdmol = Chem.MolFromSmiles(smiles, sanitize=True)
        if rdmol is None:
            raise MoleculeError(message="invalid SMILES string")
        rdmol = Chem.AddHs(rdmol)
        if rdmol.GetNumAtoms() > 1:
            self.embed_molecule(rdmol)
            if self.config.CONFIGURATION.FF_OPTIMIZATION:
                conformer = self.ff_optimize_molecule(rdmol)
        coord = Chem.MolToMolBlock(rdmol, confId=conformer)

        return coord
