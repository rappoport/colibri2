"""Build task using OpenBabel.

Classes:
    PybelBuildTask: Build task using Pybel
"""
import numpy as np
from openbabel import pybel

from .buildtask import BuildTask
from ..exceptions import ExecutionError, MoleculeError

__all__ = ['PybelBuildTask']


class PybelBuildTask(BuildTask):
    """Build task using OpenBabel (Pybel)."""

    def embed_molecule(self, obmol: pybel.Molecule):
        """Embed molecule in 3D using OpenBabel.

        Args:
            obmol (pybel.Molecule): Molecule to embed

        Raises:
            ExecutionError: Embedding failed
        """

        embed_method = self.config.CONFIGURATION.EMBED_METHOD
        embed_iterations = self.config.CONFIGURATION.EMBED_ITERATIONS
        num_conformers = self.config.CONFIGURATION.CONFORMERS

        params = {}
        if embed_method == 'UFF':
            params['forcefield'] = "uff"
        elif embed_method == 'MMFF94':
            params['forcefield'] = "mmff94"
        elif embed_method == 'GHEMICAL':
            params['forcefield'] = "ghemical"
        else:
            raise RuntimeError("invalid embedding method")

        params['steps'] = embed_iterations

        if num_conformers > 1:
            raise ExecutionError(message="Not Implemented")
        else:
            try:
                obmol.make3D(**params)
            except RuntimeError as e:
                raise ExecutionError(message=f"embedding failed due to RuntimeError: {e}")
            if not obmol.OBMol.Has3D():
                raise ExecutionError(message="embedding failed")

    def ff_optimize_molecule(self, obmol: pybel.Molecule) -> int:
        """Run FF optimization on molecule using OpenBabel.

        Args:
            obmol (pybel.Molecule): Molecule to optimize

        Raises:
            ExecutionError: FF optimization failed
        """

        ff_method = self.config.CONFIGURATION.FF_METHOD
        ff_iterations = self.config.CONFIGURATION.FF_ITERATIONS
        num_conformers = self.config.CONFIGURATION.CONFORMERS

        if num_conformers > 1:
            raise ExecutionError(message="Not Implemented")
        else:
            params = {}
            if ff_method == 'UFF':
                params['forcefield'] = "uff"
            elif ff_method == 'MMFF94':
                params['forcefield'] = "mmff94"
            elif ff_method == 'GHEMICAL':
                params['forcefield'] = "ghemical"
            else:
                raise RuntimeError("invalid FF method")
            
            params['steps'] = ff_iterations

            try:
                obmol.localopt(**params)
            except RuntimeError as e:
                raise ExecutionError(message=f"FF optimization failed due to RuntimeError: {e}")
        return

    def build_molecule(self, smiles: str) -> str:
        """Build molecule using OpenBabel.

        Args:
            smiles (int): SMILES string

        Returns:
            str: Output atomic coordinates in MOL format

        Raises:
            MoleculeError: Invalid molecule
            ExecutionError: Build failed
        """

        # Generate 3D structure from SMILES string and run FF optimization
        obmol = pybel.readstring("smi", smiles)
        if obmol is None:
            raise MoleculeError(message="invalid SMILES string")
        obmol.addh()
        if len(obmol.atoms) > 1:
            self.embed_molecule(obmol)
            if self.config.CONFIGURATION.FF_OPTIMIZATION:
                self.ff_optimize_molecule(obmol)
        else:
            obmol.make3D()
        coord = obmol.write('mol')
        
        return coord
