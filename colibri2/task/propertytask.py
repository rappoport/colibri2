"""Abstract property task.

Classes:
    PropertyTask: Abstract property task
"""

from abc import ABC, abstractmethod

from .task import Task
from ..data import Geometry, Property, FailedProperty, InvalidGeometry, \
    InvalidProperty, Molecule
from ..exceptions import ExecutionError, PreconditionError, ValidationError, \
    MoleculeError

__all__ = ['PropertyTask']


class PropertyTask(Task, ABC):
    """Abstract property task."""

    def validate_geometry(self, mol: Geometry):
        """Validate input geometry.
        
        Args:
            mol (Geometry): Input geometry
            
        Raises:
            PreconditionError: Precondition is not satisfied
        """
        self.logger.info(f"<{mol}>: validated geometry")

    def validate_property(self, inp: Geometry, out: Property):
        """Validate output property.

        Args:
            inp (Geometry): Input geometry
            out (Property): Output property

        Raises:
            ValidationError: Invalid output
        """

        if not self.config.PROPERTY.VALIDATION:
            return

        self.logger.info(f"<{out}>: validated property")

    @abstractmethod
    def generate_property(self, inp: Geometry) -> Property:
        """Generate property from input geometry.

        Args:
            inp (Geometry): Input geometry

        Returns:
            Property: Output property
        """

    def process(self, inp: Geometry) -> Molecule:
        """Compute single-point energy using XTB.

        Args:
            inp (Geometry): Input geometry

        Returns:
            Molecule: Output property
        """

        out = None

        try:
            self.validate_geometry(inp)
            out = self.generate_property(inp)
            self.validate_property(inp, out)

        except PreconditionError as e:
            self.logger.error(f"<{inp}>: {e.message}")
            out = InvalidGeometry(
                inp.coord,
                error=e.message,
                tag=inp.tag,
                program=self.config.PROPERTY.PROGRAM,
                version=self.config.PROPERTY.VERSION,
                method=self.config.PROPERTY.METHOD,
                options=self.config.PROPERTY.OPTIONS,
            )

        except (ExecutionError, MoleculeError) as e:
            self.logger.error(f"<{inp}>: {e.message}")
            out = FailedProperty(
                inp.coord,
                error=e.message,
                tag=inp.tag,
                program=self.config.PROPERTY.PROGRAM,
                version=self.config.PROPERTY.VERSION,
                method=self.config.PROPERTY.METHOD,
                options=self.config.PROPERTY.OPTIONS,
            )

        except ValidationError as e:
            self.logger.error(f"<{out}>: {e.message}")
            out = InvalidProperty(
                out.coord,
                error=e.message,
                tag=inp.tag,
                program=self.config.PROPERTY.PROGRAM,
                version=self.config.PROPERTY.VERSION,
                method=self.config.PROPERTY.METHOD,
                options=self.config.PROPERTY.OPTIONS,
            )

        return out
