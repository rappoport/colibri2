"""Geometry task using XTB.

Classes:
    XTBGeometryTask: Geometry task using XTB
"""

import pathlib
import subprocess
import tempfile
from typing import List, Tuple, Dict, Any

from .geometrytask import GeometryTask
from ..data import Configuration, Geometry
from ..exceptions import ExecutionError
from ..parse import format_xyz, parse_mol, parse_xtb_opt
from ..structure import cleanup_mol, remove_bonds, aromatize_mol, kekulize_mol

__all__ = ['XTBGeometryTask']


class XTBGeometryTask(GeometryTask):
    """Geometry task using XTB."""

    def prepare_geometry_input(self, coord) -> Tuple[str, List[str]]:
        """Generate XYZ input and make CLI options for XTB.

        Args:
            coord (str):  Molecule definition in MOL format

        Returns:
            str, List[str]: XYZ input, list of CLI options for XTB
        """

        atoms, _, properties = parse_mol(coord)

        # Generate XYZ input (MOL input supposedly works but the parser seems wonky)
        xyz = format_xyz(atoms)

        method = self.config.GEOMETRY.METHOD.lower()
        options = self.config.GEOMETRY.OPTIONS
        iterations = self.config.GEOMETRY.ITERATIONS

        if self.config.GEOMETRY.NO_OPT and self.config.GEOMETRY.FREE_ENERGY:
            raise ExecutionError(message="no optimization and free energy options are incompatible")

        # Construct CLI options for XTB
        cli_options = ['-v']

        if not self.config.GEOMETRY.NO_OPT:
            if self.config.GEOMETRY.FREE_ENERGY:
                if not options or not ('--ohess' in options):
                    cli_options.extend(['--ohess'])
            else:
                if not options or not ('-o' in options or '--opt' in options):
                    cli_options.extend(['--opt'])
            if iterations > 0:
                cli_options.extend(['--cycles', str(iterations)])

        if self.config.GEOMETRY.get('SET_CHARGE'):
            charge = int(self.config.GEOMETRY.SET_CHARGE)
        else:
            charge = properties.get('charge', 0)
        if charge != 0:
            cli_options.extend(['-c', str(charge)])

        if self.config.GEOMETRY.get('SET_MULT'):
            mult = int(self.config.GEOMETRY.SET_MULT)
        else:
            mult = properties.get('mult', 1)
        if mult > 1:
            cli_options.extend(['-u', str(mult - 1)])

        if method == 'gfn1':
            cli_options.extend(['--gfn', '1'])
        elif method == 'gfn2':
            cli_options.extend(['--gfn', '2'])

        if options:
            cli_options.extend(options.split())

        self.logger.debug(f"XTB CLI options: {' '.join(cli_options)}")

        return xyz, cli_options

    def run_geometry_calculation(self, xyz: str, cli_options: List[str]) -> Tuple[str, str, str]:
        """Run geometry optimization using XTB.

        Args:
            xyz (str): Coordinates in XYZ format
            cli_options (List[str]): XTB CLI options

        Returns:
            str, str, str: Optimized molecule in MOL format, alternate XYZ output, XTB output
        """

        # Create temporary directory
        with tempfile.TemporaryDirectory(
                prefix='xtb_', dir=self.config.GEOMETRY.SCRATCH_PATH) as tempdir_name:
            tempdir = pathlib.Path(tempdir_name)

            self.logger.debug(f"Starting XTB calculation in temporary directory {tempdir_name}")

            # Write XYZ coordinates file
            input_file = 'coord.xyz'
            with open(tempdir / input_file, 'w') as input_f:
                input_f.write(xyz)

            # Start XTB process
            try:
                proc = subprocess.run([self.config.GEOMETRY.EXECUTABLE, *cli_options, input_file],
                                      capture_output=True, cwd=tempdir_name,
                                      timeout=self.config.GEOMETRY.TIMEOUT, check=True)

                if 'normal termination of xtb' not in str(proc.stderr):
                    raise ExecutionError(message="abnormal program exit")

                # Read optimized MOL file
                opt_mol = None
                output_file = 'xtbtopo.mol'
                output_path = tempdir / output_file
                if output_path.exists():
                    with output_path.open('r') as output_f:
                        opt_mol = output_f.read()

                # Read alternative XYZ file
                alt_xyz = None
                output_file_xyz = 'xtbhess.xyz'
                output_path = tempdir / output_file_xyz
                if output_path.exists():
                    with output_path.open('r') as output_f:
                        alt_xyz = output_f.read()

                number_atoms = int(xyz.split('\n')[0])
                if not opt_mol and not alt_xyz and number_atoms != 1:
                    raise ExecutionError(message="no output coordinates")

                # Retrieve XTB output
                geometry_output = proc.stdout.decode('utf-8')

                if not geometry_output:
                    raise ExecutionError(message="no optimization output")

            except FileNotFoundError:
                raise ExecutionError(message="executable not found")

            except PermissionError:
                raise ExecutionError(message="executable lacks permissions")

            except subprocess.CalledProcessError:
                raise ExecutionError(message="optimization failed")

            except subprocess.TimeoutExpired:
                raise ExecutionError(message="optimization timed out")
            
            except UnicodeDecodeError:
                raise ExecutionError(message="Corrupted output file")

        self.logger.debug(f"** XTB output **\n{geometry_output}\n** End XTB output **")

        return opt_mol, alt_xyz, geometry_output

    def process_geometry_output(self, geometry_output: str) -> Tuple[Dict[str, Any], Dict[str, Any], bool]:
        """Parse XTB output including optimization.

        Args:
            geometry_output (str): XTB output including optimization

        Returns:
            Dict[str, Any], Dict[str, Any], bool: Properties, flags, reoptimization flag
        """

        properties, flags = parse_xtb_opt(geometry_output, thermochem=self.config.GEOMETRY.FREE_ENERGY)

        # Set flag to reoptimize if molecule found not stable and has symmetry
        reoptimize = not flags.get('is_stable', True)
        return properties, flags, reoptimize

    def optimize_molecule(self, coord: str) -> Tuple[str, Dict[str, Any], Dict[str, Any]]:
        """Optimize molecule using XTB.

        Args:
            coord (str): Input molecule definition in MOL format

        Returns:
             str, Dict[str, Any], Dict[str, Any]: Output MOL string, properties, flags

        Raises:
            ExecutionError: Optimization failed
        """

        max_reopts = self.config.GEOMETRY.REOPTIMIZATIONS

        xyz, cli_options = self.prepare_geometry_input(coord)
        opt_mol, alt_xyz, geometry_output = self.run_geometry_calculation(xyz, cli_options)
        properties, flags, reoptimize = self.process_geometry_output(geometry_output)

        reopt = 1
        while reopt < max_reopts and reoptimize and alt_xyz:
            point_group = properties.get('point_group', 'c1')
            self.logger.debug(f"Optimized structure is not stable in {point_group} point group")
            self.logger.debug(f"Reoptimization {reopt} from distorted coordinates {alt_xyz}")
            opt_mol, alt_xyz, geometry_output = self.run_geometry_calculation(alt_xyz, cli_options)
            properties, flags, reoptimize = self.process_geometry_output(geometry_output)
            reopt += 1

        if self.config.GEOMETRY.IGNORE_BONDING:
            opt_mol = remove_bonds(opt_mol)

        number_atoms = int(xyz.split('\n')[0])
        if number_atoms == 1:
            opt_mol = coord
        return opt_mol, properties, flags

    def generate_geometry(self, inp: Configuration) -> Geometry:
        """Generate geometry using XTB.

        Args:
            inp (Configuration): Input configuration

        Returns:
            Geometry: Output geometry
        """

        self.logger.debug(f"<{inp}>: optimization method: {self.config.GEOMETRY.METHOD}")
        self.logger.debug(f"<{inp}>: optimization options: {self.config.GEOMETRY.OPTIONS}")
        self.logger.debug(f"<{inp}>: free energy calculation: {self.config.GEOMETRY.FREE_ENERGY}")
        self.logger.debug(f"<{inp}>: reoptimizations: {self.config.GEOMETRY.REOPTIMIZATIONS}")

        coord, properties, flags = self.optimize_molecule(inp.coord)

        self.logger.debug(f"<{inp}>: optimized MOL structure: {coord}")

        energy = properties.get('energy', 0.0)
        if self.config.GEOMETRY.ERROR_POSITIVE_ENERGY and energy > 0:
            raise ExecutionError(message="positive free energy found")
        converged = flags.get('converged', False)
        self.logger.debug(f"<{inp}>: converged: {converged}")
        self.logger.debug(f"<{inp}>: energy / eV: {energy:8.3f}")

        free_energy = None
        is_stable = None
        if self.config.GEOMETRY.FREE_ENERGY:
            free_energy = properties.get('free_energy', 0.0)
            is_stable = flags.get('is_stable', False)
            self.logger.debug(f"<{inp}>: stable: {is_stable}")
            self.logger.debug(f"<{inp}>: free energy / eV: {free_energy:8.3f}")

        # Remove excess bonds
        if self.config.GEOMETRY.VALIDATION:
            coord = cleanup_mol(coord)
        else:
            coord = inp.coord

        self.logger.debug(f"<{inp}>: use aromatic structure: {self.config.GEOMETRY.AROMATICITY}")

        # Convert to aromatic / Kekule structure
        if self.config.GEOMETRY.AROMATICITY:
            coord = aromatize_mol(coord)
        else:
            coord = kekulize_mol(coord)

        # TODO(dr): Add raw output
        out = Geometry(
            coord,
            stereo=self.config.GEOMETRY.STEREO,
            tag=inp.tag,
            program=self.config.GEOMETRY.PROGRAM,
            version=self.config.GEOMETRY.VERSION,
            method=self.config.GEOMETRY.METHOD,
            options=self.config.GEOMETRY.OPTIONS,
            converged=converged,
            energy=energy,
            free_energy=free_energy,
            stable=is_stable,
        )

        self.logger.info(f"<{out}>: generated geometry")

        return out
