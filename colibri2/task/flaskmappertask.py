"""Flask mapper task.

Classes:
     FlaskMapperTask: Flask mapper task
"""

from typing import Tuple, List

from .task import Task
from ..data import Formula, Superformula, PendingSupergeometry, InvalidSuperformula, \
    FailedSuperformula
from ..exceptions import PreconditionError, MoleculeError, FlaskError

__all__ = ['FlaskMapperTask']


class FlaskMapperTask(Task):
    """Flask mapper task."""

    def validate_superformula(self, flask: Superformula):
        """Validate input superformula.

        Args:
            flask (Superformula): Input superformula

        Raises:
            PreconditionError: Precondition not satisfied
        """
        self.logger.info(f"<{flask}>: validated superformula")

    def map_formulas(self, inp: Superformula) -> Tuple[PendingSupergeometry, List[Formula]]:
        """Split superformula into list of formulas.

        Args:
            inp (Superformula): Input formula

        Returns:
            (PendingSupergeometry, List[Formula]): Output Pending supergeometry, list of molecules
        """

        # Collect attempts
        collect_attempts = self.config.COLLECT.ATTEMPTS

        # Construct output PendingSupergeometry
        out = PendingSupergeometry.fromdict({**inp.todict(), **{'attempts_left': collect_attempts}})

        # Extract molecules
        molecules = inp.split()

        self.logger.info(f"<{inp}>: generated formulas")

        return out, molecules

    def process(self, inp: Superformula) -> Tuple[PendingSupergeometry, List[Formula]]:
        """Split flask into formulas.

        Args:
            inp (Superformula): Input superformula

        Returns:
            (PendingSupergeometry, List[Formula]): Output Pending supergeometry, list of molecules
        """

        out = None
        formulas = []

        try:
            self.validate_superformula(inp)
            out, formulas = self.map_formulas(inp)

        except PreconditionError as e:
            self.logger.error(f"<{inp}>: {e.message}")
            out = InvalidSuperformula(inp.smiles, e.message)

        except (MoleculeError, FlaskError) as e:
            self.logger.error(f"<{inp}>: {e.message}")
            out = FailedSuperformula(inp.smiles, e.message)

        return out, formulas
