"""Geometry task using MOPAC.

Classes:
     MOPACGeometryTask: Geometry task using MOPAC
"""

import pathlib
import subprocess
import tempfile
from typing import Tuple, Dict, Any

from .geometrytask import GeometryTask
from ..data import Configuration, Geometry
from ..exceptions import ExecutionError
from ..parse import parse_mol, format_mol, format_mopac, parse_mopac
from ..structure import merge_atoms, aromatize_mol, kekulize_mol

__all__ = ['MOPACGeometryTask']


class MOPACGeometryTask(GeometryTask):
    """Geometry task using MOPAC."""

    def prepare_geometry_input(self, coord: str) -> str:
        """Generate MOPAC input.

        Args:
            coord (str):  Molecule definition in MOL format

        Returns:
            str: MOPAC input
        """

        cycles = self.config.GEOMETRY.ITERATIONS
        options = self.config.GEOMETRY.OPTIONS
        no_opt = self.config.GEOMETRY.NO_OPT
        title = ""

        atoms, _, properties = parse_mol(coord)
        geometry_input = format_mopac(atoms, properties, title, cycles, options, no_opt)

        self.logger.debug(f"** MOPAC input **\n{geometry_input}\n** End MOPAC input **")

        return geometry_input

    def run_geometry_calculation(self, geometry_input: str) -> str:
        """Run geometry optimization using MOPAC.

        Args:
            geometry_input (str): MOPAC input

        Returns:
            str: MOPAC output
        """

        # Create temporary directory
        with tempfile.TemporaryDirectory(
                prefix='mopac_', dir=self.config.GEOMETRY.SCRATCH_PATH) as tempdir_name:
            tempdir = pathlib.Path(tempdir_name)

            self.logger.debug(f"Starting MOPAC calculation in temporary directory {tempdir_name}")

            # Write MOPAC input file
            input_file = 'mopac.mop'
            with open(tempdir / input_file, 'w') as input_f:
                input_f.write(geometry_input)

            # Start MOPAC process
            try:
                proc = subprocess.run([self.config.GEOMETRY.EXECUTABLE, input_file],
                                      capture_output=True, cwd=tempdir_name,
                                      timeout=self.config.GEOMETRY.TIMEOUT, check=True)

                # Read MOPAC output file
                output_file = 'mopac.out'
                with open(tempdir / output_file, 'r') as output_f:
                    geometry_output = output_f.read()

                if not geometry_output:
                    raise ExecutionError(message="no optimization output")

            except FileNotFoundError:
                raise ExecutionError(message="executable not found")

            except PermissionError:
                raise ExecutionError(message="executable lacks permissions")

            except subprocess.CalledProcessError:
                raise ExecutionError(message="optimization failed")

            except subprocess.TimeoutExpired:
                raise ExecutionError(message="optimization timed out")

        self.logger.debug(f"** MOPAC output **\n{geometry_output}\n** End MOPAC output **")

        return geometry_output

    def process_geometry_output(self, geometry_output: str, inp_coord: str) -> \
            Tuple[str, Dict[str, Any], Dict[str, Any]]:
        """Parse MOPAC program output.

        Args:
            geometry_output (str): MOPAC program output
            inp_coord (str): Reference molecule definition in MOL format

        Returns:
            str, Dict[str, Any], Dict[str, Any]: Structure in MOL format, properties, flags
        """

        out_atoms, out_properties, out_flags = parse_mopac(geometry_output)
        inp_atoms, inp_bonds, inp_properties = parse_mol(inp_coord)

        out_charge = out_properties.get('charge', 0)
        out_mult = out_properties.get('mult', 1)
        inp_charge = inp_properties.get('charge', 0)
        inp_mult = inp_properties.get('mult', 1)
        if out_charge != inp_charge:
            raise ExecutionError(
                message=f"charge changed during optimization ({out_charge} != {inp_charge})")
        if out_mult != inp_mult:
            raise ExecutionError(
                message=f"spin multiplicity changed during optimization ({out_mult} != {inp_mult})")

        try:
            atoms = merge_atoms(inp_atoms, out_atoms)
        except ValueError:
            raise ExecutionError(message=f"atomic composition changed during optimization")

        bonds = inp_bonds
        properties = out_properties
        out_coord = format_mol(atoms, bonds, properties)

        self.logger.debug(f"combined molecular definition: {out_coord}")

        return out_coord, out_properties, out_flags

    def optimize_molecule(self, coord: str) -> Tuple[str, Dict[str, Any], Dict[str, Any]]:
        """Optimize molecule using MOPAC.

        Args:
            coord (str): Input molecule definition in MOL format

        Returns:
            str, Dict[str, Any], Dict[str, Any]]: Output MOL string, properties, flags

        Raises:
            ExecutionError: Optimization failed
        """

        geometry_input = self.prepare_geometry_input(coord)
        geometry_output = self.run_geometry_calculation(geometry_input)
        opt, properties, flags = self.process_geometry_output(geometry_output, coord)

        return opt, properties, flags

    def generate_geometry(self, inp: Configuration) -> Geometry:
        """Generate geometry using MOPAC.

        Args:
            inp (Configuration): Input configuration

        Returns:
            Geometry: Output geometry
        """

        self.logger.debug(f"<{inp}>: optimization method: {self.config.GEOMETRY.METHOD}")
        self.logger.debug(f"<{inp}>: optimization options: {self.config.GEOMETRY.OPTIONS}")
        self.logger.debug(f"<{inp}>: optimization iterations: {self.config.GEOMETRY.ITERATIONS}")

        coord, properties, flags = self.optimize_molecule(inp.coord)

        self.logger.debug(f"<{inp}>: use aromatic structure: {self.config.GEOMETRY.AROMATICITY}")

        if self.config.GEOMETRY.AROMATICITY:
            coord = aromatize_mol(coord)
        else:
            coord = kekulize_mol(coord)

        energy = properties.get('energy', 0.0)
        converged = flags.get('converged', False)

        self.logger.debug(f"<{inp}>: optimized coordinates: {coord}")
        self.logger.debug(f"<{inp}>: converged: {converged}")
        self.logger.debug(f"<{inp}>: energy: {energy}")

        # TODO(dr): Add raw output
        out = Geometry(
            coord=coord,
            stereo=self.config.GEOMETRY.STEREO,
            tag=inp.tag,
            program=self.config.GEOMETRY.PROGRAM,
            version=self.config.GEOMETRY.VERSION,
            method=self.config.GEOMETRY.METHOD,
            options=self.config.GEOMETRY.OPTIONS,
            converged=converged,
            energy=energy,
        )

        self.logger.info(f"<{out}>: generated geometry")

        return out
