"""Property task using XTB.

Classes:
    XTBEnergyPropertyTask: Property task for single-point energies using XTB
"""
import pathlib
import subprocess
import tempfile
from typing import Tuple, Dict, List, Any

from ..data import Geometry, Property
from ..exceptions import ExecutionError
from .propertytask import PropertyTask
from ..parse import parse_mol, format_xyz, parse_xtb_en

__all__ = ['XTBEnergyPropertyTask']


class XTBEnergyPropertyTask(PropertyTask):
    """Property task for single-point energies using XTB."""

    def prepare_property_input(self, coord) -> Tuple[str, List[str]]:
        """Generate XYZ input and make CLI options for XTB.

        Args:
            coord(str): Molecule definition in MOL format

        Returns:
            str, List[str]: XYZ input, list of CLI options for XTB
        """

        atoms, _, properties = parse_mol(coord)

        # Generate XYZ input (see XTBGeometryTask)
        xyz = format_xyz(atoms)

        method = self.config.PROPERTY.METHOD.lower()
        options = self.config.PROPERTY.OPTIONS

        # Construct CLI options for XTB
        cli_options = ['-v', '--sp']

        if self.config.PROPERTY.get('SET_CHARGE'):
            charge = int(self.config.PROPERTY.SET_CHARGE)
        else:
            charge = properties.get('charge', 0)
        if charge != 0:
            cli_options.extend(['-c', str(charge)])

        if self.config.PROPERTY.get('SET_MULT'):
            mult = int(self.config.PROPERTY.SET_MULT)
        else:
            mult = properties.get('mult', 1)
        if mult > 1:
            cli_options.extend(['-u', str(mult - 1)])

        if method == 'gfn1':
            cli_options.extend(['--gfn', '1'])
        elif method == 'gfn2':
            cli_options.extend(['--gfn', '2'])

        if options:
            cli_options.extend(options.split())

        return xyz, cli_options

    def run_property_calculation(self, xyz, cli_options) -> str:
        """Run single-point energy calculation using XTB.

        Args:
            xyz (str): Coordinates in XYZ format
            cli_options (List[str]): XTB CLI options

        Returns:
            str: XTB output
        """

        # Create temporary directory
        with tempfile.TemporaryDirectory(
                prefix='xtb_', dir=self.config.PROPERTY.SCRATCH_PATH) as tempdir_name:
            tempdir = pathlib.Path(tempdir_name)

            self.logger.debug(f"Starting XTB calculation in temporary directory {tempdir_name}")

            # Write XYZ coordinates file
            input_file = 'coord.xyz'
            with open(tempdir / input_file, 'w') as input_f:
                input_f.write(xyz)

            # Start XTB process
            try:
                proc = subprocess.run([self.config.PROPERTY.EXECUTABLE, *cli_options, input_file],
                                      capture_output=True, cwd=tempdir_name,
                                      timeout=self.config.PROPERTY.TIMEOUT, check=True)

                if 'normal termination of xtb' not in str(proc.stderr):
                    raise ExecutionError(message="abnormal program exit")

                # Retrieve4 XTB output
                property_output = proc.stdout.decode('utf-8')

                if not property_output:
                    raise ExecutionError(message="no property output")

            except FileNotFoundError:
                raise ExecutionError(message="executable not found")

            except PermissionError:
                raise ExecutionError(message="executable lacks permissions")

            except subprocess.CalledProcessError:
                raise ExecutionError(message="calculation failed")

            except subprocess.TimeoutExpired:
                raise ExecutionError(message="calculation timed out")
            
            except UnicodeDecodeError:
                raise ExecutionError(message="Corrupted output file")
            
        self.logger.debug(f"** XTB output **\n{property_output}\n** End XTB output**")

        return property_output

    def process_property_output(self, property_output) -> Tuple[Dict[str, Any], Dict[str, Any]]:
        """Parse XTB output including energy.

        Args:
            property_output (str): XTB output including energy

        Returns:
            Dict[Str, Any], Dict[str, Any]: Properties, flags
        """

        properties, flags = parse_xtb_en(property_output)
        return properties, flags

    def compute_properties(self, coord) -> Tuple[Dict[str, Any], Dict[str, Any]]:
        """Compute single-point energies using XTB.

        Args:
            coord (str): Input molecule definition in MOL format

        Returns:
            Dict[str, Any], Dict[str, Any]: Properties, flags

        Raises:
            ExecutionError: Computation failed
        """

        xyz, cli_options = self.prepare_property_input(coord)
        property_output = self.run_property_calculation(xyz, cli_options)
        properties, flags = self.process_property_output(property_output)

        return properties, flags

    def generate_property(self, inp: Geometry) -> Property:
        """Compute single-point energy using XTB.

        Args:
            inp (Geometry): Input geometry

        Returns:
            Property: Energy property
        """

        self.logger.debug(f"<{inp}>: computation method: {self.config.PROPERTY.METHOD}")
        self.logger.debug(f"<{inp}>: computation options: {self.config.PROPERTY.OPTIONS}")

        properties, flags = self.compute_properties(inp.coord)

        self.logger.debug(f"<{inp}>: computed properties: {properties}")

        out = Property(
            inp.coord,
            stereo=inp.stereo,
            tag=inp.tag,
            program=self.config.PROPERTY.PROGRAM,
            version=self.config.PROPERTY.VERSION,
            method=self.config.PROPERTY.METHOD,
            options=self.config.PROPERTY.OPTIONS,
            properties=properties
        )

        self.logger.info(f"<{out}>: generated property")

        return out
