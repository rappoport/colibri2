"""Molecular library generators from reaction rules using RDKit.


Classes:
    RDKitLibraryGenerator: Base class for all library generators
"""

from __future__ import annotations

__all__ = ['RDKitLibraryGenerator']

import re
from typing import Dict, Any, Iterator, List

from rdkit import Chem
from rdkit.Chem import AllChem

from ..exceptions import MoleculeError, ReactionRuleError, ExecutionError
from ..utils import ensure_logger, random_id

_RE_RULE = re.compile(
    r'^\[(.*?)\]([.\-=#]?)\[(.*?)\]>>\[(.*?)\]([.\-=#]?)\[(.*?)\]$')
_RE_LHS = re.compile(r'^(.*):(\d+)$')
_RE_RHS = re.compile(r'^([A-Z][a-z]?|#\d+|\*)([+-]\d+):(\d+)$')
_RE_LHS_AT = re.compile(r'^([A-Z][a-z]?|#\d+)([+-]\d+)$')


class RDKitLibraryGenerator:
    """RDKit-based library generator."""

    @staticmethod
    def parse(rule: str) -> Dict[str, Any]:
        """Parse reaction properties from SMARTS definition.

        Args:
            rule (str): Rule definition

        Returns:
            Dict[str, Any]: Reacting atoms
        """

        parsed_rule = {'reactant': {}, 'product': {}}

        try:
            r_at1, parsed_rule['reactant']['bond'], r_at2, \
                p_at1, parsed_rule['product']['bond'], p_at2 = \
                _RE_RULE.match(rule).groups()
        except (ValueError, AttributeError):
            raise ReactionRuleError(message="invalid rule structure")

        try:
            r_atstr1, parsed_rule['reactant']['ind1'] = \
                _RE_LHS.match(r_at1).groups()
            r_atstr2, parsed_rule['reactant']['ind2'] = \
                _RE_LHS.match(r_at2).groups()
            parsed_rule['reactant']['atom1'] = [
                dict(zip(['element', 'charge'], _RE_LHS_AT.match(
                    r_atdef1).groups())) for r_atdef1 in r_atstr1.split(',')]
            parsed_rule['reactant']['atom2'] = [
                dict(zip(['element', 'charge'], _RE_LHS_AT.match(
                    r_atdef2).groups())) for r_atdef2 in r_atstr2.split(',')]
        except (ValueError, AttributeError):
            raise ReactionRuleError(message="invalid reactant definition")

        try:
            parsed_rule['product']['atom1'] = [{}]
            parsed_rule['product']['atom2'] = [{}]
            (parsed_rule['product']['atom1'][0]['element'],
             parsed_rule['product']['atom1'][0]['charge'],
             parsed_rule['product']['ind1']) = _RE_RHS.match(p_at1).groups()
            (parsed_rule['product']['atom2'][0]['element'],
             parsed_rule['product']['atom2'][0]['charge'],
             parsed_rule['product']['ind2']) = _RE_RHS.match(p_at2).groups()
        except (ValueError, AttributeError):
            raise ReactionRuleError(message="invalid product definition")

        return parsed_rule

    @staticmethod
    def molecularity(rule: str) -> int:
        """Compute reaction molecularity (number of reactants).

        Args:
            rule (str): SMARTS rule

        Returns:
            int: Reaction molecularity
        """

        molecularity = 1
        parsed_rule = RDKitLibraryGenerator.parse(rule)
        if parsed_rule['reactant']['bond'] == '.':
            molecularity = 2
        return molecularity

    @staticmethod
    def num_products(rule: str) -> int:
        """Compute number of products in reaction.

        Args:
            rule (str): SMARTS rule

        Returns:
            int: Number of reactants
        """

        num_products = 1
        parsed_rule = RDKitLibraryGenerator.parse(rule)
        if parsed_rule['product']['bond'] == '.':
            num_products = 2
        return num_products

    @staticmethod
    def univalent_atoms(rule: str) -> bool:
        """Determine if univalent reactions are involved in reaction.

        Args:
            rule (str): SMARTS rule

        Returns:
            bool: True if univalent atoms are present
        """
        univalent_elements = ['#1', 'Li', 'Na', 'K', 'Rb', 'Cs', 'Fr',
                              'F', 'Cl', 'Br', 'I', 'At']
        parsed_rule = RDKitLibraryGenerator.parse(rule)
        return (any([r_at1['element'] in univalent_elements
                     for r_at1 in parsed_rule['reactant']['atom1']]) or
                any([r_at2['element'] in univalent_elements
                     for r_at2 in parsed_rule['reactant']['atom2']]))

    def __init__(self, rule: str, cycl: bool = False, index: bool = False):
        """Library generator using RDKit.

        Args:
            rule (str): SMARTS rule
            cycl (bool): Flag for generating cyclizations
            index (bool): Use permutation indices
        """

        molecularity = self.molecularity(rule)
        num_products = self.num_products(rule)

        # Unimolecular reactions
        if molecularity == 1:
            self.rule = rule
            self.reactivity = "unimolecular"

        # Bimolecular and ring closing reactions
        elif molecularity == 2:
            if cycl and num_products == 1:
                self.rule = ('(' + rule.split('>>')[0] + ')>>' +
                             rule.split('>>')[1])
                self.reactivity = "cyclization"
            else:
                self.rule = rule
                self.reactivity = "bimolecular"

        try:
            self._lib_gen = AllChem.ReactionFromSmarts(self.rule)
        except (ValueError, RuntimeError) as e:
            raise ReactionRuleError(message=e)

        # Use permutation indices
        self.index = index

        # Atom mapping
        if self.index:
            atom_mapping = {}
            for idx, reactant in enumerate(self._lib_gen.GetReactants()):
                for atom in reactant.GetAtoms():
                    atom_mapping[atom.GetIntProp('molAtomMapNumber')] = idx
            self._atom_mapping = atom_mapping

        self.gen_id = f"{self}-{random_id(6)}"
        self.logger = ensure_logger(self.gen_id)

    def __str__(self) -> str:
        """Return string representation.

        Returns:
            str: Generator type as string
        """

        return f"{self.reactivity}"

    def react(self, reactants: List[str]) -> Iterator[List[str]]:
        """Apply library generator to reactants.

        Args:
            reactants (List[str]): List of reactants

        Yields:
            List[str]: Lists of products
        """

        rdreactants = []
        for reactant in reactants:
            rdreactant = Chem.MolFromSmiles(reactant, sanitize=True)
            if rdreactant is None:
                raise MoleculeError(message="invalid reactant")
            rdreactant = Chem.AddHs(rdreactant)
            Chem.Kekulize(rdreactant, clearAromaticFlags=True)
            rdreactants.append(rdreactant)

        if self.index:
            for rdreactant in rdreactants:
                for atom in rdreactant.GetAtoms():
                    if atom.GetSymbol() != 'H' and not atom.HasProp('p'):
                        raise ExecutionError(message="permutation indices missing in reactants")

        try:

            for rdproducts in self._lib_gen.RunReactants(rdreactants):
                for rdproduct in rdproducts:
                    Chem.SanitizeMol(rdproduct)

                if self.index:
                    for rdproduct in rdproducts:
                        for atom in rdproduct.GetAtoms():
                            atom.SetNoImplicit(True)
                            if atom.GetSymbol() != 'H' and not atom.HasProp('p'):
                                if not atom.HasProp('old_mapno'):
                                    raise ExecutionError(message="invalid atom mapping scheme")
                                old_mapno = atom.GetIntProp('old_mapno')
                                react_atom_idx = atom.GetUnsignedProp('react_atom_idx')
                                old_rdreactant = rdreactants[self._atom_mapping[old_mapno]]
                                p = old_rdreactant.GetAtomWithIdx(react_atom_idx).GetUnsignedProp('p')
                                atom.SetUnsignedProp('p', p)
                            if atom.HasProp('react_atom_idx'):
                                atom.ClearProp('react_atom_idx')
                            if atom.HasProp('old_mapno'):
                                atom.ClearProp('old_mapno')
                    smiles_list = [Chem.MolToCXSmiles(Chem.RemoveHs(rdproduct, sanitize=True),
                                                      canonical=True)
                                   for rdproduct in rdproducts]
                else:
                    smiles_list = [Chem.MolToSmiles(Chem.RemoveHs(rdproduct, sanitize=True),
                                                    canonical=True)
                                   for rdproduct in rdproducts]
                yield tuple(sorted(smiles_list))

        except ValueError as e:
            self.logger.error(f"reactants: {reactants}, rule:{self.rule}: {e.args[0]}")
