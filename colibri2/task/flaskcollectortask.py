"""Flask collector task.

Classes:
     FlaskCollectorTask: Flask collector task
"""

from typing import Set

from .task import Task
from ..data import Flask, PendingSupergeometry, Geometry, Molecule, IncompleteSupergeometry, \
    Supergeometry, FailedSupergeometry, InvalidSupergeometry, FailedGeometry, InvalidGeometry
from ..exceptions import PreconditionError, IncompleteError


class FlaskCollectorTask(Task):
    """Flask collector class."""

    def validate_pending_supergeometry(self, flask: PendingSupergeometry):
        """Validate input pending supergeometry

        Args:
            flask (PendingSupergeometry): Input pending supergeometry

        Raises:
            PreconditionError: Precondition not satisfied
        """
        self.logger.info(f"<{flask}>: validated pending supergeometry")

    def collect_molecules(self, inp: PendingSupergeometry, molecules: Set[Molecule]) -> Flask:
        """Attempt to collect geometry results into supergeometry

        Args:
            inp (PendingSupergeometry): Input pending supergeometry
            molecules (Set[Molecule]): Available molecules

        Returns:

        """

        # Required molecules
        required = inp.split()

        # Collected molecules
        collected = {mol.smiles: mol for mol in molecules}

        # Missing molecules
        # Cannot use set difference because required are formulas
        # and molecules are geometries
        missing = {formula for formula in required if formula.smiles not in collected}

        if missing:
            self.logger.debug(f"<{inp}>: missing molecules: {missing}")
            raise IncompleteError

        energy = 0.0
        failed = set()
        invalid = set()
        errors = []

        for formula in required:
            mol = collected[formula.smiles]
            if isinstance(mol, FailedGeometry):
                failed.add(mol.smiles)
                errors.append(f'{mol.smiles}: {mol.error}')
            elif isinstance(mol, InvalidGeometry):
                invalid.add(mol.smiles)
                errors.append(f'{mol.smiles}: {mol.error}')
            elif isinstance(mol, Geometry):
                energy += mol.energy

        if failed:
            out = FailedSupergeometry(inp.smiles, ', '.join(errors))
            self.logger.info(f"<{inp}>: collected molecules, failed results: {errors}")
        elif invalid:
            out = InvalidSupergeometry(inp.smiles, ', '.join(errors))
            self.logger.info(f"<{inp}>: collected molecules, invalid results: {errors}")
        else:
            out = Supergeometry(inp.smiles, energy=energy)
            self.logger.info(f"<{inp}: collected molecules with energy: {energy}")

        return out

    def process(self, inp: (PendingSupergeometry, Set[Geometry])) -> Flask:
        """Combine geometry results into flask if available.

        Args:
            inp (PendingSupergeometry, Set[Geometry]): Input flask and molecules

        Returns:
            Flask: Output flask
        """

        flask, molecules = inp

        try:
            self.validate_pending_supergeometry(inp)
            out = self.collect_molecules(flask, molecules)

        except PreconditionError as e:
            self.logger.error(f"<{inp}>: {e.message}")
            out = FailedSupergeometry(inp.smiles, e.message)

        except IncompleteError as e:
            self.logger.error(f"{inp}", {e.message})
            if inp.attempts_left > self.config.COLLECT.ATTEMPTS:
                out = inp.copy(attempts_left=inp.attempts_left - 1)
            else:
                out = IncompleteSupergeometry(inp.smiles, e.message)

        return out
