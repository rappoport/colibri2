"""Chemical data for colibri2.

Classes:
    Element: Element data
"""

__all__ = ['Element']

# Element name lookup by number
_el_num2name = [
    'Q',
    'H',                                                                                                                                            'He',
    'Li', 'Be',                                                                                                       'B',  'C',  'N',  'O',  'F',  'Ne',
    'Na', 'Mg',                                                                                                       'Al', 'Si', 'P',  'S',  'Cl', 'Ar',
    'K',  'Ca', 'Sc',                                           'Ti', 'V',  'Cr', 'Mn', 'Fe', 'Co', 'Ni', 'Cu', 'Zn', 'Ga', 'Ge', 'As', 'Se', 'Br', 'Kr',
    'Rb', 'Sr', 'Y',                                            'Zr', 'Nb', 'Mo', 'Tc', 'Ru', 'Rh', 'Pd', 'Ag', 'Cd', 'In', 'Sn', 'Sb', 'Te', 'I',  'Xe',
    'Cs', 'Ba', 'La', 'Ce', 'Pr', 'Nd', 'Pm', 'Sm', 'Eu', 'Gd', 'Tb', 'Dy', 'Ho', 'Er', 'Tm', 'Yb', 'Lu', 'Hf', 'Ta', 'W',  'Re', 'Os', 'Ir', 'Pt', 'Au', 'Hg', 'Tl', 'Pb', 'Bi', 'Po', 'At', 'Rn',
    'Fr', 'Ra', 'Ac', 'Th', 'Pa', 'U',  'Np', 'Pu', 'Am', 'Cm', 'Bk', 'Cf', 'Es', 'Fm', 'Md', 'No', 'Lr', 'Rf', 'Db', 'Sg', 'Bh', 'Hs', 'Mt', 'Ds', 'Rg', 'Cn']

# Element number lookup by name
_el_name2num = {el: num for num, el in enumerate(_el_num2name)}

# Element mass in amu by number
_el_num2mass = [
    None,
    1.00794, 4.002602,
    6.941, 9.012182, 10.811, 12.011, 14.00674, 15.9994, 18.9984032, 20.1797,
    22.989768, 24.3050, 26.981539, 28.0855, 30.973762, 32.066, 35.4527, 39.948,
    39.0983, 40.078, 44.955910, 47.88, 50.9415, 51.9961, 54.93805, 55.847, 58.93320, 58.6934, 63.546, 65.39, 69.723, 72.61, 74.92159, 78.96, 79.904, 83.80,
    85.4678, 87.62, 88.90585, 91.224, 92.90638, 95.94, 98, 101.07, 102.90550, 106.42, 107.8682, 112.411, 114.82, 118.710, 121.757, 127.60, 126.90447, 131.29,
    132.90543, 137.327, 138.9055, 140.115, 140.90765, 144.24, 145, 150.36, 151.965, 157.25, 158.92534, 162.50, 164.93032, 167.26, 168.93421, 173.04, 174.967, 178.49, 180.9479, 183.85, 186.207, 190.2, 192.22, 195.08, 196.96654, 200.59, 204.3833, 207.2, 208.98037, 209., 210., 222.,
    223., 226.025, 227.028, 232.0381, 231.0359, 238.0289, 237.048, 244., 243., 247., 247., 251., 252., 257., 258., 259., 266., 267., 268., 269., 270., 269., 278., 281., 281., 285.]

# Element mass in amu by name
_el_name2mass = {
    _el_num2name[num]: mass for num, mass in enumerate(_el_num2mass)}

# Element covalent radius in AA by number
# From F. H. Allen et al., J. Chem. Soc. Perkin Trans. 11, S1-S19, (1987)
# and E. C. Meng, R. A. Lewis, J. Comput. Chem. 12, 891-898 (1991).
_el_num2cov = [
    None,
    0.23, None,
    0.68, 0.35, 0.83, 0.68, 0.68, 0.68, 0.64, None,
    0.97, 1.10, 1.35, 1.20, 1.05, 1.02, 0.99, None,
    1.33, 0.99, 1.44, 1.47, 1.33, 1.35, 1.35, 1.34, 1.33, 1.50, 1.52, 1.45, 1.22, 1.17, 1.21, 1.22, 1.21, None,
    1.47, 1.12, 1.78, 1.56, 1.48, 1.47, 1.35, 1.40, 1.45, 1.50, 1.59, 1.69, 1.63, 1.46, 1.46, 1.47, 1.40, None,
    1.67, 1.34, 1.87, 1.83, 1.82, 1.81, 1.80, 1.80, 1.99, 1.79, 1.76, 1.75, 1.74, 1.73, 1.72, 1.94, 1.72, 1.57, 1.43, 1.37, 1.35, 1.37, 1.32, 1.50, 1.50, 1.70, 1.55, 1.54, 1.54, 1.68, None, None,
    None, 1.90, 1.88, 1.79, 1.61, 1.58, 1.55, 1.53, 1.51]

# Element covalent radius in AA by name
_el_name2cov = {_el_num2name[num]: cov for num, cov in enumerate(_el_num2cov)}

# Element van der Waals radius in AA by number
# From CRC Handbook of Chemistry and Physics, 96th ed.,
# pp. 9-49--9-50 (2015--2016).

_el_num2vdw = [
    None,
    1.10, 1.40,
    1.82, 1.53, 1.92, 1.70, 1.55, 1.52, 1.47, 1.54,
    2.27, 1.73, 1.84, 2.10, 1.80, 1.80, 1.75, 1.88,
    2.75, 2.31, 2.15, 2.11, 2.07, 2.06, 2.05, 2.04, 2.00, 1.97, 1.96, 2.01, 1.87, 2.11, 1.85, 1.90, 1.85, 2.02,
    3.03, 2.49, 2.32, 2.23, 2.18, 2.17, 2.16, 2.13, 2.10, 2.10, 2.11, 2.18, 1.93, 2.17, 2.06, 2.06, 1.98, 2.16,
    3.43, 2.68, 2.43, 2.42, 2.40, 2.39, 2.38, 2.36, 2.35, 2.34, 2.33, 2.31, 2.30, 2.29, 2.27, 2.26, 2.24, 2.23, 2.22, 2.18, 2.16, 2.16, 2.13, 2.13, 2.14, 2.23, 1.96, 2.02, 2.07, 1.97, 2.02, 2.20,
    3.48, 2.83, 2.47, 2.45, 2.43, 2.41, 2.39, 2.43, 2.44, 2.45, 2.44, 2.45, 2.45, 2.45, 2.46, 2.46, 2.46]

# Element van der Waals radius in AA by name
_el_name2vdw = {_el_num2name[num]: vdw for num, vdw in enumerate(_el_num2vdw)}

# Element most common valences (None means no restriction)
_el_num2val = [
    None,
    1, 0,
    1, 2, 3, 4, 3, 2, 1, 0,
    1, 2, 3, 4, 3, 2, 1, 0,
    None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None,
    None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None,
    None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None,
    None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None]

# Element most common valences by name (None means no restriction)
_el_name2val = {_el_num2name[num]: val for num, val in enumerate(_el_num2val)}


class Element:
    """Element object is constructed from element name or number.

    Args:
        inp: Element name or number
    """

    #
    # Magic methods
    #

    def __init__(self, inp):

        # Input is Element or subclass
        if isinstance(inp, Element):
            name = inp.name

        # Input is element name as string
        elif isinstance(inp, str):
            if inp not in _el_name2num:
                raise ValueError("unknown element name")
            name = inp

        # Input is interpreted as element number
        else:
            inp = int(inp)
            if inp > len(_el_num2name):
                raise ValueError("unknown element number")
            name = _el_num2name[inp]

        self._name = name

    def __str__(self):
        return self.name

    def __repr__(self):
        return 'colibri2.chemistry.Element(\'{el:s}\')'.format(el=self)

    def __hash__(self):
        return hash(self.name)

    def __eq__(self, other):
        return self.name == other.name

    def __ne__(self, other):
        return self.name != other.name

    #
    # Properties
    #

    @property
    def name(self):
        """Element name."""
        return self._name

    @property
    def number(self):
        """Element number."""
        return _el_name2num[self._name]

    @property
    def mass(self):
        """Standard element mass in amu (averaged)."""
        return _el_name2mass[self._name]

    @property
    def covalent_radius(self):
        """Covalent radius in AA."""
        return _el_name2cov[self._name]

    @property
    def vdw_radius(self):
        """van der Waals radius in AA."""
        return _el_name2vdw[self._name]

    @property
    def valence(self):
        """Most common valence. Non means no restriction."""
        return _el_name2val[self._name]
